$(function ()
{
    $("#OpretDGIModulForm").steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slide",
        onFinished: function (event, currentIndex) {
            var moduleAdapter = {
                title: $('#editModuleTitleDGI').val(),
                approxTime: parseInt($('#editModalApproxTimeDGI').val()),
                description: $('#editModalDescriptionDGI').val(),
                trainingId: parseInt(new URL(window.location.href).searchParams.get("trainingId")),
                exerciseId: parseInt($('#chosenExerciseId').text())
            };

            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: '/panel/træning/redigertræningsplan',
                data: JSON.stringify(moduleAdapter),
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert('Error: ' + thrownError);
                }
            });
        },
        labels: {
            current: "Nuværende step:",
            finish: "Opret modul",
            next: "Fortsæt til detaljer",
            previous: "Forrige",
            loading: "Loader ..."
        }
    });
});

function getJsonData(){
    $.ajax({
        url: "http://" + window.location.host + "/DGI/getøvelser",
        crossDomain: true,
        success: function(json){
            return JSON.parse(json);
        },
        error: function(xhr, status, error){
        }
    });
}

/* Formatting function for row details - modify as you need */
function format ( d ) {
    console.log(d);
    // `d` is the original data object for the row
    return '<iframe class="embed-responsive-item" src="' + d.videoUrl + '" allowfullscreen style="width: 100%; height: 800px;"></iframe>';
}

$(document).ready(function() {
    var table = $('#datatable').DataTable( {
        "ajax": getJsonData(),
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { "data": "name" },
            { "data": "lead" },
            { "data": "category" },
            { "data": "emneord" },
            { "data": "time" },
            { "data": "niveau" },
            { "data": "agegroup" },
            { "data": "participants" },
            { "data": "videoUrl" },
            { "data": "checked" }
        ],
        "order": [[1, 'asc']]
    } );

    // Add event listener for opening and closing details
    $('#datatable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );

} );

$('#btnAddModule').click(function () {
    var moduleAdapter = {
        title: $('#editModuleTitle').val(),
        approxTime: parseInt($('#editModalApproxTime').val()),
        description: $('#editModalDescription').val(),
        trainingId: parseInt(new URL(window.location.href).searchParams.get("trainingId")),
        exerciseId: 0
    };

    setLoaderState($('#btnAddModule'));

    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: '/panel/træning/redigertræningsplan',
        data: JSON.stringify(moduleAdapter),
        success: function(){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert('Error: ' + thrownError);
        }
    });
});

function deleteModule(id){
    $.ajax({
        type: 'DELETE',
        contentType: "application/json",
        url: '/panel/træning/redigertræningsplan',
        data: id,
        success: function(){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert('Error: ' + thrownError);
        }
    });
}

function storeId(id) {
    $('#chosenExerciseId').text(id);
}