$('#btn_clubtimize_register').click(function () {
    if (validateForm()){
        var union = {
            "unionname": $('#inp_clubtimize_unionname').val(),
            "unionurl": $('#inp_clubtimize_unionurl').val(),
            "seasonswitchmonth": $('#sel_clubtimize_month option:checked').val(),
            "adminemail": $('#inp_clubtimize_email').val(),
            "adminpass": $('#inp_clubtimize_password').val()
        };

        setLoaderState($('#btn_clubtimize_register'));

        $.ajax({
            type: 'POST',
            contentType: "application/json",
            url: '/registerunion',
            data: JSON.stringify(union),
            success: function(url){
                if (url === "email_in_use"){
                    showNotification("Denne email er allerede i brug", "info");
                    setNormalState($('#btn_clubtimize_register'), "Opret forening");
                } else if(url === "name_in_use"){
                    showNotification("Dette forenings navn er allerede i brug", "info");
                    setNormalState($('#btn_clubtimize_register'), "Opret forening");
                } else if(url === "url_in_use") {
                    showNotification("Dette link er allerede i brug", "info");
                    setNormalState($('#btn_clubtimize_register'), "Opret forening");
                } else {
                    window.location.href = window.location.href + url;
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(xhr.status);
            }
        });
    }
});

function validateForm(){
    var validUnionName = validateRequired($('#inp_clubtimize_unionname'), $('#inp_clubtimize_unionname_validate'), 'Skriv foreningens navn');
    var validLink = validateRequired($('#inp_clubtimize_unionurl'), $('#inp_clubtimize_unionurl_validate'), 'Angiv ønskede link');
    var validEmail = validateEmail($('#inp_clubtimize_email'), $('#inp_clubtimize_email_validate'), 'Skriv email');
    var validPassowrd = validatePassword($('#inp_clubtimize_password'), $('#inp_clubtimize_password_confirm'), $('#inp_clubtimize_password_validate'), $('#inp_clubtimize_password_confirm_validate'));
    var validAcceptService = validateCheckbox($('#che_clubtimize_acceptservice'), $('#lbl_clubtimize_acceptservice'));
    var validUsageWebsite = validateCheckbox($('#che_clubtimize_acceptusagewebsite'), $('#lbl_clubtimize_acceptusagewebsite'));
    var validConditions = validateCheckbox($('#che_clubtimize_conditions'), $('#lbl_clubtimize_conditions'));

    return validUnionName && validLink && validEmail && validPassowrd && validAcceptService && validUsageWebsite && validConditions;
}

$('#inp_clubtimize_unionurl').on('input propertychange', function () {
   $('#lbl_clubtimize_linktext').text($('#inp_clubtimize_unionurl').val());
});