var bookingId;

document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendarData = JSON.parse($.ajax({
        type: 'GET',
        contentType: "application/json",
        url: $('#url_courtbooking').text() + '/banebooking/fetch',
        async: false,
        error: function(xhr, ajaxOptions, thrownError){
            alert(thrownError);
        }
    }).responseText);

    calendarData.eventClick = function(info) {
        if (info.event.title === 'Ledig'){
            bookingId = info.event.id;

            $('#content_newBookingModal_free_hall').text(info.event.extendedProps.hall);
            $('#content_newBookingModal_free_court').text(info.event.extendedProps.court);
            $('#content_newBookingModal_free_date').text(info.event.end.toLocaleDateString().split('.').join('-'));
            $('#content_newBookingModal_free_time').text(info.event.start.getHours() + ':00' + '-' + info.event.end.getHours() + ':00');
            $('#content_newBookingModal_free_toPay').text(info.event.extendedProps.price + ' DKK');

            $('#modal_newBookingModal').modal('show');
        } else if (info.event.title === "Reserveret"){
           location.href = $('#url_courtbooking').text() + "/banebooking/periode?hall=" + info.event.extendedProps.hallId;
        }
    };

    var calendar = new FullCalendar.Calendar(calendarEl, calendarData);

    calendar.render();
});

$('#btn_newBookingModal_book').click(function () {
    var data = {
        bookingId: bookingId,
        recipientName: $('#inp_newBookingModal_name').val(),
        recipientEmail: $('#inp_newBookingModal_email').val(),
        recipientAddress: $('#inp_newBookingModal_address').val() + ', ' + $('#inp_newBookingModal_zip').val() + ' ' + $('#inp_newBookingModal_city').val(),
        recipientPhone: $('#inp_newBookingModal_phone').val()
    };

    if (validateForm()){
        setLoaderState($(this));

        $.ajax({
            type: 'POST',
            contentType: "application/json",
            url: $('#url_courtbooking').text() + '/banebooking',
            async: true,
            data: JSON.stringify(data),
            success: function(link){
                location.href = link;
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(thrownError);
            }
        })
    }
});

$('#btn_newPeriodBookingModal_continue').click(function () {
    var hallId = $('#sel_newPeriodBookingModal_hall option:selected').val();

    location.href = $('#url_courtbooking').text() + "/banebooking/periode?hall=" + hallId;
});

function validateForm() {
    var validName = validateRequired($('#inp_newBookingModal_name'), $('#inp_newBookingModal_name_validate'), 'Angiv dit navn');
    var validEmail = validateEmail($('#inp_newBookingModal_email'), $('#inp_newBookingModal_email_validate'));
    var validAddress = validateRequired($('#inp_newBookingModal_address'), $('#inp_newBookingModal_address_validate'), 'Angiv en addresse');
    var validZip = validateRequired($('#inp_newBookingModal_zip'), $('#inp_newBookingModal_zip_validate'), 'Angiv et postnummer');
    var validCity = validateRequired($('#inp_newBookingModal_city'), $('#inp_newBookingModal_city_validate'), 'Angiv et postnummer');
    var validPhone = validatePhone($('#inp_newBookingModal_phone'), $('#inp_newBookingModal_phone_validate'));

    return validName && validEmail && validAddress && validZip && validCity && validPhone;
}