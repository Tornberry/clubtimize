$('#btn_resetPasswordModal_send').click(function () {
    $.ajax({
        type: 'POST',
        url: '/makeresetrequest?email=' + $('#inp_resetPasswordModal_email').val(),
        success: function(){
          $('#modal_resetPasswordModal').modal('toggle');
          showNotification("Anmodningen er behandlet! Hvis ikke du modtager en mail inden for 5 minutter, er det måske fordi, at du har tastet din mail forkert. Prøv da at nulstille igen.", "info");
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(thrownError);
        }
    });
});