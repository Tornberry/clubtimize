function acceptInvitation(id) {
    if (validateForm()) {
        var date = new Date($('#inp_invitedCoach_birthday').val());
        day = date.getDate();
        month = date.getMonth() + 1;
        year = date.getFullYear();

        var invitationAdapter = {
            "invId": id,
            "email": $('#inp_invitedCoach_email').val(),
            "password": $('#inp_invitedCoach_password').val(),
            "firstname": $('#inp_invitedCoach_firstname').val(),
            "lastname": $('#inp_invitedCoach_lastname').val(),
            "birthday": day + '-' + month + '-' + year,
            "gender": $('#sel_invitedCoach_gender option:selected').val(),
            "phone": $('#inp_invitedCoach_phone').val(),
            "address": $('#inp_invitedCoach_addreee').val() + ', ' + $('#inp_invitedCoach_zip').val() + ' ' + $('#inp_invitedCoach_city').val()
        };

        setLoaderState($('#' + id));

        $.ajax({
            type: 'POST',
            contentType: "application/json",
            url: '/invitation/træner',
            data: JSON.stringify(invitationAdapter),
            success: function (link) {
                if (link === "OK"){
                    window.location.href = "/login";
                } else if (link === "email_in_use"){
                    showNotification("En bruger med denne email eksistere allerede", "info");
                    setNormalState($('button[name="create"]'), "Opret min bruger");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    }
}

function validateForm() {

    var validEmail = validateEmail($('#inp_invitedCoach_email'), $('#inp_invitedCoach_email_validate'));
    var validPassword = validatePassword($('#inp_invitedCoach_password'), $('#inp_invitedCoach_passwordconfirm'), $('#inp_invitedCoach_password_validate'), $('#inp_invitedCoach_passwordconfirm_validate'));
    var validFirstname = validateRequired($('#inp_invitedCoach_firstname'), $('#inp_invitedCoach_firstname_validate'), 'Skriv dit fornavn.');
    var validLastname = validateRequired($('#inp_invitedCoach_lastname'), $('#inp_invitedCoach_lastname_validate'), 'Skriv dit efternavn.');
    var validBrithday = validateRequired($('#inp_invitedCoach_birthday'), $('#inp_invitedCoach_birthday_validate'), 'Angiv din fødselsdag')
    var validAddress = validateRequired($('#inp_invitedCoach_addreee'), $('#inp_invitedCoach_addreee_validate'), 'Skriv din addresse.');
    var validZIP = validateRequired($('#inp_invitedCoach_zip'), $('#inp_invitedCoach_zip_validate'), 'Skriv nummer.');
    var validCity = validateRequired($('#inp_invitedCoach_city'), $('#inp_invitedCoach_city_validate'), 'Skriv postnummer.');
    var validPhone = validatePhone($('#inp_invitedCoach_phone'), $('#inp_invitedCoach_phone_validate'));
    var validWebsiteUsage = validateCheckbox($('#che_invitedCoach_acceptusagewebsite'), $('#lbl_invitedCoach_acceptusagewebsite'));
    var validAcceptService = validateCheckbox($('#che_invitedCoach_acceptservice'), $('#lbl_invitedCoach_acceptservice'));

    return validEmail && validPassword && validFirstname && validLastname && validBrithday && validAddress && validZIP && validCity && validPhone && validWebsiteUsage && validAcceptService;
}