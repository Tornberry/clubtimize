function book(id) {

    var playerArray = [];

    $('div[data-repeater-item="spiller"]').each(function (index) {
        var player = $('div[data-repeater-item="spiller"]').get(index);

        var nameString = '"spillere[' + index + '][name]"';
        var name = $('input[name=' + nameString + ']');

        var emailString = '"spillere[' + index + '][email]"';
        var email = $('input[name=' + emailString + ']');

        var addressString = '"spillere[' + index + '][address]"';
        var address = $('input[name=' + addressString + ']');

        var zipString = '"spillere[' + index + '][zip]"';
        var zip = $('input[name=' + zipString + ']');

        var cityString = '"spillere[' + index + '][city]"';
        var city = $('input[name=' + cityString + ']');

        playerArray.push({
            Name: name.val(),
            Email: email.val(),
            Address: address.val() + ', ' + zip.val() + ' ' + city.val()
        });
    });

    if (validateForm(playerArray)){
        var data = {
            TimePeriodId: id,
            Name: $('#inp_invitedSeasonBookingPeriod_name').val(),
            Email: $('#inp_invitedSeasonBookingPeriod_email').val(),
            Address: $('#inp_invitedSeasonBookingPeriod_address').val() + ', ' + $('#inp_invitedSeasonBookingPeriod_zip').val() + ' ' + $('#inp_invitedSeasonBookingPeriod_city').val(),
            Phone: $('#inp_invitedSeasonBookingPeriod_phone').val(),
            Players: playerArray
        };

        setLoaderState($('#' + id));

        $.ajax({
            type: 'POST',
            contentType: "application/json",
            url: '/invitation/periodebooking',
            data: JSON.stringify(data),
            success: function (link) {
                if (link.startsWith("http")){
                    window.location.href = link;
                } else if (link === "email_in_use"){
                    showNotification("En bruger med denne email eksistere allerede", "info");
                    setNormalState($('button[name="create"]'), "Opret min bruger og gå til betaling");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });


    }
}

function validateForm(playerArray) {
    var validName = validateRequired($('#inp_invitedSeasonBookingPeriod_name'), $('#inp_invitedSeasonBookingPeriod_name_validate'), 'Angiv et navn');
    var validEmail = validateEmail($('#inp_invitedSeasonBookingPeriod_email'), $('#inp_invitedSeasonBookingPeriod_email_validate'));
    var validAddress = validateRequired($('#inp_invitedSeasonBookingPeriod_address'), $('#inp_invitedSeasonBookingPeriod_address_validate'), 'Angiv en adresse');
    var validZip = validateRequired($('#inp_invitedSeasonBookingPeriod_zip'), $('#inp_invitedSeasonBookingPeriod_zip_validate'), 'Angiv et postnummer');
    var validCity = validateRequired($('#inp_invitedSeasonBookingPeriod_city'), $('#inp_invitedSeasonBookingPeriod_city_validate'), 'Angiv en by');
    var validPhone = validatePhone($('#inp_invitedSeasonBookingPeriod_phone'), $('#inp_invitedSeasonBookingPeriod_phone_validate'));
    var validConditions = validateCheckbox($('#che_invitedSeasonPeriodBooking_acceptusagewebsite'), $('#lbl_invitedSeasonPeriodBooking_acceptusagewebsite'));
    var validPrivacy = validateCheckbox($('#che_invitedSeasonPeriodBooking_acceptprivacy'), $('#lbl_invitedSeasonPeriodBooking_acceptprivacy'));

    var validPlayerArray = validatePlayerArray(playerArray);

    return validName && validEmail && validAddress && validZip && validCity && validPhone && validConditions && validPrivacy && validPlayerArray ;
}

function validatePlayerArray(playerArray) {
    var playersValidated = true;

    playerArray.forEach(function (value, index) {

        var nameString = '"spillere[' + index + '][name]"';
        var nameValidationString = '"spillere[' + index + '][name_validate]"';
        var name = $('input[name=' + nameString + ']');
        var name_validation = $('ul[name=' + nameValidationString + ']');
        var validName = validateRequired(name, name_validation, 'Angiv et navn');

        var emailString = '"spillere[' + index + '][email]"';
        var emailValidationString = '"spillere[' + index + '][email_validate]"';
        var email = $('input[name=' + emailString + ']');
        var email_validation = $('ul[name=' + emailValidationString + ']');
        var validEmail = validateEmail(email, email_validation);

        var addressString = '"spillere[' + index + '][address]"';
        var addressValidationString = '"spillere[' + index + '][address_validate]"';
        var address = $('input[name=' + addressString + ']');
        var address_validation = $('ul[name=' + addressValidationString + ']');
        var validAddress = validateRequired(address, address_validation, 'Angiv en adresse');

        var zipString = '"spillere[' + index + '][zip]"';
        var zipValidationString = '"spillere[' + index + '][zip_validate]"';
        var zip = $('input[name=' + zipString + ']');
        var zip_validation = $('ul[name=' + zipValidationString + ']');
        var validZip = validateRequired(zip, zip_validation, 'Angiv postnummer');

        var cityString = '"spillere[' + index + '][city]"';
        var cityValidationString = '"spillere[' + index + '][city_validate]"';
        var city = $('input[name=' + cityString + ']');
        var city_validation = $('ul[name=' + cityValidationString + ']');
        var validCity = validateRequired(city, city_validation, 'Angiv en by');

        if(!validName || !validEmail || !validAddress || !validCity){
            playersValidated = false;
            return false;
        }
    });

    return playersValidated;
}