$(document).ready(function () {
    var priceLabel = $('#lbl_invitedPlayer_price');
    var price = parseFloat(priceLabel.text());
    priceLabel.text(calculatePrice(price) + ' DKK');
});

function calculatePrice(price) {
    var isPayingFees = ($('#isPayingFees_invitedPlayer').text() === 'true');

    if (isPayingFees){
        return price;
    } else {
        var feePercentage = parseFloat($('#hidden_invitedPlayer_fee').text()) / 100;

        var feeWithoutTax = (price * feePercentage) + 2;
        var feeWithTax = feeWithoutTax * 1.25;

        return feeWithTax + price;
    }
}

function acceptInvitation(id) {
    if (validateForm()) {
        var date = new Date($('#inp_invitedPlayer_birthday').val());
        day = date.getDate();
        month = date.getMonth() + 1;
        year = date.getFullYear();

        var invitationAdapter = {
            "invId": id,
            "email": $('#inp_invitedPlayer_email').val(),
            "password": $('#inp_invitedPlayer_password').val(),
            "firstname": $('#inp_invitedPlayer_firstname').val(),
            "lastname": $('#inp_invitedPlayer_lastname').val(),
            "birthday": day + '-' + month + '-' + year,
            "gender": $('#sel_invitedPlayer_gender option:selected').val(),
            "phone": $('#inp_invitedPlayer_phone').val(),
            "address": $('#inp_invitedPlayer_addreee').val() + ', ' + $('#inp_invitedPlayer_zip').val() + ' ' + $('#inp_invitedPlayer_city').val()
        };

        setLoaderState($('#' + id));

        $.ajax({
            type: 'POST',
            contentType: "application/json",
            url: '/invitation/spiller',
            data: JSON.stringify(invitationAdapter),
            success: function (link) {
                if (link.startsWith("http")){
                    window.location.href = link;
                } else if (link === "email_in_use"){
                    showNotification("En bruger med denne email eksistere allerede", "info");
                    setNormalState($('button[name="create"]'), "Opret min bruger og gå til betaling");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    }
}

function validateForm() {

    var validEmail = validateEmail($('#inp_invitedPlayer_email'), $('#inp_invitedPlayer_email_validate'));
    var validPassword = validatePassword($('#inp_invitedPlayer_password'), $('#inp_invitedPlayer_passwordconfirm'), $('#inp_invitedPlayer_password_validate'), $('#inp_invitedPlayer_passwordconfirm_validate'));
    var validFirstname = validateRequired($('#inp_invitedPlayer_firstname'), $('#inp_invitedPlayer_firstname_validate'), 'Skriv dit fornavn.');
    var validLastname = validateRequired($('#inp_invitedPlayer_lastname'), $('#inp_invitedPlayer_lastname_validate'), 'Skriv dit efternavn.');
    var validBrithday = validateRequired($('#inp_invitedPlayer_birthday'), $('#inp_invitedPlayer_birthday_validate'), 'Angiv din fødselsdag')
    var validAddress = validateRequired($('#inp_invitedPlayer_addreee'), $('#inp_invitedPlayer_addreee_validate'), 'Skriv din addresse.');
    var validZIP = validateRequired($('#inp_invitedPlayer_zip'), $('#inp_invitedPlayer_zip_validate'), 'Skriv nummer.');
    var validCity = validateRequired($('#inp_invitedPlayer_city'), $('#inp_invitedPlayer_city_validate'), 'Skriv postnummer.');
    var validPhone = validatePhone($('#inp_invitedPlayer_phone'), $('#inp_invitedPlayer_phone_validate'));
    var validWebsiteUsage = validateCheckbox($('#che_invitedPlayer_acceptusagewebsite'), $('#lbl_invitedPlayer_acceptusagewebsite'));
    var validAcceptService = validateCheckbox($('#che_invitedPlayer_acceptservice'), $('#lbl_invitedPlayer_acceptservice'));

    return validEmail && validPassword && validFirstname && validLastname && validBrithday && validAddress && validZIP && validCity && validPhone && validWebsiteUsage && validAcceptService;
}