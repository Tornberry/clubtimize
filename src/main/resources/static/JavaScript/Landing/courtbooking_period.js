var timePeriodId;

$('#btn_newSeasonBookingModal_book').click(function () {
    if (validForm()){
        var data = {
            timePeriodId: timePeriodId,
            recipientName: $('#inp_newSeasonBookingModal_name').val(),
            recipientEmail: $('#inp_newSeasonBookingModal_email').val(),
            recipientPhone: $('#inp_newSeasonBookingModal_phone').val(),
            recipientAddress: $('#inp_newSeasonBookingModal_address').val() + ', ' + $('#inp_newSeasonBookingModal_zip').val() + ' ' + $('#inp_newSeasonBookingModal_city').val()
        };

        setLoaderState($(this));

        $.ajax({
            type: 'POST',
            contentType: "application/json;charset=utf-8",
            url: $('#url_courtbooking').text() + "/banebooking/periode",
            data: JSON.stringify(data),
            success: function (link) {
                location.href = link;
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    }
});

function openModal(id){
    timePeriodId = id;

    $('#modal_newSeasonBookingModal').modal('show');
}

function validForm() {
    var validName = validateRequired($('#inp_newSeasonBookingModal_name'), $('#inp_newSeasonBookingModal_name_validate'), 'Angiv et navn');
    var validEmail = validateEmail($('#inp_newSeasonBookingModal_email'), $('#inp_newSeasonBookingModal_email_validate'));
    var validAddress = validateRequired($('#inp_newSeasonBookingModal_address'), $('#inp_newSeasonBookingModal_address_validate'), 'Angiv en addresse');
    var validZip = validateRequired($('#inp_newSeasonBookingModal_zip'), $('#inp_newSeasonBookingModal_zip_validate'), 'Angiv et postnummer');
    var validCity = validateRequired($('#inp_newSeasonBookingModal_city'), $('#inp_newSeasonBookingModal_city_validate'), 'Angiv en by');
    var validPhone = validatePhone($('#inp_newSeasonBookingModal_phone'), $('#inp_newSeasonBookingModal_phone_validate'));

    return validName && validEmail && validAddress && validZip && validCity && validPhone;
}
