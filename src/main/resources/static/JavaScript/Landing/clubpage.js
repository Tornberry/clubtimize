var teamId;

function changeChosenTeam(id) {
    var openModalButton = $('#btn_clubpage_openModal');
    var chosenTeamModal = $('#txt_joinTeamModal_chosenteamModal');
    var chosenteamTextElement = $('#txt_clubpage_chosenname');
    var chosenteamPriceElement = $('#txt_joinTeamModal_priceModal');

    chosenteamTextElement.text($('#txt_clubpage_title' + id).text());

    tippy('#feeAlert', {
        content: 'Tooltip'
    });

    openModalButton.removeClass('btn-secondary');
    openModalButton.addClass('btn-primary');
    openModalButton.attr("disabled", false);
    openModalButton.text('Fortsæt tilmelding');

    chosenteamPriceElement.text(function () {
        var isPayingFees = ($('#isPayingFees').text() === 'true');

        if (isPayingFees){
            var initialPrice = parseFloat($('#txt_clubpage_price' + id).text());

            return initialPrice + ' DKK';
        } else {
            var initialPrice = parseFloat($('#txt_clubpage_price' + id).text());
            var feePercent = parseFloat($('#feePercent').text()) / 100;

            var feeWithoutTax = (initialPrice * feePercent) + 2;
            var feeWithTax = feeWithoutTax * 1.25;

            return feeWithTax + initialPrice + ' DKK';
        }
    });

    chosenTeamModal.text(chosenteamTextElement.text());

    teamId = id;
}

function openModal() {
    $('#modal_joinTeamModal').modal('show');
}

$('#btn_joinTeamModal_gotopayment').click(function () {
    if (validateForm()) {

        var date = new Date($('#inp_joinTeamModal_birthday').val());
        day = date.getDate();
        month = date.getMonth() + 1;
        year = date.getFullYear();

        var joinTeamData = {
            "teamId": teamId,
            "email": $('#inp_joinTeamModal_email').val(),
            "password": $('#inp_joinTeamModal_password').val(),
            "firstname": $('#inp_joinTeamModal_firstname').val(),
            "lastname": $('#inp_joinTeamModal_lastname').val(),
            "birthday": day + '-' + month + '-' + year,
            "gender": $('#sel_joinTeamModal_gender option:selected').val(),
            "address": $('#inp_joinTeamModal_addreee').val() + ', ' + $('#inp_joinTeamModal_zip').val() + ' ' + $('#inp_joinTeamModal_city').val(),
            "phone": $('#inp_joinTeamModal_phone').val()
        };

        setLoaderState($('#btn_joinTeamModal_gotopayment'));

        $.ajax({
            type: 'POST',
            contentType: "application/json",
            url: '/jointeam',
            data: JSON.stringify(joinTeamData),
            success: function (link) {
                if (link === "email_in_use") {
                    showNotification("Denne email er allerede i brug", "info");
                    setNormalState($('#btn_joinTeamModal_gotopayment'), "Gå til betaling");
                } else {
                    window.location.href = link;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    }
});

$('#btn_joinTeamModal_existingGotopayment').click(function () {
    var joinTeamData = {
        "teamId": teamId,
        "email": $('#inp_joinTeamModal_existingEmail').val(),
        "password": $('#inp_joinTeamModal_existingPassword').val(),
    };

    setLoaderState($('#btn_joinTeamModal_gotopayment'));

    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: '/addteam',
        data: JSON.stringify(joinTeamData),
        success: function (link) {
            if (link === "already_on_team") {
                showNotification("Du er allerede tilmeldt dette hold", "info");
                setNormalState($('#btn_joinTeamModal_existingGotopayment'), "Gå til betaling");
            } else if (link === "no_valid") {
                showNotification("Denne bruger eksistere ikke eller informationerne er forkerte", "info");
                setNormalState($('#btn_joinTeamModal_existingGotopayment'), "Gå til betaling");
            } else if (link.startsWith("http")) {
                window.location.href = link;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError);
        }
    });
});

function validateForm() {

    var validEmail = validateEmail($('#inp_joinTeamModal_email'), $('#inp_joinTeamModal_email_validate'));
    var validPassword = validatePassword($('#inp_joinTeamModal_password'), $('#inp_joinTeamModal_passwordconfirm'), $('#inp_joinTeamModal_password_validate'), $('#inp_joinTeamModal_passwordconfirm_validate'));
    var validFirstname = validateRequired($('#inp_joinTeamModal_firstname'), $('#inp_joinTeamModal_firstname_validate'), 'Skriv dit fornavn.');
    var validLastname = validateRequired($('#inp_joinTeamModal_lastname'), $('#inp_joinTeamModal_lastname_validate'), 'Skriv dit efternavn.');
    var validBrithday = validateRequired($('#inp_joinTeamModal_birthday'), $('#inp_joinTeamModal_birthday_validate'), 'Angiv din fødselsdag')
    var validAddress = validateRequired($('#inp_joinTeamModal_addreee'), $('#inp_joinTeamModal_addreee_validate'), 'Skriv din addresse.');
    var validZIP = validateRequired($('#inp_joinTeamModal_zip'), $('#inp_joinTeamModal_zip_validate'), 'Skriv nummer.');
    var validCity = validateRequired($('#inp_joinTeamModal_city'), $('#inp_joinTeamModal_city_validate'), 'Skriv postnummer.');
    var validPhone = validatePhone($('#inp_joinTeamModal_phone'), $('#inp_joinTeamModal_phone_validate'));
    var validWebsiteUsage = validateCheckbox($('#che_joinTeamModal_acceptusagewebsite'), $('#lbl_joinTeamModal_acceptusagewebsite'));
    var validAcceptService = validateCheckbox($('#che_joinTeamModal_acceptservice'), $('#lbl_joinTeamModal_acceptservice'));

    return validEmail && validPassword && validFirstname && validLastname && validBrithday && validAddress && validZIP && validCity && validPhone && validWebsiteUsage && validAcceptService;
}

