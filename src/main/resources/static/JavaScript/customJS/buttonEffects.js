function setLoaderState(button) {
    button.empty();
    button.attr('disabled', '');
    button.append('<img src="/panelResources/images/spinner.svg" style="max-width: 40px;">');
}

function setNormalState(button, text) {
    button.empty();
    button.removeAttr('disabled');
    button.text(text);
}