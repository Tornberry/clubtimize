// For firld where everything but empty is ok
function validateRequired(field, errorList, message) {
    if (field.val() !== ""){
        if (field.hasClass('clubtimize-error-input-required')){
            field.removeClass('clubtimize-error-input-required');
        }
        if (!errorList.hasClass('clubtimize-hide')){
            errorList.addClass('clubtimize-hide');
        }
        return true;
    } else {
        field.addClass('clubtimize-error-input-required');
        errorList.empty();
        errorList.removeClass('clubtimize-hide');
        errorList.append('<li>' + message + '</li>');

        return false;
    }
}

// For email fields where '@' and '.' is required
function validateEmail(field, errorList) {
    if (field.val() !== ""){
        if(!field.val().includes('@') || !field.val().includes('.')){
            field.addClass('clubtimize-error-input-required');
            errorList.empty();
            errorList.removeClass('clubtimize-hide');
            errorList.append('<li>Indtast en valid email.</li>');

            return false;
        } else {
            if (field.hasClass('clubtimize-error-input-required')){
                field.removeClass('clubtimize-error-input-required');
            }
            if (!errorList.hasClass('clubtimize-hide')){
                errorList.addClass('clubtimize-hide');
            }
            return true;
        }
    } else {
        field.addClass('clubtimize-error-input-required');
        errorList.empty();
        errorList.removeClass('clubtimize-hide');
        errorList.append('<li>Vælg en email.</li>');

        return false;
    }
}

// For password fields where value must equal another field and not be empty
function validatePassword(field, matchingField, errorList, errorListConfirm) {
    if (field.val() !== ""){
        if(field.val() !== matchingField.val()){
            matchingField.addClass('clubtimize-error-input-required');
            errorList.empty();
            errorList.removeClass('clubtimize-hide');
            field.removeClass('clubtimize-error-input-required');

            errorListConfirm.empty();
            errorListConfirm.removeClass('clubtimize-hide');
            errorListConfirm.append('<li>Kodeord matcher ikke.</li>');

            return false;
        } else {
            if (matchingField.hasClass('clubtimize-error-input-required')){
                matchingField.removeClass('clubtimize-error-input-required');
            }
            if (!errorList.hasClass('clubtimize-hide')){
                errorList.addClass('clubtimize-hide');
                errorListConfirm.addClass('clubtimize-hide');
            }
            return true;
        }
    } else {
        field.addClass('clubtimize-error-input-required');
        errorList.empty();
        errorList.removeClass('clubtimize-hide');
        errorList.append('<li>Vælg et kodeord.</li>');

        return false;
    }
}

// For email fields where '@' and '.' is required
function validatePhone(field, errorList) {
    if (field.val() !== ""){
        if(field.val().length !== 8){
            field.addClass('clubtimize-error-input-required');
            errorList.empty();
            errorList.removeClass('clubtimize-hide');
            errorList.append('<li>Telefonnummer skal være 8 tal.</li>');

            return false;
        } else {
            if (field.hasClass('clubtimize-error-input-required')){
                field.removeClass('clubtimize-error-input-required');
            }
            if (!errorList.hasClass('clubtimize-hide')){
                errorList.addClass('clubtimize-hide');
            }
            return true;
        }
    } else {
        field.addClass('clubtimize-error-input-required');
        errorList.empty();
        errorList.removeClass('clubtimize-hide');
        errorList.append('<li>Vælg et telefonnummer.</li>');

        return false;
    }
}

function validateCheckbox(checkbox, label) {
    if (!checkbox.is(':checked')){
        label.addClass("clubtimize-checkboxlabel-invalid");
        return false;
    } else {
        if (label.hasClass("clubtimize-checkboxlabel-invalid")){
            label.removeClass("clubtimize-checkboxlabel-invalid");
        }
        return true;
    }

}

function validateDateRange(field) {
    if (field.val() !== ""){
        if (field.hasClass('clubtimize-error-input-required')){
            field.removeClass('clubtimize-error-input-required');
        }
        return true;
    } else {
        field.addClass('clubtimize-error-input-required');

        return false;
    }
}

function validateHour(fieldStart, fieldEnd, errorList) {
    if (fieldStart.val() !== ""){
        if(fieldStart.val() < 0 || fieldStart.val() > 23){
            fieldStart.addClass('clubtimize-error-input-required');
            errorList.empty();
            errorList.removeClass('clubtimize-hide');
            errorList.append('<li>Vælg et tidspunkt mellem 0 og 23</li>');

            return false;
        } else {
            if (fieldEnd !== null){
                if (parseInt(fieldStart.val()) > parseInt(fieldEnd.val())){
                    fieldStart.addClass('clubtimize-error-input-required');
                    errorList.empty();
                    errorList.removeClass('clubtimize-hide');
                    errorList.append('<li>Tiden "fra" skal være lavere end "til"</li>');
                } else {
                    if (fieldStart.hasClass('clubtimize-error-input-required')){
                        fieldStart.removeClass('clubtimize-error-input-required');
                    }
                    if (!errorList.hasClass('clubtimize-hide')){
                        errorList.addClass('clubtimize-hide');
                    }
                    return true;
                }
            } else {
                if (fieldStart.hasClass('clubtimize-error-input-required')){
                    fieldStart.removeClass('clubtimize-error-input-required');
                }
                if (!errorList.hasClass('clubtimize-hide')){
                    errorList.addClass('clubtimize-hide');
                }
                return true;
            }
        }
    } else {
        fieldStart.addClass('clubtimize-error-input-required');
        errorList.empty();
        errorList.removeClass('clubtimize-hide');
        errorList.append('<li>Vælg en hel time</li>');

        return false;
    }
}

function validatePrice(field, errorList) {
    if (field.val() !== ""){
        if (field.val() < 0) {
            field.addClass('clubtimize-error-input-required');
            errorList.empty();
            errorList.removeClass('clubtimize-hide');
            errorList.append('<li>' + 'Angiv en pris på 0 DKK eller over' + '</li>');
        } else {
            if (field.hasClass('clubtimize-error-input-required')){
                field.removeClass('clubtimize-error-input-required');
            }
            if (!errorList.hasClass('clubtimize-hide')){
                errorList.addClass('clubtimize-hide');
            }
        }
        return true;
    } else {
        field.addClass('clubtimize-error-input-required');
        errorList.empty();
        errorList.removeClass('clubtimize-hide');
        errorList.append('<li>' + 'Angiv en pris' + '</li>');

        return false;
    }
}
