$(document).ready(function() {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    /* initialize the calendar
     -----------------------------------------------------------------*/

    $.ajax({
        url: "/panel/api/getevents",
        crossDomain: true,
        success: function(json){
            initializeCalendar(json);
        },
        error: function(xhr, status, error){
        }
    });

});

function initializeCalendar(eventArray){
    var calendar =  $('#calendar').fullCalendar({
        locale: "da",
        header: {
            left: 'title',
            center: 'agendaDay,agendaWeek,month',
            right: 'prev,next today'
        },

        contentHeight: "auto",

        firstDay: 1,
        selectable: true,
        defaultView: 'month',

        axisFormat: 'H:mm',

        week:{ titleFormat: "DD MMMM YYYY" },
        allDaySlot: false,
        selectHelper: true,
        droppable: false, // this allows things to be dropped onto the calendar !!!

        events: JSON.parse(eventArray),
        timeFormat: 'H:mm',
        eventClick: function(event) {
            document.location.href = '/panel/træning?id=' + event.id;
        }
    });
}


