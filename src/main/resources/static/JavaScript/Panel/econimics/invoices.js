$(document).ready(function () {
    $('#tbl_subscriptions_subscriptions').DataTable({
        language: {
            "lengthMenu": "Vis _MENU_ fakturaer per side",
            "search": "Søg",
            "zeroRecords": "Ingen fakturaer fundet",
            "info": "Viser side _PAGE_ af _PAGES_",
            "infoEmpty": "Ingen fakturaer tilgængelige",
            "infoFiltered": "(Filtreret fra _MAX_ antal fakturaer)",
            "paginate": {
                "previous": "Tidligere side",
                "next": "Næste side"
            }
        }
    });
    $('#tbl_courtbookings_courtbookings').DataTable({
        language: {
            "lengthMenu": "Vis _MENU_ fakturaer per side",
            "search": "Søg",
            "zeroRecords": "Ingen fakturaer fundet",
            "info": "Viser side _PAGE_ af _PAGES_",
            "infoEmpty": "Ingen fakturaer tilgængelige",
            "infoFiltered": "(Filtreret fra _MAX_ antal fakturaer)",
            "paginate": {
                "previous": "Tidligere side",
                "next": "Næste side"
            }
        }
    });
    $('#tbl_courtbookings_courtbookingsperiod').DataTable({
        language: {
            "lengthMenu": "Vis _MENU_ fakturaer per side",
            "search": "Søg",
            "zeroRecords": "Ingen fakturaer fundet",
            "info": "Viser side _PAGE_ af _PAGES_",
            "infoEmpty": "Ingen fakturaer tilgængelige",
            "infoFiltered": "(Filtreret fra _MAX_ antal fakturaer)",
            "paginate": {
                "previous": "Tidligere side",
                "next": "Næste side"
            }
        }
    });
    $('#tbl_payouts_payouts').DataTable({
        language: {
            "lengthMenu": "Vis _MENU_ fakturaer per side",
            "search": "Søg",
            "zeroRecords": "Ingen fakturaer fundet",
            "info": "Viser side _PAGE_ af _PAGES_",
            "infoEmpty": "Ingen fakturaer tilgængelige",
            "infoFiltered": "(Filtreret fra _MAX_ antal fakturaer)",
            "paginate": {
                "previous": "Tidligere side",
                "next": "Næste side"
            }
        }
    });
    $('#tbl_fees_fees').DataTable({
        language: {
            "lengthMenu": "Vis _MENU_ gebyr rapporter per side",
            "search": "Søg",
            "zeroRecords": "Ingen gebyr rapporter fundet",
            "info": "Viser side _PAGE_ af _PAGES_",
            "infoEmpty": "Ingen gebyr rapporter tilgængelige",
            "infoFiltered": "(Filtreret fra _MAX_ antal gebyr rapporter)",
            "paginate": {
                "previous": "Tidligere side",
                "next": "Næste side"
            }
        }
    });
    $('#tbl_debt_debt').DataTable({
        language: {
            "lengthMenu": "Vis _MENU_ gælds rapporter per side",
            "search": "Søg",
            "zeroRecords": "Ingen gælds rapporter fundet",
            "info": "Viser side _PAGE_ af _PAGES_",
            "infoEmpty": "Ingen gælds rapporter tilgængelige",
            "infoFiltered": "(Filtreret fra _MAX_ antal gælds rapporter)",
            "paginate": {
                "previous": "Tidligere side",
                "next": "Næste side"
            }
        }
    });
    $('#tbl_events_events').DataTable({
        language: {
            "lengthMenu": "Vis _MENU_ fakturaer per side",
            "search": "Søg",
            "zeroRecords": "Ingen fakturaer fundet",
            "info": "Viser side _PAGE_ af _PAGES_",
            "infoEmpty": "Ingen fakturaer tilgængelige",
            "infoFiltered": "(Filtreret fra _MAX_ antal fakturaer)",
            "paginate": {
                "previous": "Tidligere side",
                "next": "Næste side"
            }
        }
    });
});