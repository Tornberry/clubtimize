var incomePrDayChart;

$(document).ready(function () {
    fillIncomePrDayChart(30, true);
});

$('#btn_incomePrDay_load').click(function () {
    var valueInputField = $('#inp_incomePrDay_days');

    if (valueInputField.val() === ""){
        valueInputField.val(7);
        fillIncomePrDayChart(7, false);
    } else {
        if(valueInputField.val() > 10000){
            valueInputField.val(10000);
            fillIncomePrDayChart(10000, false)
        } else {
            fillIncomePrDayChart(valueInputField.val(), false);
        }
    }
});


function fillIncomePrDayChart(days, isLoadPage) {
    var ctx = document.getElementById('chart_incomePrDay');
    if (!isLoadPage){
        incomePrDayChart.data = JSON.parse(getBalanceData(days));
        incomePrDayChart.update();
    } else {
        incomePrDayChart = new Chart(ctx, {
            type: 'bar',
            data: JSON.parse(getBalanceData(days)),
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }
}

function getBalanceData(days) {
    var test = $.ajax({
        type: 'GET',
        url: "/panel/økonomi/balancehistory?days=" + days,
        async: false,
        error: function(xhr, ajaxOptions, thrownError){
            alert('Error: ' + thrownError);
        }
    });

    return test.responseText;
}

$('#tbl_requestHistory_requestHistory').DataTable({
    language: {
        "lengthMenu": "Vis _MENU_ anmodninger per side",
        "search": "Søg",
        "zeroRecords": "Ingen anmodninger fundet",
        "info": "Viser side _PAGE_ af _PAGES_",
        "infoEmpty": "Ingen anmodninger tilgængelige",
        "infoFiltered": "(Filtreret fra _MAX_ antal anmodninger)",
        "paginate": {
            "previous": "Tidligere side",
            "next": "Næste side"
        }
    }
});

function hasBankInfo() {

     var test = $.ajax({
        type: 'GET',
        url: "/panel/hasbankinfo",
         async: false,
        error: function(xhr, ajaxOptions, thrownError){
            alert('Error: ' + thrownError);
        }
    });

    return test.responseJSON;
}

$('#btn_newBankInfoModal_sendbankinfo').click(function () {
    var bankInfo = {
        "accountNumber": $('#inp_newBankInfoModal_accountnumber').val(),
        "registrationNumber": $('#inp_newBankInfoModal_registrationnumber').val(),
        "bankName": $('#inp_newBankInfoModal_bankname').val(),
        "phoneNumber": $('#inp_newBankInfoModal_contactnumber').val()
    };

    setLoaderState($(this));

    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: "/panel/setbankinformations",
        data: JSON.stringify(bankInfo),
        success: function(){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert('Error: ' + thrownError);
        }
    });
});


$('#btn_economy_requestpayment').click(function () {
    if (hasBankInfo()){
        Swal({
            title: 'Er du sikker?',
            text: 'Udbetaling sker normalt i starten af næstkommende måned',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ja, fortsæt udbetaling',
            cancelButtonText: "Annuller"
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: "/panel/paymentrequest",
                    success: function(){
                        location.reload();
                    },
                    error: function(xhr, ajaxOptions, thrownError){
                        alert('Error: ' + thrownError);
                    }
                });
            }
        });
    } else {
        $('#modal_newBankInfo').modal();
    }
});

