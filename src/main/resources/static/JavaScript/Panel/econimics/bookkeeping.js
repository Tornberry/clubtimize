var chosenIntegration;

$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: "/panel/bogføring/chosenIntegration",
        async: false,
        success: function(data){
            chosenIntegration = data;
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert('Error: ' + thrownError);
        }
    });

    fillAccountDropdowns();

    if (chosenIntegration === 'Economic'){
        fillAccountYearsDropdown();
    }
});

function fillAccountYearsDropdown() {
    var accountingYearArray;

    if (chosenIntegration === 'Economic'){
        $.ajax({
            type: 'GET',
            url: "/panel/bogføring/economicregnskabsår",
            async: false,
            success: function(data){
                accountingYearArray = JSON.parse(data);
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert('Error: ' + thrownError);
            }
        });
    }

    accountingYearArray.collection.forEach(function (element) {
        if (!element.closed){
            $('#sel_settings_economicAccountingYears').append(new Option(element.year, element.year));
        }
    });
}

function fillAccountDropdowns(){

    if (chosenIntegration === 'Dinero'){
        $.ajax({
            type: 'GET',
            url: "/panel/bogføring/dinerokonto",
            success: function(data){
                if(data === 'invalid_grant'){
                    alert('API nøgle ugyldig. Gå til indstillinger og tjek at den passer med den i Dinero. Hvis dette bliver ved efter grundig gennemgang, bedes du kontakte Clubtimize');
                    location.href = "/panel/foreningsindstillinger";
                } else if (data === 'denied'){
                    alert('Firma ID ugyldig. Gå til indstillinger og tjek at den passer med den i Dinero. Hvis dette bliver ved efter grundig gennemgang, bedes du kontakte Clubtimize');
                    location.href = "/panel/foreningsindstillinger";
                } else {
                    parseDineroAccounts(JSON.parse(data));
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert('Error: ' + thrownError);
            }
        });
    } else if (chosenIntegration === 'Economic'){
        $.ajax({
            type: 'GET',
            url: "/panel/bogføring/economickonto",
            success: function(data){
                if(data === 'invalid_grant'){
                    alert('API nøgle ugyldig. Gå til indstillinger og tjek at den passer med den i Economic. Hvis dette bliver ved efter grundig gennemgang, bedes du kontakte Clubtimize');
                    location.href = "/panel/foreningsindstillinger";
                } else {
                    parseEconomicAccounts(JSON.parse(data));
                }
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert('Error: ' + thrownError);
            }
        });
    } else {
        $('#load_bookkeeping').css({'display': 'none'});
        $('#content_bookkeeping_hasNoData').css({'display': ''});
    }
}

function parseDineroAccounts(accountArray){
    // SORTER HER!!!

    $('[id^="sel_bookkeeping_accounts"]').each(function () {
        var accountSel = $(this);
        accountArray.forEach(function (element) {
            accountSel.append(new Option(element.AccountNumber + ' - ' + element.Name, element.AccountNumber));
        });
    });

    $('[id^="sel_bookkeeping_counter"]').each(function () {
        var accountSel = this;
        accountArray.forEach(function (element) {
            accountSel.append(new Option(element.AccountNumber + ' - ' + element.Name, element.AccountNumber));
        });
    });

    accountArray.forEach(function (element) {
        $('#sel_notFirstTime_account').append(new Option(element.AccountNumber + ' - ' + element.Name, element.AccountNumber));
        $('#sel_notFirstTime_counter').append(new Option(element.AccountNumber + ' - ' + element.Name, element.AccountNumber));
    });

    $('#load_bookkeeping').css({'display': 'none'});
    $('#content_bookkeeping_hasData').css({'display': ''});
}

function parseEconomicAccounts(accountArray){
    // SORTER HER!!!

    $('[id^="sel_bookkeeping_accounts"]').each(function () {
        var accountSel = $(this);
        accountArray.collection.forEach(function (element) {
            if (element.accountType === 'sumInterval' || element.accountType === 'headingStart' || element.accountType === 'heading' || element.accountType === 'totalFrom'){
                accountSel.append("<option disabled style='font-weight: bold;'>" + element.accountNumber + ' - ' + element.name + "</option>").attr("value", element.accountNumber);
            } else if (element.accountType === 'status' || element.accountType === 'profitAndLoss'){
                accountSel.append(new Option(element.accountNumber + ' - ' + element.name, element.accountNumber));
            }

        });
    });

    $('[id^="sel_bookkeeping_counter"]').each(function () {
        var accountSel = $(this);
        accountArray.collection.forEach(function (element) {
            if (element.accountType === 'sumInterval' || element.accountType === 'headingStart' || element.accountType === 'heading' || element.accountType === 'totalFrom'){
                accountSel.append("<option disabled style='font-weight: bold;'>" + element.accountNumber + ' - ' + element.name + "</option>").attr("value", element.accountNumber);
            } else if (element.accountType === 'status' || element.accountType === 'profitAndLoss'){
                accountSel.append(new Option(element.accountNumber + ' - ' + element.name, element.accountNumber));
            }
        });
    });

    accountArray.collection.forEach(function (element) {
        if (element.accountType === 'sumInterval' || element.accountType === 'headingStart' || element.accountType === 'heading' || element.accountType === 'totalFrom'){
            $('#sel_notFirstTime_account').append("<option disabled style='font-weight: bold;'>" + element.accountNumber + ' - ' + element.name + "</option>").attr("value", element.accountNumber);
            $('#sel_notFirstTime_counter').append("<option disabled style='font-weight: bold;'>" + element.accountNumber + ' - ' + element.name + "</option>").attr("value", element.accountNumber);
        } else if (element.accountType === 'status' || element.accountType === 'profitAndLoss'){
            $('#sel_notFirstTime_account').append(new Option(element.accountNumber + ' - ' + element.name, element.accountNumber));
            $('#sel_notFirstTime_counter').append(new Option(element.accountNumber + ' - ' + element.name, element.accountNumber));
        }
    });

    $('#load_bookkeeping').css({'display': 'none'});
    $('#content_bookkeeping_hasData').css({'display': ''});
}

jQuery('#date-range').datepicker({
    toggleActive: true
});

$('#btn_bookkeeping_send').click(function () {
    var data = {
        'StartDate': $('#dr_bookkeeping_start').val(),
        'EndDate': $('#dr_bookkeeping_end').val(),
        'WithFees': $('#che_bookkeeping_withfees').prop('checked'),
        'Teams': [

        ]
    };

    if(chosenIntegration === 'Economic'){
        data['Year'] = $('#sel_settings_economicAccountingYears').val();
    }

    $('#tbl_bookkeeping_teams > tbody  > tr').each(function () {
        var team = {};

        team['TeamTitle'] = $($(this).children()[0]).text();
        team['TeamID'] = $(this).attr('id');
        team['Account'] = $($($(this).children()[1]).children()[0]).val();
        team['Counter'] = $($($(this).children()[2]).children()[0]).val();

        data.Teams.push(team);
    });

    var invoicesWithoutTeams = {};
    invoicesWithoutTeams['TeamTitle'] = 'invoicesWithoutTeam';
    invoicesWithoutTeams['TeamID'] = 0;
    invoicesWithoutTeams['Account'] = $('#sel_notFirstTime_account').val();
    invoicesWithoutTeams['Counter'] = $('#sel_notFirstTime_counter').val();
    data.Teams.push(invoicesWithoutTeams);

    var sendBtn = $(this);

    Swal({
        title: 'Er du sikker?',
        text: 'Vi anbefaler, at du tjekker dine oplysninger grundigt igennem før du fortsætter',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja, send til kassekladde',
        cancelButtonText: "Annuller"
    }).then(function (result) {
        if (result.value) {
            if(validateForm()){

                setLoaderState(sendBtn);

                $.ajax({
                    type: 'POST',
                    url: "/panel/bogføring/bogfør",
                    contentType: "application/json",
                    data: JSON.stringify(data),
                    async: true,
                    success: function(response){
                        if (chosenIntegration === 'Dinero'){
                            setNormalState(sendBtn, 'Success! Genindlæs siden for at bogfører igen');
                            sendBtn.attr('disabled', '');
                            sendBtn.removeClass('btn-primary');
                            sendBtn.addClass('btn-success');

                            $('#btn_bookkeeping_history').attr('disabled', '');
                        } else if (chosenIntegration === 'Economic'){
                            validateSuccessCall(JSON.parse(response), sendBtn);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError){
                        alert('Error: ' + thrownError);
                    }
                });
            }
        }
    });
});

function validateSuccessCall(response, btn) {
    var noInvoices = true;
    var yearError = false;

    var emptyCount = 0;

    outterLoop:
    for (var team in response) {
        var statusMessage = response[team].Status;
        for (var error in response[team]['Errors']){
            if (response[team]['Errors'][error] === 'Date not in voucher accounting year.'){
                yearError = true;
                break outterLoop;
            } else if (response[team]['Errors'][error] === true) {
                emptyCount++;
            }
        }
    }

    if (Object.keys(response[team]).length + 1 !== emptyCount){
        noInvoices = false;
    }

    if (yearError){
        setNormalState(btn, 'Send til kassekladde');
        Swal.fire({
            title: 'Noget gik galt',
            text: 'Regnskabsåret passer ikke med den valgte periode. Tjek dine oplysninger og prøv igen',
            type: 'error',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Fortsæt'
        });
    } else {
        if(noInvoices === true){
            setNormalState(btn, 'Send til kassekladde');
            Swal.fire({
                title: 'Noget gik galt',
                text: 'Ingen fakturaer fundet for denne periode. Tjek dine oplysninger og prøv igen',
                type: 'error',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Fortsæt'
            });
        } else {
            setNormalState(btn, 'Success! Genindlæs siden for at bogfører igen');
            btn.attr('disabled', '');
            btn.removeClass('btn-primary');
            btn.addClass('btn-success');

            $('#btn_bookkeeping_history').attr('disabled', '');
        }
    }
}

function validateForm() {
    var validDateRangeStart = validateDateRange($('#dr_bookkeeping_start'));
    var validDateRangeEnd = validateDateRange($('#dr_bookkeeping_end'));

    return validDateRangeStart && validDateRangeEnd;
}


