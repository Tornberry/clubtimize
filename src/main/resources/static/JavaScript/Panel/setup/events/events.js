// Initial stuff

function postNewEvent(){
    var data = {
        dateFrom: $('#inp_newEventModal_fromDate').val(),
        timeFrom: $('#inp_newEventModal_fromTime').val(),
        dateTo: $('#inp_newEventModal_toDate').val(),
        timeTo: $('#inp_newEventModal_toTime').val(),
        deadlineDate: $('#inp_newEventModal_deadlineDate').val(),
        deadlineTime: $('#inp_newEventModal_deadlineTime').val(),
        title: $('#inp_newEventModal_title').val(),
        location: $('#inp_newEventModal_location').val(),
        price: $('#inp_newEventModal_price').val(),
        teamsInvited: $('#sel_newEvent_teams').select2('val'),
        usersInvited: $('#sel_newEvent_individuals').select2('val'),
        description: tinyMCE.get('editor_eventCalendar').getContent()
    };

    setLoaderState($("a[href='#finish']"));

    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: '/panel/opsætning/events',
        data: JSON.stringify(data),
        success: function(){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(xhr.status);
            alert(thrownError);
        }
    });
}

$(document).ready(function () {
    $('#tbl_events_upcomming').DataTable({
        searching: false,
        language: {
            "lengthMenu": "Vis _MENU_ events per side",
            "zeroRecords": "Ingen events fundet",
            "info": "Viser side _PAGE_ af _PAGES_",
            "infoEmpty": "Ingen events tilgængelige",
            "infoFiltered": "(Filtreret fra _MAX_ antal events)",
            "paginate": {
                "previous": "Tidligere side",
                "next": "Næste side"
            }
        }
    });
});

$("#form_newEventModal").steps({
    headerTag: "h3",
    bodyTag: "fieldset",
    transitionEffect: "slide",
    onFinished: function () {
        postNewEvent();
    },
    onStepChanging: function (event, currentIndex, newIndex) {
        if (currentIndex === 0){
            return validateForm(currentIndex);
        } else {
            return -1;
        }
    },
    labels: {
        current: "Nuværende step:",
        finish: "Opret event",
        next: "Næste",
        previous: "Forrige",
        loading: "Loader ..."
    }
});

function validateForm(index) {
    if (index === 0){
        var validDateFrom = validateRequired($('#inp_newEventModal_fromDate'), $('#inp_newEventModal_fromDate_validate'), 'Vælg en start dato');
        var validTimeFrom = validateRequired($('#inp_newEventModal_fromTime'), $('#inp_newEventModal_fromTime_validate'), 'Vælg en start tid');

        var validDateTo = validateRequired($('#inp_newEventModal_toDate'), $('#inp_newEventModal_toDate_validate'), 'Vælg en slut dato');
        var validTimeTo = validateRequired($('#inp_newEventModal_toTime'), $('#inp_newEventModal_toTime_validate'), 'Vælg en slut tid');

        var validDeadlineDate = validateRequired($('#inp_newEventModal_deadlineDate'), $('#inp_newEventModal_deadlineDate_validate'), 'Vælg en deadline dato');
        var validDeadlineTime = validateRequired($('#inp_newEventModal_deadlineTime'), $('#inp_newEventModal_deadlineTime_validate'), 'Vælg en deadline tid');

        var validTitle = validateRequired($('#inp_newEventModal_title'), $('#inp_newEventModal_title_validate'), 'Angiv en titel');
        var validLocation = validateRequired($('#inp_newEventModal_location'), $('#inp_newEventModal_location_validate'), 'Angiv et sted');
        var validPrice = validatePrice($('#inp_newEventModal_price'), $('#inp_newEventModal_price_validate'));

        return validDateFrom && validTimeFrom && validDateTo && validTimeTo && validDeadlineDate && validDeadlineTime && validTitle && validLocation && validPrice;
    }

}

$("#sel_newEvent_teams").select2({
    width: '100%'
});

$("#sel_newEvent_individuals").select2({
    width: '100%'
});

tinymce.init({
    selector: "textarea#editor_eventCalendar",
    theme: "modern",
    height:550,
    language: 'da',
    branding: false,
    fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
    plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        "save table contextmenu directionality emoticons template paste textcolor"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
    style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
});