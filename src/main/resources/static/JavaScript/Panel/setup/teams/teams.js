$(document).ready(function () {
    $("#form_newTeamModal_newTeam").steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slide",
        onFinished: function () {
            postNewTeam();
        }
    });

    $('#datatable-teams').DataTable({
        searching: false,
        language: {
            "lengthMenu": "Vis _MENU_ hold per side",
            "zeroRecords": "Ingen hold fundet",
            "info": "Viser side _PAGE_ af _PAGES_",
            "infoEmpty": "Ingen hold tilgængelige",
            "infoFiltered": "(Filtreret fra _MAX_ antal hold)",
            "paginate": {
                "previous": "Tidligere side",
                "next": "Næste side"
            }
        }
    });

    $('#sel_newTeamModal_paymentSchedule').change(function () {
        var labelUnder = $('#lbl_newTeamModal_paymentScheduleWarning');
        var labelOver = $('#lbl_newTeamModal_labelOverPaymentSchedule');
        if ($(this).val() === 'årligt'){
            labelUnder.text('Indtast den årlige pris');
            labelOver.text('Kontingent pris i DKK (Årligt)');
        } else if($(this).val() === 'halvårligt'){
            labelUnder.text('Indtast den halvårlige pris');
            labelOver.text('Kontingent pris i DKK (Halvårligt)');
        }
    });
});



function postNewTeam() {
    var teamAdapter = {
        title: $('#inp_newTeamModal_title').val(),
        department: $('#sel_newTeamModal_department').val(),
        closed: $('#che_newTeamModal_ifClosed').is(":checked"),
        avalibleSpots: parseInt($('#inp_newTeamModal_avalibleSpots').val()),
        coaches: $('#sel_newTeamModal_coaches').select2('val'),
        trainingScheduleAdapters: getTrainingSchedules(),
        ageGroup: $('#inp_newTeamModal_describedAge').val(),
        niveau: $('#inp_newTeamModal_describedNiveau').val(),
        price: parseFloat($('#inp_newTeamModal_price').val()),
        coaches: $('#sel_newTeamModal_coaches').val(),
        paymentSchedule: $('#sel_newTeamModal_paymentSchedule').val(),
        startDate: $('#datepicker-autoclose').val()
    };

    setLoaderState($("a[href='#finish']"));

    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: '/panel/opsætning/hold',
        data: JSON.stringify(teamAdapter),
        success: function(){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(xhr.status);
            alert(thrownError);
        }
    });
}

function getTrainingSchedules () {
    var returnValue = [];
    $('[data-repeater-item]').each(function(index){
        returnValue.push(
            {
                weekday: document.getElementsByName("trainings[" + index + "][day]")[0].value,
                timeFrom: document.getElementsByName("trainings[" + index + "][timefrom]")[0].value,
                timeTo: document.getElementsByName("trainings[" + index + "][timeto]")[0].value,
                location: document.getElementsByName("trainings[" + index + "][location]")[0].value
            }
        )
    });
    return returnValue;
}

function deleteTeam(id) {
    Swal({
        title: 'Er du sikker?',
        text: "Dette haller, dets træninger og medlemmer vil blive slettet",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja, slet holdet!'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: 'DELETE',
                contentType: "application/json",
                url: '/panel/opsætning/hold/deleteteam',
                data: id,
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        }
    })
}

