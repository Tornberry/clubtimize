var id;

function setId(id){
    this.id = id;
}

jQuery('#date-range').datepicker({
    toggleActive: true
});

jQuery('#date-range_single').datepicker({
    toggleActive: true
});

$('#inp_newBookingPeriodModal_start').mask("00", {placeholder: "__"});
$('#inp_newBookingPeriodModal_end').mask("00", {placeholder: "__"});

$('#btn_newBookingPeriodModal_makePeriod').click(function () {
    var data = {
        hallId: id,
        periodStartDate: $('#dr_newSingleBookingPeriodModal_start').val(),
        periodEndDate: $('#dr_newSingleBookingPeriodModal_end').val(),
        dayOfWeek: $("#sel_newBookingPeriodModal_dayOfWeek option:selected").val(),
        startTime: $('#inp_newBookingPeriodModal_start').val(),
        endTime: $('#inp_newBookingPeriodModal_end').val(),
        interval: $('#sel_newBookingPeriodModal_interval option:selected').val(),
        price: parseFloat($('#inp_newBookingPeriodModal_price').val())
    };

    if (validateForm()){
        setLoaderState($(this));

        $.ajax({
            type: 'POST',
            contentType: "application/json",
            url: '/panel/opsætning/banebooking/single',
            data: JSON.stringify(data),
            success: function(response){
                location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(thrownError);
            }
        });
    }
});

function deleteBookingPeriod(id, btn) {
    setLoaderState($(btn));

    $.ajax({
        type: 'DELETE',
        contentType: "application/json",
        url: '/panel/opsætning/banebooking/single',
        data: id,
        success: function(response){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(thrownError);
        }
    });
}

function validateForm() {
    var validStartDate = validateDateRange($('#dr_newSingleBookingPeriodModal_start'));
    var validEndDate = validateDateRange($('#dr_newSingleBookingPeriodModal_end'));
    var validStartTime = validateHour($('#inp_newBookingPeriodModal_start'), $('#inp_newBookingPeriodModal_end'), $('#inp_newBookingPeriodModal_start_validate'));
    var validEndTime = validateHour($('#inp_newBookingPeriodModal_end'), null, $('#inp_newBookingPeriodModal_end_validate'));
    var validPrice = validateRequired($('#inp_newBookingPeriodModal_price'), $('#inp_newBookingPeriodModal_price_validate'), 'Angiv en pris');

    return validStartDate && validEndDate && validStartTime && validEndTime && validPrice;
}