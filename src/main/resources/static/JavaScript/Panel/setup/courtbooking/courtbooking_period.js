var timePeriodIdModal;

$('#inp_newSeasonBookingPeriodModal_start').mask("00", {placeholder: "__"});
$('#inp_newSeasonBookingPeriodModal_end').mask("00", {placeholder: "__"});

$('#btn_newSeasonBookingPeriodModal_makePeriod').click(function () {
    if (validateForm_period()){
        var data = {
            hallId: id,
            seasonDateStart: $('#dr_newSeasonBookingPeriodModal_start').val(),
            seasonDateEnd: $('#dr_newSeasonBookingPeriodModal_end').val(),
            dayOfWeek: $('#sel_newSeasonBookingPeriodModal_dayOfWeek').val(),
            timeStart: $('#inp_newSeasonBookingPeriodModal_start').val(),
            timeEnd: $('#inp_newSeasonBookingPeriodModal_end').val(),
            interval: $('#sel_newSeasonBookingPeriodModal_interval').val(),
            price: $('#inp_newSeasonBookingPeriodModal_price').val(),
            reserve: $('#che_newSeasonBookingPeriodModal_reserve').is(":checked"),
            singlePrice: $('#inp_newSeasonBookingPeriodModal_price_single').val()
        };

        setLoaderState($(this));

        $.ajax({
            type: 'POST',
            contentType: "application/json",
            url: '/panel/opsætning/banebooking/season',
            data: JSON.stringify(data),
            success: function(response){
                location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(thrownError);
            }
        });
    }
});

function deleteSeasonBookingPeriod(id, btn) {
    Swal({
        title: 'Er du sikker?',
        text: "Er du sikker på at du vil slette denne periode?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja, slet periode!'
    }).then(function (result) {
        if (result.value) {
            setLoaderState($(btn));

            $.ajax({
                type: 'DELETE',
                contentType: "application/json",
                url: '/panel/opsætning/banebooking/season',
                data: id,
                success: function(response){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        }
    });
}

$('#che_newSeasonBookingPeriodModal_reserve').change(function () {
    var content = $('#content_newSeasonBookingPeriod_singlePrice');

    if($(this).is(":checked")) {
        content.css('display', 'none');
    } else {
        content.css('display', '');
    }
});

function validateForm_period(){
    var validStartDate = validateDateRange($('#dr_newSeasonBookingPeriodModal_start'));
    var validateEndDate = validateDateRange($('#dr_newSeasonBookingPeriodModal_end'));
    var validStartTime = validateHour($('#inp_newSeasonBookingPeriodModal_start'),$('#inp_newSeasonBookingPeriodModal_end'), $('#inp_newSeasonBookingPeriodModal_start_validate'));
    var validEndTime = validateHour($('#inp_newSeasonBookingPeriodModal_end'),null, $('#inp_newSeasonBookingPeriodModal_end_validate'));
    var validPrice = validatePrice($('#inp_newSeasonBookingPeriodModal_price'), $('#inp_newSeasonBookingPeriodModal_price_validate'));

    if($('#che_newSeasonBookingPeriodModal_reserve').is(":checked")){
        console.log("not right place")
        return validStartDate && validEndTime && validStartTime && validEndTime && validPrice;
    } else {
        console.log("right place");
        var validSinglePrice = validatePrice($('#inp_newSeasonBookingPeriodModal_price_single'), $('#inp_newSeasonBookingPeriodModal_price_single_validate'));
        return validStartDate && validEndTime && validStartTime && validEndTime && validPrice && validSinglePrice;
    }
}

function openInviteModal(id){
    timePeriodIdModal = id;

    $('#modal_inviteTimePeriodModal').modal('show');
}

$('#btn_inviteTimePeriodModal_invite').click(function () {
    if(validateInviteForm()){
        var data = {
            TimePeriodId: timePeriodIdModal,
            Reserved: $('#che_inviteTimePeriodModal_reserve').is(':checked'),
            Email: $('#inp_inviteTimePeriodModal_email').val(),
            Name: $('#inp_inviteTimePeriodModal_name').val()
        };

        setLoaderState($(this));

        $.ajax({
            type: 'POST',
            contentType: "application/json",
            url: '/panel/opsætning/banebooking/season/inviter',
            data: JSON.stringify(data),
            success: function(){
                location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(thrownError);
            }
        });
    }
});

function validateInviteForm() {
    var validEmail = validateEmail($('#inp_inviteTimePeriodModal_email'), $('#inp_inviteTimePeriodModal_email_validate'));
    var validName = validateRequired($('#inp_inviteTimePeriodModal_name'), $('#inp_inviteTimePeriodModal_name_validate'), 'Angiv et navn');

    return validEmail && validName;
}

function cancelInvite(id) {
    setLoaderState($('#' + id));

    $.ajax({
        type: 'DELETE',
        contentType: "application/json",
        url: '/panel/opsætning/banebooking/season/inviter',
        data: id,
        success: function(){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(thrownError);
        }
    });
}

