$(document).ready(function () {
    $("#nyRolleForm").steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slide",
        onFinished: function () {
           var roleAdapter = {
               roleName: $('#modalRoleName').val(),
               canAdministrate: $('#canAdministrate').is(':checked'),
               canSeeAdministrationPages: $('#canSeeAdministrationPages').is(':checked'),
               canSeeAllTeams: $('#canSeeAllTeams').is(':checked'),
               canUseTrainerFeatures: $('#canUseTrainerFeatures').is(':checked')
           };

            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: '/panel/opsætning/rolls/newrole',
                data: JSON.stringify(roleAdapter),
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        }
    });
});