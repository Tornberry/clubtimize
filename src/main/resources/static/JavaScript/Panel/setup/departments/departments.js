var date = new Date();

var calendar =  $('#calendar').fullCalendar({
    locale: "da",
    header: false,
    firstDay: 1,
    selectable: false,
    defaultView: 'month',

    axisFormat: 'H:mm',

    week:{ titleFormat: "DD MMMM YYYY" },
    allDaySlot: false,
    selectHelper: true,
    droppable: false, // this allows things to be dropped onto the calendar !!!

    events: [
        {
            title: 'hold 2',
            start: new Date(date.getFullYear(), date.getMonth(), 1),
            allDay: true,
            color: '#C6E3F1',
            textColor: '#44a2d2'
        },
        {
            title: 'hold 2',
            start: new Date(date.getFullYear(), date.getMonth(), 2),
            allDay: true,
            color: '#C6E3F1',
            textColor: '#44a2d2'
        },
        {
            title: 'hold 3',
            start: new Date(date.getFullYear(), date.getMonth(), 3),
            allDay: true,
            color: '#C6E3F1',
            textColor: '#44a2d2'
        },
        {
            title: 'hold 4',
            start: new Date(date.getFullYear(), date.getMonth(), 4),
            allDay: true,
            color: '#C6E3F1',
            textColor: '#44a2d2'
        },
        {
            title: 'hold 5',
            start: new Date(date.getFullYear(), date.getMonth(), 5),
            allDay: true,
            color: '#C6E3F1',
            textColor: '#44a2d2'

        }
    ],
    timeFormat: 'H:mm'
});

var calendar_edit =  $('#calendar-edit').fullCalendar({
    locale: "da",
    header: false,
    firstDay: 1,
    selectable: false,
    defaultView: 'month',

    axisFormat: 'H:mm',

    week:{ titleFormat: "DD MMMM YYYY" },
    allDaySlot: false,
    selectHelper: true,
    droppable: false, // this allows things to be dropped onto the calendar !!!

    events: [
        {
            title: 'hold 2',
            start: new Date(date.getFullYear(), date.getMonth(), 1),
            allDay: true,
            color: '#C6E3F1',
            textColor: '#44a2d2'
        },
        {
            title: 'hold 2',
            start: new Date(date.getFullYear(), date.getMonth(), 2),
            allDay: true,
            color: '#C6E3F1',
            textColor: '#44a2d2'
        },
        {
            title: 'hold 3',
            start: new Date(date.getFullYear(), date.getMonth(), 3),
            allDay: true,
            color: '#C6E3F1',
            textColor: '#44a2d2'
        },
        {
            title: 'hold 4',
            start: new Date(date.getFullYear(), date.getMonth(), 4),
            allDay: true,
            color: '#C6E3F1',
            textColor: '#44a2d2'
        },
        {
            title: 'hold 5',
            start: new Date(date.getFullYear(), date.getMonth(), 5),
            allDay: true,
            color: '#C6E3F1',
            textColor: '#44a2d2'

        }
    ],
    timeFormat: 'H:mm'
});

$('#tbl_departments_departments').DataTable({
    searching: false,
    language: {
        "lengthMenu": "Vis _MENU_ afdelinger per side",
        "zeroRecords": "Ingen afdelinger fundet",
        "info": "Viser side _PAGE_ af _PAGES_",
        "infoEmpty": "Ingen afdelinger tilgængelige",
        "infoFiltered": "(Filtreret fra _MAX_ antal afdelinger)",
        "paginate": {
            "previous": "Tidligere side",
            "next": "Næste side"
        }
    }
});

$('#oth_newDepartmentModal_colorpicker').colorpicker({format: "rgb"}).on('change', function (e) {
    var selectedColor = $('#inp_newDepartmentModal_color').val();

    var red = selectedColor.split("(")[1].split(")")[0].split(",")[0];
    var green = selectedColor.split("(")[1].split(")")[0].split(",")[1];
    var blue = selectedColor.split("(")[1].split(")")[0].split(",")[2];

    calendar.fullCalendar('removeEvents');
    calendar.fullCalendar('addEventSource', [
        {
            title: 'hold 2',
            start: new Date(date.getFullYear(), date.getMonth(), 1),
            allDay: true,
            color: bleach(red, green, blue),
            textColor: 'rgb(' + red + ',' + green + ',' + blue + ')'
        },
        {
            title: 'hold 2',
            start: new Date(date.getFullYear(), date.getMonth(), 2),
            allDay: true,
            color: bleach(red, green, blue),
            textColor: 'rgb(' + red + ',' + green + ',' + blue + ')'
        },
        {
            title: 'hold 3',
            start: new Date(date.getFullYear(), date.getMonth(), 3),
            allDay: true,
            color: bleach(red, green, blue),
            textColor: 'rgb(' + red + ',' + green + ',' + blue + ')'
        },
        {
            title: 'hold 4',
            start: new Date(date.getFullYear(), date.getMonth(), 4),
            allDay: true,
            color: bleach(red, green, blue),
            textColor: 'rgb(' + red + ',' + green + ',' + blue + ')'
        },
        {
            title: 'hold 5',
            start: new Date(date.getFullYear(), date.getMonth(), 5),
            allDay: true,
            color: bleach(red, green, blue),
            textColor: 'rgb(' + red + ',' + green + ',' + blue + ')'

        }
    ]);
});

$('#oth_editDepartmentModal_colorpicker').colorpicker({format: "rgb"}).on('change', function (e) {
    var selectedColor = $('#inp_editDepartmentModal_color').val();

    var red = selectedColor.split("(")[1].split(")")[0].split(",")[0];
    var green = selectedColor.split("(")[1].split(")")[0].split(",")[1];
    var blue = selectedColor.split("(")[1].split(")")[0].split(",")[2];

    calendar_edit.fullCalendar('removeEvents');
    calendar_edit.fullCalendar('addEventSource', [
        {
            title: 'hold 2',
            start: new Date(date.getFullYear(), date.getMonth(), 1),
            allDay: true,
            color: bleach(red, green, blue),
            textColor: 'rgb(' + red + ',' + green + ',' + blue + ')'
        },
        {
            title: 'hold 2',
            start: new Date(date.getFullYear(), date.getMonth(), 2),
            allDay: true,
            color: bleach(red, green, blue),
            textColor: 'rgb(' + red + ',' + green + ',' + blue + ')'
        },
        {
            title: 'hold 3',
            start: new Date(date.getFullYear(), date.getMonth(), 3),
            allDay: true,
            color: bleach(red, green, blue),
            textColor: 'rgb(' + red + ',' + green + ',' + blue + ')'
        },
        {
            title: 'hold 4',
            start: new Date(date.getFullYear(), date.getMonth(), 4),
            allDay: true,
            color: bleach(red, green, blue),
            textColor: 'rgb(' + red + ',' + green + ',' + blue + ')'
        },
        {
            title: 'hold 5',
            start: new Date(date.getFullYear(), date.getMonth(), 5),
            allDay: true,
            color: bleach(red, green, blue),
            textColor: 'rgb(' + red + ',' + green + ',' + blue + ')'

        }
    ]);
});

function bleach(red, green, blue){
    var newRed = (red*(1 - 0.7)/255 + 0.7)*255;
    var newGreen = (green*(1 - 0.7)/255 + 0.7)*255;
    var newBlue = (blue*(1 - 0.7)/255 + 0.7)*255;
    return 'rgb(' + newRed + ',' + newGreen + ',' + newBlue + ')';
}

$('#oth_editDepartmentModal_colorpicker').colorpicker({format: "rgb"});

$('#btn_newDepartmentModal_makeDepartment').click(function(){
    var categoryAdapter = {
        title: $('#inp_newDepartmentModal_title').val(),
        color: $('#oth_newDepartmentModal_colorpicker').colorpicker('getValue')
    };

    setLoaderState($('#btn_newDepartmentModal_makeDepartment'));

    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: '/panel/opsætning/afdelinger',
        data: JSON.stringify(categoryAdapter),
        success: function(){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(xhr.status);
        }
    });
});

function deleteDepartment(id) {
    Swal({
        title: 'Er du sikker?',
        text: "Alle haller der er en del af denne afdeling vil blive permanent slettet",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja, slet afdelingen!'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: 'DELETE',
                contentType: "application/json",
                url: '/panel/opsætning/afdelinger/sletafdeling',
                data: id,
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(xhr.status);
                }
            });
        }
    })
}

function editTeamDepartment(id) {
    $('#inp_editDepartmentModal_title').val($('#afdelingTitle' + id).text());
    $('#inp_editDepartmentModal_color').val($('#afdelingColor' + id).text());
    $('#oth_editDepartmentModal_colorpicker').colorpicker('setValue', $('#inp_editDepartmentModal_color').val());

    var selectedColor = $('#inp_editDepartmentModal_color').val();

    var red = selectedColor.split("(")[1].split(")")[0].split(",")[0];
    var green = selectedColor.split("(")[1].split(")")[0].split(",")[1];
    var blue = selectedColor.split("(")[1].split(")")[0].split(",")[2];

    calendar_edit.fullCalendar('removeEvents');
    calendar_edit.fullCalendar('addEventSource', [
        {
            title: 'hold 2',
            start: new Date(date.getFullYear(), date.getMonth(), 1),
            allDay: true,
            color: bleach(red, green, blue),
            textColor: 'rgb(' + red + ',' + green + ',' + blue + ')'
        },
        {
            title: 'hold 2',
            start: new Date(date.getFullYear(), date.getMonth(), 2),
            allDay: true,
            color: bleach(red, green, blue),
            textColor: 'rgb(' + red + ',' + green + ',' + blue + ')'
        },
        {
            title: 'hold 3',
            start: new Date(date.getFullYear(), date.getMonth(), 3),
            allDay: true,
            color: bleach(red, green, blue),
            textColor: 'rgb(' + red + ',' + green + ',' + blue + ')'
        },
        {
            title: 'hold 4',
            start: new Date(date.getFullYear(), date.getMonth(), 4),
            allDay: true,
            color: bleach(red, green, blue),
            textColor: 'rgb(' + red + ',' + green + ',' + blue + ')'
        },
        {
            title: 'hold 5',
            start: new Date(date.getFullYear(), date.getMonth(), 5),
            allDay: true,
            color: bleach(red, green, blue),
            textColor: 'rgb(' + red + ',' + green + ',' + blue + ')'

        }
    ]);


    $('#btn_edtiDepartmentModal_editDepartment').attr('id', id);

    $('#modal_editDepartmentModal').modal();
}

$('#btn_edtiDepartmentModal_editDepartment').click(function () {
    var categoryAdapter = {
        id: parseInt($(this).attr('id')),
        title: $('#inp_editDepartmentModal_title').val(),
        color: $('#inp_editDepartmentModal_color').val()
    };

    setLoaderState($(this));

    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: '/panel/opsætning/afdelinger',
        data: JSON.stringify(categoryAdapter),
        success: function(){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(xhr.status);
        }
    });
});