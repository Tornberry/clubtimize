var lat;
var lng;

$('#tbl_halls_halls').DataTable({
    searching: false,
    language: {
        "lengthMenu": "Vis _MENU_ haller per side",
        "zeroRecords": "Ingen haller fundet",
        "info": "Viser side _PAGE_ af _PAGES_",
        "infoEmpty": "Ingen haller tilgængelige",
        "infoFiltered": "(Filtreret fra _MAX_ antal haller)",
        "paginate": {
            "previous": "Tidligere side",
            "next": "Næste side"
        }
    }
});

// Initialize and add the map
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'),{
        center:{
            lat: 57.0291963,
            lng: 9.9369635
        },
        zoom: 12
    });

    var searchBox = new google.maps.places.SearchBox(document.getElementById('searchBox'));

    var marker = new google.maps.Marker({
        position:{
            lat: 57.0291963,
            lng: 9.9369635
        },
        map:map,
        draggable: false
    });

    google.maps.event.addListener(searchBox, 'places_changed', function () {

        var places = searchBox.getPlaces();

        var autocomplete = new google.maps.places.Autocomplete(searchBox);
        autocomplete.bindTo('bounds', map);

        var bounds = new google.maps.LatLngBounds();
        var i, place;

        for (i=0; place=places[i]; i++){
            bounds.extend(place.geometry.location);
            marker.setPosition(place.geometry.location);
            lat = place.geometry.location.lat();
            lng = place.geometry.location.lng();
        }

        map.fitBounds(bounds);
        map.setZoom(15);
    })
}

function editHall(id){
    $('#modalRedigerHallTitle').val($('#hallTitle' + id).text());
    $('#modalRedigerAvalibleCourts').val($('#hallAvalibleCourts' + id).text());
    $('#modalRedigerLongitude').val($('#hallLongitude' + id).text());
    $('#modalRedigerLatitude').val($('#hallLatitude' + id).text());

    $('#btnOpdaterHal').attr('id', id);

    $('#modal_editHallModal').modal();
}

$('#btnOpretHal').click(function () {
    var hallAdapter = {
        title: $('#modalHallTitle').val(),
        courtsAvalible:parseInt($('#modalAvalibleCourts').val()),
        longitude: lng,
        latitude: lat
    };

    setLoaderState($('#btnOpretHal'));

    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: '/panel/opsætning/haller',
        data: JSON.stringify(hallAdapter),
        success: function(){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(xhr.status);
            alert(thrownError);
        }
    });
});

$('#btnOpdaterHal').click(function () {
    var hallAdapter = {
        hallId: $(this).attr('id'),
        title: $('#modalRedigerHallTitle').val(),
        courtsAvalible: parseInt($('#modalRedigerAvalibleCourts').val()),
        longitude: parseFloat($('#modalRedigerLongitude').val()),
        latitude: parseFloat($('#modalRedigerLatitude').val())
    };

    setLoaderState($(this));

    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: '/panel/opsætning/haller',
        data: JSON.stringify(hallAdapter),
        success: function(){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(xhr.status);
            alert(thrownError);
        }
    });
});

function deleteHall(id) {
    Swal({
        title: 'Er du sikker?',
        text: "Dette kan ikke fortrydes!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja, slet hallen!'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: 'DELETE',
                contentType: "application/json",
                url: '/panel/opsætning/haller/slethal',
                data: id,
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }
    })
}