$('#btn_free_upgrade').click(function () {
    $('#modal_worthKnowing').modal();
});

$('#btn_worthKnowing_gotoPayment').click(function () {
    if(validateForm()){
        alert('Denne feature er ikke implementeret endnu, kom tilbage senere')
    }
});

function validateForm() {
    var validRead = validateCheckbox($('#che_worthKnowing_above'), $('#lbl_worthKnowing_above'));
    var validService = validateCheckbox($('#che_worthKnowing_service'), $('#lbl_worthKnowing_service'));

    return validRead && validService;
}