$('#btn_resetPassword_reset').click(function () {
    if (validateForm()){
        var urlParams = new URLSearchParams(window.location.search);

        var newPasswordData = {
            "Id": urlParams.get('c'),
            "Password": $('#inp_resetPassword_password').val()
        };

        $.ajax({
            type: 'POST',
            contentType: "application/json",
            url: '/resetpassword',
            data: JSON.stringify(newPasswordData),
            success: function(){
                location.href = '/login';
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(thrownError);
            }
        });
    }
});

function validateForm() {
    return validatePassword($('#inp_resetPassword_password'), $('#inp_resetPassword_confirmpassword'), $('#inp_resetPassword_password_validate'), $('#inp_resetPassword_confirmpassword_validate'));
}