function addTeam(id, btn) {
    Swal({
        title: 'Er du sikker?',
        text: 'Er du sikker på at du vil tilmelde dig dette hold?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Annuller',
        confirmButtonText: 'Ja, tilmeld mig hold'
    }).then(function (result) {
        if (result.value) {
            setLoaderState($(btn));

            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: '/panel/tilmeldhold',
                data: id,
                success: function(link){
                    location.href = link;
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        }
    });
}

