$('#btn_settings_editinformation').click(function () {
    if($(this).text() === "Rediger oplysninger"){
        $(this).removeClass('btn-primary');
        $(this).addClass('btn-success');
        $(this).text('Gem oplysninger');

        $('#inp_settings_firstname').removeAttr("disabled");
        $('#inp_settings_lastname').removeAttr("disabled");
        $('#inp_settings_email').removeAttr("disabled");
        $('#inp_settings_phone').removeAttr("disabled");
        $('#inp_settings_address').removeAttr("disabled");
        $('#inp_settings_birthday').removeAttr("disabled");
        $('#sel_settings_gender').removeAttr("disabled");

        $('#btn_settings_cancel').show();
    } else if($(this).text() === 'Gem oplysninger'){
        sendJson();
    }
});

function sendJson() {
    Swal({
        title: 'Er du sikker?',
        text: 'Vi anbefaler, at du dobbelttjekker dine oplysninger før du fortsætter!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Annuller',
        confirmButtonText: 'Ja, ændre mine oplysninger'
    }).then(function (result) {
        if (result.value) {
            if (validateForm()){
                var changeSettingsJson = {
                    "Firstname": $('#inp_settings_firstname').val(),
                    "Lastname": $('#inp_settings_lastname').val(),
                    "Email": $('#inp_settings_email').val(),
                    "Phone": $('#inp_settings_phone').val(),
                    "Address": $('#inp_settings_address').val(),
                    "Birthday": $('#inp_settings_birthday').val(),
                    "Gender": $('#sel_settings_gender option:selected').val()
                };

                setLoaderState($('#btn_settings_editinformation'));

                $.ajax({
                    type: 'POST',
                    contentType: "application/json",
                    url: '/panel/indstillinger',
                    data: JSON.stringify(changeSettingsJson),
                    success: function(link){
                        successFunction(link);
                    },
                    error: function(xhr, ajaxOptions, thrownError){
                        alert(thrownError);
                    }
                });
            }
        }
    });
}

function successFunction(link){
    if (link === "email_in_use"){
        showNotification("Denne email er allerede i brug, vælg en anden", "info");
        setNormalState($('#btn_settings_editinformation'), "Gem oplysninger");
    } else if(link === "no_such_user"){
        showNotification("Der er sket en fejl, kontakt venligst Clubtimize. Klargør en kort beskrivelse af hvad der skete", "info");
        setNormalState($('#btn_settings_editinformation'), "Gem oplysninger");
    } else if(link === "OK") {
        location.reload();
    } else if(link === "OK_new"){
        location.href = "/login";
    }
}

$('#btn_settings_cancel').click(function () {
    location.reload();
});

function validateForm(){

    var validFirstname = validateRequired($('#inp_settings_firstname'), $('#inp_settings_firstname_validate'), 'Skriv dit fornavn');
    var validLastname = validateRequired($('#inp_settings_lastname'), $('#inp_settings_lastname_validate'), 'Skriv dit efternavn');
    var validEmail = validateEmail($('#inp_settings_email'), $('#inp_settings_email_validate'));
    var validPhone = validatePhone($('#inp_settings_phone'), $('#inp_settings_phone_validate'));
    var validAddress = validateRequired($('#inp_settings_address'), $('#inp_settings_address_validate'), 'Skriv din addresse');
    var validBirthday = validateRequired($('#inp_settings_birthday'), $('#inp_settings_birthday_validate'), 'Angiv din fødselsdag');

    return validFirstname && validLastname && validEmail && validPhone && validAddress && validBirthday;
}