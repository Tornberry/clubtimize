$('#tbl_registerEvent_going').DataTable({
    language: {
        "lengthMenu": "Vis _MENU_ events per side",
        "zeroRecords": "Ingen events fundet",
        "info": "Viser side _PAGE_ af _PAGES_",
        "infoEmpty": "Ingen events tilgængelige",
        "infoFiltered": "(Filtreret fra _MAX_ antal events)",
        "paginate": {
            "previous": "Tidligere side",
            "next": "Næste side"
        }
    }
});

$('#tbl_registerEvent_invited').DataTable({
    language: {
        "lengthMenu": "Vis _MENU_ events per side",
        "zeroRecords": "Ingen events fundet",
        "info": "Viser side _PAGE_ af _PAGES_",
        "infoEmpty": "Ingen events tilgængelige",
        "infoFiltered": "(Filtreret fra _MAX_ antal events)",
        "paginate": {
            "previous": "Tidligere side",
            "next": "Næste side"
        }
    }
});