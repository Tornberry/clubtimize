$('#tbl_newInvitationModalPlayer_invitations').DataTable({
    searching: false,
    language: {
        "lengthMenu": "Vis _MENU_ invitationer per side",
        "zeroRecords": "Ingen invitationer fundet",
        "info": "Viser side _PAGE_ af _PAGES_",
        "infoEmpty": "Ingen invitationer tilgængelige",
        "infoFiltered": "(Filtreret fra _MAX_ antal invitationer)",
        "paginate": {
            "previous": "Tidligere side",
            "next": "Næste side"

        }
    }
});

$('#btn_newInvitationModalPlayer_sendInv').click(function () {
    var newCoachAdapter = {
        email: $('#inp_newInvitationModalPlayer_invCoachEmail').val(),
        team: $('#sel_newInvitationModalPlayer_invCoachTeam option:selected').val()
    };

    setLoaderState($(this));


    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: '/panel/invitation/spiller',
        data: JSON.stringify(newCoachAdapter),
        success: function(){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(xhr.status);
        }
    });
});

function cancleInvitation(id) {
    Swal({
        title: 'Er du sikker?',
        text: "Denne handling kan ikke fortrydes",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja, annuller invitation!',
        cancelButtonText: 'Luk'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: 'DELETE',
                contentType: "application/json",
                url: '/panel/invitation/spiller',
                data: id,
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(xhr.status);
                }
            });
        }
    })
}