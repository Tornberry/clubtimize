$('#btn_newSeasonModal_create').click(function () {
    var data = {
        halfWaySeason: $('#che_newSeasonModal_isHalfWaySeason').prop("checked"),
        sendMail: $('#che_newSeasonModal_sendMail').prop("checked")
    };

    setLoaderState($(this));

    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: '/panel/foreningsindstillinger/nysæson',
        data: JSON.stringify(data),
        success: function(){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(thrownError);
        }
    });
});