$('#file_graphics_logo').dropify({
    messages: {
        default: 'Træk og slip et billede eller klik for at skifte logo',
        replace: 'Træk og slip et billede eller klik for at skifte logo',
        remove:  'Fjern',
        error:   'Der skete en fejl'
    }
});

$('#btn_changeLogoModal_change').click(function () {
    var formData = new FormData();
    formData.append('file', $('#file_graphics_logo')[0].files[0]);

    $.ajax({
        url : '/panel/foreningsindstillinger/logo',
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        enctype: 'multipart/form-data',
        processData: false,
        success: function (response) {
            location.reload();
        }
    });
});