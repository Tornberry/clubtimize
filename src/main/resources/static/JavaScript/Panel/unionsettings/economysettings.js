var chosenProgram = 'Dinero';

$('#che_economy_whopays').change(function() {
    Swal({
        title: 'Er du sikker?',
        text: 'Er du sikker på dette?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Annuller',
        confirmButtonText: 'Ja, ændre denne indstilling!'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: '/panel/foreningsindstillinger/gebyrer',
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        } else {
            location.reload();
        }
    });
});

$('#sel_setupBookkeepingModal_accountingprogram').change(function () {
    chosenProgram = $("#sel_setupBookkeepingModal_accountingprogram option:selected").val();
    if (chosenProgram === 'Dinero'){
        $('#content_setupBookkeepingModal_Dinero').css({'display': ''});
        $('#content_setupBookkeepingModal_Economic').css({'display': 'none'});
    } else if (chosenProgram === 'Economic') {
        $('#content_setupBookkeepingModal_Dinero').css({'display': 'none'});
        $('#content_setupBookkeepingModal_Economic').css({'display': ''});
    }
});

$('#btn_setupBookkeepingModal_save').click(function () {
    var data = {};
    if (chosenProgram === 'Dinero'){
        data['AccountingProgram'] = 'Dinero';
        data['APIKey'] = $('#inp_setupBookkeepingModal_APIKey_Dinero').val();
        data['OrganizationID'] = $('#inp_setupBookkeepingModal_organizationID_Dinero').val();

        if (validateForm_Dinero()){
            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: '/panel/foreningsindstillinger/DineroSetup',
                data: JSON.stringify(data),
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        }
    } else if (chosenProgram === 'Economic') {
        data['AccountingProgram'] = 'Economic';
        data['APIKey'] = $('#inp_setupBookkeepingModal_APIKey_Economic').val();

        if (validateForm_Economic()){
            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: '/panel/foreningsindstillinger/EconomicSetup',
                data: JSON.stringify(data),
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        }
    }
});

$('#btn_dinero_editAPIKey').click(function () {
    if ($(this).text() === 'Rediger API nøgle'){
        $(this).removeClass('btn-primary');
        $(this).addClass('btn-success');
        $('#btn_dinero_cancelAPIKey').css('display', '');
        $('#inp_dinero_API').prop('disabled', false);
        $(this).text('Gem nøgle');
    } else if ($(this).text() === 'Gem nøgle'){
        if (validateForm_DineroAPIKey()){
            $.ajax({
                type: 'POST',
                contentType: "text/plain",
                url: '/panel/foreningsindstillinger/EditDineroAPIKey',
                data: $('#inp_dinero_API').val(),
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        }
    }
});

$('#btn_dinero_editOrgID').click(function () {
    if ($(this).text() === 'Rediger Firma ID'){
        $(this).removeClass('btn-primary');
        $(this).addClass('btn-success');
        $('#btn_dinero_cancelOrgID').css('display', '');
        $('#inp_dinero_orgID').prop('disabled', false);
        $(this).text('Gem ID');
    } else if ($(this).text() === 'Gem ID'){
        if (validateForm_DineroOrgID()){
            $.ajax({
                type: 'POST',
                contentType: "text/plain",
                url: '/panel/foreningsindstillinger/EditDineroOrgID',
                data: $('#inp_dinero_orgID').val(),
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        }
    }
});

$('#btn_economic_editAPIKey').click(function () {
    if ($(this).text() === 'Rediger API nøgle'){
        $(this).removeClass('btn-primary');
        $(this).addClass('btn-success');
        $('#btn_economic_cancelAPIKey').css('display', '');
        $('#inp_economic_API').prop('disabled', false);
        $(this).text('Gem nøgle');
    } else if ($(this).text() === 'Gem nøgle'){
        if (validateForm_EconomicAPIKey()){
            $.ajax({
                type: 'POST',
                contentType: "text/plain",
                url: '/panel/foreningsindstillinger/EditEconomicAPIKey',
                data: $('#inp_economic_API').val(),
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        }
    }
});

$('#btn_dinero_cancelAPIKey').click(function () {
    location.reload();
});

$('#btn_dinero_cancelOrgID').click(function () {
    location.reload();
});

$('#btn_economic_cancelAPIKey').click(function () {
    location.reload();
});

function validateForm_Dinero() {
    var validateAPIKey = validateRequired($('#inp_setupBookkeepingModal_APIKey_Dinero'), $('#inp_setupBookkeepingModal_APIKey_Dinero_validate'), 'Angiv en API nøgle');
    var validateFirmaID = validateRequired($('#inp_setupBookkeepingModal_organizationID_Dinero'), $('#inp_setupBookkeepingModal_organizationID_Dinero_validate'), 'Angiv et firma ID');
    return validateAPIKey && validateFirmaID;
}

function validateForm_Economic() {
    var validateAPIKey = validateRequired($('#inp_setupBookkeepingModal_APIKey_Economic'), $('#inp_setupBookkeepingModal_APIKey_Economic_validate'), 'Angiv en API nøgle');
    return validateAPIKey;
}

function validateForm_DineroAPIKey() {
    var validateAPIKey = validateRequired($('#inp_dinero_API'), $('#inp_dinero_API_validate'), 'Angiv en API nøgle');
    return validateAPIKey;
}

function validateForm_DineroOrgID() {
    var validateFirmID = validateRequired($('#inp_dinero_orgID'), $('#inp_dinero_orgID_validate'), 'Angiv et firma ID');
    return validateFirmID;
}

function validateForm_EconomicAPIKey() {
    var validateAPIKey = validateRequired($('#inp_economic_API'), $('#inp_economic_API_validate'), 'Angiv API nøgle');
    return validateAPIKey;
}