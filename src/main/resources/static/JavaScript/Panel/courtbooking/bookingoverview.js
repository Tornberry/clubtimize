setTimeout(function () {
    location.reload();
}, 60000);

var counterElement = $('#lbl_bookingoverview_timer');
var counter = 60;
setInterval(function () {
    counter--;
    counterElement.text(counter).fadeIn();
}, 1000);