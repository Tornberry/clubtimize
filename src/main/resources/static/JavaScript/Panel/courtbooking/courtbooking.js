document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendarData = JSON.parse($.ajax({
        type: 'GET',
        contentType: "application/json",
        url: '/panel/banebooking/fetch',
        async: false,
        success: function(response){

        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(thrownError);
        }
    }).responseText);

    calendarData.eventClick = function(info) {
        if (info.event.title === 'Optaget'){
            $('#content_newBookingModal_booked').css({'display': ''});
            $('#content_newBookingModal_free').css({'display': 'none'});

            $('#content_seeBookingModal_booked_hall').text(info.event.extendedProps.hall);
            $('#content_seeBookingModal_booked_court').text(info.event.extendedProps.court);
            $('#content_seeBookingModal_booked_date').text(info.event.end.toLocaleDateString().split('.').join('-'));
            $('#content_seeBookingModal_booked_time').text(info.event.start.getHours() + ':00' + '-' + info.event.end.getHours() + ':00');
            $('#content_seeBookingModal_booked_recipientName').text(info.event.extendedProps.recipientName);
            $('#content_seeBookingModal_booked_recipientEmail').text(info.event.extendedProps.recipientEmail);
            $('#content_seeBookingModal_booked_recipientAddress').text(info.event.extendedProps.recipientAddress);
            $('#content_seeBookingModal_booked_recipientPhone').text(info.event.extendedProps.recipientPhone);

           $('#modal_seeBookingModal').modal();
        } else if (info.event.title === 'Ledig') {
            $('#content_newBookingModal_booked').css({'display': 'none'});
            $('#content_newBookingModal_free').css({'display': ''});

            $('#content_seeBookingModal_free_hall').text(info.event.extendedProps.hall);
            $('#content_seeBookingModal_free_court').text(info.event.extendedProps.court);
            $('#content_seeBookingModal_free_date').text(info.event.end.toLocaleDateString().split('.').join('-'));
            $('#content_seeBookingModal_free_time').text(info.event.start.getHours() + ':00' + '-' + info.event.end.getHours() + ':00');

            $('#modal_seeBookingModal').modal('show');
        }
    };

    var calendar = new FullCalendar.Calendar(calendarEl, calendarData);

     calendar.render();
});

