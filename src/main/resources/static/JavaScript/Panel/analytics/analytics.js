new Chart(document.getElementById("chart_analyticsCenter_departmentSignup"), {
    type: 'bar',
    data: JSON.parse(getDepartmentSignupData()),
    options: {
        responsive: true,
        legend: {
            display: false
        },
        title: {
            display: true,
            text: 'Tilmeldinger pr. afdeling'
        },
        animation: {
            animateScale: true
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    callback: function (value) { if (Number.isInteger(value)) { return value; } },
                    stepSize: 10
                }
            }]
        }
    }
});

new Chart(document.getElementById("bar-chart-grouped"), {
    type: 'bar',
    data: {
        labels: ["Ungdom", "Senior", "Motion"],
        datasets: [
            {
                label: "Laveste point",
                backgroundColor: "#3e95cd",
                data: [750,896,851]
            }, {
                label: "Gennemsnitlig point",
                backgroundColor: "#8e5ea2",
                data: [1003,1087,993]
            }, {
                label: "Højeste point",
                backgroundColor: "#CA2E55",
                data: [1209,1188,1044]
            }
        ]
    },
    options: {
        title: {
            display: true,
            text: 'Population growth (millions)'
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});