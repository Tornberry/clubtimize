function getDepartmentSignupData() {
    var data = $.ajax({
        type: 'GET',
        url: "/panel/analysecenter/departmentsignups",
        async: false,
        error: function(xhr, ajaxOptions, thrownError){
            alert('Error: ' + thrownError);
        }
    });

    return data.responseText;
}