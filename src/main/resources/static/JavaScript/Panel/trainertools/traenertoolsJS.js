var genderChart;
var participationChart;

$(document).ready(function () {
   populateChart();
});

{
    "use strict";

    var GoogleMap = function() {};

        //creates map with markers
        GoogleMap.prototype.createMarkers = function($container) {
            var map = new GMaps({
                div: $container,
                lat: $('#divLatitude').text(),
                lng: $('#divLongitude').text()
            });

            //sample markers, but you can pass actual marker data as function parameter
            map.addMarker({
                lat: $('#divLatitude').text(),
                lng: $('#divLongitude').text(),
                title: 'Hallen',
                details: {
                    database_id: 42,
                    author: 'Foreningen'
                },
                click: function(e){
                    if(console.log)
                        console.log(e);
                    alert('You clicked in this marker');
                }
            });

            return map;
        },


        //init
        GoogleMap.prototype.init = function() {
            var $this = this;
            $(document).ready(function(){

                //with sample markers
                $this.createMarkers('#gmaps-markers');
            });
        },
        //init
        $.GoogleMap = new GoogleMap, $.GoogleMap.Constructor = GoogleMap
}(window.jQuery),

//initializing
    function($) {
        "use strict";
        $.GoogleMap.init()
    }(window.jQuery);


function updateGenderChart() {
    var table = $('#playerTable');
    var men = 0;
    var women = 0;
    var participated = 0;
    var notParticipated = table.children('tbody').children('tr').length;


    table.children('tbody').children('tr').each(function () {
        if ($(this).children('td')[1].textContent === 'Mand' && $(this).children('td').children('input').is(':checked')){
            men++;
        } else if ($(this).children('td')[1].textContent === 'Kvinde' && $(this).children('td').children('input').is(':checked')){
            women++;
        }

        if ($(this).children('td').children('input').is(':checked')){
            participated++;
            notParticipated--;
        }
    });

    $('#menValue').text(men);
    $('#menPercentValue').text(((men/participated)*100).toFixed(2) + '%');
    $('#womenValue').text(women);
    $('#womenPercentValue').text(((women/participated)*100).toFixed(2) + '%');

    genderChart.data.datasets[0].data = [men, women];

    genderChart.update();
}

function updateParticipationChart() {
    var table = $('#playerTable');
    var playerCount = table.children('tbody').children('tr').length;
    var participated = 0;
    var notParticipated = table.children('tbody').children('tr').length;

    table.children('tbody').children('tr').each(function () {
        if ($(this).children('td').children('input').is(':checked')){
            participated++;
            notParticipated--;
        }
    });

    $('#participationValue').text(participated);
    $('#notParticipationValue').text(notParticipated);
    $('#participationPercentValue').text(((notParticipated/playerCount)*100).toFixed(2) + '%');

    participationChart.data.datasets[0].data = [participated, notParticipated];

    participationChart.update();
}



function populateChart(){
    var table = $('#playerTable');

    var playerCount = table.children('tbody').children('tr').length;
    var men = 0;
    var women = 0;
    var participated = 0;
    var notParticipated = table.children('tbody').children('tr').length;

    table.children('tbody').children('tr').each(function () {
        if ($(this).children('td')[1].textContent === 'Mand' && $(this).children('td').children('input').is(':checked')){
            men++;
        } else if ($(this).children('td')[1].textContent === 'Kvinde' && $(this).children('td').children('input').is(':checked')){
            women++;
        }

        if ($(this).children('td').children('input').is(':checked')){
            participated++;
            notParticipated--;
        }
    });

    $('#participationValue').text(participated);
    $('#notParticipationValue').text(notParticipated);
    $('#participationPercentValue').text(((notParticipated/playerCount)*100).toFixed(2) + '%');

    $('#menValue').text(men);
    $('#menPercentValue').text(((men/participated)*100).toFixed(2) + '%');
    $('#womenValue').text(women);
    $('#womenPercentValue').text(((women/participated)*100).toFixed(2) + '%');

    var genderCtx = document.getElementById('genderChart').getContext("2d");
    genderChart = new Chart(genderCtx, {
        type: 'pie',
        data: {
            datasets: [{
                data: [men, women],
                backgroundColor: ["#5BC0EB", "#F55D3E"]
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Mænd',
                'Kvinder'
            ]
        },
        options: {

        }
    });

    var participationCtx = document.getElementById('participationChart').getContext("2d");
    participationChart = new Chart(participationCtx, {
        type: 'pie',
        data: {
            datasets: [{
                data: [participated, notParticipated],
                backgroundColor: ["#F19821", "#0B94A8"]
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Fremmødte',
                'Udeblevet'
            ]
        },
        options: {

        }
    });
}

function setModuleId(id) {
    $('#ChosenModuleId').text(id);
}

$('#btnAddComment').click(function () {
    var moduleCommentAdapter = {
        moduleId: $('#ChosenModuleId').text(),
        comment: $('#moduleCommentArea').val()
    };

    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: "/panel/træning/modul/addcomment",
        data: JSON.stringify(moduleCommentAdapter),
        success: function(){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert('Error: ' + thrownError);
        }
    });
});

function changeParticipationState(id) {
    var btn = $('#' + id);
    btn.attr('disabled', '');
    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: '/panel/træning/afkrydsning/changeparticipationstate',
        data: id,
        success: function(){
            //populateChart();
            updateGenderChart();
            updateParticipationChart();
            btn.removeAttr('disabled')
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(thrownError);
        }
    });
}

function fillFocusModal() {
    $('#inp_editFocusModal_title').val($('#txt_informationContent_focustitle').text());
    $('#inp_editFocusModal_description').val($('#txt_informationContent_focusdescription').text());
}

$('#btn_editFocusModal_savefocus').click(function () {
    var param = new URLSearchParams(window.location.search);

    console.log(param.get('id'));

    var focus = {
        "id": param.get('id'),
        "title": $('#inp_editFocusModal_title').val(),
        "description": $('#inp_editFocusModal_description').val()
    };


    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: '/panel/træning/focus/editfocus',
        data: JSON.stringify(focus),
        success: function(){
            window.location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(xhr.status);
        }
    });
});