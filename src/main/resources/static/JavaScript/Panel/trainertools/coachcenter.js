new Chart(document.getElementById("chart_stats_missingLast10"), {
    type: 'line',
    data: JSON.parse(getLastTenData()),
    options: {
        legend: { display: false },
        title: {
            display: true,
            text: 'Fraværds udvikling i %'
        },
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    beginAtZero: true,
                    steps: 10,
                    stepValue: 10,
                    max: 100
                }
            }]
        },
        responsive: true
    }
});

var genderData = JSON.parse(getGenderData());
new Chart(document.getElementById("chart_stats_gender"), {
        type: 'bar',
        data: {
            labels: genderData.dates,
            datasets: [
                {
                    label: 'Kvinder',
                    data: genderData.women,
                    backgroundColor: "#F55D3E",
                    hoverBackgroundColor: "#F55D3E",
                    hoverBorderWidth: 0
                },
                {
                    label: 'Mænd',
                    data: genderData.men,
                    backgroundColor: "#5BC0EB",
                    hoverBackgroundColor: "#5BC0EB",
                    hoverBorderWidth: 0
                }
            ]
        },
    options: {
        animation: {
            duration: 10
        },
        tooltips: {
            mode: 'label'
        },
        scales: {
            xAxes: [{
                stacked: true,
                gridLines: { display: false },
            }],
            yAxes: [{
                stacked: true
            }]
        },
        legend: {display: true}
    }
    }
);

getPointsData();

$('#tbl_myTeams_players').DataTable({
    searching: false,
    language: {
        "lengthMenu": "Vis _MENU_ spillere per side",
        "zeroRecords": "Ingen spillere fundet",
        "info": "Viser side _PAGE_ af _PAGES_",
        "infoEmpty": "Ingen spillere tilgængelige",
        "infoFiltered": "(Filtreret fra _MAX_ antal spillere)",
        "paginate": {
            "previous": "Tidligere side",
            "next": "Næste side"
        }
    }
});


function chooseTeam(id) {
    location.href = "trænercenter?team=" + id;
}

function getLastTenData() {
    var param = new URLSearchParams(window.location.search);

    var data = $.ajax({
        type: 'GET',
        url: "/panel/træningscenter/latest52?team=" + param.get('team'),
        async: false,
        error: function(xhr, ajaxOptions, thrownError){
            alert('Error: ' + thrownError);
        }
    });

    return data.responseText;
}

function getGenderData() {
    var param = new URLSearchParams(window.location.search);

    var data = $.ajax({
        type: 'GET',
        url: "/panel/træningscenter/gender?team=" + param.get('team'),
        async: false,
        error: function(xhr, ajaxOptions, thrownError){
            alert('Error: ' + thrownError);
        }
    });

    return data.responseText;
}

function getPointsData() {
    var param = new URLSearchParams(window.location.search);

    var data = $.ajax({
        type: 'GET',
        url: "/panel/træningscenter/points?team=" + param.get('team'),
        async: false,
        success: function(data){
            var pointsData = JSON.parse(data);
            if (!pointsData.hasOwnProperty("Error")){
                fillPointsData(pointsData);
            }
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert('Error: ' + thrownError);
        }
    });
}

function fillPointsData(pointsData){
    fill_menssingle(pointsData.mensingle);
    fill_mensdouble(pointsData.mendouble);
    fill_womensingle(pointsData.womensingle);
    fill_womendouble(pointsData.womendouble);
    fill_mixeddouble(pointsData.mixeddouble);
}
function fill_menssingle(data) {
    $('#lbl_stats_mensingles_highest_points').text(data.Highest_points);
    $('#lbl_stats_mensingles-highest-name').text(data.Highest_name);

    $('#lbl_stats_mensingles_lowest_points').text(data.Lowest_points);
    $('#lbl_stats_mensingles_lowest_name').text(data.Lowest_name);

    $('#lbl_stats_mensingles_difference').text(data.Difference);

    $('#lbl_stats_mensingles_average').text(data.Average);
}

function fill_mensdouble(data) {
    $('#lbl_stats_mendouble_highest_points').text(data.Highest_points);
    $('#lbl_stats_mendouble-highest-name').text(data.Highest_name);

    $('#lbl_stats_mendouble_lowest_points').text(data.Lowest_points);
    $('#lbl_stats_mendouble_lowest_name').text(data.Lowest_name);

    $('#lbl_stats_mendouble_difference').text(data.Difference);

    $('#lbl_stats_mendouble_average').text(data.Average);
}

function fill_womensingle(data) {
    $('#lbl_stats_womensingles_highest_points').text(data.Highest_points);
    $('#lbl_stats_womensingles-highest-name').text(data.Highest_name);

    $('#lbl_stats_womensingles_lowest_points').text(data.Lowest_points);
    $('#lbl_stats_womensingles_lowest_name').text(data.Lowest_name);

    $('#lbl_stats_womensingles_difference').text(data.Difference);

    $('#lbl_stats_womensingles_average').text(data.Average);
}

function fill_womendouble(data) {
    $('#lbl_stats_womendouble_highest_points').text(data.Highest_points);
    $('#lbl_stats_womendouble-highest-name').text(data.Highest_name);

    $('#lbl_stats_womendouble_lowest_points').text(data.Lowest_points);
    $('#lbl_stats_womendouble_lowest_name').text(data.Lowest_name);

    $('#lbl_stats_womendouble_difference').text(data.Difference);

    $('#lbl_stats_womendouble_average').text(data.Average);
}

function fill_mixeddouble(data) {
    $('#lbl_stats_mixeddouble_highest_points').text(data.Highest_points);
    $('#lbl_stats_mixeddouble-highest-name').text(data.Highest_name);

    $('#lbl_stats_mixeddouble_lowest_points').text(data.Lowest_points);
    $('#lbl_stats_mixeddouble_lowest_name').text(data.Lowest_name);

    $('#lbl_stats_mixeddouble_difference').text(data.Difference);

    $('#lbl_stats_mixeddouble_average').text(data.Average);
}

$('#tbl_invitePlayerModal_invitations').DataTable({
    searching: false,
    language: {
        "lengthMenu": "Vis _MENU_ invitationer per side",
        "zeroRecords": "Ingen invitationer fundet",
        "info": "Viser side _PAGE_ af _PAGES_",
        "infoEmpty": "Ingen invitationer tilgængelige",
        "infoFiltered": "(Filtreret fra _MAX_ antal invitationer)",
        "paginate": {
            "previous": "Tidligere side",
            "next": "Næste side"

        }
    }
});

$('#btn_invitePlayerModal_sendInv').click(function () {
    var param = new URLSearchParams(window.location.search);

    var newPlayerAdapter = {
        email: $('#inp_invitePlayerModal_email').val(),
        team: param.get('team')
    };

    setLoaderState($(this));


    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: '/panel/invitation/spiller',
        data: JSON.stringify(newPlayerAdapter),
        success: function(){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(xhr.status);
        }
    });
});

function cancleInvitation(id) {
    Swal({
        title: 'Er du sikker?',
        text: "Denne handling kan ikke fortrydes",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja, annuller invitation!',
        cancelButtonText: 'Luk'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: 'DELETE',
                contentType: "application/json",
                url: '/panel/invitation/spiller',
                data: id,
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(xhr.status);
                }
            });
        }
    })
}