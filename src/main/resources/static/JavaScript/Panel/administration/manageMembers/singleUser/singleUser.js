function deleteUser(id) {
    Swal({
        title: 'Er du sikker?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja, slet denne bruger',
        html:
        '<p>Dette medfører følgende: </p>' +
        '<p>&bull; <i>Brugeren forsvinder fra systemet</i></p>' +
        '<p>&bull; <i>Alt information om brugeren forsvinder</i></p>' +
        '<p>&bull; <i>En mail om dette sendes til angivede bruger-mail</i></p>',
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: 'DELETE',
                contentType: "application/json",
                url: '/panel/administrermedlemmer/individ',
                data: id,
                success: function(link){
                    location.href = '/panel/administrermedlemmer';
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        }
    })
}

$('#btn_editTeamInfoModal_editTeam').click(function () {
    var userId = (new URL(document.location)).searchParams.get('user');

    if (validateForm()){
        var data = {
            userId: userId,
            firstname: $('#inp_editUserModal_firstname').val(),
            lastname: $('#inp_editUserModal_lastname').val(),
            email: $('#inp_editUserModal_email').val(),
            phone: $('#inp_editUserModal_phone').val(),
            address: $('#inp_editUserModal_address').val(),
            birthday: $('#inp_editUserModal_birthday').val(),
            gender: $('#sel_editUserModal_gender option:selected').val()
        };

        setLoaderState($(this));

        $.ajax({
            type: 'POST',
            contentType: "application/json",
            url: '/panel/administrermedlemmer/individ',
            data: JSON.stringify(data),
            success: function(){
                location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(thrownError);
            }
        });
    }
});

function validateForm() {
    var validFirstname = validateRequired($('#inp_editUserModal_firstname'), $('#inp_editUserModal_firstname_validate'), 'Angiv et fornavn');
    var validLastname = validateRequired($('#inp_editUserModal_lastname'), $('#inp_editUserModal_lastname_validate'), 'Angiv et efternavn');
    var validEmail = validateEmail($('#inp_editUserModal_email'), $('#inp_editUserModal_email_validate'));
    var validPhone = validatePhone($('#inp_editUserModal_phone'), $('#inp_editUserModal_phone_validate'));
    var validAddress = validateRequired($('#inp_editUserModal_address'), $('#inp_editUserModal_address_validate'), 'Angiv en addresse');
    var validBirthday = validateRequired($('#inp_editUserModal_birthday'), $('#inp_editUserModal_birthday_validate'), 'Angiv en fødselsdag');

    return validFirstname && validLastname &&validEmail &&validPhone && validAddress && validBirthday;
}

function awardFreeContingent(id) {
    Swal({
        title: 'Er du sikker?',
        text: 'Er du sikker på, at dette er skal være et frikontingent?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja, fortsæt!',
        cancelButtonText: "Annuller"
    }).then(function (result) {
        if (result.value) {
            setLoaderState($('#btn_teams_missingpayment'));

            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: '/panel/administrermedlemmer/individ/free',
                data: id,
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        }
    })
}