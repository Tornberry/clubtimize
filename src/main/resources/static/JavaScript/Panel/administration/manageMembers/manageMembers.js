$(document).ready(function () {
    var table = $('#medlemmerTabel').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf'],
        language: {
            "search": "Søg",
            "zeroRecords": "Ingen medlemmer fundet",
            "info": "Viser side _PAGE_ af _PAGES_",
            "infoEmpty": "Ingen medlemmer tilgængelige",
            "infoFiltered": "(Filtreret fra _MAX_ antal medlemmer)",
            "paginate": {
                "previous": "Tidligere side",
                "next": "Næste side"
            }
        }
    });

    table.buttons().container()
        .appendTo('#medlemmerTabel_wrapper .col-md-6:eq(0)');
});

