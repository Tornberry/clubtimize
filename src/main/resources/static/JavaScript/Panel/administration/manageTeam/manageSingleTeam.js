$('#btn_editTeamInfoModal_editTeam').click(function () {
    if (validateForm()){
        var data = {
            title: $('#inp_editTeamInfoModal_title').val(),
            departmentId: $('#sel_editTeamInfoModal_department option:selected').val(),
            avalibleSpots: $('#inp_editTeamInfoModal_avalibleSpots').val(),
            isClosed: $('#inp_editTeamInfoModal_closed').is(':checked'),
            ageGroup: $('#inp_editTeamInfoModal_ageGroup').val(),
            niveau: $('#inp_editTeamInfoModal_niveau').val()
        };

        setLoaderState($(this));

        var hrefArray = location.href.split('/');
        var id = hrefArray[hrefArray.length - 1];

        $.ajax({
            type: 'POST',
            contentType: "application/json",
            url: '/panel/administrerhold/' + id + '/rediger',
            data: JSON.stringify(data),
            success: function(){
                location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(thrownError);
            }
        });
    }
});

function validateForm() {
    var validTitle = validateRequired($('#inp_editTeamInfoModal_title'), $('#inp_editTeamInfoModal_title_validate'), 'Angiv en titel');
    var validAvalibleSpots = validateRequired($('#inp_editTeamInfoModal_avalibleSpots'), $('#inp_editTeamInfoModal_avalibleSpots_validate'), 'Angiv ledige pladser');
    var validAgeGroup = validateRequired($('#inp_editTeamInfoModal_ageGroup'), $('#inp_editTeamInfoModal_ageGroup_validate'), 'Angiv aldersgruppe');
    var validNiveau = validateRequired($('#inp_editTeamInfoModal_niveau'), $('#inp_editTeamInfoModal_niveau_validate'), 'Angiv niveau');

    return validTitle && validAvalibleSpots && validAgeGroup && validNiveau;
}

$('#btn_newCoachModal_addCoach').click(function () {
    var pathArray = window.location.pathname.split('/');
    var teamId = pathArray[pathArray.length - 1];

    Swal({
        title: 'Er du sikker?',
        text: 'Er du sikker på, at dette medlem skal være træner på dette hold?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja, fortsæt!',
        cancelButtonText: "Annuller"
    }).then(function (result) {
        if (result.value) {
            setLoaderState($('#btn_singleTeam_addCoach'));

            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: '/panel/administrerhold/' + teamId + '/addCoach?userId=' + $('#sel_newCoachModal_users option:selected').val(),
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        }
    });
});

function removeCoach(id, btn) {
    var pathArray = window.location.pathname.split('/');
    var teamId = pathArray[pathArray.length - 1];

    Swal({
        title: 'Er du sikker?',
        text: 'Er du sikker på, at at vil fjerne denne træner fra holdet?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja, fortsæt!',
        cancelButtonText: "Annuller"
    }).then(function (result) {
        if (result.value) {
            setLoaderState($(btn));

            $.ajax({
                type: 'DELETE',
                contentType: "application/json",
                url: '/panel/administrerhold/' + teamId + '/addCoach?coachId=' + id,
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        }
    });
}

$('#btn_newPlayerModal_addPlayer').click(function () {
    var pathArray = window.location.pathname.split('/');
    var teamId = pathArray[pathArray.length - 1];

    Swal({
        title: 'Er du sikker?',
        text: 'Er du sikker på, at dette medlem skal være spiller på dette hold?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja, fortsæt!',
        cancelButtonText: "Annuller"
    }).then(function (result) {
        if (result.value) {
            setLoaderState($('#btn_singleTeam_addPlayer'));

            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: '/panel/administrerhold/' + teamId + '/addPlayer?userId=' + $('#sel_newPlayerModal_users option:selected').val(),
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        }
    });
});

function removePlayer(id, btn) {
    var pathArray = window.location.pathname.split('/');
    var teamId = pathArray[pathArray.length - 1];

    Swal({
        title: 'Er du sikker?',
        text: 'Er du sikker på, at at vil fjerne denne spiller fra holdet?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja, fortsæt!',
        cancelButtonText: "Annuller"
    }).then(function (result) {
        if (result.value) {
            setLoaderState($(btn));

            $.ajax({
                type: 'DELETE',
                contentType: "application/json",
                url: '/panel/administrerhold/' + teamId + '/addPlayer?playerId=' + id,
                success: function(){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        }
    });
}