$('#tbl_missingPayments_missingPayments').DataTable({
    language: {
        "lengthMenu": "Vis _MENU_ manglende betalinger per side",
        "search": "Søg",
        "zeroRecords": "Ingen manglende betalinger fundet",
        "info": "Viser side _PAGE_ af _PAGES_",
        "infoEmpty": "Ingen manglende betalinger tilgængelige",
        "infoFiltered": "(Filtreret fra _MAX_ antal manglende betalinger)",
        "paginate": {
            "previous": "Tidligere side",
            "next": "Næste side"
        }
    }
});

$('#tbl_confirmedPayments_confirmedPayments').DataTable({
    language: {
        "lengthMenu": "Vis _MENU_ gennemførte betalinger per side",
        "search": "Søg",
        "zeroRecords": "Ingen gennemførte betalinger fundet",
        "info": "Viser side _PAGE_ af _PAGES_",
        "infoEmpty": "Ingen gennemførte betalinger tilgængelige",
        "infoFiltered": "(Filtreret fra _MAX_ antal gennemførte betalinger)",
        "paginate": {
            "previous": "Tidligere side",
            "next": "Næste side"
        }
    }
});