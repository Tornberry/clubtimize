$(document).ready(function () {
    var clock =$('.clock').FlipClock({
        clockFace: 'DailyCounter',
        autoStart: true,
        countdown: true,
        language: 'da',
        callbacks: {
            stop: function() {
                $('.message').html('The clock has stopped!')
            }
        }
    });

    var time = parseInt($('#millis').text());

    if (time <= 0){
        clock.setTime(0);
    } else {
        clock.setTime(time);
        clock.start();
    }

});

// Edit description
tinymce.init({
    selector: "textarea#editor_eventCalendar_edit",
    theme: "modern",
    height:550,
    language: 'da',
    branding: false,
    fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
    plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        "save table contextmenu directionality emoticons template paste textcolor"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
    style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
});

$('#btn_singleEvent_editDescription').click(function () {
    tinyMCE.get('editor_eventCalendar_edit').setContent($('#lbl_singleEvent_description').html());
});

$('#btn_editDescriptionModal_edit').click(function () {
    var data = {
        eventId: (new URL(document.location)).searchParams.get('event'),
        description: tinyMCE.get('editor_eventCalendar_edit').getContent()
    };

    setLoaderState($(this));

    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: '/panel/event/description',
        data: JSON.stringify(data),
        success: function(){
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert(xhr.status);
            alert(thrownError);
        }
    });
});

function joinEvent(id, btn){
    Swal({
        title: 'Er du sikker?',
        text: "Er du sikker på at du ønsker at tilmelde dig dette event? Hvis du senere ønsker at afmelde dig, kan du ikke få dine penge retur, medmindre du har en aftale med forenignen",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja, fortsæt!'
    }).then(function (result) {
        if (result.value) {
            setLoaderState($(btn));

            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: '/panel/event/join',
                data: id,
                success: function(link){
                    if (link === 'OK'){
                        location.reload();
                    } else {
                        location.href = link;
                    }
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }
    });
}

function leaveEvent(id, btn){
    Swal({
        title: 'Er du sikker?',
        text: "Er du sikker på at du ønsker at afmelde dig dette event? Du kan ikke umiddelbart få dine penge retur, medmindre du har en aftale med forenignen",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja, fortsæt!'
    }).then(function (result) {
        if (result.value) {
            setLoaderState($(btn));

            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: '/panel/event/leave',
                data: id,
                success: function(link){
                    location.href = '/panel/tilmeldevents';
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }
    });
}