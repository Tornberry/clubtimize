var calendar =  $('#calendar').fullCalendar({
    locale: "da",
    header: {
        left: 'title',
        center: 'agendaDay,agendaWeek,month',
        right: 'prev,next today'
    },

    contentHeight: $(window).height()*0.83,

    firstDay: 1,
    selectable: true,
    defaultView: 'month',

    axisFormat: 'H:mm',

    week:{ titleFormat: "DD MMMM YYYY" },
    allDaySlot: false,
    selectHelper: true,
    droppable: false, // this allows things to be dropped onto the calendar !!!

    events: [],
    timeFormat: 'H:mm',
    eventClick: function(event) {
        document.location.href = '/panel/træning?id=' + event.id;
    }
});