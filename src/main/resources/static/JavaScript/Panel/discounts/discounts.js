$('#btn_newTwoTeamDiscountModal_add').click(function () {

    if(validateForm()){
        setLoaderState($('#btn_newTwoTeamDiscountModal_add'));

        data = {
            title: $('#inp_newTwoTeamDiscountModal_titel').val(),
            amount: parseFloat($('#inp_newTwoTeamDiscountModal_amount').val())
        };

        data["teams"] = getTeams();

        $.ajax({
            type: 'POST',
            url: "/panel/rabatter",
            contentType: "application/json",
            data: JSON.stringify(data),
            success: function(response){
                location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert('Error: ' + thrownError);
            }
        });
    }
});

function getTeams () {
    var returnValue = [];

    $('[data-repeater-item]').each(function (index) {
        returnValue.push(parseInt(document.getElementsByName("discounts[" + index + "][team]")[0].value));
    });

    return returnValue;
}

function validateForm() {
    var validTitel = validateRequired($('#inp_newTwoTeamDiscountModal_titel'), $('#inp_newTwoTeamDiscountModal_titel_validate'), 'Angiv en titel');
    var validAmount = validateRequired($('#inp_newTwoTeamDiscountModal_amount'), $('#inp_newTwoTeamDiscountModal_amount_validate'), 'Angiv et beløb');

    return validTitel && validAmount;
}

function deleteDiscount(id, btn) {
    Swal({
        title: 'Er du sikker?',
        text: 'Er du sikker på at du vil slette denne rabatgruppe?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Annuller',
        confirmButtonText: 'Ja, slet rabatgruppe'
    }).then(function (result) {
        if (result.value) {
            setLoaderState($(btn));

            $.ajax({
                type: 'DELETE',
                url: "/panel/rabatter",
                contentType: "application/json",
                data: id,
                success: function(response){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert('Error: ' + thrownError);
                }
            });
        }
    });


}