package dk.clubtimize.clubtimize.ClubtimizeAPI.general_information;

import com.fasterxml.jackson.databind.util.JSONPObject;
import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.controllers.support.EconomicSupport;
import dk.clubtimize.clubtimize.controllers.support.mail.MailSender;
import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.PaymentRequest;
import dk.clubtimize.clubtimize.models.economics.PaymentRequestComment;
import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.services.EconomicYearService;
import dk.clubtimize.clubtimize.services.InvoiceService;
import dk.clubtimize.clubtimize.services.PaymentRequestService;
import dk.clubtimize.clubtimize.services.UnionEconomicsService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;

@Controller
@RequestMapping("/API")
public class PaymentRequestAPI {

    private PaymentRequestService paymentRequestService;
    private EconomicYearService economicYearService;
    private InvoiceService invoiceService;
    private UnionEconomicsService unionEconomicsService;

    @Autowired
    public PaymentRequestAPI(PaymentRequestService paymentRequestService, EconomicYearService economicYearService, InvoiceService invoiceService, UnionEconomicsService unionEconomicsService) {
        this.paymentRequestService = paymentRequestService;
        this.economicYearService = economicYearService;
        this.invoiceService = invoiceService;
        this.unionEconomicsService = unionEconomicsService;
    }

    @CrossOrigin
    @GetMapping("/paymentrequests")
    @ResponseBody
    public String getOpenPaymentRequests(@RequestHeader(value = "authentication", defaultValue = "noauth") String authToken){
        if (authToken.equals("85TcvYRevzYcSeC2")){
            JSONArray jsonArray = new JSONArray();
            Iterable<PaymentRequest> paymentRequests = paymentRequestService.findByOpen(true);

            for (PaymentRequest paymentRequest : paymentRequests){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Id", paymentRequest.getId());
                jsonObject.put("Assosiated union", paymentRequest.getAssosiatedUnion().getName());
                jsonObject.put("Amount", paymentRequest.getAmountAtRequestTime());
                jsonObject.put("Date created", paymentRequest.getDate());
                jsonObject.put("Current status", paymentRequest.getStatus());
                jsonObject.put("Expected payout date", paymentRequest.getExpectedPayoutDate());

                JSONArray comments = new JSONArray();
                for (PaymentRequestComment comment : paymentRequest.getPaymentRequestComments()){
                    JSONObject commentObject = new JSONObject();
                    commentObject.put("Author", comment.getAuthorInitials());
                    commentObject.put("Date", comment.getDate());
                    commentObject.put("Comment", comment.getComment());
                    comments.put(commentObject);
                }
                jsonObject.put("Comments", comments);
                jsonArray.put(jsonObject);
            }

            return jsonArray.toString();
        } else {
            return "Forbidden";
        }
    }

    @CrossOrigin
    @PostMapping("/createcomment")
    @ResponseBody
    public String createComment(@RequestHeader(value = "authentication", defaultValue = "noauth") String authToken, @RequestBody String json){
        if (authToken.equals("85TcvYRevzYcSeC2")){
            JSONObject jsonObject = new JSONObject(json);
            PaymentRequest paymentRequest = paymentRequestService.findById(jsonObject.getInt("Id"));

            if (paymentRequest != null){
                paymentRequest.addComment(new PaymentRequestComment(paymentRequest, jsonObject.getString("Author"), DateFactory.getCurrentDateAndTime(), jsonObject.getString("Comment")));
                paymentRequestService.saveOrUpdate(paymentRequest);
            }

            return "OK";
        } else {
            return "Forbidden";
        }
    }

    @CrossOrigin
    @PostMapping("/updatepaymentrequest")
    @ResponseBody
    public String updatePaymentRequest(@RequestHeader(value = "authentication", defaultValue = "noauth") String authToken, @RequestBody String json) {
        if (authToken.equals("85TcvYRevzYcSeC2")){
            JSONObject jsonObject = new JSONObject(json);
            PaymentRequest paymentRequest = paymentRequestService.findById(jsonObject.getInt("Id"));
            paymentRequest.setExpectedPayoutDate(jsonObject.getString("Expected payout date"));
            paymentRequest.setOpen(jsonObject.getBoolean("Is open"));
            paymentRequest.setStatus(jsonObject.getString("Current status"));

            paymentRequestService.saveOrUpdate(paymentRequest);

            if (!paymentRequest.isOpen() && paymentRequest.getStatus().equals("Godkendt")){
                makePayoutInvoice(paymentRequest);
                MailSender.acceptPaymentRequest(paymentRequest.getAssosiatedUnion(), paymentRequest.getExpectedPayoutDate());
            } else if (!paymentRequest.isOpen() && paymentRequest.getStatus().equals("Afvist")){
                MailSender.denyPaymentRequest(paymentRequest.getAssosiatedUnion());
            }

            return "OK";
        } else {
            return "Forbidden";
        }
    }

    private void makePayoutInvoice(PaymentRequest paymentRequest) {
        EconomicYear economicYear = economicYearService.findByUnionEconomicsAndYear(paymentRequest.getAssosiatedUnion().getUnionEconomics(), DateFactory.getCurrentSeasonYear());

        Invoice invoice = EconomicSupport.makePayoutInvoice(paymentRequest, economicYear, DateFactory.getCurrentSeasonYear() + "-" + invoiceService.getNextInvoiceNumber(economicYear.getAssosiatedUnion().getName()));

        invoiceService.saveOrUpdate(invoice);
        economicYearService.saveOrUpdate(economicYear);

        UnionEconomics unionEconomics = economicYear.getUnionEconomics();
        unionEconomics.updateBalance();

        unionEconomicsService.saveOrUpdate(unionEconomics);
    }

    @CrossOrigin
    @GetMapping("/paymentrequest/{id}")
    @ResponseBody
    public String getPaymentRequestById(@PathVariable("id") int id){
        PaymentRequest paymentRequest = paymentRequestService.findById(id);

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("Union", paymentRequest.getAssosiatedUnion().getName());
        jsonObject.put("Amount", paymentRequest.getAmountAtRequestTime());
        jsonObject.put("Date", paymentRequest.getDate());

        JSONObject bankinfoObject = new JSONObject();
        bankinfoObject.put("Account", paymentRequest.getAssosiatedUnion().getUnionEconomics().getBankinfoContainer().getAccountNumber());
        bankinfoObject.put("Registration", paymentRequest.getAssosiatedUnion().getUnionEconomics().getBankinfoContainer().getRegistrationNumber());
        bankinfoObject.put("Contact", paymentRequest.getAssosiatedUnion().getUnionEconomics().getBankinfoContainer().getPhoneNumber());
        bankinfoObject.put("Bank", paymentRequest.getAssosiatedUnion().getUnionEconomics().getBankinfoContainer().getBankName());
        bankinfoObject.put("Verified", paymentRequest.getAssosiatedUnion().getUnionEconomics().getBankinfoContainer().isVerified());
        bankinfoObject.put("VerifiedBy", paymentRequest.getAssosiatedUnion().getUnionEconomics().getBankinfoContainer().getVerifiedBy());

        jsonObject.put("BankInfo", bankinfoObject);

        JSONArray commentArray = new JSONArray();
        for (PaymentRequestComment paymentRequestComment : paymentRequest.getPaymentRequestComments()){
            JSONObject commentObject = new JSONObject();

            commentObject.put("Author", paymentRequestComment.getAuthorInitials());
            commentObject.put("Date", paymentRequestComment.getDate());
            commentObject.put("Comment", paymentRequestComment.getComment());

            commentArray.put(commentObject);
        }

        jsonObject.put("Comments", commentArray);
        jsonObject.put("Open", paymentRequest.isOpen());
        jsonObject.put("Status", paymentRequest.getStatus());

        return jsonObject.toString();
    }
}
