package dk.clubtimize.clubtimize.ClubtimizeAPI.general_information;

import dk.clubtimize.clubtimize.models.economics.BankinfoContainer;
import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.services.BankinfoContainerService;
import dk.clubtimize.clubtimize.services.TeamService;
import dk.clubtimize.clubtimize.services.UnionEconomicsService;
import dk.clubtimize.clubtimize.services.UnionService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/API")
public class UnionDataAPI {

    private UnionService unionService;
    private UnionEconomicsService unionEconomicsService;
    private BankinfoContainerService bankinfoContainerService;
    private TeamService teamService;

    @Autowired
    public UnionDataAPI(UnionService unionService, UnionEconomicsService unionEconomicsService, BankinfoContainerService bankinfoContainerService, TeamService teamService) {
        this.unionService = unionService;
        this.unionEconomicsService = unionEconomicsService;
        this.bankinfoContainerService = bankinfoContainerService;
        this.teamService = teamService;
    }

    @CrossOrigin
    @GetMapping("/unions")
    @ResponseBody
    public Object getAllUnionsSimple(@RequestHeader(value = "authentication", defaultValue = "noauth") String authToken, @RequestParam("mode") String mode){
        if (authToken.equals("85TcvYRevzYcSeC2")){
            switch (mode) {
                case "simple":
                    return getUnionsSimple();
                case "detailed":
                    return getUnionsDetailed();
                default:
                    return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    private String getUnionsDetailed() {
        JSONArray allUnions = new JSONArray();

        for (Union union : unionService.findAll()){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Name", union.getName());
            jsonObject.put("URL", union.getUrl());
            jsonObject.put("Email", union.getAdminAccountEmail());
            jsonObject.put("Fee", union.getUnionEconomics().getFeePercent());
            jsonObject.put("Balance", union.getUnionEconomics().getBalance());
            jsonObject.put("Members", getAllUnionUsers(union).size());

            JSONArray teams = new JSONArray();
            for (Team team : union.getTeams()){
                JSONObject teamObject = new JSONObject();
                teamObject.put("Title", team.getTitle());
                teamObject.put("IsClosed", team.isClosed());
                teamObject.put("AvalibleSpots", team.getAvalibleSpots());
                teamObject.put("CoacheAmount", team.getCoaches().size());
                teamObject.put("PlayerAmount", team.getPlayers().size());
                teamObject.put("AwaitingCoachInvitations", team.getRecruitmentInvitations().size());
                teamObject.put("AgeGroup", team.getAgeGroup());
                teamObject.put("Niveau", team.getNiveau());
                teamObject.put("Price", team.getPrice());
                teams.put(teamObject);
            }

            jsonObject.put("Teams", teams);
            allUnions.put(jsonObject);
        }

        return allUnions.toString();
    }

    private String getUnionsSimple() {
        JSONArray allUnions = new JSONArray();

        for (Union union : unionService.findAll()){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Name", union.getName());
            jsonObject.put("URL", union.getUrl());
            jsonObject.put("Email", union.getAdminAccountEmail());
            jsonObject.put("Fee", union.getUnionEconomics().getFeePercent());
            jsonObject.put("Balance", union.getUnionEconomics().getBalance());
            jsonObject.put("Members", getAllUnionUsers(union).size());
            allUnions.put(jsonObject);
        }

        return allUnions.toString();
    }


    private List<User> getAllUnionUsers(Union union) {
        List<User> users = new ArrayList<>();
        union.getUnionAssosiations().forEach(unionAssosiation -> users.add(unionAssosiation.getUser()));
        return users;
    }

    @CrossOrigin
    @DeleteMapping("/union")
    @ResponseBody
    public String deleteUnion(@RequestHeader(value = "authentication", defaultValue = "noauth") String authToken, @RequestBody int id){
        if (authToken.equals("85TcvYRevzYcSeC2")){
            Union union = unionService.findById(id);

            System.out.println("deleted union " + union.getName());
//            unionService.deleteUnion(union);

            return "OK";
        } else {
            return "Forbidden";
        }
    }

    @CrossOrigin
    @PostMapping("/union")
    @ResponseBody
    public ResponseEntity updateUnion(@RequestHeader(value = "authentication", defaultValue = "noauth") String authToken, @RequestBody String json){
        if (authToken.equals("85TcvYRevzYcSeC2")){
            JSONObject jsonObject = new JSONObject(json);

            if (unionService.existsByName(jsonObject.getString("Name"))){
                Union union = unionService.findByName(jsonObject.getString("Name"));
                UnionEconomics unionEconomics = union.getUnionEconomics();
                unionEconomics.setFeePercent(jsonObject.getFloat("Fee"));

                unionEconomicsService.saveOrUpdate(unionEconomics);
                return new ResponseEntity(HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @PostMapping("/verifybankinfo")
    public String verifyBankInfo(@RequestHeader(value = "authentication", defaultValue = "noauth") String authToken, @RequestBody String json){
        if (authToken.equals("85TcvYRevzYcSeC2")){
            JSONObject jsonObject = new JSONObject(json);

            BankinfoContainer bankinfoContainer = unionService.findByName(jsonObject.getString("Union")).getUnionEconomics().getBankinfoContainer();
            bankinfoContainer.setVerified(true);
            bankinfoContainer.setVerifiedBy(jsonObject.getString("VerifiedBy"));

            bankinfoContainerService.saveOrUpdate(bankinfoContainer);

            return "OK";
        } else {
            return "Forbidden";
        }
    }

    @CrossOrigin
    @GetMapping("/getUnionDetailed")
    @ResponseBody
    public String getUnionDetailsDetailed(@RequestHeader(value = "authentication", defaultValue = "noauth") String authToken, @RequestParam("unionName") String unionName){
        if (authToken.equals("85TcvYRevzYcSeC2")){
            Union union = unionService.findByName(unionName);

            // General info
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("UnionName", union.getName());
            jsonObject.put("AdministratorEmail", union.getAdminAccountEmail());
            jsonObject.put("URL", union.getUrl());


            // Bank info container
            JSONObject bankinfo = new JSONObject();
            if ((union.getUnionEconomics().getBankinfoContainer() != null)) {
                bankinfo.put("AccountNumber", union.getUnionEconomics().getBankinfoContainer().getAccountNumber());
                bankinfo.put("RegistrationNumber", union.getUnionEconomics().getBankinfoContainer().getRegistrationNumber());
                bankinfo.put("BankName", union.getUnionEconomics().getBankinfoContainer().getBankName());
                bankinfo.put("ContactNumber", union.getUnionEconomics().getBankinfoContainer().getPhoneNumber());
                bankinfo.put("Verified", union.getUnionEconomics().getBankinfoContainer().isVerified());
                bankinfo.put("VerifiedBy", union.getUnionEconomics().getBankinfoContainer().getVerifiedBy());
            } else {
                bankinfo.put("AccountNumber", "No info given yet");
                bankinfo.put("RegistrationNumber", "No info given yet");
                bankinfo.put("BankName", "No info given yet");
                bankinfo.put("ContactNumber", "No info given yet");
                bankinfo.put("Verified", "Verification unnecessary");
                bankinfo.put("VerifiedBy", "Verification unnecessary");
            }

            jsonObject.put("Bankinfo", bankinfo);

            return jsonObject.toString();
        } else {
            return "Forbidden";
        }
    }

    @CrossOrigin
    @GetMapping("/totalBalance")
    @ResponseBody
    public String getTotalBalance(@RequestHeader(value = "authentication", defaultValue = "noauth") String authToken){
        if (authToken.equals("85TcvYRevzYcSeC2")){
            float balance = 0;
            for(Union union : unionService.findAll()){
                balance += union.getUnionEconomics().getBalance();
            }

            return String.valueOf(balance);
        } else {
            return "Forbidden";
        }
    }

    @CrossOrigin
    @DeleteMapping("/deleteUnion")
    @ResponseBody
    public String deleteUnion(@RequestHeader(value = "authentication", defaultValue = "noauth") String authToken, @RequestParam("URL") String URL){
        if (authToken.equals("85TcvYRevzYcSeC2")){
            unionService.deleteUnion(unionService.findByUrl(URL));
            return "OK";
        } else {
            return "Forbidden";
        }
    }
}
