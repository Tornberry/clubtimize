package dk.clubtimize.clubtimize.ClubtimizeAPI;

import dk.clubtimize.clubtimize.controllers.support.ClubtimizeSettings;
import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.ClubtimizeAPI.dinero.DineroClubtimizeAPI;
import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.invoice.DebtReport;
import dk.clubtimize.clubtimize.models.economics.invoice.FeeReport;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.economics.invoice.InvoiceItem;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.services.EconomicYearService;
import dk.clubtimize.clubtimize.services.InvoiceService;
import dk.clubtimize.clubtimize.services.UnionService;
import org.joda.time.LocalDate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/API/invoice/")
public class InvoiceAPI {

    private Logger logger = LoggerFactory.getLogger(InvoiceAPI.class);

    private InvoiceService invoiceService;
    private UnionService unionService;
    private EconomicYearService economicYearService;

    @Autowired
    public InvoiceAPI(InvoiceService invoiceService, UnionService unionService, EconomicYearService economicYearService) {
        this.invoiceService = invoiceService;
        this.unionService = unionService;
        this.economicYearService = economicYearService;
    }

    @GetMapping("/Invoice")
    @ResponseBody
    public String getAllInvoices(@RequestHeader(value = "authentication", defaultValue = "noauth") String authToken){
        if (authToken.equals("85TcvYRevzYcSeC2")){
            Iterable<Invoice> invoices = invoiceService.findAll();

            JSONArray jsonArray = new JSONArray();

            for (Invoice invoice : invoices){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("user", invoice.getUser().getEmail());
                jsonObject.put("union", invoice.getAssosiatedUnion().getName());
                JSONArray itemArray = new JSONArray();
                for (InvoiceItem invoiceItem : invoice.getItems()){
                    JSONObject item = new JSONObject();
                    item.put("quantity", invoiceItem.getQuantity());
                    item.put("item", invoiceItem.getItem());
                    item.put("description", invoiceItem.getDescription());
                    item.put("unit_cost", invoiceItem.getUnitCost());
                    item.put("fee", invoiceItem.getFee());
                    item.put("tax", invoiceItem.getTax());
                    item.put("total_cost", invoiceItem.getTotalCost());
                    itemArray.put(item);
                }
                jsonObject.put("items", itemArray);
                jsonObject.put("invoice_number", invoice.getInvoiceNumber());
                jsonObject.put("date", invoice.getDate());
                jsonObject.put("total_cost_with_additional", invoice.getTotalCostWithAdditional());
                jsonObject.put("total_cost_without_additional", invoice.getTotalCostWithoutAdditional());
                jsonArray.put(jsonObject);
            }

            return jsonArray.toString();
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN).toString();
        }
    }


    /**
     *  Call this rout to generate a monthly invoice for all unions. This should be called automatically in the beginning og every month
      * @return OK message if everything ok, forbidden if auth token not correct
     */
    @GetMapping("/monthlyinvoice")
    @ResponseBody
    public String makeMonthlyInvoices(@RequestHeader(value = "authentication", defaultValue = "noauth") String authToken){
        if (authToken.equals("85TcvYRevzYcSeC2")){
            List<Union> unions = (List<Union>) unionService.findAll();
            for (Union union : unions){
                logger.info("----------------------------------------");
                generateAndSaveMonthlyInvoice(union);
            }
            return new ResponseEntity(HttpStatus.OK).toString();
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN).toString();
        }

    }

    @GetMapping(value = "/downloadInvoice", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadReport(@RequestParam("guid") String guid) throws IOException {

        File file = DineroClubtimizeAPI.downloadReport(guid);

        return new FileSystemResource(file);
    }


    private void generateAndSaveMonthlyInvoice(Union union) {
        logger.info("Union: " + union.getName());
        logger.info("Is production enviroment: " + ClubtimizeSettings.isProductionEnvironment());

        // The current econimic year
        EconomicYear economicYear = economicYearService.findByUnionEconomicsAndYear(union.getUnionEconomics(), DateFactory.getCurrentSeasonYear());

        // Add feeInvoice items for each activity
        LocalDate today = LocalDate.now();
        LocalDate lastMonth = today.minusMonths(1).minusDays(1);
        logger.info("Day chosen: " + lastMonth.toString());

        // Fee invoice--------------------------------------------------------------------------------------------------
        Invoice feeInvoice = new Invoice();
        feeInvoice.setUnionAssosiatedName(union.getName());

        // Set the general feeInvoice information
        feeInvoice.setEconomicYear(economicYear);
        feeInvoice.setAddToBalance(false);
        feeInvoice.setInvoiceNumber(DateFactory.getCurrentSeasonYear() + "-" + invoiceService.getNextInvoiceNumber(union.getName()));
        feeInvoice.setType("Månedligt faktura");
        feeInvoice.setUnionAssosiatedName(union.getName());
        feeInvoice.setDate(DateFactory.getCurrentDate());

        for (Invoice unionInvoice : economicYear.getInvoices()){
            if (unionInvoice.isAddToBalance()){
                LocalDate invoiceDate = LocalDate.parse(unionInvoice.getDate(), org.joda.time.format.DateTimeFormat.forPattern("dd-MM-yyyy"));
                if (invoiceDate.isAfter(lastMonth) && invoiceDate.isBefore(lastMonth.plusMonths(1).plusDays(1))){
                    if (unionInvoice.getTotalCostWithoutAdditional() != unionInvoice.getTotalCostWithAdditional()){
                        switch(unionInvoice.getType()){
                            case "Kontingent":
                                InvoiceItem contingentItem = feeUserItem(unionInvoice, feeInvoice);
                                feeInvoice.addInvoiceItem(contingentItem);
                                break;
                            case "Event":
                                InvoiceItem eventItem = feeUserItem(unionInvoice, feeInvoice);
                                feeInvoice.addInvoiceItem(eventItem);
                                break;
                            case "Banebooking periode":
                                InvoiceItem bookingPeriodItem = feeBookingPeriodItem(unionInvoice, feeInvoice);
                                feeInvoice.addInvoiceItem(bookingPeriodItem);
                                break;
                            case "Banebooking":
                                InvoiceItem bookingItem = feeBookingItem(unionInvoice, feeInvoice);
                                feeInvoice.addInvoiceItem(bookingItem);
                                break;
                        }
                    }
                }
            }
        }

        // Debt invoice--------------------------------------------------------------------------------------------------

        Invoice debtInvoice = new Invoice();
        debtInvoice.setUnionAssosiatedName(union.getName());

        // Set the general feeInvoice information
        debtInvoice.setEconomicYear(economicYear);
        debtInvoice.setAddToBalance(false);
        debtInvoice.setInvoiceNumber(DateFactory.getCurrentSeasonYear() + "-" + invoiceService.getNextInvoiceNumber(union.getName()));
        debtInvoice.setType("Månedligt faktura");
        debtInvoice.setUnionAssosiatedName(union.getName());
        debtInvoice.setDate(DateFactory.getCurrentDate());

        for (Invoice unionInvoice : economicYear.getInvoices()){
            if (unionInvoice.isAddToBalance()){
                LocalDate invoiceDate = LocalDate.parse(unionInvoice.getDate(), org.joda.time.format.DateTimeFormat.forPattern("dd-MM-yyyy"));
                if (invoiceDate.isAfter(lastMonth) && invoiceDate.isBefore(lastMonth.plusMonths(1).plusDays(1))){
                    if (unionInvoice.getTotalCostWithoutAdditional() != unionInvoice.getTotalCostWithAdditional()){
                        switch(unionInvoice.getType()){
                            case "Kontingent":
                                InvoiceItem contingentItem = debtUserItem(unionInvoice, debtInvoice);
                                debtInvoice.addInvoiceItem(contingentItem);
                                break;
                            case "Event":
                                InvoiceItem eventItem = debtUserItem(unionInvoice, debtInvoice);
                                debtInvoice.addInvoiceItem(eventItem);
                                break;
                            case "Banebooking periode":
                                InvoiceItem bookingPeriodItem = debtBookingPeriodItem(unionInvoice, debtInvoice);
                                debtInvoice.addInvoiceItem(bookingPeriodItem);
                                break;
                            case "Banebooking":
                                InvoiceItem bookingItem = debtBookingItem(unionInvoice, debtInvoice);
                                debtInvoice.addInvoiceItem(bookingItem);
                                break;
                        }
                    }
                }
            }
        }

        // Fortsæt hvis faktura ikke er tom
        if (!feeInvoice.getItems().isEmpty()){

            // Udregn fakturas totalpris
            for (InvoiceItem invoiceItem : feeInvoice.getItems()){
                feeInvoice.setTotalCostWithAdditional(feeInvoice.getTotalCostWithAdditional() + invoiceItem.getTotalCost());
                feeInvoice.setTotalCostWithoutAdditional(feeInvoice.getTotalCostWithoutAdditional() + (invoiceItem.getTotalCost() - (invoiceItem.getFee() + invoiceItem.getTax())));
            }
            logger.info("Amount of items for fee report:" + feeInvoice.getItems().size());
            logger.info("Total amount for fee report: " + feeInvoice.getTotalCostWithAdditional());

            // Kun hvis dette er produktions miljø
            if (ClubtimizeSettings.isProductionEnvironment()){
                // Opret faktura
                String responseString = DineroClubtimizeAPI.createInvoice(feeInvoice, 1000, union.getGuid());
                logger.info("Created invoice with response: " + responseString);
                JSONObject responseJsonObject = new JSONObject(responseString);

                // Book faktura
                String bookingResponse = DineroClubtimizeAPI.bookInvoice(responseJsonObject.getString("Guid"), responseJsonObject.getString("TimeStamp"));
                logger.info("Booked invoice with return value: " + bookingResponse);

                // Tilføj gebyr rapport
                FeeReport feeReport = new FeeReport();
                feeReport.setEconomicYear(economicYear);
                feeReport.setGuid(responseJsonObject.getString("Guid"));
                feeReport.setMonth(DateFactory.numberToMonth(lastMonth.plusDays(1).getMonthOfYear()));
                feeReport.setAmount(feeInvoice.getTotalCostWithAdditional());

                economicYear.addFeeReport(feeReport);

                logger.info("Added fee report to union");
            } else {
                // Tilføj gebyr rapport
                FeeReport feeReport = new FeeReport();
                feeReport.setEconomicYear(economicYear);
                feeReport.setGuid("None");
                feeReport.setMonth(DateFactory.numberToMonth(lastMonth.plusDays(1).getMonthOfYear()));
                feeReport.setAmount(feeInvoice.getTotalCostWithAdditional());

                economicYear.addFeeReport(feeReport);
            }

            // Kun hvis dette er produktions miljø
            if (ClubtimizeSettings.isProductionEnvironment()){
                // Hvis ikke foreningen har en konto i Dinero, opret en
                if (union.getAccount() == 0){
                    int accountNumber = getNextAccountNumber();
                    DineroClubtimizeAPI.createAccount(union.getName(), accountNumber);
                    union.setAccount(accountNumber);
                    unionService.saveOrUpdate(union);
                    logger.info("Created new account for this union");
                }
            }
        }

        // Fortsæt hvis faktura ikke er tom
        if (!debtInvoice.getItems().isEmpty()){
            // Udregn fakturas totalpris
            for (InvoiceItem invoiceItem : debtInvoice.getItems()){
                debtInvoice.setTotalCostWithAdditional(debtInvoice.getTotalCostWithAdditional() + invoiceItem.getTotalCost());
                debtInvoice.setTotalCostWithoutAdditional(debtInvoice.getTotalCostWithoutAdditional() + (invoiceItem.getTotalCost() - (invoiceItem.getFee() + invoiceItem.getTax())));
            }
            logger.info("Amount of items for debt report:" + debtInvoice.getItems().size());
            logger.info("Total amount for debt report: " + debtInvoice.getTotalCostWithAdditional());

            // Kun hvis dette er produktions miljø
            if (ClubtimizeSettings.isProductionEnvironment()){
                // Opret ledger i Dinero
                String ledgerResponse = DineroClubtimizeAPI.addLedgerItem(debtInvoice, union.getAccount(), DateFactory.numberToMonth(lastMonth.getMonthOfYear()));
                logger.info("Created ledger item with response: " + ledgerResponse);
            }

            // Tilføj gælds rapport
            DebtReport debtReport = new DebtReport();
            debtReport.setEconomicYear(economicYear);
            debtReport.setMonth(DateFactory.numberToMonth(lastMonth.plusDays(1).getMonthOfYear()));
            debtReport.setAmount(debtInvoice.getTotalCostWithAdditional());

            economicYear.addDebtReport(debtReport);
            logger.info("Added debt report to union");
        }

        economicYearService.saveOrUpdate(economicYear);
    }

    private InvoiceItem debtBookingItem(Invoice unionInvoice, Invoice debtInvoice) {
        InvoiceItem invoiceItem = new InvoiceItem();
        invoiceItem.setInvoice(debtInvoice);
        invoiceItem.setItem("Gæld");
        invoiceItem.setTotalCost(unionInvoice.getTotalCostWithoutAdditional());
        invoiceItem.setTax(0);
        invoiceItem.setFee(0);
        invoiceItem.setQuantity(1);
        invoiceItem.setUnitCost(unionInvoice.getTotalCostWithoutAdditional());
        invoiceItem.setDescription("Gæld efter transaktion mellem klub og medlem, af typen " + unionInvoice.getType());

        return invoiceItem;
    }

    private InvoiceItem debtBookingPeriodItem(Invoice unionInvoice, Invoice debtInvoice) {
        InvoiceItem invoiceItem = new InvoiceItem();
        invoiceItem.setInvoice(debtInvoice);
        invoiceItem.setItem("Gæld");
        invoiceItem.setTotalCost(unionInvoice.getTotalCostWithoutAdditional());
        invoiceItem.setTax(0);
        invoiceItem.setFee(0);
        invoiceItem.setQuantity(1);
        invoiceItem.setUnitCost(unionInvoice.getTotalCostWithoutAdditional());
        invoiceItem.setDescription("Gæld efter transaktion mellem klub og medlem, af typen " + unionInvoice.getType());

        return invoiceItem;
    }

    private InvoiceItem debtUserItem(Invoice unionInvoice, Invoice debtInvoice) {
        InvoiceItem invoiceItem = new InvoiceItem();
        invoiceItem.setInvoice(debtInvoice);
        invoiceItem.setItem("Gæld");
        invoiceItem.setTotalCost(unionInvoice.getTotalCostWithoutAdditional());
        invoiceItem.setTax(0);
        invoiceItem.setFee(0);
        invoiceItem.setQuantity(1);
        invoiceItem.setUnitCost(unionInvoice.getTotalCostWithoutAdditional());
        invoiceItem.setDescription("Gæld efter transaktion mellem klub og medlem, af typen " + unionInvoice.getType());

        return invoiceItem;
    }

    private InvoiceItem feeBookingItem(Invoice unionInvoice, Invoice feeInvoice) {
        InvoiceItem invoiceItem = new InvoiceItem();
        invoiceItem.setInvoice(feeInvoice);
        invoiceItem.setItem("Gebyr");
        invoiceItem.setTotalCost(unionInvoice.getTotalCostWithAdditional() - unionInvoice.getTotalCostWithoutAdditional());
        invoiceItem.setTax(0);
        invoiceItem.setFee(0);
        invoiceItem.setQuantity(1);
        invoiceItem.setUnitCost(unionInvoice.getTotalCostWithAdditional() - unionInvoice.getTotalCostWithoutAdditional());
        invoiceItem.setDescription("Gæld efter transaktion mellem klub og medlem, af typen " + unionInvoice.getType());

        return invoiceItem;
    }

    private InvoiceItem feeBookingPeriodItem(Invoice unionInvoice, Invoice feeInvoice) {
        InvoiceItem invoiceItem = new InvoiceItem();
        invoiceItem.setInvoice(feeInvoice);
        invoiceItem.setItem("Gebyr");
        invoiceItem.setTotalCost(unionInvoice.getTotalCostWithAdditional() - unionInvoice.getTotalCostWithoutAdditional());
        invoiceItem.setTax(0);
        invoiceItem.setFee(0);
        invoiceItem.setQuantity(1);
        invoiceItem.setUnitCost(unionInvoice.getTotalCostWithAdditional() - unionInvoice.getTotalCostWithoutAdditional());
        invoiceItem.setDescription("Gæld efter transaktion mellem klub og medlem, af typen " + unionInvoice.getType());

        return invoiceItem;
    }

    private InvoiceItem feeUserItem(Invoice unionInvoice, Invoice feeInvoice) {
        InvoiceItem invoiceItem = new InvoiceItem();
        invoiceItem.setInvoice(feeInvoice);
        invoiceItem.setItem("Gebyr");
        invoiceItem.setTotalCost(unionInvoice.getTotalCostWithAdditional() - unionInvoice.getTotalCostWithoutAdditional());
        invoiceItem.setTax(0);
        invoiceItem.setFee(0);
        invoiceItem.setQuantity(1);
        invoiceItem.setUnitCost(unionInvoice.getTotalCostWithAdditional() - unionInvoice.getTotalCostWithoutAdditional());
        invoiceItem.setDescription("Gebyr efter transaktion mellem klub og medlem, af typen " + unionInvoice.getType());

        return invoiceItem;
    }

    private int getNextAccountNumber() {
        JSONArray jsonArray = new JSONArray(DineroClubtimizeAPI.getAccountList());

        boolean isFirstAccount = true;

        int largestAccount = 0;
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject item = jsonArray.getJSONObject(i);

            int accountNumber = item.getInt("AccountNumber");
            if (accountNumber >= 63200 && accountNumber <= 63999 && accountNumber > largestAccount ){
                largestAccount = accountNumber;
                isFirstAccount = false;
            }
        }

        if (isFirstAccount){
            return 63200;
        } else {
            return largestAccount + 1;
        }
    }
}
