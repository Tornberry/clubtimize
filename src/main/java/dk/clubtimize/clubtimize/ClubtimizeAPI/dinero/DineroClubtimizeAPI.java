package dk.clubtimize.clubtimize.ClubtimizeAPI.dinero;

import dk.clubtimize.clubtimize.controllers.support.ClubtimizeSettings;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.economics.invoice.InvoiceItem;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DineroClubtimizeAPI {

    // Constants
    private static final String apiKey = "716c43d694f9488caf23c1d5839d772f";
    private static final int firmId = 239486;


    // Dinero API calls ------------------------------------------------------------------------------------------------

    // Get the access token for all calls
    private static String getAccessToken(){
        HttpClient httpClient = HttpClientBuilder.create().build();

        String encoding = Base64.encodeBase64String("Clubtimize:nFrixeCZwyhs8UAfnzy71KWGkQZUxj8cRcbYMWOk".getBytes());

        try {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("username", apiKey));
            params.add(new BasicNameValuePair("password", apiKey));
            params.add(new BasicNameValuePair("grant_type", "password"));
            params.add(new BasicNameValuePair("scope", "read write"));

            HttpPost request = new HttpPost("https://authz.dinero.dk/dineroapi/oauth/token");
            request.addHeader("Authorization", "Basic " + encoding);
            request.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse response = httpClient.execute(request);

            return new JSONObject(EntityUtils.toString(response.getEntity())).getString("access_token");

        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    // Add a contact on the Clubtimize Dinero account
    public static String addSimpleContact(String name, String countryKey, boolean isPerson){
        HttpClient httpClient = HttpClientBuilder.create().build();

        try {
            HttpPost request = new HttpPost("https://api.dinero.dk/v1/" + firmId + "/contacts");
            request.addHeader("Authorization", "Bearer " + getAccessToken());
            request.addHeader("content-type", "application/json");
            StringEntity json = new StringEntity(convertDanishCharacters(getSimpleContactJSON(name, countryKey, isPerson)));
            request.setEntity(json);
            HttpResponse response = httpClient.execute(request);

            return EntityUtils.toString(response.getEntity());

        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    // Create an invoice
    public static String createInvoice(Invoice invoice, int accountNumber, String guid){
        HttpClient httpClient = HttpClientBuilder.create().build();

        try {
            HttpPost request = new HttpPost("https://api.dinero.dk/v1/" + firmId + "/invoices");
            request.addHeader("Authorization", "Bearer " + getAccessToken());
            request.addHeader("content-type", "application/json");
            StringEntity json = new StringEntity(convertDanishCharacters(getInvoiceJSON(invoice, accountNumber, guid)));
            request.setEntity(json);
            HttpResponse response = httpClient.execute(request);

            return EntityUtils.toString(response.getEntity());

        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    // Book an invoice
    public static String bookInvoice(String guid, String timeStamp){
        HttpClient httpClient = HttpClientBuilder.create().build();

        try {
            HttpPost request = new HttpPost("https://api.dinero.dk/v1/" + firmId + "/invoices/" + guid + "/book");
            request.addHeader("Authorization", "Bearer " + getAccessToken());
            request.addHeader("content-type", "application/json");
            StringEntity json = new StringEntity(convertDanishCharacters(getInvoiceBookingJSON(timeStamp)));
            request.setEntity(json);
            HttpResponse response = httpClient.execute(request);

            return EntityUtils.toString(response.getEntity());

        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    // Create an account on the Clubtimize Dinero account
    public static void createAccount(String name, int accountNumber){
        HttpClient httpClient = HttpClientBuilder.create().build();

        try {
            HttpPost request = new HttpPost("https://api.dinero.dk/v1/" + firmId + "/accounts/entry");
            request.addHeader("Authorization", "Bearer " + getAccessToken());
            request.addHeader("content-type", "application/json");
            StringEntity json = new StringEntity(convertDanishCharacters(getAccountJSON(name, accountNumber)));
            request.setEntity(json);
            HttpResponse response = httpClient.execute(request);

        }catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    // Get a list of all accounts for the Clubtimize account
    public static String getAccountList(){
        HttpClient httpClient = HttpClientBuilder.create().build();

        try {
            HttpGet request = new HttpGet("https://api.dinero.dk/v1/" + firmId + "/accounts/entry");
            request.addHeader("Authorization", "Bearer " + getAccessToken());
            request.addHeader("content-type", "application/json");
            HttpResponse response = httpClient.execute(request);

            return EntityUtils.toString(response.getEntity());

        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    // Add a ledger item for a specific month
    public static String addLedgerItem(Invoice invoice, int accountNumber, String month){
        HttpClient httpClient = HttpClientBuilder.create().build();

        try {
            HttpPost request = new HttpPost("https://api.dinero.dk/v1.1/" + firmId + "/ledgeritems");
            request.addHeader("Authorization", "Bearer " + getAccessToken());
            request.addHeader("content-type", "application/json");
            StringEntity json = new StringEntity(convertDanishCharacters(getLedgerItemJSON(invoice, accountNumber, month)));
            request.setEntity(json);
            HttpResponse response = httpClient.execute(request);

            return EntityUtils.toString(response.getEntity());

        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "Error";
        }
    }

    // Download a fee report from Dinero
    public static File downloadReport(String guid) throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();

        try {
            HttpGet request = new HttpGet("https://api.dinero.dk/v1/" + firmId + "/invoices/" + guid);
            request.addHeader("Authorization", "Bearer " + getAccessToken());
            request.addHeader("Accept", "application/octet-stream");
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            entity.writeTo(baos);

            File file = new File("faktura.pdf");

            FileUtils.writeByteArrayToFile(file, baos.toByteArray());

            return file;

        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    // JSON parsers ---------------------------------------------------------------------------------------------------

    // Get the invoice JSON
    private static String getInvoiceJSON(Invoice invoice, int accountNumber, String guid){
        JSONObject jsonObject = new JSONObject();

        // Get the date of the invoice in the right format
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd-MM-yyyy");
        LocalDate invoiceDate = dateTimeFormatter.parseLocalDate(invoice.getDate());

        jsonObject.put("Date", invoiceDate.toString("yyyy-MM-dd"));
        jsonObject.put("ShowLinesInclVat", true);
        jsonObject.put("ContactGuid", guid);


        //
        JSONArray jsonArray = new JSONArray();

        for (InvoiceItem invoiceItem : invoice.getItems()){
            JSONObject product = new JSONObject();
            product.put("BaseAmountValue", invoiceItem.getTotalCost());
            product.put("Description", invoiceItem.getDescription());
            product.put("Quantity", (float)invoiceItem.getQuantity());
            product.put("AccountNumber", accountNumber);
            product.put("Unit", "parts");

            jsonArray.put(product);
        }

        jsonObject.put("ProductLines", jsonArray);
        jsonObject.put("Address", JSONObject.NULL);

        return jsonObject.toString();
    }

    // Get simple contact JSON
    private static String getSimpleContactJSON(String name, String countryKey, boolean isPerson){
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("Name", name);
        jsonObject.put("CountryKey", countryKey);
        jsonObject.put("isPerson", isPerson);

        return jsonObject.toString();
    }

    // Get invoice booking JSON
    private static String getInvoiceBookingJSON(String timeStamp){
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("TimeStamp", timeStamp);

        return jsonObject.toString();
    }

    // Get account JSON
    private static String getAccountJSON(String name, int accountNumber) {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("Number", accountNumber);
        jsonObject.put("Name", name);
        jsonObject.put("VatCode", "none");

        return jsonObject.toString();
    }

    // Get ledger item JSON
    private static String getLedgerItemJSON(Invoice invoice, int accountNumber, String month) {
        JSONArray jsonArray = new JSONArray();

        JSONObject jsonObject = new JSONObject();

        DateTime dateTime = new DateTime();

        jsonObject.put("Id", JSONObject.NULL);
        jsonObject.put("VoucherNumber", 1);
        jsonObject.put("AccountNumber", accountNumber);
        jsonObject.put("Amount", -invoice.getTotalCostWithAdditional());
        jsonObject.put("BalancingAccountNumber", 55050);
        jsonObject.put("Description", "Gaeld for " + month + " for klub " + invoice.getUnionAssosiatedName());
        jsonObject.put("VoucherDate", dateTime.toString("yyyy-MM-dd"));

        jsonArray.put(jsonObject);

        return  jsonArray.toString();
    }

    // Convert danish charaters to global characters
    private static String convertDanishCharacters(String data){
        data = data.replace("æ", "ae");
        data = data.replace("Æ", "AE");
        data = data.replace("ø", "oe");
        data = data.replace("Ø", "OE");
        data = data.replace("å", "aa");
        data = data.replace("Å", "AA");

        return data;
    }
}
