package dk.clubtimize.clubtimize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClubtimizeApplication {

    public static void main(String[] args) {
       SpringApplication.run(ClubtimizeApplication.class, args);
    }
}
