package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.union.event.Event;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventRepository extends JpaRepository<Event, Integer> {
    Event findById(int id);
}
