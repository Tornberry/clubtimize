package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.union.Union;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {
    Invoice findByInvoiceNumber(String invoiceNumber);
    Invoice findByUnionAssosiatedNameAndInvoiceNumber(String unionName, String invoiceNumber);
    Iterable<Invoice> findByUnionAssosiatedName(String unionName);
    Iterable<Invoice> findByUnionAssosiatedNameAndDate(String unionName, String date);
    Invoice findById(int id);
}
