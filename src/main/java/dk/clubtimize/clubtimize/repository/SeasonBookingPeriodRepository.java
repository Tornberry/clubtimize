package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.opsætning.booking.SeasonBookingPeriod;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeasonBookingPeriodRepository extends JpaRepository<SeasonBookingPeriod, Integer> {
    SeasonBookingPeriod findById(int id);
}
