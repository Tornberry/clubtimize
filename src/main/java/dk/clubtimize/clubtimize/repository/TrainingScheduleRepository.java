package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.training.TrainingSchedule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrainingScheduleRepository extends JpaRepository<TrainingSchedule, Integer> {
}
