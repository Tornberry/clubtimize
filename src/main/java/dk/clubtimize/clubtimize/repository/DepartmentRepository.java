package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.opsætning.TeamDepartment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<TeamDepartment, Integer> {
    TeamDepartment findById(int id);
}
