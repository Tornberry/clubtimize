package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.training.Training;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrainingRepository extends JpaRepository<Training, Integer> {
    Training findById(int id);
}
