package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.union.matches.Match;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MatchRepository extends JpaRepository<Match, Integer> {
}
