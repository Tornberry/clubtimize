package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.user.RolePermissions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolePermissionsRepository extends JpaRepository<RolePermissions, Integer> {
}
