package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.opsætning.Hall;
import dk.clubtimize.clubtimize.models.union.Union;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HallRepository extends JpaRepository<Hall, Integer> {
    Hall findByHallId(int hallId);
    Iterable<Hall> findByUnion(Union union);
}
