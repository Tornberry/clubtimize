package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.union.event.Event;
import dk.clubtimize.clubtimize.models.union.event.EventInvitation;
import dk.clubtimize.clubtimize.models.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventInvitationRepository extends JpaRepository<EventInvitation, Integer> {
    EventInvitation findByEventAndUser(Event event, User user);
    EventInvitation findById(int id);
    boolean existsByEventAndUser(Event event, User user);
}
