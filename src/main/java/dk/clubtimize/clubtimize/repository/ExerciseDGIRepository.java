package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.opsætning.ExerciseDGI;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExerciseDGIRepository extends JpaRepository<ExerciseDGI, Integer> {
    ExerciseDGI findById(int id);
}
