package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.user.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByRoleName(String roleName);
    Role findByUnionAndRoleName(Union union, String roleName);
}
