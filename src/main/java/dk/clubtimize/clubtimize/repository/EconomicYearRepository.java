package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EconomicYearRepository extends JpaRepository<EconomicYear, Integer> {
    EconomicYear findByUnionEconomicsAndYear(UnionEconomics unionEconomics, String year);
}
