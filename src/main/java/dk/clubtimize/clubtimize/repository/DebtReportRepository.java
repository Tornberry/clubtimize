package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.economics.invoice.DebtReport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DebtReportRepository extends JpaRepository<DebtReport, Integer> {
    DebtReport findById(int id);
}
