package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.opsætning.booking.TimePeriod;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TimePeriodRepository extends JpaRepository<TimePeriod, Integer> {
    TimePeriod findById(int id);
}
