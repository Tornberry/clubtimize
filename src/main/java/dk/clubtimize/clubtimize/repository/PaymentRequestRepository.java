package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.economics.PaymentRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRequestRepository extends JpaRepository<PaymentRequest, Integer> {
    Iterable<PaymentRequest> findByOpen(boolean open);
    PaymentRequest findById(int id);
}
