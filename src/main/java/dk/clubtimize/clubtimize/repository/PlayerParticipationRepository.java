package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.training.PlayerParticipation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerParticipationRepository extends JpaRepository<PlayerParticipation, Integer> {
    PlayerParticipation findById(int id);
}
