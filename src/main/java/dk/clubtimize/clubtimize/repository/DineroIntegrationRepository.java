package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.economics.bookkeeping.accountingIntegrations.DineroIntegration;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DineroIntegrationRepository extends JpaRepository<DineroIntegration, Integer> {
}
