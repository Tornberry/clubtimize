package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.union.Coach;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoachRepository extends JpaRepository<Coach, Integer> {
    Coach findByCoachId(int id);
}
