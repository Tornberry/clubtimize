package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.user.UnionAssosiation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnionAssosiationReposetory extends JpaRepository<UnionAssosiation, Integer> {
    UnionAssosiation findById(int id);
}
