package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.opsætning.Court;
import dk.clubtimize.clubtimize.models.opsætning.booking.Booking;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<Booking, Integer> {
    Booking findById(int id);
    boolean existsByDateAndStartHourAndEndHourAndCourt(String date, String startHour, String endHour, Court court);
    Booking findByDateAndStartHourAndEndHourAndCourt(String date, String startHour, String endHour, Court court);
}
