package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.training.TrainingModule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrainingModuleRepository extends JpaRepository<TrainingModule, Integer> {

    TrainingModule findById(int id);

}
