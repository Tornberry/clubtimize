package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.union.RecruitmentInvitation;
import dk.clubtimize.clubtimize.models.union.Union;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecruitmentInvitationRepository extends JpaRepository<RecruitmentInvitation, Integer> {
    List<RecruitmentInvitation> findByUnion(Union union);
    RecruitmentInvitation findByInvNumber(int invNumber);
}
