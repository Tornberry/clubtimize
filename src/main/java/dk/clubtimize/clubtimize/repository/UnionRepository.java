package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.union.Union;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnionRepository extends JpaRepository<Union, Integer> {
    Union findByName(String name);
    Union findByUrl(String url);
    boolean existsByName(String name);
    boolean existsByUrl(String url);
    Union findById(int id);
}
