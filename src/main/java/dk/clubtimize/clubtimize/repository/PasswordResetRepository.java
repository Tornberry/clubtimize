package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.user.PasswordReset;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PasswordResetRepository extends JpaRepository<PasswordReset, Integer> {
    PasswordReset findByResetId(String id);
    PasswordReset findByEmail(String email);
    boolean existsByEmail(String email);
    boolean existsByResetId(String id);
}
