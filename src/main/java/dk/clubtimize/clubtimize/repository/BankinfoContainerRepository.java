package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.economics.BankinfoContainer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankinfoContainerRepository extends JpaRepository<BankinfoContainer,Integer> {
}
