package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.union.UnionSeason;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;
import dk.clubtimize.clubtimize.models.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserSeasonRepository extends JpaRepository<UserSeason, Integer> {
    UserSeason findByUnionSeasonAndUser(UnionSeason unionSeason, User user);
}
