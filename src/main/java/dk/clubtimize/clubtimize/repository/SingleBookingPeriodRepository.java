package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.opsætning.booking.SingleBookingPeriod;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SingleBookingPeriodRepository extends JpaRepository<SingleBookingPeriod, Integer> {
    SingleBookingPeriod findById(int id);
}
