package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.opsætning.Court;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourtRepository extends JpaRepository<Court, Integer> {
    Court findById(int id);
}
