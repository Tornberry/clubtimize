package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.user.FormerEmail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FormerEmailRepository extends JpaRepository<FormerEmail, Integer> {
}
