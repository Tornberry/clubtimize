package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.invoice.FeeReport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonthlyReportRepository extends JpaRepository<FeeReport, Integer> {
    Iterable<FeeReport> findByEconomicYear(EconomicYear economicYear);
}
