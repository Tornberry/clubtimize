package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.Union;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamRepository extends JpaRepository<Team, Integer> {
    Team findById(int id);
    Iterable<Team> findByUnion(Union union);
}
