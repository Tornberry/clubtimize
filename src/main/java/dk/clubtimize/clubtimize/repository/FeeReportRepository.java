package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.economics.invoice.FeeReport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeeReportRepository extends JpaRepository<FeeReport, Integer> {
    FeeReport findById(int id);
}
