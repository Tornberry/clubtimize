package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.UnionSeason;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnionSeasonRepository extends JpaRepository<UnionSeason, Integer> {
    UnionSeason findByUnionAndSeasonYear(Union union, String SeasonYear);
    Iterable<UnionSeason> findByUnion(Union union);
}
