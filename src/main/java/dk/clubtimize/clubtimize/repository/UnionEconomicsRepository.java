package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnionEconomicsRepository extends JpaRepository<UnionEconomics, Integer> {
}
