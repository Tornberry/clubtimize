package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByEmail(String email);
    User findByUserId(int id);
    boolean existsByEmail(String email);
    Iterable<User> findByUnion(Union union);
}
