package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.union.discount.discountTypes.TwoTeamDiscount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TwoTeamDiscountRepository extends JpaRepository<TwoTeamDiscount, Integer> {
    TwoTeamDiscount findById(int id);
}
