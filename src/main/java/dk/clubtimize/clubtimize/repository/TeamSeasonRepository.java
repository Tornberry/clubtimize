package dk.clubtimize.clubtimize.repository;

import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamSeasonRepository extends JpaRepository<TeamSeason, Integer> {
    boolean existsByPaymentCode(String paymentCode);
    TeamSeason findByPaymentCode(String paymentCode);
    TeamSeason findByUserSeasonAndTeam(UserSeason userSeason, Team team);
    TeamSeason findById(int id);
}
