package dk.clubtimize.clubtimize;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.services.UnionService;
import dk.clubtimize.clubtimize.services.UserService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.security.Principal;

@ControllerAdvice(basePackages = {"dk.clubtimize.clubtimize.controllers.panel"})
public class Global {

    private final String version = "1.0";
    private final int year = DateFactory.getCurrentYear();
    private UserService userService;
    private UnionService unionService;

    @Autowired
    public Global(UserService userService, UnionService unionService) {
        this.userService = userService;
        this.unionService = unionService;
    }

    @ModelAttribute("version")
    public String returnVersion(){
        return version;
    }

    @ModelAttribute("year")
    public int returnYear(){
        return year;
    }

    @ModelAttribute("currentUser")
    public User returnUser(Principal principal){
        return userService.findByUsername(principal.getName());
    }

    @ModelAttribute("currentUnion")
    public Union returnUnion(Principal principal){
        return unionService.findByName(userService.findByUsername(principal.getName()).getUnion().getName());
    }

    @ModelAttribute("currentUserIsCoach")
    public boolean isCoach(Principal principal){
        return !userService.findByUsername(principal.getName()).getUnionAssosiation().getCoachPositions().isEmpty();
    }

    public String getVersion() {
        return version;
    }
}
