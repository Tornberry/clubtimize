package dk.clubtimize.clubtimize.models.user;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.union.Union;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "role")
public class Role implements UnionValidator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "role_id")
    private int roleId;

    // the union the roleName is made by
    @ManyToOne
    @JoinColumn(name = "union_id")
    private Union union;

    private String roleName;

    // List of users with this role
    @OneToMany(mappedBy = "role")
    private List<User> users = new ArrayList<>();

    // Information about what users with this role can do
    @OneToOne(mappedBy = "role", cascade = CascadeType.ALL)
    private RolePermissions rolePermissions;

    public Role(String roleName) {
        this.roleName = roleName;
    }

    public Role() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return union;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public RolePermissions getRolePermissions() {
        return rolePermissions;
    }

    public void setRolePermissions(RolePermissions rolePermissions) {
        this.rolePermissions = rolePermissions;
    }

    public void addUser(User user){
        this.users.add(user);
    }

    public Union getUnion() {
        return union;
    }

    public void setUnion(Union union) {
        this.union = union;
    }
}
