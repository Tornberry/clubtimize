package dk.clubtimize.clubtimize.models.user;

import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.*;

@Entity
public class MatchStats {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne(mappedBy = "matchStats")
    private User user;

    @OneToOne(cascade = CascadeType.ALL)
    private EloPoints eloPoints;

    public MatchStats() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public EloPoints getEloPoints() {
        return eloPoints;
    }

    public void setEloPoints(EloPoints eloPoints) {
        this.eloPoints = eloPoints;
    }
}
