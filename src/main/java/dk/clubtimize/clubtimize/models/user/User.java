package dk.clubtimize.clubtimize.models.user;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.controllers.support.UnionSeasonStringService;
import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.event.Event;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.event.EventInvitation;
import dk.clubtimize.clubtimize.models.union.matches.matchtypes.*;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user")
public class User implements UnionValidator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private int userId;

// General information
// --------------------------------------------------------------------------------------------------------------------

    private String password;
    @Column(unique = true)
    private String email;
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<FormerEmail> formerEmails = new ArrayList<>();
    private String profilePicture;
    private String dateJoined;
    private String firstname;
    private String lastname;
    private String birthday;
    private int phone;
    private String gender;
    private String address;

// --------------------------------------------------------------------------------------------------------------------

    // For each season a user season is made
    // a user season stores information about if you payed for your teams or not
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<UserSeason> userSeasons = new ArrayList<>();

    // If user administrates a union or not
    private boolean isUnionAdministrator;

    @ManyToOne
    @JoinColumn(name = "union_id")
    private Union union;

    // Stores information about the different type of assosiations you have in a union
    // example: you have 1 coach position and plays for 2 teams
    @OneToOne(cascade = CascadeType.ALL)
    private UnionAssosiation unionAssosiation;

    // Invoices regarding this user
    @OneToMany(mappedBy = "user")
    private List<Invoice> invoices = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    private MatchStats matchStats;

    @ManyToMany(mappedBy = "player")
    private List<MensSingleTeam> mensSingleTeamsPlayer1 = new ArrayList<>();
    @ManyToMany(mappedBy = "player")
    private List<WomensSingleTeam> womensSingleTeamsPlayer1 = new ArrayList<>();
    @ManyToMany(mappedBy = "players")
    private List<MensDoubleTeam> mensDoubleTeamsPlayer1 = new ArrayList<>();
    @ManyToMany(mappedBy = "players")
    private List<WomensDoubleTeam> womensDoubleTeamsPlayer1 = new ArrayList<>();
    @ManyToMany(mappedBy = "players")
    private List<MixedDoubleTeam> mixedDoubleTeamsP1ayer1 = new ArrayList<>();

    @ManyToMany
    private List<Event> events = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    private List<EventInvitation> eventInvitations = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    public User(User user) {
        this.password = user.getPassword();
        this.email = user.getEmail();
        this.profilePicture = user.getProfilePicture();
        this.dateJoined = user.getDateJoined();
        this.role = user.getRole();
    }

    public User() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return union;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(String dateJoined) {
        this.dateJoined = dateJoined;
    }

    public boolean isUnionAdministrator() {
        return isUnionAdministrator;
    }

    public void setUnionAdministrator(boolean unionAdministrator) {
        isUnionAdministrator = unionAdministrator;
    }

    public UnionAssosiation getUnionAssosiation() {
        return unionAssosiation;
    }

    public void setUnionAssosiation(UnionAssosiation unionAssosiation) {
        this.unionAssosiation = unionAssosiation;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<UserSeason> getUserSeasons() {
        return userSeasons;
    }

    public void setUserSeasons(List<UserSeason> userSeasons) {
        this.userSeasons = userSeasons;
    }

    public void addUserSeason(UserSeason userSeason){
        this.userSeasons.add(userSeason);
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    public Union getUnion() {
        return union;
    }

    public void setUnion(Union union) {
        this.union = union;
    }

    public List<FormerEmail> getFormerEmails() {
        return formerEmails;
    }

    public void setFormerEmails(List<FormerEmail> formerEmails) {
        this.formerEmails = formerEmails;
    }

    public void addFormerEmail(FormerEmail formerEmail){
        this.formerEmails.add(formerEmail);
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public MatchStats getMatchStats() {
        return matchStats;
    }

    public void setMatchStats(MatchStats matchStats) {
        this.matchStats = matchStats;
    }

    public List<MensSingleTeam> getMensSingleTeamsPlayer1() {
        return mensSingleTeamsPlayer1;
    }

    public void setMensSingleTeamsPlayer1(List<MensSingleTeam> mensSingleTeamsPlayer1) {
        this.mensSingleTeamsPlayer1 = mensSingleTeamsPlayer1;
    }

    public List<WomensSingleTeam> getWomensSingleTeamsPlayer1() {
        return womensSingleTeamsPlayer1;
    }

    public void setWomensSingleTeamsPlayer1(List<WomensSingleTeam> womensSingleTeamsPlayer1) {
        this.womensSingleTeamsPlayer1 = womensSingleTeamsPlayer1;
    }

    public List<MensDoubleTeam> getMensDoubleTeamsPlayer1() {
        return mensDoubleTeamsPlayer1;
    }

    public void setMensDoubleTeamsPlayer1(List<MensDoubleTeam> mensDoubleTeamsPlayer1) {
        this.mensDoubleTeamsPlayer1 = mensDoubleTeamsPlayer1;
    }

    public List<WomensDoubleTeam> getWomensDoubleTeamsPlayer1() {
        return womensDoubleTeamsPlayer1;
    }

    public void setWomensDoubleTeamsPlayer1(List<WomensDoubleTeam> womensDoubleTeamsPlayer1) {
        this.womensDoubleTeamsPlayer1 = womensDoubleTeamsPlayer1;
    }

    public List<MixedDoubleTeam> getMixedDoubleTeamsP1ayer1() {
        return mixedDoubleTeamsP1ayer1;
    }

    public void setMixedDoubleTeamsP1ayer1(List<MixedDoubleTeam> mixedDoubleTeamsP1ayer1) {
        this.mixedDoubleTeamsP1ayer1 = mixedDoubleTeamsP1ayer1;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public void addEvent(Event event){
        this.events.add(event);
    }


    public List<EventInvitation> getEventInvitations() {
        return eventInvitations;
    }

    public void setEventInvitations(List<EventInvitation> eventInvitations) {
        this.eventInvitations = eventInvitations;
    }

    public void addEventInvitaion(EventInvitation eventInvitation){
        this.eventInvitations.add(eventInvitation);
    }

    public void removeEvent(Event event){
        this.events.remove(event);
    }

    public void removeEventInvitation(EventInvitation eventInvitation){
        this.eventInvitations.remove(eventInvitation);
    }

    public String getFullNameUpperCase(){
        return (this.firstname + " " + this.lastname).toUpperCase();
    }

    public boolean payedForCurrentSeason(Team team){
        for (UserSeason userSeason : this.userSeasons){
            if (userSeason.getUnionSeason().getSeasonYear().equals(DateFactory.getCurrentSeasonYear())){
                for (TeamSeason teamSeason : userSeason.getTeamSeasons()){
                    if (teamSeason.getTeam() == team){
                        return teamSeason.isPaymentSuccessful();
                    }
                }
            }
        }

        return false;
    }
}
