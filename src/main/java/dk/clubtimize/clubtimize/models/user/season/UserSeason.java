package dk.clubtimize.clubtimize.models.user.season;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.UnionSeason;
import dk.clubtimize.clubtimize.models.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class UserSeason implements UnionValidator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    // The season of the union, that this users season is connected to
    @ManyToOne
    @JoinColumn(name = "unionSeason_id")
    private UnionSeason unionSeason;

    // The season information for each specific team
    @OneToMany(mappedBy = "userSeason", cascade = CascadeType.ALL)
    private List<TeamSeason> teamSeasons = new ArrayList<>();

    public UserSeason() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return unionSeason.getUnion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UnionSeason getUnionSeason() {
        return unionSeason;
    }

    public void setUnionSeason(UnionSeason unionSeason) {
        this.unionSeason = unionSeason;
    }

    public List<TeamSeason> getTeamSeasons() {
        return teamSeasons;
    }

    public void setTeamSeasons(List<TeamSeason> teamSeasons) {
        this.teamSeasons = teamSeasons;
    }

    public void addTeamSeason(TeamSeason teamSeason){
        this.teamSeasons.add(teamSeason);
    }

    public void removeTeamSeason(TeamSeason teamSeason){
        this.teamSeasons.remove(teamSeason);
    }
}
