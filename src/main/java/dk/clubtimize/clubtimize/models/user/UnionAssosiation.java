package dk.clubtimize.clubtimize.models.user;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.union.Coach;
import dk.clubtimize.clubtimize.models.union.Personel;
import dk.clubtimize.clubtimize.models.union.Player;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class UnionAssosiation implements UnionValidator {

     @Id
     @GeneratedValue(strategy = GenerationType.AUTO)
     private int id;

     @OneToOne
     private User user;

     @ManyToOne
     @JoinColumn(name = "union_id")
     private Union union;

     @OneToMany(mappedBy = "unionAssosiation", cascade = CascadeType.ALL)
     private List<Coach> coachPositions = new ArrayList<>();
     @OneToMany(mappedBy = "unionAssosiation", cascade = CascadeType.ALL)
     private List<Personel> personelPositions = new ArrayList<>();
     @OneToMany(mappedBy = "unionAssosiation", cascade = CascadeType.ALL)
     private List<Player> playerPositions = new ArrayList<>();

     public UnionAssosiation() {
     }

     @Override
     public Union getAssosiatedUnion() {
          return union;
     }

     public int getId() {
          return id;
     }

     public void setId(int id) {
          this.id = id;
     }

     public User getUser() {
          return user;
     }

     public void setUser(User user) {
          this.user = user;
     }

     public Union getUnion() {
          return union;
     }

     public void setUnion(Union union) {
          this.union = union;
     }

     public List<Coach> getCoachPositions() {
          return coachPositions;
     }

     public void setCoachPositions(List<Coach> coachPositions) {
          this.coachPositions = coachPositions;
     }

     public List<Personel> getPersonelPositions() {
          return personelPositions;
     }

     public void setPersonelPositions(List<Personel> personelPositions) {
          this.personelPositions = personelPositions;
     }

     public List<Player> getPlayerPositions() {
          return playerPositions;
     }

     public void setPlayerPositions(List<Player> playerPositions) {
          this.playerPositions = playerPositions;
     }

     public void addPlayerPosition(Player player){
          this.playerPositions.add(player);
     }

     public void removePlayerPosition(Player player){
          this.playerPositions.remove(player);
     }

     public void addCoachPosition(Coach coach){
          this.coachPositions.add(coach);
     }

     public void removeCoachPosition(Coach coach){
          this.coachPositions.remove(coach);
     }
}
