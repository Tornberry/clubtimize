package dk.clubtimize.clubtimize.models.user;

import javax.persistence.*;

@Entity
public class FormerEmail {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    private String email;
    private String dateChanged;

    public FormerEmail() {
    }

    public FormerEmail(User user, String dateChanged, String email) {
        this.user = user;
        this.dateChanged = dateChanged;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDateChanged() {
        return dateChanged;
    }

    public void setDateChanged(String dateChanged) {
        this.dateChanged = dateChanged;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
