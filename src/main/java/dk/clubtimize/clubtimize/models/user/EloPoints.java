package dk.clubtimize.clubtimize.models.user;

import dk.clubtimize.clubtimize.controllers.support.EloEnum;

import javax.persistence.*;

@Entity
public class EloPoints {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne(mappedBy = "eloPoints")
    private MatchStats matchStats;

    private int mensSingleElo;
    private int womensSingleElo;
    private int mensDoubleElo;
    private int womensDoubleElo;
    private int mixedDoubleElo;

    public EloPoints() {
    }

    public EloPoints(int mensSingleElo, int womensSingleElo, int mensDoubleElo, int womensDoubleElo, int mixedDoubleElo) {
        this.mensSingleElo = mensSingleElo;
        this.womensSingleElo = womensSingleElo;
        this.mensDoubleElo = mensDoubleElo;
        this.womensDoubleElo = womensDoubleElo;
        this.mixedDoubleElo = mixedDoubleElo;
    }

    public void setEloPoints(int points, EloEnum category){
        switch (category){
            case mensSingle:
                this.mensSingleElo = points;
            case womensSingle:
                this.womensSingleElo = points;
            case mensDouble:
                this.mensDoubleElo = points;
            case womensDouble:
                this.womensDoubleElo = points;
            case mixedDouble:
                this.mixedDoubleElo = points;
        }

    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMensSingleElo(int mensSingleElo) {
        this.mensSingleElo = mensSingleElo;
    }

    public void setWomensSingleElo(int womensSingleElo) {
        this.womensSingleElo = womensSingleElo;
    }

    public void setMensDoubleElo(int mensDoubleElo) {
        this.mensDoubleElo = mensDoubleElo;
    }

    public void setWomensDoubleElo(int womensDoubleElo) {
        this.womensDoubleElo = womensDoubleElo;
    }

    public void setMixedDoubleElo(int mixedDoubleElo) {
        this.mixedDoubleElo = mixedDoubleElo;
    }

    public int getMensSingleElo() {
        return mensSingleElo;
    }

    public int getWomensSingleElo() {
        return womensSingleElo;
    }

    public int getMensDoubleElo() {
        return mensDoubleElo;
    }

    public int getWomensDoubleElo() {
        return womensDoubleElo;
    }

    public int getMixedDoubleElo() {
        return mixedDoubleElo;
    }

    public MatchStats getMatchStats() {
        return matchStats;
    }

    public void setMatchStats(MatchStats matchStats) {
        this.matchStats = matchStats;
    }
}
