package dk.clubtimize.clubtimize.models.user;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.union.Union;

import javax.persistence.*;

@Entity
public class RolePermissions implements UnionValidator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne
    @JoinColumn(name = "role_id")
    private Role role;

    // Administration
    private boolean canAdministrate;
    private boolean canSeeAdministrationPages;

    // Training overview
    private boolean canSeeAllTeams;

    // Training
    private boolean canUseTrainerFeatures;

    public RolePermissions() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return role.getUnion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isCanAdministrate() {
        return canAdministrate;
    }

    public void setCanAdministrate(boolean canAdministrate) {
        this.canAdministrate = canAdministrate;
    }

    public boolean isCanSeeAdministrationPages() {
        return canSeeAdministrationPages;
    }

    public void setCanSeeAdministrationPages(boolean canSeeAdministrationPages) {
        this.canSeeAdministrationPages = canSeeAdministrationPages;
    }

    public boolean isCanSeeAllTeams() {
        return canSeeAllTeams;
    }

    public void setCanSeeAllTeams(boolean canSeeAllTeams) {
        this.canSeeAllTeams = canSeeAllTeams;
    }

    public boolean isCanUseTrainerFeatures() {
        return canUseTrainerFeatures;
    }

    public void setCanUseTrainerFeatures(boolean canUseTrainerFeatures) {
        this.canUseTrainerFeatures = canUseTrainerFeatures;
    }
}
