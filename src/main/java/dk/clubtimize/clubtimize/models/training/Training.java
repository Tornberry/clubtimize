package dk.clubtimize.clubtimize.models.training;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.opsætning.Hall;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.matches.Match;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Training implements UnionValidator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;

    @OneToOne(mappedBy = "training", cascade = CascadeType.ALL)
    private Focus focus;

    @ManyToOne
    @JoinColumn(name = "hall_id")
    private Hall hall;

    private String date;
    private String timeFrom;
    private String timeTo;

    @OneToOne(mappedBy = "training", cascade = CascadeType.ALL)
    private Trainingsplan trainingsplan;

    @OneToMany(mappedBy = "training", cascade = CascadeType.ALL)
    private List<PlayerParticipation> playerParticipations = new ArrayList<>();

    @OneToMany(mappedBy = "training")
    private List<Match> matches = new ArrayList<>();

    public Training() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return team.getUnion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Focus getFocus() {
        return focus;
    }

    public void setFocus(Focus focus) {
        this.focus = focus;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Trainingsplan getTrainingsplan() {
        return trainingsplan;
    }

    public void setTrainingsplan(Trainingsplan trainingsplan) {
        this.trainingsplan = trainingsplan;
    }

    public List<PlayerParticipation> getPlayerParticipations() {
        return playerParticipations;
    }

    public void setPlayerParticipations(List<PlayerParticipation> playerParticipations) {
        this.playerParticipations = playerParticipations;
    }

    public void addPlayerParticipation(PlayerParticipation playerParticipation){
        this.playerParticipations.add(playerParticipation);
    }

    public void addMatch(Match match){
        this.matches.add(match);
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

}
