package dk.clubtimize.clubtimize.models.training;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.union.Player;
import dk.clubtimize.clubtimize.models.union.Union;

import javax.persistence.*;

@Entity
public class PlayerParticipation implements UnionValidator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private boolean participated = false;

    @ManyToOne
    @JoinColumn(name = "training_id")
    private Training training;

    @ManyToOne
    @JoinColumn(name = "player_id")
    private Player player;

    public PlayerParticipation() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return training.getTeam().getUnion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Training getTraining() {
        return training;
    }

    public void setTraining(Training training) {
        this.training = training;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public boolean isParticipated() {
        return participated;
    }

    public void setParticipated(boolean participated) {
        this.participated = participated;
    }
}
