package dk.clubtimize.clubtimize.models.training;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.union.Union;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Trainingsplan implements UnionValidator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne
    @JoinColumn(name = "training_id")
    private Training training;
    @OneToMany(mappedBy = "trainingsplan", cascade = CascadeType.ALL)
    private List<TrainingModule> trainingModules = new ArrayList<>();

    public Trainingsplan() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return training.getTeam().getUnion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<TrainingModule> getTrainingModules() {
        return trainingModules;
    }

    public void setTrainingModules(List<TrainingModule> trainingModules) {
        this.trainingModules = trainingModules;
    }

    public Training getTraining() {
        return training;
    }

    public void setTraining(Training training) {
        this.training = training;
    }

    public void addModule(TrainingModule module){
        this.trainingModules.add(module);
    }
}
