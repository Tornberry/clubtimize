package dk.clubtimize.clubtimize.models.training;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.opsætning.ExerciseDGI;
import dk.clubtimize.clubtimize.models.union.Union;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TrainingModule implements UnionValidator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String title;
    private String description;
    private int approxTime;

    @ManyToOne
    @JoinColumn(name = "trainingsplan_id")
    private Trainingsplan trainingsplan;

    @ManyToOne
    @JoinColumn(name = "exercise_id")
    private ExerciseDGI exerciseDGI;

    @OneToMany(mappedBy = "trainingModule", cascade = CascadeType.ALL)
    private List<ModuleComment> moduleComments = new ArrayList<>();

    public TrainingModule() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return trainingsplan.getTraining().getTeam().getUnion();
    }

    public TrainingModule(String title, String description, int approxTime, Trainingsplan trainingsplan) {
        this.title = title;
        this.description = description;
        this.approxTime = approxTime;
        this.trainingsplan = trainingsplan;
    }

    public TrainingModule(String title, String description, int approxTime, Trainingsplan trainingsplan, ExerciseDGI exerciseDGI) {
        this.title = title;
        this.description = description;
        this.approxTime = approxTime;
        this.trainingsplan = trainingsplan;
        this.exerciseDGI = exerciseDGI;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getApproxTime() {
        return approxTime;
    }

    public void setApproxTime(int approxTime) {
        this.approxTime = approxTime;
    }

    public Trainingsplan getTrainingsplan() {
        return trainingsplan;
    }

    public void setTrainingsplan(Trainingsplan trainingsplan) {
        this.trainingsplan = trainingsplan;
    }

    public ExerciseDGI getExerciseDGI() {
        return exerciseDGI;
    }

    public void setExerciseDGI(ExerciseDGI exerciseDGI) {
        this.exerciseDGI = exerciseDGI;
    }

    public void addComment(ModuleComment moduleComment){
        this.moduleComments.add(moduleComment);
    }

    public List<ModuleComment> getModuleComments() {
        return moduleComments;
    }

    public void setModuleComments(List<ModuleComment> moduleComments) {
        this.moduleComments = moduleComments;
    }
}
