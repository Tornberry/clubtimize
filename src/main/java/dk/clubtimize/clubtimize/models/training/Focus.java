package dk.clubtimize.clubtimize.models.training;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.training.Training;
import dk.clubtimize.clubtimize.models.union.Union;

import javax.persistence.*;

@Entity
public class Focus implements UnionValidator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String title = "Ingen title sat";
    @Column(length = 1000)
    private String description = "Ingen beskrivelse";

    @OneToOne
    @JoinColumn(name = "training_id")
    private Training training;

    public Focus() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return training.getTeam().getUnion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Training getTraining() {
        return training;
    }

    public void setTraining(Training training) {
        this.training = training;
    }
}
