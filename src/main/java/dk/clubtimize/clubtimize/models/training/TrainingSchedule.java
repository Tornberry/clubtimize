package dk.clubtimize.clubtimize.models.training;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.opsætning.Hall;
import dk.clubtimize.clubtimize.models.union.Union;

import javax.persistence.*;

@Entity
public class TrainingSchedule implements UnionValidator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int trainingScheduleId;
    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;
    private String weekday;
    private String timeFrom;
    private String timeTo;

    @ManyToOne
    @JoinColumn(name = "halllocation_id")
    private Hall hall;

    public TrainingSchedule() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return team.getUnion();
    }

    public int getTrainingScheduleId() {
        return trainingScheduleId;
    }

    public void setTrainingScheduleId(int trainingScheduleId) {
        this.trainingScheduleId = trainingScheduleId;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public String getWeekday() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }
}


