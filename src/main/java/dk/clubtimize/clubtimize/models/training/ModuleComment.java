package dk.clubtimize.clubtimize.models.training;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.union.Union;

import javax.persistence.*;

@Entity
public class ModuleComment implements UnionValidator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String author;
    private String date;
    @Column(length = 1000)
    private String comment;

    @ManyToOne
    @JoinColumn(name = "trainingModule_id")
    private TrainingModule trainingModule;

    public ModuleComment() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return trainingModule.getTrainingsplan().getTraining().getTeam().getUnion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public TrainingModule getTrainingModule() {
        return trainingModule;
    }

    public void setTrainingModule(TrainingModule trainingModule) {
        this.trainingModule = trainingModule;
    }
}
