package dk.clubtimize.clubtimize.models.union;

import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import dk.clubtimize.clubtimize.models.opsætning.Hall;
import dk.clubtimize.clubtimize.models.opsætning.TeamDepartment;
import dk.clubtimize.clubtimize.models.union.discount.Discount;
import dk.clubtimize.clubtimize.models.union.event.Event;
import dk.clubtimize.clubtimize.models.union.matches.Match;
import dk.clubtimize.clubtimize.models.user.Role;
import dk.clubtimize.clubtimize.models.user.UnionAssosiation;
import dk.clubtimize.clubtimize.models.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "BadmintonUnion")
public class Union {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
// General information
// --------------------------------------------------------------------------------------------------------------------

    // The name of the union
    // example: GUG badminton
    @Column(unique = true)
    private String name;
    // The url, which to go to for members to sign up
    @Column(unique = true)
    private String url;
    // The day the union was created
    private String dateJoined;
    // If the union is premium
    private boolean isPremium;

// Fields regarding people related to the union
// --------------------------------------------------------------------------------------------------------------------

    @OneToMany(mappedBy = "union", cascade = CascadeType.ALL)
    private List<User> users = new ArrayList<>();

    // The union's coaches
    @OneToMany(mappedBy = "union", cascade = CascadeType.ALL)
    private List<Coach> coaches = new ArrayList<>();
    // The union's personel
    @OneToMany(mappedBy = "union", cascade = CascadeType.ALL)
    private List<Personel> personel = new ArrayList<>();
    // The union's players/members
    @OneToMany(mappedBy = "union", cascade = CascadeType.ALL)
    private List<Player> players = new ArrayList<>();

    // Used to see all the users assosiated with the union
    @OneToMany(mappedBy = "union", cascade = CascadeType.ALL)
    private List<UnionAssosiation> unionAssosiations = new ArrayList<>();

// Invitations to people for different positions
// --------------------------------------------------------------------------------------------------------------------

    @OneToMany(mappedBy = "union", cascade = CascadeType.ALL)
    private List<RecruitmentInvitation> recruitmentInvitations = new ArrayList<>();

// Stuff related to events
// --------------------------------------------------------------------------------------------------------------------

    @OneToMany(mappedBy = "union", cascade = CascadeType.ALL)
    private List<Event> events = new ArrayList<>();

// Things assosiated with the union - typically stuff setup by the administrator
// --------------------------------------------------------------------------------------------------------------------

    // The teams of the union
    @OneToMany(mappedBy = "union", cascade = CascadeType.ALL)
    private List<Team> teams = new ArrayList<>();

    // Halls of the union
    @OneToMany(mappedBy = "union", cascade = CascadeType.ALL)
    private List<Hall> halls = new ArrayList<>();

    // Departments of the union
    @OneToMany(mappedBy = "union", cascade = CascadeType.ALL)
    private List<TeamDepartment> departments = new ArrayList<>();

    @OneToMany(mappedBy = "union", cascade = CascadeType.ALL)
    private List<Match> matches = new ArrayList<>();

// Season related stuff
// --------------------------------------------------------------------------------------------------------------------

    // The seasons of the union
    @OneToMany(mappedBy = "union", cascade = CascadeType.ALL)
    private List<UnionSeason> unionSeasons = new ArrayList<>();

    // The month which a new season begins and the old ends
    private String seasonSwitchMonth;

// Stuff related to economics
// --------------------------------------------------------------------------------------------------------------------

    @OneToOne(mappedBy = "union", cascade = CascadeType.ALL)
    private UnionEconomics unionEconomics;

    // The Dinero id of the union
    @Column(unique = true)
    private String guid;

    // Account assigned in Clubtimize Dinero
    private int account;

    @OneToOne(mappedBy = "union", cascade = CascadeType.ALL)
    private Discount discount;

// Stuff related to rolls and permissions
// --------------------------------------------------------------------------------------------------------------------

    private String adminAccountEmail;

    private String logoPath = "/unionlogo/default.png";

    // The roles registered by the union
    @OneToMany(mappedBy = "union", cascade = CascadeType.ALL)
    private List<Role> roles = new ArrayList<>();

    public Union() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Coach> getCoaches() {
        return coaches;
    }

    public void setCoaches(List<Coach> coaches) {
        this.coaches = coaches;
    }

    public List<Personel> getPersonel() {
        return personel;
    }

    public void setPersonel(List<Personel> personel) {
        this.personel = personel;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public List<Hall> getHalls() {
        return halls;
    }

    public void setHalls(List<Hall> halls) {
        this.halls = halls;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<UnionAssosiation> getUnionAssosiations() {
        return unionAssosiations;
    }

    public void setUnionAssosiations(List<UnionAssosiation> unionAssosiations) {
        this.unionAssosiations = unionAssosiations;
    }

    public void addTeam(Team team){
        this.teams.add(team);
    }

    public void addHall(Hall hall){
        this.halls.add(hall);
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public void addRole(Role role){
        this.roles.add(role);
    }

    public List<RecruitmentInvitation> getRecruitmentInvitations() {
        return recruitmentInvitations;
    }

    public void setRecruitmentInvitations(List<RecruitmentInvitation> recruitmentInvitations) {
        this.recruitmentInvitations = recruitmentInvitations;
    }

    public void addRecruitmentInvitation(RecruitmentInvitation recruitmentInvitation){
        this.recruitmentInvitations.add(recruitmentInvitation);
    }

    public void addCoach(Coach coach){
        this.coaches.add(coach);
    }

    public void removeRecruitmentInvitation(RecruitmentInvitation recruitmentInvitation){
        this.recruitmentInvitations.remove(recruitmentInvitation);
    }

    public List<UnionSeason> getUnionSeasons() {
        return unionSeasons;
    }

    public void setUnionSeasons(List<UnionSeason> unionSeasons) {
        this.unionSeasons = unionSeasons;
    }

    public String getSeasonSwitchMonth() {
        return seasonSwitchMonth;
    }

    public void setSeasonSwitchMonth(String seasonSwitchMonth) {
        this.seasonSwitchMonth = seasonSwitchMonth;
    }

    public void addUnionSeason(UnionSeason unionSeason){
        this.unionSeasons.add(unionSeason);
    }

    public UnionEconomics getUnionEconomics() {
        return unionEconomics;
    }

    public void setUnionEconomics(UnionEconomics unionEconomics) {
        this.unionEconomics = unionEconomics;
    }


    public List<TeamDepartment> getDepartments() {
        return departments;
    }

    public void setDepartments(List<TeamDepartment> departments) {
        this.departments = departments;
    }

    public void addTeamDepartment(TeamDepartment teamDepartment){
        this.departments.add(teamDepartment);
    }

    public String getAdminAccountEmail() {
        return adminAccountEmail;
    }

    public void setAdminAccountEmail(String adminAccountEmail) {
        this.adminAccountEmail = adminAccountEmail;
    }

    public void addUser(User user){
        this.users.add(user);
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void addPlayer(Player player){
        this.players.add(player);
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public int getAccount() {
        return account;
    }

    public void setAccount(int account) {
        this.account = account;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    public void addMatch(Match match){
        this.matches.add(match);
    }

    public boolean isPremium() {
        return isPremium;
    }

    public void setPremium(boolean premium) {
        isPremium = premium;
    }


    public String getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(String dateJoined) {
        this.dateJoined = dateJoined;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public void addEvent(Event event){
        this.events.add(event);
    }
}
