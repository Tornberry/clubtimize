package dk.clubtimize.clubtimize.models.union;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class UnionSeason implements UnionValidator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "union_id")
    private Union union;

    private String seasonYear;

    @OneToMany(mappedBy = "unionSeason")
    private List<UserSeason> userSeasons = new ArrayList<>();

    public UnionSeason() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return union;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Union getUnion() {
        return union;
    }

    public void setUnion(Union union) {
        this.union = union;
    }

    public String getSeasonYear() {
        return seasonYear;
    }

    public void setSeasonYear(String seasonYear) {
        this.seasonYear = seasonYear;
    }

    public List<UserSeason> getUserSeasons() {
        return userSeasons;
    }

    public void setUserSeasons(List<UserSeason> userSeasons) {
        this.userSeasons = userSeasons;
    }

    public void addUserSeason(UserSeason userSeason){
        this.userSeasons.add(userSeason);
    }
}
