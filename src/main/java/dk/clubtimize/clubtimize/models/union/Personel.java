package dk.clubtimize.clubtimize.models.union;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.user.UnionAssosiation;

import javax.persistence.*;

@Entity
public class Personel implements UnionValidator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int personelId;
    @ManyToOne
    @JoinColumn(name = "union_id")
    private Union union;
    @ManyToOne
    @JoinColumn(name = "unionassosiation")
    private UnionAssosiation unionAssosiation;
    private String title;
    private float hourlyWage;

    public Personel() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return union;
    }

    public int getPersonelId() {
        return personelId;
    }

    public void setPersonelId(int personelId) {
        this.personelId = personelId;
    }

    public Union getUnion() {
        return union;
    }

    public void setUnion(Union union) {
        this.union = union;
    }

    public UnionAssosiation getUnionAssosiation() {
        return unionAssosiation;
    }

    public void setUnionAssosiation(UnionAssosiation unionAssosiation) {
        this.unionAssosiation = unionAssosiation;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getHourlyWage() {
        return hourlyWage;
    }

    public void setHourlyWage(float hourlyWage) {
        this.hourlyWage = hourlyWage;
    }
}
