package dk.clubtimize.clubtimize.models.union.discount.discountTypes;

import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.discount.Discount;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TwoTeamDiscount {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    String title;

    @ManyToOne
    @JoinColumn(name = "discount_id")
    private Discount discount;

    //---------------------------------------------------------

    @ManyToMany(mappedBy = "twoTeamDiscounts")
    private List<Team> teams = new ArrayList<>();

    float amount;

    public TwoTeamDiscount() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void addTeam(Team team){
        this.teams.add(team);
    }

    public int getTeamsDiscountCount(List<Team> teams){
        int teamCount = 0;

        for (Team team : teams){
            if (this.getTeams().contains(team)){
                teamCount++;
            }
        }

        return teamCount;
    }
}
