package dk.clubtimize.clubtimize.models.union.matches.matchtypes;

import dk.clubtimize.clubtimize.models.union.matches.Match;
import dk.clubtimize.clubtimize.models.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class MixedDoubleTeam implements MatchTeam {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "match_id")
    private Match match;

    @ManyToMany
    @JoinColumn(name = "player_id")
    private List<User> players = new ArrayList<>();

    private int set1Points = 0;
    private int set2Points = 0;
    private int set3Points = 0;

    private int teamMMR;

    public MixedDoubleTeam() {
    }

    public MixedDoubleTeam(User player1, User player2, Match match) {
        this.players.add(player1);
        this.players.add(player2);
        this.match = match;
        calculateTeamMMR(player1, player2);
    }

    private void calculateTeamMMR(User player1, User player2) {
        this.teamMMR = (player1.getMatchStats().getEloPoints().getMixedDoubleElo() + player2.getMatchStats().getEloPoints().getMixedDoubleElo())/2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<User> getPlayers() {
        return players;
    }

    public void setPlayers(List<User> players) {
        this.players = players;
    }

    public int getTeamMMR() {
        return teamMMR;
    }

    public void setTeamMMR(int teamMMR) {
        this.teamMMR = teamMMR;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public void updatePoints(int set1, int set2, int set3){
        this.set1Points = set1;
        this.set2Points = set2;
        this.set3Points = set3;
    }

    @Override
    public int getSet1Points() {
        return set1Points;
    }

    @Override
    public int getSet2Points() {
        return set2Points;
    }

    @Override
    public int getSet3Points() {
        return set3Points;
    }
}
