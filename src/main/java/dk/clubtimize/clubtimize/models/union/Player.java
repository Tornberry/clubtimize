package dk.clubtimize.clubtimize.models.union;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.training.PlayerParticipation;
import dk.clubtimize.clubtimize.models.user.UnionAssosiation;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Player implements UnionValidator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int playerId;
    @ManyToOne
    @JoinColumn(name = "union_id")
    private Union union;
    @ManyToOne
    @JoinColumn(name = "unionassosiation_id")
    private UnionAssosiation unionAssosiation;
    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;

    @OneToMany(mappedBy = "player", cascade = CascadeType.ALL)
    private List<PlayerParticipation> playerParticipations = new ArrayList<>();

    public Player() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return union;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public Union getUnion() {
        return union;
    }

    public void setUnion(Union union) {
        this.union = union;
    }

    public UnionAssosiation getUnionAssosiation() {
        return unionAssosiation;
    }

    public void setUnionAssosiation(UnionAssosiation unionAssosiation) {
        this.unionAssosiation = unionAssosiation;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public List<PlayerParticipation> getPlayerParticipations() {
        return playerParticipations;
    }

    public void setPlayerParticipations(List<PlayerParticipation> playerParticipations) {
        this.playerParticipations = playerParticipations;
    }
}
