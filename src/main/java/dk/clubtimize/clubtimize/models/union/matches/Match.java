package dk.clubtimize.clubtimize.models.union.matches;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.models.training.Training;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.matches.matchtypes.*;
import dk.clubtimize.clubtimize.models.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "BadmintonMatch")
public class Match {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "union_id")
    private Union union;
    @ManyToOne
    @JoinColumn(name = "training_id")
    private Training training;

    private String date;
    private String matchType;

    // If a mens single match
    @OneToMany(mappedBy = "match", cascade = CascadeType.ALL)
    private List<MensSingleTeam> mensSingleTeams = new ArrayList<>();

    // If a womens single match
    @OneToMany(mappedBy = "match", cascade = CascadeType.ALL)
    private List<WomensSingleTeam> womensSingleTeams = new ArrayList<>();

    // If a mens double match
    @OneToMany(mappedBy = "match", cascade = CascadeType.ALL)
    private List<MensDoubleTeam> mensDoubleTeams = new ArrayList<>();

    // If a womens double match
    @OneToMany(mappedBy = "match", cascade = CascadeType.ALL)
    private List<WomensDoubleTeam> womensDoubleTeams = new ArrayList<>();

    // If a mixed double match
    @OneToMany(mappedBy = "match", cascade = CascadeType.ALL)
    private List<MixedDoubleTeam> mixedDoubleTeams = new ArrayList<>();


    public Match() {
    }

    public Match(User player1, User player2) {
        this.date = DateFactory.getCurrentDate();

        if (player1.getGender().equals("Mand") && player2.getGender().equals("Mand")) {
            this.matchType = "Herresingle";
            this.mensSingleTeams.add(new MensSingleTeam(player1, this));
            this.mensSingleTeams.add(new MensSingleTeam(player2, this));
        } else if (player1.getGender().equals("Kvinde") && player2.getGender().equals("Kvinde")) {
            this.matchType = "Damesingle";
            this.womensSingleTeams.add( new WomensSingleTeam(player1, this));
            this.womensSingleTeams.add( new WomensSingleTeam(player2, this));
        } else {
            this.matchType = "Andetsingle";
            if (player1.getGender().equals("Mand")) {
                this.mensSingleTeams.add(new MensSingleTeam(player1, this));
            } else {
                this.womensSingleTeams.add(new WomensSingleTeam(player1, this));
            }

            if (player2.getGender().equals("Mand")) {
                this.mensSingleTeams.add(new MensSingleTeam(player2, this));
            } else {
                this.womensSingleTeams.add(new WomensSingleTeam(player2, this));
            }
        }

    }

    public Match(User player1_team1, User player2_team1, User player1_team2, User player2_team2) {
        this.date = DateFactory.getCurrentDate();

        if (allSameGender(player1_team1, player2_team1, player1_team2, player2_team2, "Mand")) {
            this.matchType = "Herredouble";
            this.mensDoubleTeams.add(new MensDoubleTeam(player1_team1, player2_team1, this));
            this.mensDoubleTeams.add(new MensDoubleTeam(player1_team2, player2_team2, this));
        } else if (allSameGender(player1_team1, player2_team1, player1_team2, player2_team2, "Kvinde")) {
            this.matchType = "Damedouble";
            this.womensDoubleTeams.add(new WomensDoubleTeam(player1_team1, player2_team1, this));
            this.womensDoubleTeams.add(new WomensDoubleTeam(player1_team2, player2_team2, this));
        } else if (isMixedDouble(player1_team1, player2_team1, player1_team2, player2_team2)) {
            this.matchType = "Mixeddouble";
            this.mixedDoubleTeams.add(new MixedDoubleTeam(player1_team1, player2_team1, this));
            this.mixedDoubleTeams.add(new MixedDoubleTeam(player1_team2, player2_team2, this));
        } else {
            this.matchType = "Andetdouble";
            this.mixedDoubleTeams.add(new MixedDoubleTeam(player1_team1, player2_team1, this));
            this.mixedDoubleTeams.add(new MixedDoubleTeam(player1_team2, player2_team2, this));
        }

    }

    private boolean isMixedDouble(User player1_team1, User player2_team1, User player1_team2, User player2_team2) {
        boolean team1Mix = player1_team1.getGender().equals("Mand") && player2_team1.getGender().equals("Kvinde") || (player1_team1.getGender().equals("Kvinde") && player2_team1.getGender().equals("Mand"));
        boolean team2Mix = player1_team2.getGender().equals("Mand") && player2_team2.getGender().equals("Kvinde") || (player1_team2.getGender().equals("Kvinde") && player2_team2.getGender().equals("Mand"));
        return team1Mix && team2Mix;
    }

    private boolean allSameGender(User player1_team1, User player2_team1, User player1_team2, User player2_team2, String gender) {
        return player1_team1.getGender().equals(gender) && player2_team1.getGender().equals(gender) && player1_team2.getGender().equals(gender) && player2_team2.getGender().equals(gender);
    }

    public Object getLowestMMRTeam() {
        switch (matchType) {
            case "Herresingle":
                if (mensSingleTeams.get(0).getTeamMMR() < mensSingleTeams.get(1).getTeamMMR()) {
                    return mensSingleTeams.get(0);
                } else {
                    return mensSingleTeams.get(1);
                }
            case "Herredouble":
                if (mensDoubleTeams.get(0).getTeamMMR() < mensDoubleTeams.get(1).getTeamMMR()) {
                    return mensDoubleTeams.get(0);
                } else {
                    return mensDoubleTeams.get(1);
                }
            case "Damesingle":
                if (womensSingleTeams.get(0).getTeamMMR() < womensSingleTeams.get(1).getTeamMMR()) {
                    return womensSingleTeams.get(0);
                } else {
                    return womensSingleTeams.get(1);
                }
            case "Damedouble":
                if (womensDoubleTeams.get(0).getTeamMMR() < womensDoubleTeams.get(1).getTeamMMR()) {
                    return womensDoubleTeams.get(0);
                } else {
                    return womensDoubleTeams.get(1);
                }
            case "Mixeddouble":
                if (mixedDoubleTeams.get(0).getTeamMMR() < mixedDoubleTeams.get(1).getTeamMMR()) {
                    return mixedDoubleTeams.get(0);
                } else {
                    return mixedDoubleTeams.get(1);
                }
            default:
                return null;
        }
    }

    public void updateScore(int[] team1, int[] team2){
        switch (matchType) {
            case "Herresingle":
                mensSingleTeams.get(0).updatePoints(team1[0], team1[1], team1[2]);
                mensSingleTeams.get(1).updatePoints(team2[0], team2[1], team2[2]);
                break;
            case "Damesingle":
                womensSingleTeams.get(0).updatePoints(team1[0], team1[1], team1[2]);
                womensSingleTeams.get(1).updatePoints(team2[0], team2[1], team2[2]);
                break;
            case "Herredouble":
                mensDoubleTeams.get(0).updatePoints(team1[0], team1[1], team1[2]);
                mensDoubleTeams.get(1).updatePoints(team2[0], team2[1], team2[2]);
                break;
            case "Damedouble":
                womensDoubleTeams.get(0).updatePoints(team1[0], team1[1], team1[2]);
                womensDoubleTeams.get(1).updatePoints(team2[0], team2[1], team2[2]);
                break;
            case "Mixeddouble":
                mixedDoubleTeams.get(0).updatePoints(team1[0], team1[1], team1[2]);
                mixedDoubleTeams.get(1).updatePoints(team2[0], team2[1], team2[2]);
                break;
        }

    }

    public Union getUnion() {
        return union;
    }

    public void setUnion(Union union) {
        this.union = union;
    }

    public Training getTraining() {
        return training;
    }

    public void setTraining(Training training) {
        this.training = training;
    }


    public int getId() {
        return id;
    }

    public String getMatchType() {
        return matchType;
    }

    public List<MensSingleTeam> getMensSingleTeams() {
        return mensSingleTeams;
    }

    public List<WomensSingleTeam> getWomensSingleTeams() {
        return womensSingleTeams;
    }

    public List<MensDoubleTeam> getMensDoubleTeams() {
        return mensDoubleTeams;
    }

    public List<WomensDoubleTeam> getWomensDoubleTeams() {
        return womensDoubleTeams;
    }

    public List<MixedDoubleTeam> getMixedDoubleTeams() {
        return mixedDoubleTeams;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
