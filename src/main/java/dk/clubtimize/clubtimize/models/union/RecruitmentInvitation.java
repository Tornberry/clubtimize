package dk.clubtimize.clubtimize.models.union;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class RecruitmentInvitation implements UnionValidator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    // The union that the invitation belongs to
    @ManyToOne
    @JoinColumn(name = "union_id")
    private Union union;

    // The type of invitaion
    private String type;
    // The invitation number
    private int invNumber;
    // The email address of the reciever
    private String email;

    // If type coach, the team they are going to coach
    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;
    // The persons hourly wage
    private float hourWageOffered;
    // The status of the invitation
    private String status;

    public RecruitmentInvitation() {
    }

    public RecruitmentInvitation(Union union, String type, int invNumber, String email, Team team, String status) {
        this.union = union;
        this.type = type;
        this.invNumber = invNumber;
        this.email = email;
        this.team = team;
        this.status = status;
    }

    public RecruitmentInvitation(Union union, String type, int invNumber, String email, Team team, float hourWageOffered, String status) {
        this.union = union;
        this.type = type;
        this.invNumber = invNumber;
        this.email = email;
        this.team = team;
        this.hourWageOffered = hourWageOffered;
        this.status = status;
    }

    @Override
    public Union getAssosiatedUnion() {
        return union;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getInvNumber() {
        return invNumber;
    }

    public void setInvNumber(int invNumber) {
        this.invNumber = invNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public float getHourWageOffered() {
        return hourWageOffered;
    }

    public void setHourWageOffered(float hourWageOffered) {
        this.hourWageOffered = hourWageOffered;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Union getUnion() {
        return union;
    }

    public void setUnion(Union union) {
        this.union = union;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}
