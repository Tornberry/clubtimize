package dk.clubtimize.clubtimize.models.union.discount;

import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.discount.discountTypes.TwoTeamDiscount;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Discount {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne
    @JoinColumn(name = "union_id")
    private Union union;

    @OneToMany(mappedBy = "discount", cascade = CascadeType.ALL)
    private List<TwoTeamDiscount> twoTeamDiscounts = new ArrayList<>();

    public Discount() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<TwoTeamDiscount> getTwoTeamDiscounts() {
        return twoTeamDiscounts;
    }

    public void setTwoTeamDiscounts(List<TwoTeamDiscount> twoTeamDiscounts) {
        this.twoTeamDiscounts = twoTeamDiscounts;
    }

    public Union getUnion() {
        return union;
    }

    public void setUnion(Union union) {
        this.union = union;
    }

    public void addTwoTeamDiscount(TwoTeamDiscount twoTeamDiscount){
        this.twoTeamDiscounts.add(twoTeamDiscount);
    }
}
