package dk.clubtimize.clubtimize.models.union.matches.matchtypes;

public interface MatchTeam {
    int getSet1Points();
    int getSet2Points();
    int getSet3Points();

    int getTeamMMR();
}
