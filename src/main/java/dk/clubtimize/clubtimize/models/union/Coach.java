package dk.clubtimize.clubtimize.models.union;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.user.UnionAssosiation;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Coach implements UnionValidator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int coachId;
    @ManyToOne
    @JoinColumn(name = "union_id")
    private Union union;
    @ManyToOne
    @JoinColumn(name = "unionassosiation_id")
    private UnionAssosiation unionAssosiation;
    @ManyToMany
    private List<Team> teams = new ArrayList<>();
    private float hourlyWage;

    public Coach() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return union;
    }

    public int getCoachId() {
        return coachId;
    }

    public void setCoachId(int coachId) {
        this.coachId = coachId;
    }

    public Union getUnion() {
        return union;
    }

    public void setUnion(Union union) {
        this.union = union;
    }

    public UnionAssosiation getUnionAssosiation() {
        return unionAssosiation;
    }

    public void setUnionAssosiation(UnionAssosiation unionAssosiation) {
        this.unionAssosiation = unionAssosiation;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public float getHourlyWage() {
        return hourlyWage;
    }

    public void setHourlyWage(float hourlyWage) {
        this.hourlyWage = hourlyWage;
    }

    public void addTeam(Team team){
        this.teams.add(team);
    }

    public void removeTeam(Team team){
        this.teams.remove(team);
    }
}
