package dk.clubtimize.clubtimize.models.union;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.opsætning.TeamDepartment;
import dk.clubtimize.clubtimize.models.training.Training;
import dk.clubtimize.clubtimize.models.training.TrainingSchedule;
import dk.clubtimize.clubtimize.models.union.discount.discountTypes.TwoTeamDiscount;
import dk.clubtimize.clubtimize.models.union.event.Event;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import org.joda.time.format.DateTimeFormat;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Team implements UnionValidator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int teamId;

    // The title of the tea,
    private String title;

    // The department of the team
    @ManyToOne
    @JoinColumn(name = "teamcategory_id")
    private TeamDepartment teamDepartment;

    // If team has closed recruitment(invitations only)
    private boolean closed;

    // How many spots on team
    private int avalibleSpots;

    private String paymentSchedule;

    private String startDate;

    // People involved with this team
    @ManyToMany
    private List<Coach> coaches = new ArrayList<>();
    @OneToMany(mappedBy = "team", cascade = CascadeType.ALL)
    private List<Player> players = new ArrayList<>();

    // If any was invitated to coach on this team
    @OneToMany(mappedBy = "team")
    private List<RecruitmentInvitation> recruitmentInvitations = new ArrayList<>();

    // The teams training schedule
    @OneToMany(mappedBy = "team", cascade = CascadeType.ALL)
    private List<TrainingSchedule> trainingSchedules = new ArrayList<>();

    // All the trainings of the team
    @OneToMany(mappedBy = "team", cascade = CascadeType.ALL)
    private List<Training> trainings = new ArrayList<>();

    // All the teamseasons of the team
    @OneToMany(mappedBy = "team")
    private List<TeamSeason> teamSeasons = new ArrayList<>();

    // The agegroup of the team
    private String ageGroup;
    //The general niveau of the team
    private String niveau;
    // The price to pay when joining this team
    private float price;

    @OneToMany(mappedBy = "assosiatedTeam")
    private List<Invoice> assosiatedInvoices = new ArrayList<>();

    @ManyToMany
    private List<TwoTeamDiscount> twoTeamDiscounts = new ArrayList<>();

    @ManyToMany
    private List<Event> events = new ArrayList<>();

    // The union this team belongs to
    @ManyToOne
    @JoinColumn(name = "union_id")
    private Union union;

    public Team() {
    }

    public Team(String title, boolean closed, int avalibleSpots, String ageGroup, String niveau, float price, String paymentSchedule, String startDate) {
        this.title = title;
        this.closed = closed;
        this.avalibleSpots = avalibleSpots;
        this.ageGroup = ageGroup;
        this.niveau = niveau;
        this.price = price;
        this.paymentSchedule = paymentSchedule;
        this.startDate = DateTimeFormat.forPattern("MM/dd/yyyy").parseDateTime(startDate).toString("dd-MM-yyyy");

    }

    @Override
    public Union getAssosiatedUnion() {
        return union;
    }

    public Union getUnion() {
        return union;
    }

    public void setUnion(Union union) {
        this.union = union;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TeamDepartment getTeamDepartment() {
        return teamDepartment;
    }

    public void setTeamDepartment(TeamDepartment teamDepartment) {
        this.teamDepartment = teamDepartment;
    }

    public List<Coach> getCoaches() {
        return coaches;
    }

    public void setCoaches(List<Coach> coaches) {
        this.coaches = coaches;
    }

    public List<TrainingSchedule> getTrainingSchedules() {
        return trainingSchedules;
    }

    public void setTrainingSchedules(List<TrainingSchedule> trainingSchedules) {
        this.trainingSchedules = trainingSchedules;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void addTrainingSchedule(TrainingSchedule trainingSchedule){
        this.trainingSchedules.add(trainingSchedule);
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public int getAvalibleSpots() {
        return avalibleSpots;
    }

    public void setAvalibleSpots(int avalibleSpots) {
        this.avalibleSpots = avalibleSpots;
    }

    public List<Training> getTrainings() {
        return trainings;
    }

    public void setTrainings(List<Training> trainings) {
        this.trainings = trainings;
    }

    public void addTraining(Training training){
        this.trainings.add(training);
    }

    public String getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
    }

    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void addPlayer(Player player){
        this.players.add(player);
    }

    public List<RecruitmentInvitation> getRecruitmentInvitations() {
        return recruitmentInvitations;
    }

    public void setRecruitmentInvitations(List<RecruitmentInvitation> recruitmentInvitations) {
        this.recruitmentInvitations = recruitmentInvitations;
    }

    public void addRecruitmentInvitation(RecruitmentInvitation recruitmentInvitation){
        this.recruitmentInvitations.add(recruitmentInvitation);
    }

    public void addCoach(Coach coach){
        this.coaches.add(coach);
    }

    public List<TeamSeason> getTeamSeasons() {
        return teamSeasons;
    }

    public void setTeamSeasons(List<TeamSeason> teamSeasons) {
        this.teamSeasons = teamSeasons;
    }

    public void addTeamSeason(TeamSeason teamSeason){
        this.teamSeasons.add(teamSeason);
    }

    public int getMaleCount(){
        int maleCount = 0;
        for (Player player : this.players){
            if (player.getUnionAssosiation().getUser().getGender().equals("Mand")){
                maleCount++;
            }
        }
        return maleCount;
    }

    public int getFemaleCount(){
        int femaleCount = 0;
        for (Player player : this.players){
            if (player.getUnionAssosiation().getUser().getGender().equals("Kvinde")){
                femaleCount++;
            }
        }
        return femaleCount;
    }

    public List<Invoice> getAssosiatedInvoices() {
        return assosiatedInvoices;
    }

    public void setAssosiatedInvoices(List<Invoice> assosiatedInvoices) {
        this.assosiatedInvoices = assosiatedInvoices;
    }

    public void addInvoice(Invoice invoice){
        this.assosiatedInvoices.add(invoice);
    }

    public String getPaymentSchedule() {
        return paymentSchedule;
    }

    public void setPaymentSchedule(String paymentSchedule) {
        this.paymentSchedule = paymentSchedule;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public List<TwoTeamDiscount> getTwoTeamDiscounts() {
        return twoTeamDiscounts;
    }

    public void setTwoTeamDiscounts(List<TwoTeamDiscount> twoTeamDiscounts) {
        this.twoTeamDiscounts = twoTeamDiscounts;
    }

    public void addTwoTeamDiscount(TwoTeamDiscount twoTeamDiscount){
        this.twoTeamDiscounts.add(twoTeamDiscount);
    }

    public void removeTwoTeamDiscount(TwoTeamDiscount twoTeamDiscount){
        this.twoTeamDiscounts.remove(twoTeamDiscount);
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public void addEvent(Event event){
        this.events.add(event);
    }

    public void removeCoach(Coach coach){
        this.coaches.remove(coach);
    }

    public void removePlayer(Player player){
        this.players.remove(player);
    }
}
