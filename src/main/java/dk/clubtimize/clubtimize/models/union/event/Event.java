package dk.clubtimize.clubtimize.models.union.event;

import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "union_id")
    private Union union;

    @ManyToMany
    private List<User> participants = new ArrayList<>();

    private String dateFrom;
    private String timeFrom;

    private String dateTo;
    private String timeTo;

    private String deadlineDate;
    private String deadlineTime;

    private String title;
    private String location;
    private float price;

    @ManyToMany
    private List<Team> teamsInvited = new ArrayList<>();

    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL)
    private List<EventInvitation> eventInvitations = new ArrayList<>();

    @Column(length = 10000)
    private String description;

    public Event() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Union getUnion() {
        return union;
    }

    public void setUnion(Union union) {
        this.union = union;
    }

    public List<User> getParticipants() {
        return participants;
    }

    public void setParticipants(List<User> participants) {
        this.participants = participants;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public List<Team> getTeamsInvited() {
        return teamsInvited;
    }

    public void setTeamsInvited(List<Team> teamsInvited) {
        this.teamsInvited = teamsInvited;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addParticipant(User user){
        this.participants.add(user);
    }

    public void addTeam(Team team){
        this.teamsInvited.add(team);
    }

    public List<EventInvitation> getEventInvitations() {
        return eventInvitations;
    }

    public void setEventInvitations(List<EventInvitation> eventInvitations) {
        this.eventInvitations = eventInvitations;
    }

    public void addUserInvitation(EventInvitation eventInvitation){
        this.eventInvitations.add(eventInvitation);
    }

    public void removeParticipant(User user){
        this.participants.remove(user);
    }

    public void removeUserInvitation(EventInvitation eventInvitation){
        this.eventInvitations.remove(eventInvitation);
    }

    public String getDeadlineDate() {
        return deadlineDate;
    }

    public void setDeadlineDate(String deadlineDate) {
        this.deadlineDate = deadlineDate;
    }

    public String getDeadlineTime() {
        return deadlineTime;
    }

    public void setDeadlineTime(String deadlineTime) {
        this.deadlineTime = deadlineTime;
    }
}
