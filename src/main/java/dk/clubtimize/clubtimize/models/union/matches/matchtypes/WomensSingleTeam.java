package dk.clubtimize.clubtimize.models.union.matches.matchtypes;

import dk.clubtimize.clubtimize.models.union.matches.Match;
import dk.clubtimize.clubtimize.models.user.User;

import javax.persistence.*;

@Entity
public class WomensSingleTeam implements MatchTeam {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne
    @JoinColumn(name = "match_id")
    private Match match;

    @ManyToOne
    @JoinColumn(name = "player_id")
    private User player;

    private int set1Points = 0;
    private int set2Points = 0;
    private int set3Points = 0;

    private int teamMMR;

    public WomensSingleTeam() {
    }

    public WomensSingleTeam(User player, Match match) {
        this.player = player;
        this.match = match;
        calculateTeamMMR(player);
    }

    private void calculateTeamMMR(User user) {
        this.teamMMR = user.getMatchStats().getEloPoints().getWomensSingleElo();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getPlayer() {
        return player;
    }

    public void setPlayer(User player) {
        this.player = player;
    }

    public int getTeamMMR() {
        return teamMMR;
    }

    public void setTeamMMR(int teamMMR) {
        this.teamMMR = teamMMR;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public void updatePoints(int set1, int set2, int set3){
        this.set1Points = set1;
        this.set2Points = set2;
        this.set3Points = set3;
    }

    @Override
    public int getSet1Points() {
        return set1Points;
    }

    @Override
    public int getSet2Points() {
        return set2Points;
    }

    @Override
    public int getSet3Points() {
        return set3Points;
    }
}
