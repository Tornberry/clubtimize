package dk.clubtimize.clubtimize.models.economics;

import dk.clubtimize.clubtimize.models.economics.PaymentRequest;

import javax.persistence.*;

@Entity
public class PaymentRequestComment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "paymentRequest_id")
    private PaymentRequest paymentRequest;

    // The initials of the author
    private String authorInitials;
    // The date which the comment was created
    private String date;
    // The comment itself
    @Column(length = 10000)
    private String comment;

    public PaymentRequestComment() {
    }

    public PaymentRequestComment(PaymentRequest paymentRequest, String authorInitials, String date, String comment) {
        this.paymentRequest = paymentRequest;
        this.authorInitials = authorInitials;
        this.date = date;
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthorInitials() {
        return authorInitials;
    }

    public void setAuthorInitials(String authorInitials) {
        this.authorInitials = authorInitials;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
