package dk.clubtimize.clubtimize.models.economics.invoice;

import dk.clubtimize.clubtimize.models.economics.EconomicYear;

import javax.persistence.*;

@Entity
public class DebtReport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "economicYear_id")
    private EconomicYear economicYear;
    private String month;

    private float amount;

    public DebtReport() {
    }

    public DebtReport(EconomicYear economicYear, String month, float amount) {
        this.economicYear = economicYear;
        this.month = month;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EconomicYear getEconomicYear() {
        return economicYear;
    }

    public void setEconomicYear(EconomicYear economicYear) {
        this.economicYear = economicYear;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
