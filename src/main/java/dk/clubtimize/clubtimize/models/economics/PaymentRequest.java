package dk.clubtimize.clubtimize.models.economics;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.union.Union;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class PaymentRequest implements UnionValidator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    // Payment request are bound to a specific Economic Year
    @ManyToOne
    @JoinColumn(name = "economicYear_id")
    private EconomicYear economicYear;

    // The status of the request.
    private String status;

    // The date of request creation
    private String date;

    // The amount to pay at creation time
    private float amountAtRequestTime;

    // If request is completed or not
    private boolean open = true;

    @OneToMany(mappedBy = "paymentRequest", cascade = CascadeType.ALL)
    private List<PaymentRequestComment> paymentRequestComments = new ArrayList<>();

    private String expectedPayoutDate;

    public PaymentRequest() {
    }

    public PaymentRequest(EconomicYear economicYear) {
        this.economicYear = economicYear;
        this.status = "Afventer godkendelse";
        this.date = DateFactory.getCurrentDate();

        amountAtRequestTime = economicYear.getUnionEconomics().getBalance();
    }

    @Override
    public Union getAssosiatedUnion() {
        return economicYear.getAssosiatedUnion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EconomicYear getEconomicYear() {
        return economicYear;
    }

    public void setEconomicYear(EconomicYear economicYear) {
        this.economicYear = economicYear;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public float getAmountAtRequestTime() {
        return amountAtRequestTime;
    }

    public void setAmountAtRequestTime(float amountAtRequestTime) {
        this.amountAtRequestTime = amountAtRequestTime;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public List<PaymentRequestComment> getPaymentRequestComments() {
        return paymentRequestComments;
    }

    public void setPaymentRequestComments(List<PaymentRequestComment> paymentRequestComments) {
        this.paymentRequestComments = paymentRequestComments;
    }

    public void addComment(PaymentRequestComment comment){
        this.paymentRequestComments.add(comment);
    }

    public String getExpectedPayoutDate() {
        return expectedPayoutDate;
    }

    public void setExpectedPayoutDate(String expectedPayoutDate) {
        this.expectedPayoutDate = expectedPayoutDate;
    }
}
