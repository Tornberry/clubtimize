package dk.clubtimize.clubtimize.models.economics.invoice;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.union.Union;

import javax.persistence.*;

@Entity
public class InvoiceItem implements UnionValidator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @ManyToOne
    @JoinColumn(name = "invoiceItem_id")
    private Invoice invoice;

    private int quantity;
    private String item;
    private String description;
    private float unitCost;
    private float fee;
    private float tax;
    private float totalCost;

    public InvoiceItem() {
    }

    public InvoiceItem(Invoice invoice, int quantity, String item, String description, float unitCost, float fee, float tax, float totalCost) {
        this.invoice = invoice;
        this.quantity = quantity;
        this.item = item;
        this.description = description;
        this.unitCost = unitCost;
        this.fee = fee;
        this.tax = tax;
        this.totalCost = totalCost;
    }

    @Override
    public Union getAssosiatedUnion() {
        return invoice.getEconomicYear().getUnionEconomics().getUnion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(float unitCost) {
        this.unitCost = unitCost;
    }

    public float getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(float totalCost) {
        this.totalCost = totalCost;
    }

    public float getFee() {
        return fee;
    }

    public void setFee(float fee) {
        this.fee = fee;
    }

    public float getTax() {
        return tax;
    }

    public void setTax(float tax) {
        this.tax = tax;
    }
}
