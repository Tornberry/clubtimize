package dk.clubtimize.clubtimize.models.economics.bookkeeping;

import javax.persistence.*;

@Entity
public class BookkeepingHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "bookkeeping_id")
    private Bookkeeping bookkeeping;

    private String integration;
    private String date;
    private String dateStart;
    private String dateEnd;
    private boolean withFees;

    public BookkeepingHistory() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Bookkeeping getBookkeeping() {
        return bookkeeping;
    }

    public void setBookkeeping(Bookkeeping bookkeeping) {
        this.bookkeeping = bookkeeping;
    }

    public String getIntegration() {
        return integration;
    }

    public void setIntegration(String integration) {
        this.integration = integration;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public boolean isWithFees() {
        return withFees;
    }

    public void setWithFees(boolean withFees) {
        this.withFees = withFees;
    }
}
