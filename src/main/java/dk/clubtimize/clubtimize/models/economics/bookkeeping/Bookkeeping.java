package dk.clubtimize.clubtimize.models.economics.bookkeeping;

import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import dk.clubtimize.clubtimize.models.economics.bookkeeping.accountingIntegrations.DineroIntegration;
import dk.clubtimize.clubtimize.models.economics.bookkeeping.accountingIntegrations.EconomicIntegration;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Bookkeeping {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private boolean firstTimeSetup = false;

    private String chosenIntegration;

    @OneToOne
    @JoinColumn(name = "unionEconomic_id")
    private UnionEconomics unionEconomics;

    @OneToOne(mappedBy = "bookkeeping", cascade = CascadeType.ALL)
    private DineroIntegration dineroIntegration;

    @OneToOne(mappedBy = "bookkeeping", cascade = CascadeType.ALL)
    private EconomicIntegration economicIntegration;

    @OneToMany(mappedBy = "bookkeeping", cascade = CascadeType.ALL)
    private List<BookkeepingHistory> bookkeepingHistories = new ArrayList<>();

    public Bookkeeping() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UnionEconomics getUnionEconomics() {
        return unionEconomics;
    }

    public void setUnionEconomics(UnionEconomics unionEconomics) {
        this.unionEconomics = unionEconomics;
    }

    public DineroIntegration getDineroIntegration() {
        return dineroIntegration;
    }

    public void setDineroIntegration(DineroIntegration dineroIntegration) {
        this.dineroIntegration = dineroIntegration;
    }

    public EconomicIntegration getEconomicIntegration() {
        return economicIntegration;
    }

    public void setEconomicIntegration(EconomicIntegration economicIntegration) {
        this.economicIntegration = economicIntegration;
    }

    public boolean isFirstTimeSetup() {
        return firstTimeSetup;
    }

    public void setFirstTimeSetup(boolean firstTimeSetup) {
        this.firstTimeSetup = firstTimeSetup;
    }

    public String getChosenIntegration() {
        return chosenIntegration;
    }

    public void setChosenIntegration(String chosenIntegration) {
        this.chosenIntegration = chosenIntegration;
    }

    public List<BookkeepingHistory> getBookkeepingHistories() {
        return bookkeepingHistories;
    }

    public void setBookkeepingHistories(List<BookkeepingHistory> bookkeepingHistories) {
        this.bookkeepingHistories = bookkeepingHistories;
    }

    public void addBookkeepingHistory(BookkeepingHistory bookkeepingHistory){
        this.bookkeepingHistories.add(bookkeepingHistory);
    }
}
