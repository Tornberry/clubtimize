package dk.clubtimize.clubtimize.models.economics.invoice;

import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.union.Union;

import javax.persistence.*;

@Entity
public class FeeReport {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "economicYear_id")
    private EconomicYear economicYear;
    private String month;
    private String guid;

    private float amount;

    public FeeReport() {
    }

    public FeeReport(EconomicYear economicYear, String month, String guid, float amount) {
        this.economicYear = economicYear;
        this.month = month;
        this.guid = guid;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EconomicYear getEconomicYear() {
        return economicYear;
    }

    public void setEconomicYear(EconomicYear economicYear) {
        this.economicYear = economicYear;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
