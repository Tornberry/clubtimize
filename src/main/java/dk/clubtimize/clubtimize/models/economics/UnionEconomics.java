package dk.clubtimize.clubtimize.models.economics;

import dk.clubtimize.clubtimize.controllers.support.EconomicSupport;
import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.economics.bookkeeping.Bookkeeping;
import dk.clubtimize.clubtimize.models.union.Union;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class UnionEconomics implements UnionValidator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private float feePercent;

    @OneToOne
    @JoinColumn(name = "union_id")
    private Union union;

    // the current balance of union
    private float balance;

    // invoices
    @OneToMany(mappedBy = "unionEconomics", cascade = CascadeType.ALL)
    private List<EconomicYear> economicYears = new ArrayList<>();

    @OneToOne(mappedBy = "unionEconomics", cascade = CascadeType.ALL, orphanRemoval = true)
    private BankinfoContainer bankinfoContainer;

    private boolean isPayingFees = false;

    @OneToOne(mappedBy = "unionEconomics", cascade = CascadeType.ALL, orphanRemoval = true)
    private Bookkeeping bookkeeping;

    public UnionEconomics() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return union;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Union getUnion() {
        return union;
    }

    public void setUnion(Union union) {
        this.union = union;
    }

    public List<EconomicYear> getEconomicYears() {
        return economicYears;
    }

    public void setEconomicYears(List<EconomicYear> economicYears) {
        this.economicYears = economicYears;
    }

    public void addEconomicYear(EconomicYear economicYear){
        this.economicYears.add(economicYear);
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public float getFeePercent() {
        return feePercent;
    }

    public void setFeePercent(float feePercent) {
        this.feePercent = feePercent;
    }

    public void updateBalance(){
        balance = EconomicSupport.calculateBalanceByUnionEconomic(this);
    }

    public BankinfoContainer getBankinfoContainer() {
        return bankinfoContainer;
    }

    public void setBankinfoContainer(BankinfoContainer bankinfoContainer) {
        this.bankinfoContainer = bankinfoContainer;
    }

    public boolean isPayingFees() {
        return isPayingFees;
    }

    public void setPayingFees(boolean payingFees) {
        isPayingFees = payingFees;
    }

    public Bookkeeping getBookkeeping() {
        return bookkeeping;
    }

    public void setBookkeeping(Bookkeeping bookkeeping) {
        this.bookkeeping = bookkeeping;
    }
}
