package dk.clubtimize.clubtimize.models.economics.bookkeeping.accountingIntegrations;

import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import dk.clubtimize.clubtimize.models.economics.bookkeeping.Bookkeeping;

import javax.persistence.*;

@Entity
public class EconomicIntegration {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne
    @JoinColumn(name = "bookkeeping_id")
    private Bookkeeping bookkeeping;

    private String APIKey;

    public EconomicIntegration() {
    }

    public String getAPIKey() {
        return APIKey;
    }

    public void setAPIKey(String APIKey) {
        this.APIKey = APIKey;
    }

    public Bookkeeping getBookkeeping() {
        return bookkeeping;
    }

    public void setBookkeeping(Bookkeeping bookkeeping) {
        this.bookkeeping = bookkeeping;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
