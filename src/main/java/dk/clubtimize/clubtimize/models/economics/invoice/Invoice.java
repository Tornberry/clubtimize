package dk.clubtimize.clubtimize.models.economics.invoice;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.opsætning.booking.Booking;
import dk.clubtimize.clubtimize.models.opsætning.booking.TimePeriod;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.event.EventInvitation;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Invoice implements UnionValidator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String unionAssosiatedName;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "econimocYear_id")
    private EconomicYear economicYear;

    @OneToMany(mappedBy = "invoice", cascade = CascadeType.ALL)
    private List<InvoiceItem> items = new ArrayList<>();

    private String type;
    private String invoiceNumber;
    private String date;

    private boolean addToBalance;
    private float totalCostWithAdditional;
    private float totalCostWithoutAdditional;

    @OneToOne(mappedBy = "invoice")
    private TeamSeason teamSeason;

    @ManyToOne()
    @JoinColumn(name = "team_id")
    private Team assosiatedTeam;

    @OneToOne(mappedBy = "invoice")
    private Booking assosiatedBooking;

    @OneToOne(mappedBy = "invoice")
    private TimePeriod assosiatedTimePeriod;

    @OneToOne(mappedBy = "invoice")
    private EventInvitation assosiatedEventInvitation;

    public Invoice() {
    }

    public Invoice(List<InvoiceItem> invoiceItems, EconomicYear economicYear, User user){
        this.items = invoiceItems;
        this.economicYear = economicYear;
        this.user = user;
    }

    @Override
    public Union getAssosiatedUnion() {
        return economicYear.getUnionEconomics().getUnion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EconomicYear getEconomicYear() {
        return economicYear;
    }

    public void setEconomicYear(EconomicYear economicYear) {
        this.economicYear = economicYear;
    }

    public List<InvoiceItem> getItems() {
        return items;
    }

    public void setItems(List<InvoiceItem> items) {
        this.items = items;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void addInvoiceItem(InvoiceItem invoiceItem){
        this.items.add(invoiceItem);
    }

    public float getTotalCostWithAdditional() {
        return totalCostWithAdditional;
    }

    public void setTotalCostWithAdditional(float totalCostWithAdditional) {
        this.totalCostWithAdditional = totalCostWithAdditional;
    }

    public float getTotalCostWithoutAdditional() {
        return totalCostWithoutAdditional;
    }

    public void setTotalCostWithoutAdditional(float totalCostWithoutAdditional) {
        this.totalCostWithoutAdditional = totalCostWithoutAdditional;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isAddToBalance() {
        return addToBalance;
    }

    public void setAddToBalance(boolean addToBalance) {
        this.addToBalance = addToBalance;
    }

    public String getUnionAssosiatedName() {
        return unionAssosiatedName;
    }

    public void setUnionAssosiatedName(String unionAssosiatedName) {
        this.unionAssosiatedName = unionAssosiatedName;
    }

    public TeamSeason getTeamSeason() {
        return teamSeason;
    }

    public void setTeamSeason(TeamSeason teamSeason) {
        this.teamSeason = teamSeason;
    }

    public Team getAssosiatedTeam() {
        return assosiatedTeam;
    }

    public void setAssosiatedTeam(Team assosiatedTeam) {
        this.assosiatedTeam = assosiatedTeam;
    }

    public Booking getAssosiatedBooking() {
        return assosiatedBooking;
    }

    public void setAssosiatedBooking(Booking assosiatedBooking) {
        this.assosiatedBooking = assosiatedBooking;
    }

    public TimePeriod getAssosiatedTimePeriod() {
        return assosiatedTimePeriod;
    }

    public void setAssosiatedTimePeriod(TimePeriod assosiatedTimePeriod) {
        this.assosiatedTimePeriod = assosiatedTimePeriod;
    }

    public EventInvitation getAssosiatedEventInvitation() {
        return assosiatedEventInvitation;
    }

    public void setAssosiatedEventInvitation(EventInvitation assosiatedEventInvitation) {
        this.assosiatedEventInvitation = assosiatedEventInvitation;
    }
}
