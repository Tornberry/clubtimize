package dk.clubtimize.clubtimize.models.economics;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.economics.invoice.DebtReport;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.economics.invoice.FeeReport;
import dk.clubtimize.clubtimize.models.union.Union;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class EconomicYear implements UnionValidator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    /**
     * A Economic Year is bound to the economics of a specific union
     */
    @ManyToOne
    @JoinColumn(name = "unionEconomics_id")
    private UnionEconomics unionEconomics;

    /**
     * The year in format: current_year/next_year
     */
    private String year;

    /**
     * Has invoices
     */
    @OneToMany(mappedBy = "economicYear", cascade = CascadeType.ALL)
    private List<Invoice> invoices = new ArrayList<>();

    /**
     * Has monthly generated reports
     */
    @OneToMany(mappedBy = "economicYear", cascade = CascadeType.ALL)
    private List<FeeReport> feeReports = new ArrayList<>();
    @OneToMany(mappedBy = "economicYear", cascade = CascadeType.ALL)
    private List<DebtReport> debtReports = new ArrayList<>();

    @OneToMany(mappedBy = "economicYear", cascade = CascadeType.ALL)
    private List<PaymentRequest> paymentRequests = new ArrayList<>();

    public EconomicYear() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return unionEconomics.getUnion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UnionEconomics getUnionEconomics() {
        return unionEconomics;
    }

    public void setUnionEconomics(UnionEconomics unionEconomics) {
        this.unionEconomics = unionEconomics;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    public void addInvoice(Invoice invoice){
        this.invoices.add(invoice);
    }

    public void addFeeReport(FeeReport feeReport){
        this.feeReports.add(feeReport);
    }

    public List<FeeReport> getFeeReports() {
        return feeReports;
    }

    public void setFeeReports(List<FeeReport> feeReports) {
        this.feeReports = feeReports;
    }

    public List<DebtReport> getDebtReports() {
        return debtReports;
    }

    public void setDebtReports(List<DebtReport> debtReports) {
        this.debtReports = debtReports;
    }

    public void addDebtReport(DebtReport debtReport){
        this.debtReports.add(debtReport);
    }

    public List<PaymentRequest> getPaymentRequests() {
        return paymentRequests;
    }

    public void setPaymentRequests(List<PaymentRequest> paymentRequests) {
        this.paymentRequests = paymentRequests;
    }

    public void addPaymentRequest(PaymentRequest paymentRequest){
        this.paymentRequests.add(paymentRequest);
    }
}
