package dk.clubtimize.clubtimize.models.economics;

import javax.persistence.*;

@Entity
public class BankinfoContainer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne
    @JoinColumn(name = "unionEconomics_id")
    private UnionEconomics unionEconomics;

    private String informationDate;

    private String accountNumber;
    private String registrationNumber;
    private String bankName;
    private String phoneNumber;

    private boolean verified;
    private String verifiedBy;

    public BankinfoContainer() {
    }

    public BankinfoContainer(UnionEconomics unionEconomics, String informationDate, String accountNumber, String registrationNumber, String bankName, String phoneNumber) {
        this.unionEconomics = unionEconomics;
        this.informationDate = informationDate;
        this.accountNumber = accountNumber;
        this.registrationNumber = registrationNumber;
        this.bankName = bankName;
        this.phoneNumber = phoneNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UnionEconomics getUnionEconomics() {
        return unionEconomics;
    }

    public void setUnionEconomics(UnionEconomics unionEconomics) {
        this.unionEconomics = unionEconomics;
    }

    public String getInformationDate() {
        return informationDate;
    }

    public void setInformationDate(String informationDate) {
        this.informationDate = informationDate;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }
}
