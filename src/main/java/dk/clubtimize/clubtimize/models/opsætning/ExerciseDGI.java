package dk.clubtimize.clubtimize.models.opsætning;

import dk.clubtimize.clubtimize.models.training.TrainingModule;
import org.w3c.dom.stylesheets.LinkStyle;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ExerciseDGI {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "exerciseName")
    private String name;
    @Column(name = "exerciseLead")
    private String lead;
    @Column(name = "exerciseSport")
    private String sport;
    @Column(name = "exerciseCategory")
    private String category;
    @Column(name = "exerciseEmneord")
    private String emneord;
    @Column(name = "exerciseTime")
    private String time;
    @Column(name = "exerciseLevel")
    private String level;
    @Column(name = "exerciseAgegroup")
    private String agegroup;
    @Column(name = "exerciseParticipants")
    private String participants;

    @Column(name = "exerciseDescription", length = 10000)
    private String description;

    private String videoUrl;

    @OneToMany(mappedBy = "exerciseDGI")
    private List<TrainingModule> trainingModules = new ArrayList<>();

    public ExerciseDGI() {
    }

    public ExerciseDGI(String name, String lead, String sport, String category, String emneord, String time, String level, String agegroup, String participants, String description, String videoUrl) {
        this.name = name;
        this.lead = lead;
        this.sport = sport;
        this.category = category;
        this.emneord = emneord;
        this.time = time;
        this.level = level;
        this.agegroup = agegroup;
        this.participants = participants;
        this.description = description;
        this.videoUrl = videoUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLead() {
        return lead;
    }

    public void setLead(String lead) {
        this.lead = lead;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getEmneord() {
        return emneord;
    }

    public void setEmneord(String emneord) {
        this.emneord = emneord;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getAgegroup() {
        return agegroup;
    }

    public void setAgegroup(String agegroup) {
        this.agegroup = agegroup;
    }

    public String getParticipants() {
        return participants;
    }

    public void setParticipants(String participants) {
        this.participants = participants;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
}
