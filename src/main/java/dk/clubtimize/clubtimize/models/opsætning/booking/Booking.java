package dk.clubtimize.clubtimize.models.opsætning.booking;

import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.opsætning.Court;

import javax.persistence.*;

@Entity
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "singleBookingPeriod_id")
    private SingleBookingPeriod singleBookingPeriod;

    @ManyToOne
    @JoinColumn(name = "seasonPeriodBooking_id")
    private SeasonBookingPeriod seasonBookingPeriod;

    @ManyToOne
    @JoinColumn(name = "timePeriod_id")
    private TimePeriod timePeriod;

    @ManyToOne
    @JoinColumn(name = "court_id")
    private Court court;

    private boolean booked;

    // Date of booking
    private String date;
    // Begining hour of booking
    private String startHour;
    // Ending hour of booking
    private String endHour;

    // Price of booking
    private float price;
    // Name of recipient
    private String recipientName;
    // Email of recipient
    private String recipientEmail;
    // Email of recipient
    private String recipientAddress;
    // Phone of recipient
    private String recipientPhone;
    // What time it was booked
    private String bookingTime;

    @OneToOne
    @JoinColumn(name = "invoice_id")
    private Invoice invoice;

    public Booking() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getEndHour() {
        return endHour;
    }

    public void setEndHour(String endHour) {
        this.endHour = endHour;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getRecipientPhone() {
        return recipientPhone;
    }

    public void setRecipientPhone(String recipientPhone) {
        this.recipientPhone = recipientPhone;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public Court getCourt() {
        return court;
    }

    public void setCourt(Court court) {
        this.court = court;
    }

    public SingleBookingPeriod getSingleBookingPeriod() {
        return singleBookingPeriod;
    }

    public void setSingleBookingPeriod(SingleBookingPeriod singleBookingPeriod) {
        this.singleBookingPeriod = singleBookingPeriod;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isBooked() {
        return booked;
    }

    public void setBooked(boolean booked) {
        this.booked = booked;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public String getRecipientAddress() {
        return recipientAddress;
    }

    public void setRecipientAddress(String recipientAddress) {
        this.recipientAddress = recipientAddress;
    }

    public SeasonBookingPeriod getSeasonBookingPeriod() {
        return seasonBookingPeriod;
    }

    public void setSeasonBookingPeriod(SeasonBookingPeriod seasonBookingPeriod) {
        this.seasonBookingPeriod = seasonBookingPeriod;
    }

    public TimePeriod getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(TimePeriod timePeriod) {
        this.timePeriod = timePeriod;
    }
}
