package dk.clubtimize.clubtimize.models.opsætning;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.opsætning.booking.SeasonBookingPeriod;
import dk.clubtimize.clubtimize.models.opsætning.booking.SingleBookingPeriod;
import dk.clubtimize.clubtimize.models.training.Training;
import dk.clubtimize.clubtimize.models.training.TrainingSchedule;
import dk.clubtimize.clubtimize.models.union.Union;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "trainingHall")
public class Hall implements UnionValidator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int hallId;
    @ManyToOne
    @JoinColumn(name = "union_id")
    private Union union;
    private String title;
    private float  longitude;
    private float latitude;

    @OneToMany(mappedBy = "hall")
    private List<TrainingSchedule> trainingSchedules = new ArrayList<>();

    @OneToMany(mappedBy = "hall")
    private List<Training> trainings;

    private int courtsAvalible;
    @OneToMany(mappedBy = "hall", cascade = CascadeType.ALL)
    private List<Court> courts = new ArrayList<>();

    @OneToMany(mappedBy = "hall")
    private List<SingleBookingPeriod> singleBookingPeriods = new ArrayList<>();

    @OneToMany(mappedBy = "hall")
    private List<SeasonBookingPeriod> seasonBookingPeriods = new ArrayList<>();

    public Hall() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return union;
    }

    public int getHallId() {
        return hallId;
    }

    public void setHallId(int hallId) {
        this.hallId = hallId;
    }

    public Union getUnion() {
        return union;
    }

    public void setUnion(Union union) {
        this.union = union;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public int getCourtsAvalible() {
        return courtsAvalible;
    }

    public void setCourtsAvalible(int courtsAvalible) {
        this.courtsAvalible = courtsAvalible;
    }

    public List<TrainingSchedule> getTrainingSchedules() {
        return trainingSchedules;
    }

    public void setTrainingSchedules(List<TrainingSchedule> trainingSchedules) {
        this.trainingSchedules = trainingSchedules;
    }

    public List<Training> getTrainings() {
        return trainings;
    }

    public void setTrainings(List<Training> trainings) {
        this.trainings = trainings;
    }

    public List<Court> getCourts() {
        return courts;
    }

    public void setCourts(List<Court> courts) {
        this.courts = courts;
    }

    public void addCourt(Court court){
        this.courts.add(court);
    }

    public List<SingleBookingPeriod> getSingleBookingPeriods() {
        return singleBookingPeriods;
    }

    public void setSingleBookingPeriods(List<SingleBookingPeriod> singleBookingPeriods) {
        this.singleBookingPeriods = singleBookingPeriods;
    }

    public void addBookingPeriod(SingleBookingPeriod singleBookingPeriod){
        this.singleBookingPeriods.add(singleBookingPeriod);
    }

    public boolean hasBookings(){
        boolean hasBookings = false;

        for(Court court : this.courts){
            if (court.getBookings().size() != 0){
                hasBookings = true;
            }
        }

        return hasBookings;
    }

    public List<SeasonBookingPeriod> getSeasonBookingPeriods() {
        return seasonBookingPeriods;
    }

    public void setSeasonBookingPeriods(List<SeasonBookingPeriod> seasonBookingPeriods) {
        this.seasonBookingPeriods = seasonBookingPeriods;
    }

    public void addBookingPeriodSeason(SeasonBookingPeriod seasonBookingPeriod){
        this.seasonBookingPeriods.add(seasonBookingPeriod);
    }
}
