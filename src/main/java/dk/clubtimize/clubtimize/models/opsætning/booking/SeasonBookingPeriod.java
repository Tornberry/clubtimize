package dk.clubtimize.clubtimize.models.opsætning.booking;

import dk.clubtimize.clubtimize.models.opsætning.Hall;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class SeasonBookingPeriod {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "hall_id")
    private Hall hall;

    @OneToMany(mappedBy = "seasonBookingPeriod")
    private List<Booking> bookings = new ArrayList<>();

    @OneToMany(mappedBy = "seasonBookingPeriod", cascade = CascadeType.ALL)
    private List<TimePeriod> timePeriods = new ArrayList<>();

    // Season description
    private String seasonStartDate;
    private String seasonEndDate;

    // Booking description
    private String dayOfWeek;
    private String startTime;
    private String endTime;
    private String bookingInterval;

    // Price
    private float price;

    // Price for single
    private float singlePrice;

    private boolean reserveSingleBookings;

    public SeasonBookingPeriod() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }

    public String getSeasonStartDate() {
        return seasonStartDate;
    }

    public void setSeasonStartDate(String seasonStartDate) {
        this.seasonStartDate = seasonStartDate;
    }

    public String getSeasonEndDate() {
        return seasonEndDate;
    }

    public void setSeasonEndDate(String seasonEndDate) {
        this.seasonEndDate = seasonEndDate;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getBookingInterval() {
        return bookingInterval;
    }

    public void setBookingInterval(String bookingInterval) {
        this.bookingInterval = bookingInterval;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void addBooking(Booking booking){
        this.bookings.add(booking);
    }

    public List<TimePeriod> getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(List<TimePeriod> timePeriods) {
        this.timePeriods = timePeriods;
    }

    public void addTimePeriod(TimePeriod timePeriod){
        this.timePeriods.add(timePeriod);
    }

    public boolean isReserveSingleBookings() {
        return reserveSingleBookings;
    }

    public void setReserveSingleBookings(boolean reserveSingleBookings) {
        this.reserveSingleBookings = reserveSingleBookings;
    }

    public float getSinglePrice() {
        return singlePrice;
    }

    public void setSinglePrice(float singlePrice) {
        this.singlePrice = singlePrice;
    }
}
