package dk.clubtimize.clubtimize.models.opsætning;

import dk.clubtimize.clubtimize.models.opsætning.booking.Booking;
import dk.clubtimize.clubtimize.models.opsætning.booking.TimePeriod;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Court {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "hall_id")
    private Hall hall;

    private String title;

    @OneToMany(mappedBy = "court", cascade = CascadeType.ALL)
    private List<Booking> bookings = new ArrayList<>();

    @OneToMany(mappedBy = "court")
    private List<TimePeriod> timePeriods = new ArrayList<>();

    public Court() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }

    public void addBookings(Booking booking){
        this.bookings.add(booking);
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public List<TimePeriod> getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(List<TimePeriod> timePeriods) {
        this.timePeriods = timePeriods;
    }

    public void addTimePeriod(TimePeriod timePeriod){
        this.timePeriods.add(timePeriod);
    }
}

