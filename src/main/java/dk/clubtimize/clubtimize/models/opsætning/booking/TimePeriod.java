package dk.clubtimize.clubtimize.models.opsætning.booking;

import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.opsætning.Court;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TimePeriod {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "seasonPeriodBooking_id")
    private SeasonBookingPeriod seasonBookingPeriod;

    @OneToMany(mappedBy = "timePeriod")
    private List<Booking> bookings = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "court_id")
    private Court court;

    private String timeFrom;
    private String timeTo;

    private boolean activeInvitation = false;
    private boolean isReservedByInvited;
    private String inviteName;
    private String inviteEmail;

    private boolean isBooked = false;

    @OneToOne
    @JoinColumn(name = "invoice_id")
    private Invoice invoice;

    // Name of recipient
    private String recipientName;
    // Email of recipient
    private String recipientEmail;
    // Email of recipient
    private String recipientAddress;
    // Phone of recipient
    private String recipientPhone;
    // What time it was booked
    private String bookingTime;

    @OneToMany(mappedBy = "timePeriod", cascade = CascadeType.ALL)
    private List<TimePeriodPlayer> players = new ArrayList<>();

    public TimePeriod() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SeasonBookingPeriod getSeasonBookingPeriod() {
        return seasonBookingPeriod;
    }

    public void setSeasonBookingPeriod(SeasonBookingPeriod seasonBookingPeriod) {
        this.seasonBookingPeriod = seasonBookingPeriod;
    }

    public String getInviteName() {
        return inviteName;
    }

    public void setInviteName(String inviteName) {
        this.inviteName = inviteName;
    }

    public String getInviteEmail() {
        return inviteEmail;
    }

    public void setInviteEmail(String inviteEmail) {
        this.inviteEmail = inviteEmail;
    }

    public boolean isBooked() {
        return isBooked;
    }

    public void setBooked(boolean booked) {
        isBooked = booked;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public void addBooking(Booking booking){
        this.bookings.add(booking);
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }

    public Court getCourt() {
        return court;
    }

    public void setCourt(Court court) {
        this.court = court;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getRecipientAddress() {
        return recipientAddress;
    }

    public void setRecipientAddress(String recipientAddress) {
        this.recipientAddress = recipientAddress;
    }

    public String getRecipientPhone() {
        return recipientPhone;
    }

    public void setRecipientPhone(String recipientPhone) {
        this.recipientPhone = recipientPhone;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public boolean isActiveInvitation() {
        return activeInvitation;
    }

    public void setActiveInvitation(boolean activeInvitation) {
        this.activeInvitation = activeInvitation;
    }

    public boolean isReservedByInvited() {
        return isReservedByInvited;
    }

    public void setReservedByInvited(boolean reservedByInvited) {
        isReservedByInvited = reservedByInvited;
    }

    public List<TimePeriodPlayer> getPlayers() {
        return players;
    }

    public void setPlayers(List<TimePeriodPlayer> players) {
        this.players = players;
    }

    public void addPlayer(TimePeriodPlayer timePeriodPlayer){
        this.players.add(timePeriodPlayer);
    }
}
