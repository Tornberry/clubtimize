package dk.clubtimize.clubtimize.models.opsætning;

import dk.clubtimize.clubtimize.controllers.support.UnionValidator;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.Union;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TeamDepartment implements UnionValidator {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String title;
    private String color;
    private String textColor;

    @OneToMany(mappedBy = "teamDepartment")
    private List<Team> teams = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "union_id")
    private Union union;

    public TeamDepartment() {
    }

    @Override
    public Union getAssosiatedUnion() {
        return union;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public Union getUnion() {
        return union;
    }

    public void setUnion(Union union) {
        this.union = union;
    }

    public void removeTeam(Team team){
        this.teams.remove(team);
    }

    public void addTeam(Team team){
        this.teams.add(team);
    }
}
