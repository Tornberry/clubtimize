package dk.clubtimize.clubtimize.models.adapters;

public class ModuleCommentAdapter{
    private int moduleId;
    private String comment;

    public ModuleCommentAdapter() {
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
