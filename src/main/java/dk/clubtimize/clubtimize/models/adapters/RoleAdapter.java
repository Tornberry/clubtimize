package dk.clubtimize.clubtimize.models.adapters;

public class RoleAdapter {

    private String roleName;

    // Administration
    private boolean canAdministrate;
    private boolean canSeeAdministrationPages;

    // Training overview
    private boolean canSeeAllTeams;

    // Training
    private boolean canUseTrainerFeatures;

    public RoleAdapter() {
    }

    public boolean isCanAdministrate() {
        return canAdministrate;
    }

    public void setCanAdministrate(boolean canAdministrate) {
        this.canAdministrate = canAdministrate;
    }

    public boolean isCanSeeAdministrationPages() {
        return canSeeAdministrationPages;
    }

    public void setCanSeeAdministrationPages(boolean canSeeAdministrationPages) {
        this.canSeeAdministrationPages = canSeeAdministrationPages;
    }

    public boolean isCanSeeAllTeams() {
        return canSeeAllTeams;
    }

    public void setCanSeeAllTeams(boolean canSeeAllTeams) {
        this.canSeeAllTeams = canSeeAllTeams;
    }

    public boolean isCanUseTrainerFeatures() {
        return canUseTrainerFeatures;
    }

    public void setCanUseTrainerFeatures(boolean canUseTrainerFeatures) {
        this.canUseTrainerFeatures = canUseTrainerFeatures;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
