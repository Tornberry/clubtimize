package dk.clubtimize.clubtimize.models.adapters;

public class ModuleAdapter {
    private String title;
    private int approxTime;
    private String description;
    private int exerciseId;

    private int trainingId;

    public ModuleAdapter() {
    }

    public ModuleAdapter(String title, int approxTime, String description, int exerciseId, int trainingId) {
        this.title = title;
        this.approxTime = approxTime;
        this.description = description;
        this.exerciseId = exerciseId;
        this.trainingId = trainingId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getApproxTime() {
        return approxTime;
    }

    public void setApproxTime(int approxTime) {
        this.approxTime = approxTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTrainingId() {
        return trainingId;
    }

    public void setTrainingId(int trainingId) {
        this.trainingId = trainingId;
    }

    public int getExerciseId() {
        return exerciseId;
    }

    public void setExerciseId(int exerciseId) {
        this.exerciseId = exerciseId;
    }
}
