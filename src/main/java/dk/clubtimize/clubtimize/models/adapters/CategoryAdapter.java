package dk.clubtimize.clubtimize.models.adapters;

public class CategoryAdapter {

    private int id;
    private String title;
    private String color;

    public CategoryAdapter() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
