package dk.clubtimize.clubtimize.models.adapters;

public class HallAdapter {

    private int hallId;
    private String title;
    private int courtsAvalible;
    private float longitude;
    private float latitude;

    public HallAdapter() {
    }

    public int getHallId() {
        return hallId;
    }

    public void setHallId(int hallId) {
        this.hallId = hallId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCourtsAvalible() {
        return courtsAvalible;
    }

    public void setCourtsAvalible(int courtsAvalible) {
        this.courtsAvalible = courtsAvalible;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }
}
