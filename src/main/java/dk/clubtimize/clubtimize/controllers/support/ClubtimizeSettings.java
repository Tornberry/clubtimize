package dk.clubtimize.clubtimize.controllers.support;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ClubtimizeSettings {

    private static Logger logger = LoggerFactory.getLogger(ClubtimizeSettings.class);

    private static String readSettings(){
        String path = "C://Clubtimize/settings.json";

        if (!checkISettingsExist(path)){
            if (createNewSettingsFile(path)){
                return getSettings(path);
            } else {
                return null;
            }
        } else {
            return getSettings(path);
        }
    }

    private static String getSettings(String path) {
        try {
            String settings = new String(Files.readAllBytes(Paths.get(path)));
            logger.trace("Returned settings with success");
            return settings;
        } catch (IOException e) {
            logger.error("Could not read settings: " + e.getMessage());
            return null;
        }
    }

    private static boolean createNewSettingsFile(String path) {
        File file = new File(path);
        try {
            boolean createdFile = file.createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(getDefaultSettingsString());
            writer.close();
            logger.info("Created new settings file with success");
            logger.warn("The default environment is set to 'test'. Go to " + path + " and change these settings if necessary!");
            return createdFile;
        } catch (IOException e) {
            logger.error("Could not create new settings file: " + e.getMessage());
            return false;
        }

    }

    private static String getDefaultSettingsString() {
        JSONObject main = new JSONObject();
        main.put("Enviroment", "test");

        JSONObject resources = new JSONObject();
        resources.put("UnionLogoLocation", "C://Clubtimize" + "/resources/graphics/unionlogos/");
        main.put("Resources", resources);

        return main.toString();
    }

    private static boolean checkISettingsExist(String path) {
        File file = new File(path);
        return file.exists();
    }

    public static boolean isProductionEnvironment(){
        JSONObject jsonObject = new JSONObject(readSettings());
        return jsonObject.getString("Enviroment").equals("production");
    }

    public static String getUnionLogoResourcePath() {
        JSONObject jsonObject = new JSONObject(readSettings());
        return jsonObject.getJSONObject("Resources").getString("UnionLogoLocation");
    }
}
