package dk.clubtimize.clubtimize.controllers.support;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class CurrencyPipe {

    public static String floatToDKKFormat(float value){
        DecimalFormatSymbols danishSymbol = new DecimalFormatSymbols(Locale.getDefault());
        danishSymbol.setDecimalSeparator(',');
        danishSymbol.setGroupingSeparator('.');
        DecimalFormat formatter = new DecimalFormat("###,##0.00", danishSymbol);
        return formatter.format(value) + " kr.";
    }
}
