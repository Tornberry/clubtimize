package dk.clubtimize.clubtimize.controllers.support;

public enum PermissionEnum {
    canAdministrate, canSeeAdministrationPages, canSeeAllTeams, canUseTrainerFeatures
}
