package dk.clubtimize.clubtimize.controllers.support.mail;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.economics.invoice.InvoiceItem;
import dk.clubtimize.clubtimize.models.opsætning.booking.TimePeriod;
import dk.clubtimize.clubtimize.models.union.RecruitmentInvitation;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.user.User;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

public class MailSender {

    public static void playerWelcomeMail(User user) {
        List<BasicNameValuePair> params = MailLibrary.getBasicValuePairs("Velkommen til Unie", "Unie", "hej@unie.dk", user.getEmail(), "playerwelcome");

        HashMap<String, String> variables = new HashMap<>();
        variables.put("firstname", user.getFirstname());
        variables.put("clubname", user.getUnion().getName());
        variables.put("team", user.getUnionAssosiation().getPlayerPositions().get(0).getTeam().getTitle());

        MailLibrary.sendEmail(params, variables, user.getEmail(), false);
    }

    public static void adminWelcomeMail(User user) {
        List<BasicNameValuePair> params = MailLibrary.getBasicValuePairs("Velkommen til Unieunie.d", "Unie", "hej@unie.dk", user.getEmail(), "adminwelcome");

        HashMap<String, String> variables = new HashMap<>();
        variables.put("clubname", user.getUnion().getName());

        MailLibrary.sendEmail(params, variables, user.getEmail(), false);
    }

    public static void resetPassword(User user, String resetId) {
        List<BasicNameValuePair> params = MailLibrary.getBasicValuePairs("Nulstil dit kodeord", "Unie", "hej@unie.dk", user.getEmail(), "forgotpassword");

        HashMap<String, String> variables = new HashMap<>();
        variables.put("link", "http://app.clubtimize.dk/resetpassword?c=" + resetId);

        MailLibrary.sendEmail(params, variables, user.getEmail(), false);
    }

    public static void deleteUser(User user) {
        List<BasicNameValuePair> params = MailLibrary.getBasicValuePairs("Din bruger er slettet", "Unie", "hej@unie.dk", user.getEmail(), "deleteuser");

        HashMap<String, String> variables = new HashMap<>();
        variables.put("clubname", user.getUnion().getName());
        variables.put("unionmail", user.getUnion().getAdminAccountEmail());

        MailLibrary.sendEmail(params, variables, user.getEmail(), false);
    }

    public static void acceptPaymentRequest(Union union, String payoutdate) {
        List<BasicNameValuePair> params = MailLibrary.getBasicValuePairs("Udbetaling af balance godkendt", "Unie", "hej@unie.dk", union.getAdminAccountEmail(), "payoutrequestaccepted");

        HashMap<String, String> variables = new HashMap<>();
        variables.put("date", DateFactory.getCurrentDate());
        variables.put("payoutdate", payoutdate);

        MailLibrary.sendEmail(params, variables, union.getAdminAccountEmail(), false);
    }

    public static void denyPaymentRequest(Union union) {
        List<BasicNameValuePair> params = MailLibrary.getBasicValuePairs("Udbetaling af balance afvist", "Unie", "hej@unie.dk", union.getAdminAccountEmail(), "payoutrequestdenied");

        HashMap<String, String> variables = new HashMap<>();
        variables.put("date", DateFactory.getCurrentDate());

        MailLibrary.sendEmail(params, variables, union.getAdminAccountEmail(), false);
    }

    public static void inviteCoach(RecruitmentInvitation recruitmentInvitation) {
        List<BasicNameValuePair> params = MailLibrary.getBasicValuePairs("Invitation til at blive træner i foreningen " + recruitmentInvitation.getUnion().getName(), "Unie", "hej@unie.dk", recruitmentInvitation.getEmail(), "coachinvitation");

        HashMap<String, String> variables = new HashMap<>();
        variables.put("clubname", recruitmentInvitation.getUnion().getName());
        variables.put("team", recruitmentInvitation.getTeam().getTitle());
        variables.put("inv", String.valueOf(recruitmentInvitation.getInvNumber()));

        MailLibrary.sendEmail(params, variables, recruitmentInvitation.getEmail(), false);
    }

    public static void invitePlayer(RecruitmentInvitation recruitmentInvitation) {
        List<BasicNameValuePair> params = MailLibrary.getBasicValuePairs("Invitation til at blive spiller i foreningen " + recruitmentInvitation.getUnion().getName(), "Unie", "hej@unie.dk", recruitmentInvitation.getEmail(), "playerinvitation");

        HashMap<String, String> variables = new HashMap<>();
        variables.put("clubname", recruitmentInvitation.getUnion().getName());
        variables.put("team", recruitmentInvitation.getTeam().getTitle());
        variables.put("inv", String.valueOf(recruitmentInvitation.getInvNumber()));

        MailLibrary.sendEmail(params, variables, recruitmentInvitation.getEmail(), false);
    }

    public static void sendInvoice(Invoice invoice, String email, String fullName, String address) {
        List<BasicNameValuePair> params = MailLibrary.getBasicValuePairs("Faktura", "Unie", "hej@unie.dk", email, "invoice");

        JSONArray jsonArray = new JSONArray();

        for (InvoiceItem invoiceItem : invoice.getItems()){
            JSONObject itemObject = new JSONObject();

            itemObject.put("item", invoiceItem.getDescription());
            itemObject.put("price", invoiceItem.getTotalCost());

            jsonArray.put(itemObject);
        }

        HashMap<String, String> variables = new HashMap<>();
        variables.put("totalPrice", String.valueOf(invoice.getTotalCostWithAdditional()));
        variables.put("name", fullName);
        variables.put("address", address);
        variables.put("date", invoice.getDate());
        variables.put("invoiceNumber", "#" + invoice.getInvoiceNumber());
        variables.put("invoiceItems", jsonArray.toString());

        MailLibrary.sendEmail(params, variables, email, true);
    }

    public static void invitePeriodBooking(TimePeriod timePeriod){
        List<BasicNameValuePair> params = MailLibrary.getBasicValuePairs("Invitation til at booke en bane i en periode", "Unie", "hej@unie.dk", timePeriod.getInviteEmail(), "seasonbookingperiodinvitation");

        HashMap<String, String> variables = new HashMap<>();
        variables.put("clubname", timePeriod.getCourt().getHall().getUnion().getName());
        variables.put("court", timePeriod.getCourt().getTitle());
        variables.put("hall", timePeriod.getCourt().getHall().getTitle());
        variables.put("dayOfWeek", timePeriod.getSeasonBookingPeriod().getDayOfWeek());
        variables.put("timeFrom", timePeriod.getTimeFrom());
        variables.put("timeTo", timePeriod.getTimeTo());
        variables.put("id", String.valueOf(timePeriod.getId()));

        MailLibrary.sendEmail(params, variables, timePeriod.getInviteEmail(), false);
    }

    public static void addMemberToTeam(User user, Team team){
        List<BasicNameValuePair> params = MailLibrary.getBasicValuePairs("Du er tilføjet til et hold", "Unie", "hej@unie.dk", user.getEmail(), "addplayertoteam");

        HashMap<String, String> variables = new HashMap<>();
        variables.put("clubname", user.getUnion().getName());
        variables.put("team", team.getTitle());


        MailLibrary.sendEmail(params, variables, user.getEmail(), false);
    }

    public static void newSeasonForMembers(User user){
        List<BasicNameValuePair> params = MailLibrary.getBasicValuePairs("Ny sæson er oprettet", "Unie", "hej@unie.dk", user.getEmail(), "newseasonforplayers");

        HashMap<String, String> variables = new HashMap<>();
        variables.put("clubname", user.getUnion().getName());


        MailLibrary.sendEmail(params, variables, user.getEmail(), false);
    }
}
