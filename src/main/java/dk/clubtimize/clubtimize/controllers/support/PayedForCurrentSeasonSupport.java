package dk.clubtimize.clubtimize.controllers.support;

import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PayedForCurrentSeasonSupport {

    private UnionSeasonStringService unionSeasonStringService;

    @Autowired
    public PayedForCurrentSeasonSupport(UnionSeasonStringService unionSeasonStringService) {
        this.unionSeasonStringService = unionSeasonStringService;
    }

    public boolean payedForCurrentSeason(User user, Team team){
        for (UserSeason userSeason : user.getUserSeasons()){
            if (userSeason.getUnionSeason().getSeasonYear().equals(unionSeasonStringService.getSeasonStringByUnionSeason(user.getUnion()))){
                for (TeamSeason teamSeason : userSeason.getTeamSeasons()){
                    if (teamSeason.getTeam() == team){
                        return teamSeason.isPaymentSuccessful();
                    }
                }
            }
        }

        return false;
    }
}
