package dk.clubtimize.clubtimize.controllers.support;

import dk.clubtimize.clubtimize.models.union.matches.Match;
import dk.clubtimize.clubtimize.models.union.matches.matchtypes.*;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.services.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
public class EloCalculator {

    private UserService userService;

    public EloCalculator(UserService userService) {
        this.userService = userService;
    }

    public void playMatch(Match match) {
        determineWinner(match, match.getLowestMMRTeam());
    }

    private void determineWinner(Match match, Object lowestMMRTeam) {
        if (lowestMMRTeam.getClass() == MensSingleTeam.class){
            MensSingleTeam mensSingleTeamLowest = (MensSingleTeam) lowestMMRTeam;

            if (mensSingleTeamLowest == match.getMensSingleTeams().get(0)) {
                resolveMensSingleTeam1(match);
            } else {
                resolveMensSingleTeam2(match);
            }
        } else if (lowestMMRTeam.getClass() == WomensSingleTeam.class){
            WomensSingleTeam womensSingleTeamLowest = (WomensSingleTeam) lowestMMRTeam;

            if (womensSingleTeamLowest == match.getWomensSingleTeams().get(0)) {
                resolveWomensSingleTeam1(match);
            } else {
                resolveWomensSingleTeam2(match);
            }
        } else if (lowestMMRTeam.getClass() == MensDoubleTeam.class){
            MensDoubleTeam mensDoubleTeamLowest = (MensDoubleTeam) lowestMMRTeam;

            if (mensDoubleTeamLowest == match.getMensDoubleTeams().get(0)) {
                resolveMensDoubleTeam1(match);
            } else {
                resolveMensDoubleTeam2(match);
            }
        } else if (lowestMMRTeam.getClass() == WomensDoubleTeam.class){
            WomensDoubleTeam womensDoubleTeamLowest = (WomensDoubleTeam) lowestMMRTeam;

            if (womensDoubleTeamLowest == match.getWomensDoubleTeams().get(0)) {
                resolveWomensDoubleTeam1(match);
            } else {
                resolveWomensDoubleTeam2(match);
            }
        } else if (lowestMMRTeam.getClass() == MixedDoubleTeam.class){
            MixedDoubleTeam mixedDoubleTeamLowest = (MixedDoubleTeam) lowestMMRTeam;

            if (mixedDoubleTeamLowest == match.getMixedDoubleTeams().get(0)) {
                resolveMixedDoubleTeam1(match);
            } else {
                resolveMixedDoubleTeam2(match);
            }
        }
    }

    private int[] calculateMMR(float lowestMMR, float highestMMR, float[] winChance, boolean didLowestWin) {
        if (didLowestWin) {
            float lowestMMRNew = lowestMMR + 30 * (1 - winChance[0]);
            float highestMMRNew = highestMMR + 30 * (0 - winChance[1]);
            return new int[]{(int) lowestMMRNew, (int) highestMMRNew};
        } else {
            float lowestMMRNew = lowestMMR + 30 * (0 - winChance[0]);
            float highestMMRNew = highestMMR + 30 * (1 - winChance[1]);
            return new int[]{(int) lowestMMRNew, (int) highestMMRNew};
        }
    }

    private float[] calculateWinChance(float a, float b) {
        double x = 1;
        double y = 1 + Math.pow(10, (b - a) / 400);
        double chance = x / y;

        return new float[]{(float) chance, (float) (1 - chance)};
    }

    private <t extends MatchTeam> void givePoints(t lowestMMRTeam, t highestMMRTeam, boolean didLowestWin) {
        if (lowestMMRTeam.getClass() == MensSingleTeam.class){
            MensSingleTeam mensSingleTeamLowst = (MensSingleTeam) lowestMMRTeam;
            MensSingleTeam mensSingleTeamHighest = (MensSingleTeam) highestMMRTeam;

            float[] winChance = calculateWinChance(lowestMMRTeam.getTeamMMR(), highestMMRTeam.getTeamMMR());
            int[] newMMR = calculateMMR(lowestMMRTeam.getTeamMMR(), highestMMRTeam.getTeamMMR(),winChance, didLowestWin);

            mensSingleTeamLowst.getPlayer().getMatchStats().getEloPoints().setMensSingleElo(newMMR[0]);
            mensSingleTeamHighest.getPlayer().getMatchStats().getEloPoints().setMensSingleElo(newMMR[1]);

            userService.saveOrUpdate(mensSingleTeamLowst.getPlayer());
            userService.saveOrUpdate(mensSingleTeamHighest.getPlayer());
        } else if (lowestMMRTeam.getClass() == WomensSingleTeam.class){
            WomensSingleTeam womensSingleTeamLowst = (WomensSingleTeam) lowestMMRTeam;
            WomensSingleTeam womensSingleTeamHighest = (WomensSingleTeam) highestMMRTeam;

            float[] winChance = calculateWinChance(lowestMMRTeam.getTeamMMR(), highestMMRTeam.getTeamMMR());
            int[] newMMR = calculateMMR(lowestMMRTeam.getTeamMMR(), highestMMRTeam.getTeamMMR(),winChance, didLowestWin);

            womensSingleTeamLowst.getPlayer().getMatchStats().getEloPoints().setWomensSingleElo(newMMR[0]);
            womensSingleTeamHighest.getPlayer().getMatchStats().getEloPoints().setWomensSingleElo(newMMR[1]);

            userService.saveOrUpdate(womensSingleTeamLowst.getPlayer());
            userService.saveOrUpdate(womensSingleTeamHighest.getPlayer());
        }
    }

    private int distributeTeamPoints(float MMRToDistribute, int myMMR, int teamMemberMMR){
        float MMRDifference = (float)myMMR/(float)teamMemberMMR;
        float myMMrToAdd = (MMRToDistribute/2)*MMRDifference;

        return (int) (myMMrToAdd);
    }

    private <t extends MatchTeam> void giveTeamPoints(t lowestMMRTeam, t highestMMRTeam, boolean didLowestWin){
        if (lowestMMRTeam.getClass() == MensDoubleTeam.class){
            MensDoubleTeam mensDoubleTeamLowest = (MensDoubleTeam) lowestMMRTeam;
            MensDoubleTeam mensDoubleTeamHighest = (MensDoubleTeam) highestMMRTeam;

            float[] winChance = calculateWinChance(lowestMMRTeam.getTeamMMR(), highestMMRTeam.getTeamMMR());
            int[] newMMR = calculateMMR(lowestMMRTeam.getTeamMMR(), highestMMRTeam.getTeamMMR(),winChance, didLowestWin);

            int player1_lowest_mmr = mensDoubleTeamLowest.getPlayers().get(0).getMatchStats().getEloPoints().getMensDoubleElo();
            int player2_lowest_mmr = mensDoubleTeamLowest.getPlayers().get(1).getMatchStats().getEloPoints().getMensDoubleElo();
            int player1_highest_mmr = mensDoubleTeamHighest.getPlayers().get(0).getMatchStats().getEloPoints().getMensDoubleElo();
            int player2_highest_mmr = mensDoubleTeamHighest.getPlayers().get(1).getMatchStats().getEloPoints().getMensDoubleElo();

            int player1_team1 = mensDoubleTeamLowest.getPlayers().get(0).getMatchStats().getEloPoints().getMensDoubleElo() + distributeTeamPoints(newMMR[0] - lowestMMRTeam.getTeamMMR(), player1_lowest_mmr, player2_lowest_mmr);
            mensDoubleTeamLowest.getPlayers().get(0).getMatchStats().getEloPoints().setMensDoubleElo(player1_team1);

            int player2_team1 = mensDoubleTeamLowest.getPlayers().get(1).getMatchStats().getEloPoints().getMensDoubleElo() + distributeTeamPoints(newMMR[0] - lowestMMRTeam.getTeamMMR(), player2_lowest_mmr, player1_lowest_mmr);
            mensDoubleTeamLowest.getPlayers().get(1).getMatchStats().getEloPoints().setMensDoubleElo(player2_team1);

            int player1_team2 = mensDoubleTeamHighest.getPlayers().get(0).getMatchStats().getEloPoints().getMensDoubleElo() + distributeTeamPoints(newMMR[1] - highestMMRTeam.getTeamMMR(), player1_highest_mmr, player2_highest_mmr);
            mensDoubleTeamHighest.getPlayers().get(0).getMatchStats().getEloPoints().setMensDoubleElo(player1_team2);

            int player2_team2 = mensDoubleTeamHighest.getPlayers().get(1).getMatchStats().getEloPoints().getMensDoubleElo() + distributeTeamPoints(newMMR[1] - highestMMRTeam.getTeamMMR(), player2_highest_mmr, player1_highest_mmr);
            mensDoubleTeamHighest.getPlayers().get(1).getMatchStats().getEloPoints().setMensDoubleElo(player2_team2);

            userService.saveOrUpdate(mensDoubleTeamLowest.getPlayers().get(0));
            userService.saveOrUpdate(mensDoubleTeamLowest.getPlayers().get(1));
            userService.saveOrUpdate(mensDoubleTeamHighest.getPlayers().get(0));
            userService.saveOrUpdate(mensDoubleTeamHighest.getPlayers().get(1));
        } else if(lowestMMRTeam.getClass() == WomensDoubleTeam.class){
            WomensDoubleTeam womensDoubleTeamLowest = (WomensDoubleTeam) lowestMMRTeam;
            WomensDoubleTeam womensDoubleTeamHighest = (WomensDoubleTeam) highestMMRTeam;

            float[] winChance = calculateWinChance(lowestMMRTeam.getTeamMMR(), highestMMRTeam.getTeamMMR());
            int[] newMMR = calculateMMR(lowestMMRTeam.getTeamMMR(), highestMMRTeam.getTeamMMR(),winChance, didLowestWin);

            int player1_lowest_mmr = womensDoubleTeamLowest.getPlayers().get(0).getMatchStats().getEloPoints().getWomensDoubleElo();
            int player2_lowest_mmr = womensDoubleTeamLowest.getPlayers().get(1).getMatchStats().getEloPoints().getWomensDoubleElo();
            int player1_highest_mmr = womensDoubleTeamHighest.getPlayers().get(0).getMatchStats().getEloPoints().getWomensDoubleElo();
            int player2_highest_mmr = womensDoubleTeamHighest.getPlayers().get(1).getMatchStats().getEloPoints().getWomensDoubleElo();

            int player1_team1 = womensDoubleTeamLowest.getPlayers().get(0).getMatchStats().getEloPoints().getWomensDoubleElo() + distributeTeamPoints(newMMR[0] - lowestMMRTeam.getTeamMMR(), player1_lowest_mmr, player2_lowest_mmr);
            womensDoubleTeamLowest.getPlayers().get(0).getMatchStats().getEloPoints().setWomensDoubleElo(player1_team1);

            int player2_team1 = womensDoubleTeamLowest.getPlayers().get(1).getMatchStats().getEloPoints().getWomensDoubleElo() + distributeTeamPoints(newMMR[0] - lowestMMRTeam.getTeamMMR(), player2_lowest_mmr, player1_lowest_mmr);
            womensDoubleTeamLowest.getPlayers().get(1).getMatchStats().getEloPoints().setWomensDoubleElo(player2_team1);

            int player1_team2 = womensDoubleTeamHighest.getPlayers().get(0).getMatchStats().getEloPoints().getWomensDoubleElo() + distributeTeamPoints(newMMR[1] - highestMMRTeam.getTeamMMR(), player1_highest_mmr, player2_highest_mmr);
            womensDoubleTeamHighest.getPlayers().get(0).getMatchStats().getEloPoints().setWomensDoubleElo(player1_team2);

            int player2_team2 = womensDoubleTeamHighest.getPlayers().get(1).getMatchStats().getEloPoints().getWomensDoubleElo() + distributeTeamPoints(newMMR[1] - highestMMRTeam.getTeamMMR(), player2_highest_mmr, player1_highest_mmr);
            womensDoubleTeamHighest.getPlayers().get(1).getMatchStats().getEloPoints().setWomensDoubleElo(player2_team2);

            userService.saveOrUpdate(womensDoubleTeamLowest.getPlayers().get(0));
            userService.saveOrUpdate(womensDoubleTeamLowest.getPlayers().get(1));
            userService.saveOrUpdate(womensDoubleTeamHighest.getPlayers().get(0));
            userService.saveOrUpdate(womensDoubleTeamHighest.getPlayers().get(1));
        } else if (lowestMMRTeam.getClass() == MixedDoubleTeam.class){
            MixedDoubleTeam mixedDoubleTeamLowest = (MixedDoubleTeam) lowestMMRTeam;
            MixedDoubleTeam mixedDoubleTeamHighest = (MixedDoubleTeam) highestMMRTeam;

            float[] winChance = calculateWinChance(lowestMMRTeam.getTeamMMR(), highestMMRTeam.getTeamMMR());
            int[] newMMR = calculateMMR(lowestMMRTeam.getTeamMMR(), highestMMRTeam.getTeamMMR(),winChance, didLowestWin);

            int player1_lowest_mmr = mixedDoubleTeamLowest.getPlayers().get(0).getMatchStats().getEloPoints().getMixedDoubleElo();
            int player2_lowest_mmr = mixedDoubleTeamLowest.getPlayers().get(1).getMatchStats().getEloPoints().getMixedDoubleElo();
            int player1_highest_mmr = mixedDoubleTeamHighest.getPlayers().get(0).getMatchStats().getEloPoints().getMixedDoubleElo();
            int player2_highest_mmr = mixedDoubleTeamHighest.getPlayers().get(1).getMatchStats().getEloPoints().getMixedDoubleElo();

            int player1_team1 = mixedDoubleTeamLowest.getPlayers().get(0).getMatchStats().getEloPoints().getMixedDoubleElo() + distributeTeamPoints(newMMR[0] - lowestMMRTeam.getTeamMMR(), player1_lowest_mmr, player2_lowest_mmr);
            mixedDoubleTeamLowest.getPlayers().get(0).getMatchStats().getEloPoints().setMixedDoubleElo(player1_team1);

            int player2_team1 = mixedDoubleTeamLowest.getPlayers().get(1).getMatchStats().getEloPoints().getMixedDoubleElo() + distributeTeamPoints(newMMR[0] - lowestMMRTeam.getTeamMMR(), player2_lowest_mmr, player1_lowest_mmr);
            mixedDoubleTeamLowest.getPlayers().get(1).getMatchStats().getEloPoints().setMixedDoubleElo(player2_team1);

            int player1_team2 = mixedDoubleTeamHighest.getPlayers().get(0).getMatchStats().getEloPoints().getMixedDoubleElo() + distributeTeamPoints(newMMR[1] - highestMMRTeam.getTeamMMR(), player1_highest_mmr, player2_highest_mmr);
            mixedDoubleTeamHighest.getPlayers().get(0).getMatchStats().getEloPoints().setMixedDoubleElo(player1_team2);

            int player2_team2 = mixedDoubleTeamHighest.getPlayers().get(1).getMatchStats().getEloPoints().getMixedDoubleElo() + distributeTeamPoints(newMMR[1] - highestMMRTeam.getTeamMMR(), player2_highest_mmr, player1_highest_mmr);
            mixedDoubleTeamHighest.getPlayers().get(1).getMatchStats().getEloPoints().setMixedDoubleElo(player2_team2);

            userService.saveOrUpdate(mixedDoubleTeamLowest.getPlayers().get(0));
            userService.saveOrUpdate(mixedDoubleTeamLowest.getPlayers().get(1));
            userService.saveOrUpdate(mixedDoubleTeamHighest.getPlayers().get(0));
            userService.saveOrUpdate(mixedDoubleTeamHighest.getPlayers().get(1));
        }

    }

    private <t extends MatchTeam> boolean determineSetWinner(t lowestMMRTeam, t highestMMRTeam, int set) {
        switch(set){
            case 1:
                return lowestMMRTeam.getSet1Points() > highestMMRTeam.getSet1Points();
            case 2:
                return lowestMMRTeam.getSet2Points() > highestMMRTeam.getSet2Points();
            case 3:
                return lowestMMRTeam.getSet3Points() > highestMMRTeam.getSet3Points();
            default:
                return false;
        }
    }

    private void resolveMensSingleTeam2(Match match) {
        boolean didLowestWinSet1 = determineSetWinner(match.getMensSingleTeams().get(1), match.getMensSingleTeams().get(0), 1);
        boolean didLowestWinSet2 = determineSetWinner(match.getMensSingleTeams().get(1), match.getMensSingleTeams().get(0), 2);
        boolean didLowestWinSet3 = determineSetWinner(match.getMensSingleTeams().get(1), match.getMensSingleTeams().get(0), 3);

        boolean extendedMatch = (didLowestWinSet1 && !didLowestWinSet2) || (!didLowestWinSet1 && didLowestWinSet2);

        if (!extendedMatch){
            if (didLowestWinSet1){
                givePoints(match.getMensSingleTeams().get(1), match.getMensSingleTeams().get(0), true);
            } else {
                givePoints(match.getMensSingleTeams().get(1), match.getMensSingleTeams().get(0), false);
            }
        } else {
            if (didLowestWinSet3){
                givePoints(match.getMensSingleTeams().get(1), match.getMensSingleTeams().get(0), true);
            } else {
                givePoints(match.getMensSingleTeams().get(1), match.getMensSingleTeams().get(0), false);
            }
        }
    }

    private void resolveMensSingleTeam1(Match match) {
        boolean didLowestWinSet1 = determineSetWinner(match.getMensSingleTeams().get(0), match.getMensSingleTeams().get(1), 1);
        boolean didLowestWinSet2 = determineSetWinner(match.getMensSingleTeams().get(0), match.getMensSingleTeams().get(1), 2);
        boolean didLowestWinSet3 = determineSetWinner(match.getMensSingleTeams().get(0), match.getMensSingleTeams().get(1), 3);

        boolean extendedMatch = (didLowestWinSet1 && !didLowestWinSet2) || (!didLowestWinSet1 && didLowestWinSet2);

        if (!extendedMatch){
            if (didLowestWinSet1){
                givePoints(match.getMensSingleTeams().get(0), match.getMensSingleTeams().get(1), true);
            } else {
                givePoints(match.getMensSingleTeams().get(0), match.getMensSingleTeams().get(1), false);
            }
        } else {
            if (didLowestWinSet3){
                givePoints(match.getMensSingleTeams().get(0), match.getMensSingleTeams().get(1), true);
            } else {
                givePoints(match.getMensSingleTeams().get(0), match.getMensSingleTeams().get(1), false);
            }
        }
    }

    private void resolveWomensSingleTeam1(Match match) {
        boolean didLowestWinSet1 = determineSetWinner(match.getWomensSingleTeams().get(0), match.getWomensSingleTeams().get(1), 1);
        boolean didLowestWinSet2 = determineSetWinner(match.getWomensSingleTeams().get(0), match.getWomensSingleTeams().get(1), 2);
        boolean didLowestWinSet3 = determineSetWinner(match.getWomensSingleTeams().get(0), match.getWomensSingleTeams().get(1), 3);

        boolean extendedMatch = (didLowestWinSet1 && !didLowestWinSet2) || (!didLowestWinSet1 && didLowestWinSet2);

        if (!extendedMatch){
            if (didLowestWinSet1){
                givePoints(match.getWomensSingleTeams().get(0), match.getWomensSingleTeams().get(1), true);
            } else {
                givePoints(match.getWomensSingleTeams().get(0), match.getWomensSingleTeams().get(1), false);
            }
        } else {
            if (didLowestWinSet3){
                givePoints(match.getWomensSingleTeams().get(0), match.getWomensSingleTeams().get(1), true);
            } else {
                givePoints(match.getWomensSingleTeams().get(0), match.getWomensSingleTeams().get(1), false);
            }
        }
    }

    private void resolveWomensSingleTeam2(Match match) {
        boolean didLowestWinSet1 = determineSetWinner(match.getWomensSingleTeams().get(1), match.getWomensSingleTeams().get(0), 1);
        boolean didLowestWinSet2 = determineSetWinner(match.getWomensSingleTeams().get(1), match.getWomensSingleTeams().get(0), 2);
        boolean didLowestWinSet3 = determineSetWinner(match.getWomensSingleTeams().get(1), match.getWomensSingleTeams().get(0), 3);

        boolean extendedMatch = (didLowestWinSet1 && !didLowestWinSet2) || (!didLowestWinSet1 && didLowestWinSet2);

        if (!extendedMatch){
            if (didLowestWinSet1){
                givePoints(match.getWomensSingleTeams().get(1), match.getWomensSingleTeams().get(0), true);
            } else {
                givePoints(match.getWomensSingleTeams().get(1), match.getWomensSingleTeams().get(0), false);
            }
        } else {
            if (didLowestWinSet3){
                givePoints(match.getWomensSingleTeams().get(1), match.getWomensSingleTeams().get(0), true);
            } else {
                givePoints(match.getWomensSingleTeams().get(1), match.getWomensSingleTeams().get(0), false);
            }
        }
    }

    private void resolveMensDoubleTeam1(Match match) {
        boolean didLowestWinSet1 = determineSetWinner(match.getMensDoubleTeams().get(0), match.getMensDoubleTeams().get(1), 1);
        boolean didLowestWinSet2 = determineSetWinner(match.getMensDoubleTeams().get(0), match.getMensDoubleTeams().get(1), 2);
        boolean didLowestWinSet3 = determineSetWinner(match.getMensDoubleTeams().get(0), match.getMensDoubleTeams().get(1), 3);

        boolean extendedMatch = (didLowestWinSet1 && !didLowestWinSet2) || (!didLowestWinSet1 && didLowestWinSet2);

        if (!extendedMatch){
            if (didLowestWinSet1){
                giveTeamPoints(match.getMensDoubleTeams().get(0), match.getMensDoubleTeams().get(1), true);
            } else {
                giveTeamPoints(match.getMensDoubleTeams().get(0), match.getMensDoubleTeams().get(1), false);
            }
        } else {
            if (didLowestWinSet3){
                giveTeamPoints(match.getMensDoubleTeams().get(0), match.getMensDoubleTeams().get(1), true);
            } else {
                giveTeamPoints(match.getMensDoubleTeams().get(0), match.getMensDoubleTeams().get(1), false);
            }
        }
    }

    private void resolveMensDoubleTeam2(Match match) {
        boolean didLowestWinSet1 = determineSetWinner(match.getMensDoubleTeams().get(1), match.getMensDoubleTeams().get(0), 1);
        boolean didLowestWinSet2 = determineSetWinner(match.getMensDoubleTeams().get(1), match.getMensDoubleTeams().get(0), 2);
        boolean didLowestWinSet3 = determineSetWinner(match.getMensDoubleTeams().get(1), match.getMensDoubleTeams().get(0), 3);

        boolean extendedMatch = (didLowestWinSet1 && !didLowestWinSet2) || (!didLowestWinSet1 && didLowestWinSet2);

        if (!extendedMatch){
            if (didLowestWinSet1){
                giveTeamPoints(match.getMensDoubleTeams().get(1), match.getMensDoubleTeams().get(0), true);
            } else {
                giveTeamPoints(match.getMensDoubleTeams().get(1), match.getMensDoubleTeams().get(0), false);
            }
        } else {
            if (didLowestWinSet3){
                giveTeamPoints(match.getMensDoubleTeams().get(1), match.getMensDoubleTeams().get(0), true);
            } else {
                giveTeamPoints(match.getMensDoubleTeams().get(1), match.getMensDoubleTeams().get(0), false);
            }
        }
    }

    private void resolveWomensDoubleTeam1(Match match) {
        boolean didLowestWinSet1 = determineSetWinner(match.getWomensDoubleTeams().get(0), match.getWomensDoubleTeams().get(1), 1);
        boolean didLowestWinSet2 = determineSetWinner(match.getWomensDoubleTeams().get(0), match.getWomensDoubleTeams().get(1), 2);
        boolean didLowestWinSet3 = determineSetWinner(match.getWomensDoubleTeams().get(0), match.getWomensDoubleTeams().get(1), 3);

        boolean extendedMatch = (didLowestWinSet1 && !didLowestWinSet2) || (!didLowestWinSet1 && didLowestWinSet2);

        if (!extendedMatch){
            if (didLowestWinSet1){
                giveTeamPoints(match.getWomensDoubleTeams().get(0), match.getWomensDoubleTeams().get(1), true);
            } else {
                giveTeamPoints(match.getWomensDoubleTeams().get(0), match.getWomensDoubleTeams().get(1), false);
            }
        } else {
            if (didLowestWinSet3){
                giveTeamPoints(match.getWomensDoubleTeams().get(0), match.getWomensDoubleTeams().get(1), true);
            } else {
                giveTeamPoints(match.getWomensDoubleTeams().get(0), match.getWomensDoubleTeams().get(1), false);
            }
        }
    }

    private void resolveWomensDoubleTeam2(Match match) {
        boolean didLowestWinSet1 = determineSetWinner(match.getWomensDoubleTeams().get(1), match.getWomensDoubleTeams().get(0), 1);
        boolean didLowestWinSet2 = determineSetWinner(match.getWomensDoubleTeams().get(1), match.getWomensDoubleTeams().get(0), 2);
        boolean didLowestWinSet3 = determineSetWinner(match.getWomensDoubleTeams().get(1), match.getWomensDoubleTeams().get(0), 3);

        boolean extendedMatch = (didLowestWinSet1 && !didLowestWinSet2) || (!didLowestWinSet1 && didLowestWinSet2);

        if (!extendedMatch){
            if (didLowestWinSet1){
                giveTeamPoints(match.getWomensDoubleTeams().get(1), match.getWomensDoubleTeams().get(0), true);
            } else {
                giveTeamPoints(match.getWomensDoubleTeams().get(1), match.getWomensDoubleTeams().get(0), false);
            }
        } else {
            if (didLowestWinSet3){
                giveTeamPoints(match.getWomensDoubleTeams().get(1), match.getWomensDoubleTeams().get(0), true);
            } else {
                giveTeamPoints(match.getWomensDoubleTeams().get(1), match.getWomensDoubleTeams().get(0), false);
            }
        }
    }

    private void resolveMixedDoubleTeam1(Match match) {
        boolean didLowestWinSet1 = determineSetWinner(match.getMixedDoubleTeams().get(0), match.getMixedDoubleTeams().get(1), 1);
        boolean didLowestWinSet2 = determineSetWinner(match.getMixedDoubleTeams().get(0), match.getMixedDoubleTeams().get(1), 2);
        boolean didLowestWinSet3 = determineSetWinner(match.getMixedDoubleTeams().get(0), match.getMixedDoubleTeams().get(1), 3);

        boolean extendedMatch = (didLowestWinSet1 && !didLowestWinSet2) || (!didLowestWinSet1 && didLowestWinSet2);

        if (!extendedMatch){
            if (didLowestWinSet1){
                giveTeamPoints(match.getMixedDoubleTeams().get(0), match.getMixedDoubleTeams().get(1), true);
            } else {
                giveTeamPoints(match.getMixedDoubleTeams().get(0), match.getMixedDoubleTeams().get(1), false);
            }
        } else {
            if (didLowestWinSet3){
                giveTeamPoints(match.getMixedDoubleTeams().get(0), match.getMixedDoubleTeams().get(1), true);
            } else {
                giveTeamPoints(match.getMixedDoubleTeams().get(0), match.getMixedDoubleTeams().get(1), false);
            }
        }
    }

    private void resolveMixedDoubleTeam2(Match match) {
        boolean didLowestWinSet1 = determineSetWinner(match.getMixedDoubleTeams().get(1), match.getMixedDoubleTeams().get(0), 1);
        boolean didLowestWinSet2 = determineSetWinner(match.getMixedDoubleTeams().get(1), match.getMixedDoubleTeams().get(0), 2);
        boolean didLowestWinSet3 = determineSetWinner(match.getMixedDoubleTeams().get(1), match.getMixedDoubleTeams().get(0), 3);

        boolean extendedMatch = (didLowestWinSet1 && !didLowestWinSet2) || (!didLowestWinSet1 && didLowestWinSet2);

        if (!extendedMatch){
            if (didLowestWinSet1){
                giveTeamPoints(match.getMixedDoubleTeams().get(1), match.getMixedDoubleTeams().get(0), true);
            } else {
                giveTeamPoints(match.getMixedDoubleTeams().get(1), match.getMixedDoubleTeams().get(0), false);
            }
        } else {
            if (didLowestWinSet3){
                giveTeamPoints(match.getMixedDoubleTeams().get(1), match.getMixedDoubleTeams().get(0), true);
            } else {
                giveTeamPoints(match.getMixedDoubleTeams().get(1), match.getMixedDoubleTeams().get(0), false);
            }
        }
    }
}
