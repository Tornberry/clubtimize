package dk.clubtimize.clubtimize.controllers.support;

import dk.clubtimize.clubtimize.models.union.Union;

public interface UnionValidator {
    Union getAssosiatedUnion();
}
