package dk.clubtimize.clubtimize.controllers.support.data;

import dk.clubtimize.clubtimize.models.training.*;
import dk.clubtimize.clubtimize.models.union.*;
import dk.clubtimize.clubtimize.models.union.matches.Match;
import dk.clubtimize.clubtimize.models.union.matches.matchtypes.*;
import dk.clubtimize.clubtimize.models.user.User;
import org.json.JSONArray;
import org.json.JSONObject;


public class UnionSerializer {

    public UnionSerializer() {
    }

    /**
     * The main method for assembling a serialized union object
     * @param union
     * @return a fully serialized union object
     */
    public String archiveUnionData(Union union){
        JSONObject main = new JSONObject();

        JSONObject unionObject = new JSONObject();

        unionObject.put("Name", union.getName());
        unionObject.put("URL", union.getUrl());
        unionObject.put("IsPremium", union.isPremium());
        unionObject.put("Coaches", getCoaches(union));
        unionObject.put("Personel", getPersonel(union));
        unionObject.put("Players", getPlayers(union));

        unionObject.put("Teams", getTeams(union));

        main.put("Union", unionObject);

        return main.toString().replaceAll(" ", "");
    }

    /**
     * Serializes coaches
     * @param union
     * @return coaches
     */
    private JSONArray getCoaches(Union union) {
        JSONArray coachesArray = new JSONArray();
        for (Coach coach : union.getCoaches()){
            JSONObject coachObject = new JSONObject();
            coachObject.put("CoachID", coach.getCoachId());
            coachObject.put("Union", coach.getUnion().getId());
            coachObject.put("UnionAssosiation", coach.getUnionAssosiation().getId());

            JSONArray teamsArray = new JSONArray();
            for (Team team : coach.getTeams()){
                teamsArray.put(team.getTeamId());
            }

            coachObject.put("Teams", teamsArray);
            coachObject.put("HourlyWage", coach.getHourlyWage());
        }
        return coachesArray;
    }

    /**
     * Serializes personel
     * @param union
     * @return personel
     */
    private JSONArray getPersonel(Union union) {
        JSONArray personelArray = new JSONArray();
        for (Personel personel : union.getPersonel()){
            JSONObject personelObject = new JSONObject();
            personelObject.put("PersonelID", personel.getPersonelId());
            personelObject.put("Union", personel.getUnion().getId());
            personelObject.put("UnionAssosiation", personel.getUnionAssosiation().getId());
            personelObject.put("Title", personel.getTitle());
            personelObject.put("HourlyWage", personel.getHourlyWage());
        }
        return personelArray;
    }

    /**
     * Serializes players
     * @param union
     * @return players
     */
    private JSONArray getPlayers(Union union) {
        JSONArray playersArray = new JSONArray();
        for (Player player : union.getPlayers()){
            JSONObject playerObject = new JSONObject();
            playerObject.put("PlayerID", player.getPlayerId());
            playerObject.put("Union", player.getUnion().getId());
            playerObject.put("UnionAssosiation", player.getUnionAssosiation().getId());
            playerObject.put("Team", player.getTeam().getTeamId());
            playersArray.put(playerObject);
        }
        return playersArray;
    }

    /**
     * Serializes teams
     * @param union
     * @return players
     */
    private JSONArray getTeams(Union union) {
        JSONArray teamsArray = new JSONArray();
        for (Team team : union.getTeams()){
            JSONObject teamObject = new JSONObject();
            teamObject.put("TeamID", team.getTeamId());
            teamObject.put("Title", team.getTitle());
            teamObject.put("TeamDepartment", team.getTeamDepartment().getId());
            teamObject.put("IsClosed", team.isClosed());
            teamObject.put("AvalibleSpots", team.getAvalibleSpots());

            // coaches
            JSONArray coachesArray = new JSONArray();
            for (Coach coach : team.getCoaches()){
                coachesArray.put(coach.getCoachId());
            }
            teamObject.put("Coaches", coachesArray);


            // players
            JSONArray playersArray = new JSONArray();
            for (Player player : union.getPlayers()){
                playersArray.put(player.getPlayerId());
            }
            teamObject.put("Players", playersArray);
            teamObject.put("TrainingSchedules", getTrainingSchedules(team));
            teamObject.put("Trainings", getTrainings(team));

            teamsArray.put(teamObject);
        }
        return teamsArray;
    }

    /**
     * Serializes training schedules FOR teams
     * @param team
     * @return players
     */
    private JSONArray getTrainingSchedules(Team team) {
        JSONArray trainingSchedulesArray = new JSONArray();
        for (TrainingSchedule trainingSchedule : team.getTrainingSchedules()){
            JSONObject trainingScheduleObject = new JSONObject();
            trainingScheduleObject.put("TrainingScheduleID", trainingSchedule.getTrainingScheduleId());
            trainingScheduleObject.put("Team", trainingSchedule.getTeam().getTeamId());
            trainingScheduleObject.put("Weekday", trainingSchedule.getWeekday());
            trainingScheduleObject.put("TimeFrom", trainingSchedule.getTimeFrom());
            trainingScheduleObject.put("TimeTo", trainingSchedule.getTimeTo());
            trainingScheduleObject.put("Hall", trainingSchedule.getHall().getHallId());
            trainingSchedulesArray.put(trainingScheduleObject);
        }
        return trainingSchedulesArray;
    }

    private JSONArray getTrainings(Team team) {
        JSONArray trainingsArray = new JSONArray();
        for (Training training : team.getTrainings()){
            JSONObject trainingObject = new JSONObject();
            trainingObject.put("TrainingID", training.getId());

            JSONObject focusObject = new JSONObject();
            focusObject.put("FocusID", training.getFocus().getId());
            focusObject.put("Description", training.getFocus().getDescription());
            focusObject.put("Training", training.getId());

            trainingObject.put("Focus", focusObject);
            trainingObject.put("Hall", training.getHall().getHallId());
            trainingObject.put("Date", training.getDate());
            trainingObject.put("TimeFrom", training.getTimeFrom());
            trainingObject.put("TimeTo", training.getTimeTo());
            trainingObject.put("Trainingsplan", getTrainingsplan(training.getTrainingsplan()));
            trainingObject.put("PlayerParticipations", getPlayerParticipations(training));
            trainingObject.put("Matches", getMatches(training));

            trainingsArray.put(trainingObject);
        }
        return trainingsArray;
    }

    private JSONObject getTrainingsplan(Trainingsplan trainingsplan) {
        JSONObject trainingsplanObject = new JSONObject();
        trainingsplanObject.put("TrainingsplanID", trainingsplan.getId());
        trainingsplanObject.put("Training", trainingsplan.getTraining().getId());
        trainingsplanObject.put("TrainingModules", getTrainingModules(trainingsplan));
        return trainingsplanObject;
    }

    private JSONArray getTrainingModules(Trainingsplan trainingsplan) {
        JSONArray trainingModuleArray = new JSONArray();
        for (TrainingModule trainingModule : trainingsplan.getTrainingModules()){
            JSONObject trainingModuleObject = new JSONObject();
            trainingModuleObject.put("TrainingsModuleID", trainingModule.getId());
            trainingModuleObject.put("Title", trainingModule.getTitle());
            trainingModuleObject.put("Description", trainingModule.getDescription());
            trainingModuleObject.put("ApproxTime", trainingModule.getApproxTime());
            trainingModuleObject.put("Trainingsplan", trainingsplan.getId());
            trainingModuleObject.put("ExerciseDGI", trainingModule.getExerciseDGI());

            // module Comments
            JSONArray moduleCommentsArray = new JSONArray();
            for (ModuleComment moduleComment : trainingModule.getModuleComments()){
                JSONObject moduleCommentObject = new JSONObject();
                moduleCommentObject.put("ModuleCommentID", moduleComment.getId());
                moduleCommentObject.put("Author", moduleComment.getAuthor());
                moduleCommentObject.put("Date", moduleComment.getDate());
                moduleCommentObject.put("Comment", moduleComment.getComment());
                moduleCommentObject.put("TrainingModule", moduleComment.getTrainingModule());
                moduleCommentsArray.put(moduleCommentObject);
            }
            trainingModuleArray.put(trainingModuleObject);
        }
        return trainingModuleArray;
    }

    private JSONArray getPlayerParticipations(Training training) {
        JSONArray playerParticipationArray = new JSONArray();
        for (PlayerParticipation playerParticipation : training.getPlayerParticipations()){
            JSONObject playerParticipationObject = new JSONObject();
            playerParticipationObject.put("PlayerParticipationID", playerParticipation.getId());
            playerParticipationObject.put("Participated", playerParticipation.isParticipated());
            playerParticipationObject.put("Training", playerParticipation.getTraining().getId());
            playerParticipationObject.put("Player", playerParticipation.getPlayer().getPlayerId());
            playerParticipationArray.put(playerParticipationObject);
        }
        return playerParticipationArray;
    }

    private JSONArray getMatches(Training training) {
        JSONArray matchesArray = new JSONArray();
        for (Match match : training.getMatches()){
            JSONObject matchObject = new JSONObject();
            matchObject.put("MatchID", match.getId());
            matchObject.put("Union", match.getUnion().getId());
            matchObject.put("Training", training.getId());
            matchObject.put("Date", match.getDate());
            matchObject.put("MatchType", match.getMatchType());
            matchObject.put("MensSingleTeams", getMensSingleTeams(match));
            matchObject.put("WomensSingleTeams", getWomensSingleTeams(match));
            matchObject.put("MensDoubleTeams", getMensDoubleTeams(match));
            matchObject.put("WomensDoubleTeams", getWomensDoubleTeams(match));
            matchObject.put("MixedDoubleTeams", getMixedDoubleTeams(match));
            matchesArray.put(matchObject);
        }
        return matchesArray;
    }

    private JSONArray getMensSingleTeams(Match match) {
        JSONArray mensSingleTeamsArray = new JSONArray();
        for (MensSingleTeam mensSingleTeam : match.getMensSingleTeams()){
            JSONObject mensSingleTeamObject = new JSONObject();
            mensSingleTeamObject.put("MensSingleTeamID", mensSingleTeam.getId());
            mensSingleTeamObject.put("Match", match.getId());
            mensSingleTeamObject.put("Player", mensSingleTeam.getPlayer().getUserId());
            mensSingleTeamObject.put("Set1Points", mensSingleTeam.getSet1Points());
            mensSingleTeamObject.put("Set2Points", mensSingleTeam.getSet2Points());
            mensSingleTeamObject.put("Set3Points", mensSingleTeam.getSet3Points());
            mensSingleTeamObject.put("TeamMMR", mensSingleTeam.getTeamMMR());
            mensSingleTeamsArray.put(mensSingleTeamObject);
        }
        return mensSingleTeamsArray;
    }

    private JSONArray getWomensSingleTeams(Match match) {
        JSONArray womensSingleTeamsArray = new JSONArray();
        for (WomensSingleTeam womensSingleTeam : match.getWomensSingleTeams()){
            JSONObject womensSingleTeamObject = new JSONObject();
            womensSingleTeamObject.put("WomensSingleTeamID", womensSingleTeam.getId());
            womensSingleTeamObject.put("Match", match.getId());
            womensSingleTeamObject.put("Player", womensSingleTeam.getPlayer().getUserId());
            womensSingleTeamObject.put("Set1Points", womensSingleTeam.getSet1Points());
            womensSingleTeamObject.put("Set2Points", womensSingleTeam.getSet2Points());
            womensSingleTeamObject.put("Set3Points", womensSingleTeam.getSet3Points());
            womensSingleTeamObject.put("TeamMMR", womensSingleTeam.getTeamMMR());
            womensSingleTeamsArray.put(womensSingleTeamObject);
        }
        return womensSingleTeamsArray;
    }

    private JSONArray getMensDoubleTeams(Match match) {
        JSONArray mensDoubleTeamsArray = new JSONArray();
        for (MensDoubleTeam mensDoubleTeam : match.getMensDoubleTeams()){
            JSONObject mensDoubleTeamObject = new JSONObject();
            mensDoubleTeamObject.put("MensDoubleTeamID", mensDoubleTeam.getId());
            mensDoubleTeamObject.put("Match", match.getId());

            JSONArray playersArray = new JSONArray();
            for (User player : mensDoubleTeam.getPlayers()){
                playersArray.put(player.getUserId());
            }
            mensDoubleTeamObject.put("Player", playersArray);
            mensDoubleTeamObject.put("Set1Points", mensDoubleTeam.getSet1Points());
            mensDoubleTeamObject.put("Set2Points", mensDoubleTeam.getSet2Points());
            mensDoubleTeamObject.put("Set3Points", mensDoubleTeam.getSet3Points());
            mensDoubleTeamObject.put("TeamMMR", mensDoubleTeam.getTeamMMR());
            mensDoubleTeamsArray.put(mensDoubleTeamObject);
        }
        return mensDoubleTeamsArray;
    }

    private JSONArray getWomensDoubleTeams(Match match) {
        JSONArray womensDoubleTeamsArray = new JSONArray();
        for (WomensDoubleTeam womensDoubleTeam : match.getWomensDoubleTeams()){
            JSONObject womensDoubleTeamObject = new JSONObject();
            womensDoubleTeamObject.put("WomensDoubleTeamID", womensDoubleTeam.getId());
            womensDoubleTeamObject.put("Match", match.getId());

            JSONArray playersArray = new JSONArray();
            for (User player : womensDoubleTeam.getPlayers()){
                playersArray.put(player.getUserId());
            }
            womensDoubleTeamObject.put("Player", playersArray);
            womensDoubleTeamObject.put("Set1Points", womensDoubleTeam.getSet1Points());
            womensDoubleTeamObject.put("Set2Points", womensDoubleTeam.getSet2Points());
            womensDoubleTeamObject.put("Set3Points", womensDoubleTeam.getSet3Points());
            womensDoubleTeamObject.put("TeamMMR", womensDoubleTeam.getTeamMMR());
            womensDoubleTeamsArray.put(womensDoubleTeamObject);
        }
        return womensDoubleTeamsArray;
    }

    private JSONArray getMixedDoubleTeams(Match match) {
        JSONArray mixedDoubleTeamsArray = new JSONArray();
        for (MixedDoubleTeam mixedDoubleTeam : match.getMixedDoubleTeams()){
            JSONObject mixedDoubleTeamObject = new JSONObject();
            mixedDoubleTeamObject.put("MixedDoubleTeams", mixedDoubleTeam.getId());
            mixedDoubleTeamObject.put("Match", match.getId());

            JSONArray playersArray = new JSONArray();
            for (User player : mixedDoubleTeam.getPlayers()){
                playersArray.put(player.getUserId());
            }
            mixedDoubleTeamObject.put("Player", playersArray);
            mixedDoubleTeamObject.put("Set1Points", mixedDoubleTeam.getSet1Points());
            mixedDoubleTeamObject.put("Set2Points", mixedDoubleTeam.getSet2Points());
            mixedDoubleTeamObject.put("Set3Points", mixedDoubleTeam.getSet3Points());
            mixedDoubleTeamObject.put("TeamMMR", mixedDoubleTeam.getTeamMMR());
            mixedDoubleTeamsArray.put(mixedDoubleTeamObject);
        }
        return mixedDoubleTeamsArray;
    }
}
