package dk.clubtimize.clubtimize.controllers.support;

import java.security.SecureRandom;

public class SecretFactory {

    public static String makeAlphaNumericString(int capacity){
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        StringBuilder stringBuilder = new StringBuilder(capacity);
        SecureRandom secureRandom = new SecureRandom();

        for(int i=0; i<capacity; i++){
            stringBuilder.append(AB.charAt(secureRandom.nextInt(AB.length())));
        }

        return stringBuilder.toString();
    }
}
