package dk.clubtimize.clubtimize.controllers.support;

import dk.clubtimize.clubtimize.models.opsætning.booking.Booking;
import dk.clubtimize.clubtimize.models.opsætning.booking.SeasonBookingPeriod;
import dk.clubtimize.clubtimize.models.opsætning.booking.SingleBookingPeriod;
import dk.clubtimize.clubtimize.models.opsætning.Court;
import dk.clubtimize.clubtimize.models.opsætning.Hall;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class CourtBookingFetcher {

    public static String makeJsonObject(List<Booking> bookings, List<Hall> halls) {

        JSONObject jsonObject = new JSONObject();

        jsonObject = fillStartAndEndTime(jsonObject, halls);
        jsonObject = fillStaticProperties(jsonObject);
        jsonObject = fillResources(jsonObject, halls);
        jsonObject = fillEvents(jsonObject, bookings);

        return jsonObject.toString();
    }

    private static JSONObject fillStartAndEndTime(JSONObject jsonObject, List<Hall> halls) {

        String lowestStart = null;
        String highestEnd = null;

        for (Hall hall : halls){
            for (SingleBookingPeriod singleBookingPeriod : hall.getSingleBookingPeriods()){
                if (lowestStart == null && highestEnd == null){
                    lowestStart = singleBookingPeriod.getStartTime();
                    highestEnd = singleBookingPeriod.getEndTime();
                } else {
                    if (Integer.parseInt(lowestStart.split(":")[0]) > Integer.parseInt(singleBookingPeriod.getStartTime().split(":")[0])){
                        lowestStart = singleBookingPeriod.getStartTime();
                    }

                    if (Integer.parseInt(highestEnd.split(":")[0]) < Integer.parseInt(singleBookingPeriod.getEndTime().split(":")[0])){
                        highestEnd = singleBookingPeriod.getEndTime();
                    }
                }
            }
        }

        for (Hall hall : halls){
            for (SeasonBookingPeriod seasonBookingPeriod : hall.getSeasonBookingPeriods()){
                if (lowestStart == null && highestEnd == null){
                    lowestStart = seasonBookingPeriod.getStartTime();
                    highestEnd = seasonBookingPeriod.getEndTime();
                } else {
                    if (Integer.parseInt(lowestStart.split(":")[0]) > Integer.parseInt(seasonBookingPeriod.getStartTime().split(":")[0])){
                        lowestStart = seasonBookingPeriod.getStartTime();
                    }

                    if (Integer.parseInt(highestEnd.split(":")[0]) < Integer.parseInt(seasonBookingPeriod.getEndTime().split(":")[0])){
                        highestEnd = seasonBookingPeriod.getEndTime();
                    }
                }
            }
        }

//        for (Hall hall : halls){
//            for (SingleBookingPeriod singleBookingPeriod : hall.getSingleBookingPeriods()){
//                if (lowestStart == null && highestEnd == null){
//                    lowestStart = singleBookingPeriod.getStartTime();
//                    highestEnd = singleBookingPeriod.getEndTime();
//                } else {
//                    if (Integer.parseInt(lowestStart.split(":")[0]) > Integer.parseInt(singleBookingPeriod.getStartTime().split(":")[0])){
//                        lowestStart = singleBookingPeriod.getStartTime();
//                    }
//
//                    if (Integer.parseInt(highestEnd.split(":")[0]) < Integer.parseInt(singleBookingPeriod.getEndTime().split(":")[0])){
//                        highestEnd = singleBookingPeriod.getEndTime();
//                    }
//                }
//            }
//        }

        jsonObject.put("minTime", lowestStart);
        jsonObject.put("maxTime", highestEnd);

        return jsonObject;

    }

    private static JSONObject fillStaticProperties(JSONObject jsonObject) {

        // Properties
        String schedulerLicenseKey = "GPL-My-Project-Is-Open-Source";
        String locale = "da";
        JSONArray plugins = new JSONArray().put("interaction").put("dayGrid").put("timeGrid").put("resourceTimeline");
        String now = new DateTime().toString("yyyy-MM-dd");
        boolean editable = false;
        float aspectRatio = (float)1.8;
        String scrollTime = "00:00";
        JSONObject header = new JSONObject().put("left", "today prev,next").put("center", "title").put("right", "resourceTimelineDay,resourceTimelineThreeDays,resourceTimelineSevenDays");
        String defaultView = "resourceTimelineDay";
        JSONObject views = new JSONObject().put("resourceTimelineThreeDays", new JSONObject().put("type", "resourceTimeline").put("duration", new JSONObject().put("days", 3)).put("buttonText", "3 dage")).put("resourceTimelineSevenDays", new JSONObject().put("type", "resourceTimeline").put("duration", new JSONObject().put("days", 7)).put("buttonText", "Uge"));
        String resourceAreaWidth = "20%";
        JSONArray resourceColumns = new JSONArray().put(new JSONObject().put("group", true).put("labelText", "Hal").put("field", "hal")).put(new JSONObject().put("labelText", "Bane").put("field", "bane"));
        String height = "auto";
        String timeFormat = "HH:mm";

        // Add properties to object
        jsonObject.put("schedulerLicenseKey", schedulerLicenseKey);
        jsonObject.put("locale", locale);
        jsonObject.put("plugins", plugins);
        jsonObject.put("now", now);
        jsonObject.put("editable", editable);
        jsonObject.put("aspectRatio", aspectRatio);
        jsonObject.put("scrollTime", scrollTime);
        jsonObject.put("header", header);
        jsonObject.put("defaultView", defaultView);
        jsonObject.put("views", views);
        jsonObject.put("resourceAreaWidth", resourceAreaWidth);
        jsonObject.put("resourceColumns", resourceColumns);
        jsonObject.put("height", height);
        jsonObject.put("timeFormat", timeFormat);

        return jsonObject;
    }

    private static JSONObject fillResources(JSONObject jsonObject, List<Hall> halls) {
        JSONArray jsonArray = new JSONArray();
        for (Hall hall : halls){
            for (Court court : hall.getCourts()){
                JSONObject resourceObject = new JSONObject();

                resourceObject.put("id", hall.getHallId() + "_" + court.getId());
                resourceObject.put("hal", hall.getTitle());
                resourceObject.put("bane", court.getTitle());

                jsonArray.put(resourceObject);
            }
        }

        return jsonObject.put("resources", jsonArray);
    }

    private static JSONObject fillEvents(JSONObject jsonObject, List<Booking> bookings) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");

        JSONArray jsonArray = new JSONArray();
        for (Booking booking : bookings){
            JSONObject eventObject = new JSONObject();

            eventObject.put("id", String.valueOf(booking.getId()));
            eventObject.put("resourceId", booking.getCourt().getHall().getHallId() + "_" + booking.getCourt().getId());
            eventObject.put("start", formatter.parseDateTime(booking.getDate()).toString("yyyy-MM-dd") + "T" + booking.getStartHour() + ":00");
            eventObject.put("end", formatter.parseDateTime(booking.getDate()).toString("yyyy-MM-dd") + "T" + booking.getEndHour() + ":00");

            // ExtendedProps

            if (booking.getTimePeriod() != null){
                if (booking.getCourt().getHall().getUnion().getUnionEconomics().isPayingFees()){
                    eventObject.put("price", booking.getTimePeriod().getSeasonBookingPeriod().getSinglePrice());
                } else {
                    eventObject.put("price", booking.getTimePeriod().getSeasonBookingPeriod().getSinglePrice() + EconomicSupport.calculateFeesAddedUp(booking.getTimePeriod().getSeasonBookingPeriod().getSinglePrice(), booking.getCourt().getHall().getUnion().getUnionEconomics().getFeePercent()));
                }
            } else {
                if (booking.getCourt().getHall().getUnion().getUnionEconomics().isPayingFees()){
                    eventObject.put("price", booking.getPrice());
                } else {
                    eventObject.put("price", booking.getPrice() + EconomicSupport.calculateFeesAddedUp(booking.getPrice(), booking.getCourt().getHall().getUnion().getUnionEconomics().getFeePercent()));
                }
            }

            eventObject.put("court", booking.getCourt().getTitle());
            eventObject.put("hall", booking.getCourt().getHall().getTitle());
            eventObject.put("hallId", booking.getCourt().getHall().getHallId());

            if (booking.getTimePeriod() == null){
                DateTime today = new DateTime();
                DateTimeFormatter bookingTimeFormatter = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm");
                DateTime bookingDate = bookingTimeFormatter.parseDateTime(booking.getDate() + " " + booking.getEndHour());


                if (!booking.isBooked()){
                    if (bookingDate.isBefore(today)){
                        eventObject.put("title", "Overstået");
                        eventObject.put("color", "dingrey");
                        eventObject.put("textColor", "black");
                    } else {
                        eventObject.put("title", "Ledig");
                        eventObject.put("color", "LimeGreen");
                        eventObject.put("textColor", "white");
                    }


                } else {
                    eventObject.put("title", "Optaget");
                    eventObject.put("color", "grey");
                    eventObject.put("textColor", "black");

                    eventObject.put("recipientName", booking.getRecipientName());
                    eventObject.put("recipientEmail", booking.getRecipientEmail());
                    eventObject.put("recipientAddress", booking.getRecipientAddress());
                    eventObject.put("recipientPhone", booking.getRecipientPhone());
                }
            } else {
                DateTime today = new DateTime();
                DateTimeFormatter bookingTimeFormatter = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm");
                DateTime bookingDate = bookingTimeFormatter.parseDateTime(booking.getDate() + " " + booking.getEndHour());

                if (booking.isBooked()){
                    eventObject.put("title", "Optaget");
                    eventObject.put("color", "grey");
                    eventObject.put("textColor", "black");

                    eventObject.put("recipientName", booking.getRecipientName());
                    eventObject.put("recipientEmail", booking.getRecipientEmail());
                    eventObject.put("recipientAddress", booking.getRecipientAddress());
                    eventObject.put("recipientPhone", booking.getRecipientPhone());
                } else {
                    if (booking.getTimePeriod().getSeasonBookingPeriod().isReserveSingleBookings()){
                        eventObject.put("title", "Reserveret");
                        eventObject.put("color", "LightSlateGray");
                        eventObject.put("textColor", "black");
                    } else if (!booking.getTimePeriod().getSeasonBookingPeriod().isReserveSingleBookings() && booking.isBooked()){
                        eventObject.put("title", "Optaget");
                        eventObject.put("color", "grey");
                        eventObject.put("textColor", "black");

                        eventObject.put("recipientName", booking.getRecipientName());
                        eventObject.put("recipientEmail", booking.getRecipientEmail());
                        eventObject.put("recipientAddress", booking.getRecipientAddress());
                        eventObject.put("recipientPhone", booking.getRecipientPhone());
                    } else if (!booking.getTimePeriod().getSeasonBookingPeriod().isReserveSingleBookings() && !booking.isBooked()) {
                        if (bookingDate.isBefore(today)){
                            eventObject.put("title", "Overstået");
                            eventObject.put("color", "dingrey");
                            eventObject.put("textColor", "black");
                        } else {
                            eventObject.put("title", "Ledig");
                            eventObject.put("color", "LimeGreen");
                            eventObject.put("textColor", "white");
                        }
                    }
                }
            }


            jsonArray.put(eventObject);
        }

        return jsonObject.put("events", jsonArray);
    }

}
