package dk.clubtimize.clubtimize.controllers.support;

import dk.clubtimize.clubtimize.models.union.UnionSeason;
import dk.clubtimize.clubtimize.services.UnionSeasonService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@Service
public class DateFactory {

    public static String getCurrentSeasonYear(){
        DateTime dateTime = new DateTime();
        return dateTime.getYear() + "/" + (dateTime.getYear() + 1);
    }

    public static String getLastSeasonYear(){
        DateTime dateTime = new DateTime().minusYears(1);
        return dateTime.getYear() + "/" + (dateTime.getYear() + 1);
    }

    public static String getCurrentDate(){
       return new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
    }

    public static String getCurrentDateAndTime(){
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
    }

    public static int getCurrentYear(){
        return new DateTime().getYear();
    }

    public static String numberToMonth(int month){
        String monthString;
        switch (month) {
            case 1:  monthString = "januar";        break;
            case 2:  monthString = "februar";        break;
            case 3:  monthString = "marts";         break;
            case 4:  monthString = "april";         break;
            case 5:  monthString = "maj";           break;
            case 6:  monthString = "juni";          break;
            case 7:  monthString = "juli";          break;
            case 8:  monthString = "august";        break;
            case 9:  monthString = "september";     break;
            case 10: monthString = "oktober";       break;
            case 11: monthString = "november";      break;
            case 12: monthString = "december";      break;
            default: monthString = "Invalid month"; break;
        }
        return monthString;
    }

    public static String numberToMonthCapital(int month){
        String monthString;
        switch (month) {
            case 1:  monthString = "Januar";        break;
            case 2:  monthString = "Februar";        break;
            case 3:  monthString = "Marts";         break;
            case 4:  monthString = "April";         break;
            case 5:  monthString = "Maj";           break;
            case 6:  monthString = "Juni";          break;
            case 7:  monthString = "Juli";          break;
            case 8:  monthString = "August";        break;
            case 9:  monthString = "September";     break;
            case 10: monthString = "Oktober";       break;
            case 11: monthString = "November";      break;
            case 12: monthString = "December";      break;
            default: monthString = "Invalid month"; break;
        }
        return monthString;
    }

    public static int monthToNumber(String month){
        switch (month) {
            case "Januar":  return 1;
            case "Februar":  return 2;
            case "Marts":  return 3;
            case "April":  return 4;
            case "Maj":  return 5;
            case "Juni":  return 6;
            case "Juli":  return 7;
            case "August":  return 8;
            case "September":  return 9;
            case "Oktober": return 10;
            case "November": return 11;
            case "December": return 12;
            default: return 0;
        }
    }
}
