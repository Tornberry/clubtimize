package dk.clubtimize.clubtimize.controllers.support;

import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.PaymentRequest;
import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.economics.invoice.InvoiceItem;
import dk.clubtimize.clubtimize.models.opsætning.booking.Booking;
import dk.clubtimize.clubtimize.models.opsætning.booking.TimePeriod;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.event.Event;
import dk.clubtimize.clubtimize.models.union.event.EventInvitation;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.ArrayList;
import java.util.List;

public class EconomicSupport {

    /**
     * If the team did not have a discount. Return a filled invoice.
     * @param teamSeason
     * @param economicYear
     * @param invoiceNumber
     * @return
     */
    public static Invoice makeContingentInvoice(TeamSeason teamSeason, EconomicYear economicYear, String invoiceNumber){

        Invoice invoice = new Invoice();

        List<InvoiceItem> invoiceItemList = new ArrayList<>();
        InvoiceItem invoiceItem = new InvoiceItem(
                invoice,
                1,
                "Kontingent",
                teamSeason.getTeam().getTitle() + " " + teamSeason.getTeam().getTeamDepartment().getTitle(),
                getTotalCost(teamSeason.getTeam().getPrice(), economicYear.getAssosiatedUnion()),
                0,
                0,
                getTotalCost(teamSeason.getTeam().getPrice(), economicYear.getAssosiatedUnion()));
        invoiceItemList.add(invoiceItem);

        invoice.setAssosiatedTeam(teamSeason.getTeam());
        invoice.setUnionAssosiatedName(teamSeason.getAssosiatedUnion().getName());
        invoice.setEconomicYear(economicYear);
        invoice.setUser(teamSeason.getUserSeason().getUser());
        invoice.setItems(invoiceItemList);
        invoice.setType("Kontingent");
        invoice = fillContingenInvoice(invoice);
        invoice.setInvoiceNumber(invoiceNumber);
        invoice.setTeamSeason(teamSeason);

        return invoice;
    }


    public static Invoice makeContingentInvoiceWithDiscount(TeamSeason teamSeason, EconomicYear economicYear, String invoiceNumber, float price){
        Invoice invoice = new Invoice();

        float[] additional = calculateFees(price, teamSeason.getAssosiatedUnion().getUnionEconomics().getFeePercent());
        float totalAditional = additional[0] + additional[1];

        List<InvoiceItem> invoiceItemList = new ArrayList<>();

        // If the union is paying fees the cost is just the price with the additinal subtracted
        if (teamSeason.getAssosiatedUnion().getUnionEconomics().isPayingFees()){
            InvoiceItem invoiceItem = new InvoiceItem(
                    invoice,
                    1,
                    "Kontingent",
                    teamSeason.getTeam().getTitle() + " " + teamSeason.getTeam().getTeamDepartment().getTitle(),
                    teamSeason.getTeam().getPrice() - totalAditional,
                    0,
                    0,
                    teamSeason.getTeam().getPrice() - totalAditional);
            invoiceItemList.add(invoiceItem);
        } else {
            InvoiceItem invoiceItem = new InvoiceItem(
                    invoice,
                    1,
                    "Kontingent",
                    teamSeason.getTeam().getTitle() + " " + teamSeason.getTeam().getTeamDepartment().getTitle(),
                    getTotalCost(teamSeason.getTeam().getPrice(), economicYear.getAssosiatedUnion()),
                    0,
                    0,
                    getTotalCost(teamSeason.getTeam().getPrice(), economicYear.getAssosiatedUnion()));
            invoiceItemList.add(invoiceItem);
        }


        InvoiceItem discountItem = new InvoiceItem(
                invoice,
                1,
                "Rabat",
                "Rabat",
                -(teamSeason.getTeam().getPrice() - price),
                0,
                0,
                -(teamSeason.getTeam().getPrice() - price)
        );
        invoiceItemList.add(discountItem);

        invoice.setAssosiatedTeam(teamSeason.getTeam());
        invoice.setUnionAssosiatedName(teamSeason.getAssosiatedUnion().getName());
        invoice.setEconomicYear(economicYear);
        invoice.setUser(teamSeason.getUserSeason().getUser());
        invoice.setItems(invoiceItemList);
        invoice.setType("Kontingent");
        invoice = fillContingenInvoice(invoice);
        invoice.setInvoiceNumber(invoiceNumber);
        invoice.setTeamSeason(teamSeason);

        return invoice;
    }

    public static Invoice makeSingleCourtBookingInvoice(Booking booking, EconomicYear economicYear, String invoiceNumber){

        Invoice invoice = new Invoice();

        List<InvoiceItem> invoiceItemList = new ArrayList<>();
        InvoiceItem invoiceItem = new InvoiceItem(
                invoice,
                1,
                "Banebooking",
                "Banebooking i " + booking.getCourt().getHall().getTitle() + ", " + booking.getCourt().getTitle() + " i perioden " + booking.getStartHour() + "-" + booking.getEndHour() + " d. " + booking.getDate(),
                getTotalCost(booking.getPrice(), economicYear.getAssosiatedUnion()),
                0,
                0,
                getTotalCost(booking.getPrice(), economicYear.getAssosiatedUnion()));
        invoiceItemList.add(invoiceItem);

        invoice.setAssosiatedBooking(booking);
        invoice.setUnionAssosiatedName(booking.getCourt().getHall().getUnion().getName());
        invoice.setEconomicYear(economicYear);
        invoice.setUser(null);
        invoice.setItems(invoiceItemList);
        invoice.setType("Banebooking");
        invoice = fillCourtbookingInvoice(invoice);
        invoice.setInvoiceNumber(invoiceNumber);

        return invoice;
    }

    public static Invoice makeSeasonCourtBookingInvoice(TimePeriod timePeriod, EconomicYear economicYear, String invoiceNumber){

        Invoice invoice = new Invoice();

        List<InvoiceItem> invoiceItemList = new ArrayList<>();
        InvoiceItem invoiceItem = new InvoiceItem(
                invoice,
                1,
                "Banebooking periode",
                "Banebooking i " + timePeriod.getCourt().getHall().getTitle() + " i perioden " + timePeriod.getSeasonBookingPeriod().getSeasonStartDate() + " - " +  timePeriod.getSeasonBookingPeriod().getSeasonEndDate() + ", klokken " + timePeriod.getTimeFrom() + "-" + timePeriod.getTimeTo() + ". Ugedag " + timePeriod.getSeasonBookingPeriod().getDayOfWeek(),
                getTotalCost(timePeriod.getSeasonBookingPeriod().getPrice(), economicYear.getAssosiatedUnion()),
                0,
                0,
                getTotalCost(timePeriod.getSeasonBookingPeriod().getPrice(), economicYear.getAssosiatedUnion()));
        invoiceItemList.add(invoiceItem);

        invoice.setAssosiatedTimePeriod(timePeriod);
        invoice.setUnionAssosiatedName(timePeriod.getCourt().getHall().getUnion().getName());
        invoice.setEconomicYear(economicYear);
        invoice.setUser(null);
        invoice.setItems(invoiceItemList);
        invoice.setType("Banebooking periode");
        invoice = fillCourtbookingInvoice(invoice);
        invoice.setInvoiceNumber(invoiceNumber);

        return invoice;
    }

    public static Invoice makeEventInvoice(EventInvitation eventInvitation, EconomicYear economicYear, String invoiceNumber){

        Invoice invoice = new Invoice();

        List<InvoiceItem> invoiceItemList = new ArrayList<>();
        InvoiceItem invoiceItem = new InvoiceItem(
                invoice,
                1,
                "Event",
                eventInvitation.getEvent().getTitle(),
                getTotalCost(eventInvitation.getEvent().getPrice(), economicYear.getAssosiatedUnion()),
                0,
                0,
                getTotalCost(eventInvitation.getEvent().getPrice(), economicYear.getAssosiatedUnion()));
        invoiceItemList.add(invoiceItem);

        invoice.setAssosiatedEventInvitation(eventInvitation);
        invoice.setUnionAssosiatedName(eventInvitation.getEvent().getUnion().getName());
        invoice.setEconomicYear(economicYear);
        invoice.setUser(eventInvitation.getUser());
        invoice.setItems(invoiceItemList);
        invoice.setType("Event");
        invoice = fillCourtbookingInvoice(invoice);
        invoice.setInvoiceNumber(invoiceNumber);

        return invoice;
    }

    public static Invoice makeFreeContingentInvoice(TeamSeason teamSeason, EconomicYear economicYear, String invoiceNumber){
        Invoice invoice = new Invoice();

        float[] additional = calculateFees(0, teamSeason.getAssosiatedUnion().getUnionEconomics().getFeePercent());
        float totalAditional = additional[0] + additional[1];

        List<InvoiceItem> invoiceItemList = new ArrayList<>();

        // If the union is paying fees the cost is just the price with the additinal subtracted
        InvoiceItem invoiceItem = new InvoiceItem(
                invoice,
                1,
                "Kontingent",
                teamSeason.getTeam().getTitle() + " " + teamSeason.getTeam().getTeamDepartment().getTitle(),
                teamSeason.getTeam().getPrice() - totalAditional,
                0,
                0,
                teamSeason.getTeam().getPrice() - totalAditional);
        invoiceItemList.add(invoiceItem);


        InvoiceItem discountItem = new InvoiceItem(
                invoice,
                1,
                "Frikontingent",
                "Gratis kontingent",
                -teamSeason.getTeam().getPrice(),
                0,
                0,
                -teamSeason.getTeam().getPrice()
        );
        invoiceItemList.add(discountItem);

        invoice.setAssosiatedTeam(teamSeason.getTeam());
        invoice.setUnionAssosiatedName(teamSeason.getAssosiatedUnion().getName());
        invoice.setEconomicYear(economicYear);
        invoice.setUser(teamSeason.getUserSeason().getUser());
        invoice.setItems(invoiceItemList);
        invoice.setType("Kontingent");
        invoice = fillContingenInvoice(invoice);
        invoice.setInvoiceNumber(invoiceNumber);
        invoice.setTeamSeason(teamSeason);

        return invoice;
    }

    private static float getTotalCost(float price, Union union) {
        if (union.getUnionEconomics().isPayingFees()){
            float[] feeData = calculateFees(price, union.getUnionEconomics().getFeePercent());

            return 1 * price - (feeData[0] + feeData[1]);
        } else {
            return 1 * price;
        }
    }


    private static Invoice fillContingenInvoice(Invoice invoice){
        invoice.setDate(getInvoiceDate(invoice));
        invoice = makeAndAddAdditionalFeeAndTaxInvoiceItem(invoice);
        invoice.setTotalCostWithAdditional(calculateTotalInvoiceCostWithAddotional(invoice));
        invoice.setAddToBalance(true);
        return invoice;
    }

    private static Invoice fillCourtbookingInvoice(Invoice invoice){
        invoice.setDate(DateFactory.getCurrentDate());
        invoice = makeAndAddAdditionalFeeAndTaxInvoiceItem(invoice);
        invoice.setTotalCostWithAdditional(calculateTotalInvoiceCostWithAddotional(invoice));
        invoice.setAddToBalance(true);
        return invoice;
    }

    private static String getInvoiceDate(Invoice invoice) {
        DateTime today = new DateTime();
        DateTime startDate = DateTimeFormat.forPattern("dd-MM-yyyy").parseDateTime(invoice.getAssosiatedTeam().getStartDate());

        if (startDate.isAfter(today)){
            return DateTimeFormat.forPattern("dd-MM-yyyy").parseDateTime(invoice.getAssosiatedTeam().getStartDate()).toString("dd-MM-yyyy");
        } else {
            return today.toString("dd-MM-yyyy");
        }

    }

    public static Invoice makePayoutInvoice(PaymentRequest paymentRequest, EconomicYear economicYear, String invoiceNumber){
        Invoice invoice = new Invoice();

        List<InvoiceItem> invoiceItemList = new ArrayList<>();
        InvoiceItem invoiceItem = new InvoiceItem(
                invoice,
                1,
                "Balance Udbetaling",
                "Udbetaling af balance fra dato " + paymentRequest.getDate() + ", med forventet udbetalingsdato " + paymentRequest.getExpectedPayoutDate(),
                paymentRequest.getAmountAtRequestTime() * -1,
                0,
                0,
                1 * (paymentRequest.getAmountAtRequestTime() * -1));

        invoiceItemList.add(invoiceItem);

        invoice.setUnionAssosiatedName(paymentRequest.getAssosiatedUnion().getName());
        invoice.setEconomicYear(economicYear);
        invoice.setUser(null);
        invoice.setItems(invoiceItemList);
        invoice.setType("Balance Udbetaling");
        invoice.setTotalCostWithAdditional(paymentRequest.getAmountAtRequestTime() * -1);
        invoice.setTotalCostWithoutAdditional(paymentRequest.getAmountAtRequestTime() * -1);
        invoice.setInvoiceNumber(invoiceNumber);
        invoice.setDate(DateFactory.getCurrentDate());
        invoice.setAddToBalance(true);

        return invoice;

    }

    private static Invoice makeAndAddAdditionalFeeAndTaxInvoiceItem(Invoice invoice){
        float totalCost = 0;

        for (InvoiceItem invoiceItem : invoice.getItems()){
            switch (invoiceItem.getItem()) {
                case "Kontingent":
                    totalCost += invoice.getAssosiatedTeam().getPrice();
                    break;
                case "Banebooking":
                    totalCost += invoice.getAssosiatedBooking().getPrice();
                    break;
                case "Banebooking periode":
                    totalCost += invoice.getAssosiatedTimePeriod().getSeasonBookingPeriod().getPrice();
                    break;
                case "Event":
                    totalCost += invoice.getAssosiatedEventInvitation().getEvent().getPrice();
                    break;
                default:
                    totalCost += (invoiceItem.getUnitCost() * invoiceItem.getQuantity());
                    break;
            }
        }

        float[] additional = calculateFees(totalCost, invoice.getEconomicYear().getUnionEconomics().getFeePercent());

        InvoiceItem additionalInvoiceItem = new InvoiceItem();
        additionalInvoiceItem.setItem("Administrationsgebyr");
        additionalInvoiceItem.setDescription("Administrationsgebyr");
        additionalInvoiceItem.setQuantity(1);
        additionalInvoiceItem.setUnitCost(additional[0] + additional[1]);
        additionalInvoiceItem.setFee(additional[0]);
        additionalInvoiceItem.setTax(additional[1]);
        additionalInvoiceItem.setTotalCost(additionalInvoiceItem.getFee() + additionalInvoiceItem.getTax());
        additionalInvoiceItem.setInvoice(invoice);

        invoice.addInvoiceItem(additionalInvoiceItem);

        invoice.setTotalCostWithAdditional(calculateTotalInvoiceCostWithAddotional(invoice));
        invoice.setTotalCostWithoutAdditional(calculateTotalInvoiceCostWithoutAddotional(invoice));

        return invoice;
    }

    public static float[] calculateFees(float price, float percent){
        float fee = price * (percent/100) + 2;
        float tax = (fee * (float)1.25) - fee;

        return new float[]{fee, tax};
    }

    public static float calculateFeesAddedUp(float price, float percent){
        float fee = price * (percent/100) + 2;
        float tax = (fee * (float)1.25) - fee;

        return fee+tax;
    }

    private static float calculateTotalInvoiceCostWithAddotional(Invoice invoice){
        float invoiceCost = 0;
        for (InvoiceItem invoiceItem : invoice.getItems()){
            invoiceCost += invoiceItem.getTotalCost();
        }
        return invoiceCost;
    }

    private static float calculateTotalInvoiceCostWithoutAddotional(Invoice invoice){
        float invoiceCost = 0;
        for (InvoiceItem invoiceItem : invoice.getItems()){
            if (invoiceItem.getTax() == 0 && invoiceItem.getFee() == 0){
                invoiceCost += invoiceItem.getTotalCost();
            }
        }
        return invoiceCost;
    }

    public static float calculateBalanceByUnionEconomic(UnionEconomics unionEconomics){
        float balance = 0;

        for (EconomicYear economicYear : unionEconomics.getEconomicYears()){
            for (Invoice invoice : economicYear.getInvoices()){
                if (invoice.isAddToBalance()){
                    balance += invoice.getTotalCostWithoutAdditional();
                }
            }
        }

        return balance;
    }

    public static EconomicYear getCurrentEconomicYearByUnionEconimic(UnionEconomics unionEconomics){
        DateTime dateTime = new DateTime();
        return unionEconomics.getEconomicYears().stream().filter(economicYear -> economicYear.getYear().equals(dateTime.getYear() + "/" + (dateTime.getYear() + 1))).findFirst().orElse(null);
    }

}
