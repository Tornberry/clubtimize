package dk.clubtimize.clubtimize.controllers.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class LogFactory {

    private static Logger logger = LoggerFactory.getLogger(LogFactory.class);

    public static String getLog(){
        try{
            String log = new String(Files.readAllBytes(Paths.get("C:/clubtimize/logs/MyLog.log")));
            logger.info("Read log file with success");
            return log;
        } catch (IOException e) {
            logger.error("Could not read log file: " + e.getMessage());
            return "Could not read log file, see Clubtimize console";
        }
    }

}
