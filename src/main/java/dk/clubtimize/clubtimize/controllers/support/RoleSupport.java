package dk.clubtimize.clubtimize.controllers.support;

import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.user.Role;
import dk.clubtimize.clubtimize.models.user.RolePermissions;
import dk.clubtimize.clubtimize.models.user.User;

public class RoleSupport {

    public static Role makeRole(Union union, String roleName, boolean canAdministrate, boolean canSeeAdministrationPages, boolean canSeeAllTeams, boolean canUseTrainerFeatures){

        Role role = new Role();

        RolePermissions rolePermissions = new RolePermissions();

        rolePermissions.setCanAdministrate(canAdministrate);
        rolePermissions.setCanSeeAdministrationPages(canSeeAdministrationPages);
        rolePermissions.setCanSeeAllTeams(canSeeAllTeams);
        rolePermissions.setCanUseTrainerFeatures(canUseTrainerFeatures);
        rolePermissions.setRole(role);

        role.setRolePermissions(rolePermissions);
        role.setUnion(union);
        role.setRoleName(roleName);

        return role;
    }

    public static boolean checkPermission(PermissionEnum permissionEnum, User user){
        switch (permissionEnum){
            case canAdministrate:
                if (user.getRole().getRolePermissions().isCanAdministrate()){
                    return true;
                }
            case canSeeAdministrationPages:
                if (user.getRole().getRolePermissions().isCanSeeAdministrationPages()){
                    return true;
                }
            case canSeeAllTeams:
                if (user.getRole().getRolePermissions().isCanSeeAllTeams()){
                    return true;
                }
            case canUseTrainerFeatures:
                if (user.getRole().getRolePermissions().isCanUseTrainerFeatures()){
                    return true;
                }
            default:
                return false;
        }
    }

    public static boolean checkIfIsCoach(User user){
        return user.getUnionAssosiation().getCoachPositions().size() > 0;
    }
}
