package dk.clubtimize.clubtimize.controllers.support;

import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.UnionSeason;
import dk.clubtimize.clubtimize.services.UnionSeasonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnionSeasonStringService {

    private UnionSeasonService unionSeasonService;

    @Autowired
    public UnionSeasonStringService(UnionSeasonService unionSeasonService) {
        this.unionSeasonService = unionSeasonService;
    }

    public String getSeasonStringByUnionSeason(Union union){
        Iterable<UnionSeason> unionSeasonList = unionSeasonService.findByUnion(union);

        return getCurrentUnionSeason(unionSeasonList);
    }

    private String getCurrentUnionSeason(Iterable<UnionSeason> unionSeasonList) {
        int highestFirstYear = 0;
        int count = 0;

        for (UnionSeason unionSeason : unionSeasonList){
            int unionSeasonYear = Integer.parseInt(unionSeason.getSeasonYear().split("/")[0]);

            if (unionSeasonYear > highestFirstYear){
                highestFirstYear = unionSeasonYear;
            }
        }

        for (UnionSeason unionSeason : unionSeasonList){
            int unionSeasonYear = Integer.parseInt(unionSeason.getSeasonYear().split("/")[0]);

            if (unionSeasonYear == highestFirstYear){
                if(unionSeason.getSeasonYear().split(" ").length > 1){
                    if (Integer.parseInt(unionSeason.getSeasonYear().split(" ")[2]) > count){
                        count = Integer.parseInt(unionSeason.getSeasonYear().split(" ")[2]);
                    }
                }
            }
        }

        if (count > 0){
            return highestFirstYear + "/" + (highestFirstYear + 1) + " - " + count;
        } else {
            return highestFirstYear + "/" + (highestFirstYear + 1);
        }
    }
}
