package dk.clubtimize.clubtimize.controllers.support;

import dk.clubtimize.clubtimize.models.opsætning.booking.Booking;
import dk.clubtimize.clubtimize.models.opsætning.booking.TimePeriod;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.discount.discountTypes.TwoTeamDiscount;
import dk.clubtimize.clubtimize.models.union.event.Event;
import dk.clubtimize.clubtimize.models.union.event.EventInvitation;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class PaymentGatewaySupport {

    private Team team;
    private String paymentCode;

    public PaymentGatewaySupport(Team team, String paymentCode) {
        this.team = team;
        this.paymentCode = paymentCode;
    }

    /**
     * Returns a link to the ScanPay payment gateway.
     *
     * The 'successurl' parameter will have 'price' if the price without additional is different from the team price, to let the invoice generator know that there was a discount
     *
     * @param priceWithAdditional Used as the price for the user to pay
     * @param priceWithoutAdditional Used as the discounted price for the payment successfull link. That code then calculates the fees again and adds an item to the invoice.
     * @return
     */
    public String getPaymentGateway(float priceWithAdditional, float priceWithoutAdditional){
        HttpClient httpClient = HttpClientBuilder.create().build();

        JSONObject item = new JSONObject();

        // If the union pays for the fees, charge the user the discounted amount with the fees subtracted
        if (team.getUnion().getUnionEconomics().isPayingFees()){
            item.put("total", priceWithoutAdditional + " DKK");
        } else {
            item.put("total", priceWithAdditional + " DKK");
        }

        JSONArray items = new JSONArray();
        items.put(item);

        JSONObject main = new JSONObject();
        main.put("items", items);
        if (priceWithoutAdditional != team.getPrice()){
            main.put("successurl", "http://app.unie.dk/paymentsuccessfull/contingent?c=" + paymentCode + "&price=" + priceWithoutAdditional);
            System.out.println(main.getString("successurl"));
        } else {
            main.put("successurl", "http://app.unie.dk/paymentsuccessfull/contingent?c=" + paymentCode);
            System.out.println(main.getString("successurl"));
        }

        main.put("autocapture", true);

        String encoding = Base64.encodeBase64String("22785:s2VNAbZYngBKI5cOIkyKcN1p9x848cOf/p2SRrb5qvEkcZF5Z1KbOJttbPcZtGmX".getBytes());

        try {
            HttpPost request = new HttpPost("https://api.scanpay.dk/v1/new");
            StringEntity params = new StringEntity(main.toString());
            request.addHeader("Authorization", "Basic " + encoding);
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);

            if(team.getUnion().getUnionEconomics().isPayingFees() && priceWithoutAdditional == 0){
                return "http://app.clubtimize.dk/paymentsuccessfull/contingent?c=" + paymentCode + "&price=" + priceWithoutAdditional;
            } else {
                return extractURL(EntityUtils.toString(response.getEntity()));
            }


        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    public String getPaymentGatewayForSingleCourtBooking(float priceWithAdditional, Booking booking){
        HttpClient httpClient = HttpClientBuilder.create().build();

        JSONObject item = new JSONObject();

        // If the union pays for the fees, charge the user the discounted amount with the fees subtracted
        if (booking.getCourt().getHall().getUnion().getUnionEconomics().isPayingFees()){
            item.put("total", booking.getPrice() + " DKK");
        } else {
            item.put("total", priceWithAdditional + " DKK");
        }

        JSONArray items = new JSONArray();
        items.put(item);

        JSONObject main = new JSONObject();
        main.put("items", items);
        main.put("successurl", "http://app.unie.dk/paymentsuccessfull/courtbooking_single?" + "recipientName=" + booking.getRecipientName() + "&recipientEmail=" + booking.getRecipientEmail() + "&recipientAddress=" + booking.getRecipientAddress() + "&recipientPhone=" + booking.getRecipientPhone() + "&bookingId=" + booking.getId());
        System.out.println(main.getString("successurl"));
        main.put("autocapture", true);

        String encoding = Base64.encodeBase64String("22785:s2VNAbZYngBKI5cOIkyKcN1p9x848cOf/p2SRrb5qvEkcZF5Z1KbOJttbPcZtGmX".getBytes());

        try {
            HttpPost request = new HttpPost("https://api.scanpay.dk/v1/new");
            StringEntity params = new StringEntity(main.toString());
            request.addHeader("Authorization", "Basic " + encoding);
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);

            return extractURL(EntityUtils.toString(response.getEntity()));


        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    public String getPaymentGatewayForSeasonCourtBooking(float priceWithAdditional, TimePeriod timePeriod){
        HttpClient httpClient = HttpClientBuilder.create().build();

        JSONObject item = new JSONObject();

        // If the union pays for the fees, charge the user the discounted amount with the fees subtracted
        if (timePeriod.getCourt().getHall().getUnion().getUnionEconomics().isPayingFees()){
            item.put("total", timePeriod.getSeasonBookingPeriod().getPrice() + " DKK");
        } else {
            item.put("total", priceWithAdditional + " DKK");
        }

        JSONArray items = new JSONArray();
        items.put(item);

        JSONObject main = new JSONObject();
        main.put("items", items);
        main.put("successurl", "http://app.unie.dk/paymentsuccessfull/courtbooking_period?" + "recipientName=" + timePeriod.getRecipientName() + "&recipientEmail=" + timePeriod.getRecipientEmail() + "&recipientAddress=" + timePeriod.getRecipientAddress() + "&recipientPhone=" + timePeriod.getRecipientPhone() + "&timePeriodId=" + timePeriod.getId());
        System.out.println(main.getString("successurl"));
        main.put("autocapture", true);

        String encoding = Base64.encodeBase64String("22785:s2VNAbZYngBKI5cOIkyKcN1p9x848cOf/p2SRrb5qvEkcZF5Z1KbOJttbPcZtGmX".getBytes());

        try {
            HttpPost request = new HttpPost("https://api.scanpay.dk/v1/new");
            StringEntity params = new StringEntity(main.toString());
            request.addHeader("Authorization", "Basic " + encoding);
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);

            return extractURL(EntityUtils.toString(response.getEntity()));


        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    public String getPaymentGatewayForEvent(float priceWithAdditional, EventInvitation eventInvitation){
        HttpClient httpClient = HttpClientBuilder.create().build();

        JSONObject item = new JSONObject();

        // If the union pays for the fees, charge the user the discounted amount with the fees subtracted
        if (eventInvitation.getEvent().getUnion().getUnionEconomics().isPayingFees()){
            item.put("total", eventInvitation.getEvent().getPrice() + " DKK");
        } else {
            item.put("total", priceWithAdditional + " DKK");
        }

        JSONArray items = new JSONArray();
        items.put(item);

        JSONObject main = new JSONObject();
        main.put("items", items);
        main.put("successurl", "http://app.unie.dk/paymentsuccessfull/event?&eventInv=" + eventInvitation.getId());
        System.out.println(main.getString("successurl"));
        main.put("autocapture", true);

        String encoding = Base64.encodeBase64String("22785:s2VNAbZYngBKI5cOIkyKcN1p9x848cOf/p2SRrb5qvEkcZF5Z1KbOJttbPcZtGmX".getBytes());

        try {
            HttpPost request = new HttpPost("https://api.scanpay.dk/v1/new");
            StringEntity params = new StringEntity(main.toString());
            request.addHeader("Authorization", "Basic " + encoding);
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);

            return extractURL(EntityUtils.toString(response.getEntity()));


        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    public float[] calculatePrices(Team team, List<Team> alreadyJoinedTeams){
        TwoTeamDiscount largestAmount = getLargestDiscount(team.getTwoTeamDiscounts(), alreadyJoinedTeams);

        float price = 0;

        if (largestAmount != null){
            if (!((team.getPrice() - largestAmount.getAmount()) <= 0)){
                price = team.getPrice() - largestAmount.getAmount();
            }
        } else {
            price = team.getPrice();
        }

        return new float[]{price, price + (EconomicSupport.calculateFees(price, team.getUnion().getUnionEconomics().getFeePercent())[0] + EconomicSupport.calculateFees(price, team.getUnion().getUnionEconomics().getFeePercent())[1])};
    }

    private TwoTeamDiscount getLargestDiscount(List<TwoTeamDiscount> twoTeamDiscounts, List<Team> alreadyJoinedTeams) {
        TwoTeamDiscount largestAmount = null;

        boolean firstTime = true;
        for (TwoTeamDiscount twoTeamDiscount : twoTeamDiscounts){

            // If you are eligible for this discount
            if (twoTeamDiscount.getTeams().contains(team) && twoTeamDiscount.getTeamsDiscountCount(alreadyJoinedTeams) == 1) {
                if (!firstTime) {
                    // If this discount amount is greater
                    if (largestAmount.getAmount() < twoTeamDiscount.getAmount()){
                        largestAmount = twoTeamDiscount;
                    }
                } else {
                    largestAmount = twoTeamDiscount;
                    firstTime = false;
                }
            }
        }

        return largestAmount;
    }

    private String extractURL(String JSON) {
        JSONObject jsonObject = new JSONObject(JSON);
        return jsonObject.get("url").toString();
    }
}
