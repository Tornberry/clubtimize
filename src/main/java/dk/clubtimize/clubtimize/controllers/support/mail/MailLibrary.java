package dk.clubtimize.clubtimize.controllers.support.mail;

import org.apache.commons.codec.Encoder;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MailLibrary {

    private static final String username = "api";
    private static final String apiKey = "dd6d67d6fbed5d522c3e9eb1ffcd619c-de7062c6-a4bc784d";
    private static final String baseURL = "https://api.eu.mailgun.net/v3";
    private static final String domain = "mg.unie.dk";

    static List<BasicNameValuePair> getBasicValuePairs(String subject, String fromText, String email, String to, String template){
        List<BasicNameValuePair> basicNameValuePairs = new ArrayList<>();

        basicNameValuePairs.add(new BasicNameValuePair("subject", subject));
        basicNameValuePairs.add(new BasicNameValuePair("from", fromText + " " + "<" + email + ">"));
        basicNameValuePairs.add(new BasicNameValuePair("to", to));
        basicNameValuePairs.add(new BasicNameValuePair("template", template));

        return basicNameValuePairs;
    }

    static String sendEmail(List<BasicNameValuePair> basicNameValuePairs, HashMap<String, String> variables, String recipent, boolean isInvoice) {
        HttpClient httpClient = HttpClientBuilder.create().build();

        if (!isInvoice){
            basicNameValuePairs.add(new BasicNameValuePair("h:X-Mailgun-Variables", makeVariablesString(variables, recipent)));
        } else {
            basicNameValuePairs.add(new BasicNameValuePair("h:X-Mailgun-Variables", makeVariablesStringInvoice(variables, recipent)));
        }


        try {
            HttpPost request = new HttpPost(baseURL + "/" + domain + "/messages");
            request.addHeader("Authorization", "Basic " + getAuthEncoding());
            request.setEntity(new UrlEncodedFormEntity(basicNameValuePairs, "UTF-8"));
            HttpResponse response = httpClient.execute(request);

            return EntityUtils.toString(response.getEntity());
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "Error";
        }
    }

    private static String makeVariablesStringInvoice(HashMap<String,String> variables, String recipent) {
        JSONObject mainJson = new JSONObject();

        JSONObject jsonVariableObject = new JSONObject();

        for (Map.Entry entry : variables.entrySet()){
            if (entry.getKey().toString().equals("invoiceItems")){
                JSONArray jsonArray = new JSONArray(entry.getValue().toString());

                mainJson.put("invoiceItems", jsonArray);
            } else {
                mainJson.put(entry.getKey().toString(), entry.getValue().toString());
            }
        }

        jsonVariableObject.put(recipent, jsonVariableObject);

        return mainJson.toString();
    }

    private static String makeVariablesString(HashMap<String,String> variables, String recipent) {
        JSONObject mainJson = new JSONObject();

        JSONObject jsonVariableObject = new JSONObject();

        for (Map.Entry entry : variables.entrySet()){
            mainJson.put(entry.getKey().toString(), entry.getValue().toString());
        }

        jsonVariableObject.put(recipent, jsonVariableObject);

        return mainJson.toString();
    }

    private static String getAuthEncoding(){
        return Base64.encodeBase64String((username + ":" + apiKey).getBytes());
    }
}
