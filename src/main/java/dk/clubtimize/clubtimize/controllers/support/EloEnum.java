package dk.clubtimize.clubtimize.controllers.support;

public enum EloEnum {
    mensSingle, womensSingle, mensDouble, womensDouble, mixedDouble
}
