package dk.clubtimize.clubtimize.controllers.support;

import dk.clubtimize.clubtimize.models.user.User;

public class OwnershipValidator {

    public static <T extends UnionValidator> boolean validateOwnership(T object, User user){
        return object.getAssosiatedUnion() == user.getUnion();
    }
}
