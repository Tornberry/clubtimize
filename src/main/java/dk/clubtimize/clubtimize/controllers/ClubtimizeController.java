package dk.clubtimize.clubtimize.controllers;

import dk.clubtimize.clubtimize.controllers.support.ClubtimizeSettings;
import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.controllers.support.RoleSupport;
import dk.clubtimize.clubtimize.ClubtimizeAPI.dinero.DineroClubtimizeAPI;
import dk.clubtimize.clubtimize.controllers.support.mail.MailSender;
import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import dk.clubtimize.clubtimize.models.economics.bookkeeping.Bookkeeping;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.discount.Discount;
import dk.clubtimize.clubtimize.models.user.UnionAssosiation;
import dk.clubtimize.clubtimize.models.union.UnionSeason;
import dk.clubtimize.clubtimize.models.user.Role;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.services.RoleService;
import dk.clubtimize.clubtimize.services.UnionService;
import dk.clubtimize.clubtimize.services.UserService;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Calendar;

@Controller
public class ClubtimizeController {

    private UnionService unionService;
    private RoleService roleService;
    private UserService userService;
    private Logger logger = LoggerFactory.getLogger(ClubtimizeController.class);

    @Autowired
    public ClubtimizeController(UnionService unionService, RoleService roleService, UserService userService) {
        this.unionService = unionService;
        this.roleService = roleService;
        this.userService = userService;
    }

    @GetMapping("/")
    public String showLandingPage(){
        return "clubtimize/clubtimize.html";
    }

    @PostMapping("/registerunion")
    @ResponseBody
    public String postRegisterUnion(@RequestBody String json){
        DateTime dateTime = new DateTime();

        JSONObject jsonObject = new JSONObject(json);

        if(!userService.existsByEmail(jsonObject.getString("adminemail"))){
            if (!unionService.existsByName(jsonObject.getString("unionname"))){
                if (!unionService.existsByUrl("/" + jsonObject.getString("unionurl"))){
                    // make a new union
                    Union union = new Union();

                    union.setUrl("/" + jsonObject.getString("unionurl"));
                    union.setName(jsonObject.getString("unionname"));
                    union.setSeasonSwitchMonth(jsonObject.getString("seasonswitchmonth"));
                    union.setDateJoined(DateFactory.getCurrentDateAndTime());

                    User user = new User();
                    user.setEmail(jsonObject.get("adminemail").toString());
                    user.setFirstname("Administrator");
                    user.setLastname("");
                    user.setUnionAdministrator(true);
                    user.setPassword(jsonObject.get("adminpass").toString());
                    user.setDateJoined(new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime()));
                    user.setUnion(union);
                    union.setAdminAccountEmail(user.getEmail());


                    UnionAssosiation unionAssosiation = new UnionAssosiation();
                    unionAssosiation.setUnion(union);
                    unionAssosiation.setUser(user);
                    user.setUnionAssosiation(unionAssosiation);

                    UnionEconomics unionEconomics = new UnionEconomics();

                    EconomicYear economicYear = new EconomicYear();
                    economicYear.setUnionEconomics(unionEconomics);
                    economicYear.setYear(dateTime.getYear() + "/" + (dateTime.getYear() + 1));

                    unionEconomics.addEconomicYear(economicYear);
                    unionEconomics.setBalance(0);
                    unionEconomics.setFeePercent(3);
                    union.setUnionEconomics(unionEconomics);
                    unionEconomics.setUnion(union);

                    Bookkeeping bookkeeping = new Bookkeeping();
                    bookkeeping.setUnionEconomics(unionEconomics);
                    unionEconomics.setBookkeeping(bookkeeping);

                    Discount discount = new Discount();
                    discount.setUnion(union);
                    union.setDiscount(discount);

                    unionService.saveOrUpdate(union);

                    setupStandardRoles(union);
                    user.setRole(roleService.findByUnionAndRole(union,"Forenings administrator"));
                    union.addRole(roleService.findByUnionAndRole(union,"Forenings administrator"));
                    union.addRole(roleService.findByUnionAndRole(union,"Spiller"));
                    union.addRole(roleService.findByUnionAndRole(union, "Træner"));

                    UnionSeason unionSeason = new UnionSeason();
                    unionSeason.setSeasonYear(dateTime.getYear() + "/" + (dateTime.getYear() + 1));
                    unionSeason.setUnion(union);
                    union.addUnionSeason(unionSeason);

                    if (ClubtimizeSettings.isProductionEnvironment()){
                        JSONObject response = new JSONObject(DineroClubtimizeAPI.addSimpleContact(union.getName(), "DK", false));
                        try {
                            union.setGuid(response.getString("ContactGuid"));
                        } catch (Exception e){
                            logger.error("Could not create Dinero contact");
                        }
                    }


                    union.setAccount(0);

                    userService.createUser(user, "Forenings administrator");
                    unionService.saveOrUpdate(union);

                    MailSender.adminWelcomeMail(user);

                    return jsonObject.get("unionurl").toString();
                } else {
                    return "url_in_use";
                }
            } else {
                return "name_in_use";
            }
        } else {
            return "email_in_use";
        }
    }

    private void setupStandardRoles(Union union){
        roleService.saveOrUpdate(makePlayerRole(union));
        roleService.saveOrUpdate(makeUnionAdministratorRole(union));
        roleService.saveOrUpdate(makeCoachRole(union));
    }

    private Role makeCoachRole(Union union){
        return RoleSupport.makeRole(union, "Træner", false, false, false, true);
    }

    private Role makePlayerRole(Union union) {
        return RoleSupport.makeRole(union, "Spiller", false, false, false, false);
    }

    private Role makeUnionAdministratorRole(Union union) {
        return RoleSupport.makeRole(union, "Forenings administrator", true, true, true, true);
    }
}
