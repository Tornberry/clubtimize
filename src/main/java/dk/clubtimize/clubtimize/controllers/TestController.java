package dk.clubtimize.clubtimize.controllers;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.controllers.support.EloCalculator;
import dk.clubtimize.clubtimize.controllers.support.SecretFactory;
import dk.clubtimize.clubtimize.controllers.support.UnionSeasonStringService;
import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import dk.clubtimize.clubtimize.models.economics.invoice.DebtReport;
import dk.clubtimize.clubtimize.models.economics.invoice.FeeReport;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.opsætning.Court;
import dk.clubtimize.clubtimize.models.training.Training;
import dk.clubtimize.clubtimize.models.union.Player;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.UnionSeason;
import dk.clubtimize.clubtimize.models.union.event.Event;
import dk.clubtimize.clubtimize.models.union.event.EventInvitation;
import dk.clubtimize.clubtimize.models.union.matches.Match;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;
import dk.clubtimize.clubtimize.repository.UserSeasonRepository;
import dk.clubtimize.clubtimize.services.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
public class TestController {

    private UnionService unionService;
    private UserService userService;
    private PlayerParticipationService playerParticipationService;
    private TeamSeasonService teamSeasonService;
    private PaymentRequestService paymentRequestService;
    private TrainingService trainingService;
    private MatchService matchService;
    private TeamService teamService;
    private InvoiceService invoiceService;
    private HallService hallService;
    private BookingService bookingService;
    private CourtService courtService;
    private EventInvitationService eventInvitationService;
    private EventService eventService;
    private RoleService roleService;
    private UnionEconomicsService unionEconomicsService;
    private DebtReportService debtReportService;
    private FeeReportService feeReportService;
    private UnionSeasonStringService unionSeasonStringService;
    private UnionSeasonService unionSeasonService;
    private UserSeasonService userSeasonService;
    private UserSeasonRepository userSeasonRepository;

    @Autowired
    public TestController(UnionService unionService, UserService userService, PlayerParticipationService playerParticipationService, TeamSeasonService teamSeasonService, PaymentRequestService paymentRequestService, TrainingService trainingService, MatchService matchService, TeamService teamService, InvoiceService invoiceService, HallService hallService, BookingService bookingService, CourtService courtService, EventInvitationService eventInvitationService, EventService eventService, RoleService roleService, UnionEconomicsService unionEconomicsService, DebtReportService debtReportService, FeeReportService feeReportService, UnionSeasonStringService unionSeasonStringService, UnionSeasonService unionSeasonService, UserSeasonService userSeasonService, UserSeasonRepository userSeasonRepository) {
        this.unionService = unionService;
        this.userService = userService;
        this.playerParticipationService = playerParticipationService;
        this.teamSeasonService = teamSeasonService;
        this.paymentRequestService = paymentRequestService;
        this.trainingService = trainingService;
        this.matchService = matchService;
        this.teamService = teamService;
        this.invoiceService = invoiceService;
        this.hallService = hallService;
        this.bookingService = bookingService;
        this.courtService = courtService;
        this.eventInvitationService = eventInvitationService;
        this.eventService = eventService;
        this.roleService = roleService;
        this.unionEconomicsService = unionEconomicsService;
        this.debtReportService = debtReportService;
        this.feeReportService = feeReportService;
        this.unionSeasonStringService = unionSeasonStringService;
        this.unionSeasonService = unionSeasonService;
        this.userSeasonService = userSeasonService;
        this.userSeasonRepository = userSeasonRepository;
    }

    @GetMapping("/test/elo")
    @ResponseBody
    public String testElo(@RequestParam("lowestMMR") float lowestMMR, @RequestParam("highestMMR") float highestMMR, @RequestParam("didLowestWin") boolean didLowestWin){
        return "test";
    }

    @GetMapping("/test/opretdouble")
    @ResponseBody
    public String makeSingle(@RequestParam("trainingId") int trainingId, @RequestParam("player1ID") int player1ID, @RequestParam("player2ID") int player2ID){
        User player1 = userService.findById(player1ID);
        User player2 = userService.findById(player2ID);
        Union union = player1.getUnion();
        Training training = trainingService.findById(trainingId);

        Match match = new Match(player1, player2);

        union.addMatch(match);
        match.setUnion(union);

        training.addMatch(match);

        match.setTraining(training);

        trainingService.saveOrUpdate(training);
        unionService.saveOrUpdate(union);

        return "Created match between " + player1.getFirstname() + " " + player1.getLastname() + " and " + player2.getFirstname() + " " + player2.getLastname();
    }

    @GetMapping("/test/angivscore")
    @ResponseBody
    public String testMatches(@RequestParam("trainingId") int trainingId, Principal principal){
        User player1 = userService.findById(119);
        User player2 = userService.findById(160);
        User player3 = userService.findById(201);
        User player4 = userService.findById(242);

        Union union = player1.getUnion();
        Training training = trainingService.findById(trainingId);

        int[] match1_team1 = new int[]{21,19,21};
        int[] match1_team2 = new int[]{19,21,19};

        int[] match2_team1 = new int[]{21,19,21};
        int[] match2_team2 = new int[]{19,21,19};

        int[] match3_team1 = new int[]{21,19,21};
        int[] match3_team2 = new int[]{19,21,19};

        matchMaker(player1, player3, union, training, match1_team1, match1_team2);
        matchMaker(player2, player4, union, training, match2_team1, match2_team2);
        matchMaker(player1, player2, player3, player4, union, training, match3_team1, match3_team2);

        return matchService.findAll().toString();
    }

    private void matchMaker(User player1, User player2, Union union, Training training, int[] team1, int[] team2){
//        EloCalculator eloCalculator = new EloCalculator(userService);
//
//        eloCalculator.playMatch(match);
//
//        union.addMatch(match);
//        match.setUnion(union);
//
//        training.addMatch(match);
//
//        match.setTraining(training);
//
//        trainingService.saveOrUpdate(training);
//        unionService.saveOrUpdate(union);
    }

    private void matchMaker(User player1, User player2, User player3, User player4, Union union, Training training, int[] team1, int[] team2){
        EloCalculator eloCalculator = new EloCalculator(userService);

        Match match = new Match(player1, player2, player3, player4);
        match.updateScore(team1, team2);

        eloCalculator.playMatch(match);

        union.addMatch(match);
        match.setUnion(union);

        training.addMatch(match);

        match.setTraining(training);

        trainingService.saveOrUpdate(training);
        unionService.saveOrUpdate(union);
    }

    @GetMapping("/test/exists")
    @ResponseBody
    public String addDiscount(@RequestParam("date") String date, @RequestParam("startHour") String startHour, @RequestParam("endHour") String endHour, @RequestParam("court") int courtId){

        Court court = courtService.findById(courtId);

        System.out.println(bookingService.existsByDateAndStartHourAndEndHourAndCourt(date, startHour, endHour, court));

        return "OK";
    }

    @GetMapping("/test/delinv")
    @ResponseBody
    public String deleteInvitationById(@RequestParam("id") int id){
        EventInvitation eventInvitation = eventInvitationService.findById(id);

        Event event = eventInvitation.getEvent();
        User user = eventInvitation.getUser();

        user.removeEventInvitation(eventInvitation);
        userService.saveOrUpdate(user);

        event.removeUserInvitation(eventInvitation);
        eventService.saveOrUpdate(event);

        eventInvitationService.deleteEventInvitation(eventInvitation);

        return "OK";
    }

    @GetMapping("/test/event")
    @ResponseBody
    public String makeInvitations(){
        Union union = unionService.findById(1);

        DateTime now = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm");

        int invCount = 0;

        for (User user : union.getUsers()){
            for (Player player : user.getUnionAssosiation().getPlayerPositions()){
                Team team = player.getTeam();
                for (Event event : team.getEvents()){
                    if(!eventInvitationService.existsByEventAndUser(event, user)){
                        DateTime eventDeadlineTime = formatter.parseDateTime(event.getDeadlineDate() + " " + event.getDeadlineTime());

                        if (now.isBefore(eventDeadlineTime)){
                            invCount++;

                            EventInvitation eventInvitation = new EventInvitation();
                            eventInvitation.setEvent(event);
                            eventInvitation.setUser(user);

                            event.addUserInvitation(eventInvitation);
                            eventService.saveOrUpdate(event);
                        }
                    }
                }
            }
        }


        return String.valueOf(invCount);
    }

    @GetMapping("/test/invoice")
    @ResponseBody
    public String deletePaymentRequest(@RequestParam("id") int id){

        Invoice invoice = invoiceService.findById(id);
        invoiceService.deleteInvoice(invoice);

        UnionEconomics unionEconomics =invoice.getAssosiatedUnion().getUnionEconomics();
        unionEconomics.updateBalance();

        unionEconomicsService.saveOrUpdate(unionEconomics);

        return "OK";
    }

    @GetMapping("/test/debtreport")
    @ResponseBody
    public String deleteDebtReport(@RequestParam("id") int id){
        DebtReport debtReport = debtReportService.findById(id);

        debtReportService.deleteDebtReport(debtReport);

        return "OK";
    }

    @GetMapping("/test/feereport")
    @ResponseBody
    public String deleteFeeReport(@RequestParam("id") int id){
        FeeReport feeReport = feeReportService.findById(id);

        feeReportService.deleteFeeReport(feeReport);

        return "OK";
    }

    @GetMapping("/test/uniondelete")
    @ResponseBody
    public String getAccessToken(){

        Union union = unionService.findById(49123);

        unionService.deleteUnion(union);

        return "OK";
    }

    @GetMapping("/test/deluser")
    @ResponseBody
    public String deleteUser(){
        userService.deleteUser(userService.findById(50385));

        return "OK";
    }

    @GetMapping("/test/fixpayment")
    @ResponseBody
    public String fixPayments(){
        TeamSeason teamSeason = teamSeasonService.findByPaymentCode("YLX4PMi6Dd8oQbQBMOpN");
        teamSeason.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason);

        TeamSeason teamSeason1 = teamSeasonService.findByPaymentCode("B6HdkDNtDwcI0sF2TYZd");
        teamSeason1.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason1);

        TeamSeason teamSeason2 = teamSeasonService.findByPaymentCode("U0ms9dMCew3HZ1Nn3xFy");
        teamSeason2.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason2);

        TeamSeason teamSeason3 = teamSeasonService.findByPaymentCode("GthD6VF3nQ5zpqKu619q");
        teamSeason3.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason3);

        TeamSeason teamSeason4 = teamSeasonService.findByPaymentCode("4iY9Q0J7DGRz1lVRXWzD");
        teamSeason4.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason4);

        TeamSeason teamSeason5 = teamSeasonService.findByPaymentCode("CabqdDeHHmQc3jHZi54R");
        teamSeason5.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason5);

        TeamSeason teamSeason6 = teamSeasonService.findByPaymentCode("w5oUHJsaIpgLA7OS0n3i");
        teamSeason6.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason6);

        TeamSeason teamSeason7 = teamSeasonService.findByPaymentCode("ZhGFLEqc2lVZOdXpShwa");
        teamSeason7.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason7);

        TeamSeason teamSeason8 = teamSeasonService.findByPaymentCode("bpbZlKnQqPftpvPGToyD");
        teamSeason8.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason8);

        TeamSeason teamSeason9 = teamSeasonService.findByPaymentCode("NLcnKphzhgUw2iAEDgeK");
        teamSeason9.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason9);

        TeamSeason teamSeason10 = teamSeasonService.findByPaymentCode("qo9wzu5XpPgWbPEbjnHg");
        teamSeason10.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason10);

        TeamSeason teamSeason11 = teamSeasonService.findByPaymentCode("tOg94PtYPa4W38OYnJMe");
        teamSeason11.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason11);

        TeamSeason teamSeason12 = teamSeasonService.findByPaymentCode("TvQAlM4jPQV1kHueouTK");
        teamSeason12.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason12);

        TeamSeason teamSeason13 = teamSeasonService.findByPaymentCode("ZHplhD8Vu5D0Mn4zNzFw");
        teamSeason13.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason13);

        TeamSeason teamSeason14 = teamSeasonService.findByPaymentCode("WsSwoQZ5lzOExL7QcrQY");
        teamSeason14.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason14);

        TeamSeason teamSeason15 = teamSeasonService.findByPaymentCode("K2IJO65eOeKynWVcqEIP");
        teamSeason15.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason15);

        TeamSeason teamSeason16 = teamSeasonService.findByPaymentCode("IcPlcJ6IqpDhBmMxPy15");
        teamSeason16.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason16);

        TeamSeason teamSeason17 = teamSeasonService.findByPaymentCode("lT1cbMeSyEXkqxYgzHPo");
        teamSeason17.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason17);

        TeamSeason teamSeason18 = teamSeasonService.findByPaymentCode("VzusebVd8mmkpiFfhJey");
        teamSeason18.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason18);

        TeamSeason teamSeason19 = teamSeasonService.findByPaymentCode("xoZ8AIOQP51krfvaq1Rb");
        teamSeason19.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason19);

        TeamSeason teamSeason20 = teamSeasonService.findByPaymentCode("h9GWv7Uc7JBbVCW0cuPq");
        teamSeason20.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason20);

        TeamSeason teamSeason21 = teamSeasonService.findByPaymentCode("JNZaT0q6UjJF8ImMcyi6");
        teamSeason21.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason21);

        TeamSeason teamSeason22 = teamSeasonService.findByPaymentCode("GPzArRugY9U87uAriBFm");
        teamSeason22.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason22);

        TeamSeason teamSeason23 = teamSeasonService.findByPaymentCode("8IH4oPX1rzO960OaEEny");
        teamSeason23.setPaymentSuccessful(false);
        teamSeasonService.saveOrUpdate(teamSeason23);

        return "OK";
    }

    @GetMapping("/test/eco")
    @ResponseBody
    public String makeEconomicYears(){
        for (Union union : unionService.findAll()){
            UnionEconomics unionEconomics = union.getUnionEconomics();

            EconomicYear economicYear = new EconomicYear();
            economicYear.setUnionEconomics(unionEconomics);
            economicYear.setYear(DateFactory.getCurrentSeasonYear());

            unionEconomics.addEconomicYear(economicYear);
            unionEconomicsService.saveOrUpdate(unionEconomics);
        }

        return "OK";
    }

    @GetMapping("/test/userseason")
    @ResponseBody
    public String fixUserSeason(){
        for (UserSeason userSeason : userSeasonRepository.findAll()){
            if (userSeason.getUnionSeason() == null){
                userSeasonRepository.delete(userSeason);
            }
        }

        return "OK";
    }

    @GetMapping("/test/payment")
    @ResponseBody
    public String setNotPayed(@RequestParam("code") String code){
        TeamSeason teamSeason = teamSeasonService.findByPaymentCode(code);

        teamSeason.setPaymentSuccessful(false);

        teamSeasonService.saveOrUpdate(teamSeason);

        return "OK";
    }

    @GetMapping("/test/userr")
    @ResponseBody
    public String showUser(@RequestParam("id") int id, @RequestParam("invoice") int invoice, @RequestParam("team") int teamid){

        User user = userService.findById(id);

        UnionSeason unionSeason = unionSeasonService.findByUnionAndSeasonYear(user.getUnion(), "2019/2020 - 1");

        UserSeason userSeason = new UserSeason();
        userSeason.setUnionSeason(unionSeason);
        userSeason.setUser(user);
        userSeasonService.saveOrUpdate(userSeason);

        unionSeason.addUserSeason(userSeason);
        unionSeasonService.saveOrUpdate(unionSeason);

        user.addUserSeason(userSeason);
        userService.saveOrUpdate(user);

        TeamSeason teamSeason = new TeamSeason();
        teamSeason.setPaymentSuccessful(true);
        teamSeason.setPaymentCode(SecretFactory.makeAlphaNumericString(20));
        teamSeason.setDateJoined(DateFactory.getCurrentDate());
        teamSeason.setInvoice(invoiceService.findById(invoice));
        teamSeason.setUserSeason(userSeason);
        teamSeasonService.saveOrUpdate(teamSeason);

        userSeason.addTeamSeason(teamSeason);
        userSeasonService.saveOrUpdate(userSeason);

        Team team = teamService.findById(teamid);
        team.addTeamSeason(teamSeason);
        teamService.saveOrUpdate(team);

        teamSeason.setTeam(team);
        teamSeasonService.saveOrUpdate(teamSeason);


        return "OK";
    }
}


