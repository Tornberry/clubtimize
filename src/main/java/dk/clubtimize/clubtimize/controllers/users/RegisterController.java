package dk.clubtimize.clubtimize.controllers.users;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.controllers.support.PaymentGatewaySupport;
import dk.clubtimize.clubtimize.controllers.support.SecretFactory;
import dk.clubtimize.clubtimize.controllers.support.UnionSeasonStringService;
import dk.clubtimize.clubtimize.controllers.support.mail.MailSender;
import dk.clubtimize.clubtimize.models.training.PlayerParticipation;
import dk.clubtimize.clubtimize.models.training.Training;
import dk.clubtimize.clubtimize.models.union.*;
import dk.clubtimize.clubtimize.models.union.event.Event;
import dk.clubtimize.clubtimize.models.union.event.EventInvitation;
import dk.clubtimize.clubtimize.models.user.*;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;
import dk.clubtimize.clubtimize.services.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import java.security.Principal;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

@Controller
public class RegisterController {

    private UserService userService;
    private TeamService teamService;
    private TrainingService trainingService;
    private UnionSeasonService unionSeasonService;
    private UserSeasonService userSeasonService;
    private TeamSeasonService teamSeasonService;
    private EconomicYearService economicYearService;
    private UnionService unionService;
    private PlayerService playerService;
    private PasswordResetService passwordResetService;
    private EventService eventService;
    private EventInvitationService eventInvitationService;
    private UnionSeasonStringService unionSeasonStringService;

    @Autowired
    public RegisterController(UserService userService, TeamService teamService, TrainingService trainingService, UnionSeasonService unionSeasonService, UserSeasonService userSeasonService, TeamSeasonService teamSeasonService, EconomicYearService economicYearService, UnionService unionService, PlayerService playerService, PasswordResetService passwordResetService, EventService eventService, EventInvitationService eventInvitationService, UnionSeasonStringService unionSeasonStringService) {
        this.userService = userService;
        this.teamService = teamService;
        this.trainingService = trainingService;
        this.unionSeasonService = unionSeasonService;
        this.userSeasonService = userSeasonService;
        this.teamSeasonService = teamSeasonService;
        this.economicYearService = economicYearService;
        this.unionService = unionService;
        this.playerService = playerService;
        this.passwordResetService = passwordResetService;
        this.eventService = eventService;
        this.eventInvitationService = eventInvitationService;
        this.unionSeasonStringService = unionSeasonStringService;
    }

    @PostMapping("/register")
    public String postRegister(@Valid User user, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            System.out.println(bindingResult.getAllErrors());
        }

        userService.createUser(user, "USER");

        return "users/login";
    }

    @PostMapping("/makeresetrequest")
    @ResponseBody
    public void requestReset(@RequestParam("email") String email){
        if (userService.existsByEmail(email)){
            User user = userService.findByEmail(email);
            if (passwordResetService.existsByEmail(email)){
                PasswordReset passwordReset = passwordResetService.findByEmail(email);
                passwordResetService.deleteByResetId(passwordReset.getResetId());

                String id = SecretFactory.makeAlphaNumericString(20);
                if (passwordResetService.existsByResetId(id)){
                    id = SecretFactory.makeAlphaNumericString(20);
                }
                passwordResetService.saveOrUpdate(new PasswordReset(user.getEmail(), id, DateFactory.getCurrentDateAndTime()));
                MailSender.resetPassword(user, id);
            } else {
                String id = SecretFactory.makeAlphaNumericString(20);
                if (passwordResetService.existsByResetId(id)){
                    id = SecretFactory.makeAlphaNumericString(20);
                }
                passwordResetService.saveOrUpdate(new PasswordReset(user.getEmail(), id, DateFactory.getCurrentDateAndTime()));
                MailSender.resetPassword(user, id);
            }
        }
    }

    @GetMapping("/resetpassword")
    public Object getResetPassword(@RequestParam("c") String id){
        if (passwordResetService.existsByResetId(id)){
            return "users/resetPassword/resetPassword";
        } else {
            return new RedirectView("/login");
        }
    }

    @PostMapping("/resetpassword")
    @ResponseBody
    public String PostResetPassword(@RequestBody String json){
        JSONObject jsonObject = new JSONObject(json);
        if (passwordResetService.existsByResetId(jsonObject.getString("Id"))){

            PasswordReset passwordReset = passwordResetService.findByResetId(jsonObject.getString("Id"));
            User user = userService.findByEmail(passwordReset.getEmail());

            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            user.setPassword(bCryptPasswordEncoder.encode(jsonObject.getString("Password")));

            userService.saveOrUpdate(user);
            passwordResetService.deleteByResetId(jsonObject.getString("Id"));

            return "OK";
        } else {
            return "does_not_exist";
        }
    }

    private String formatDateNumber(String value) {
        if (value.length() == 1){
            return "0" + value;
        } else {
            return value;
        }
    }

    @PostMapping("/jointeam")
    @ResponseBody
    public String joinTeam(@RequestBody String json){

        JSONObject jsonObject = new JSONObject(json);

        if (!userService.existsByEmail(jsonObject.getString("email"))){
            Team team = teamService.findById(jsonObject.getInt("teamId"));

            User user = new User();
            user.setEmail(jsonObject.getString("email"));
            user.setPassword(jsonObject.getString("password"));
            user.setFirstname(jsonObject.getString("firstname"));
            user.setLastname(jsonObject.getString("lastname"));

            String day = jsonObject.getString("birthday").split("-")[0];
            String month = jsonObject.getString("birthday").split("-")[1];
            String year = jsonObject.getString("birthday").split("-")[2];

            user.setBirthday(formatDateNumber(day) + "-" + formatDateNumber(month) + "-" + year);
            user.setGender(jsonObject.getString("gender"));
            user.setAddress(jsonObject.getString("address"));
            user.setPhone(jsonObject.getInt("phone"));
            user.setUnion(team.getUnion());

            Player player = new Player();
            player.setUnion(team.getUnion());
            player.setTeam(team);
            team.addPlayer(player);

            UnionAssosiation unionAssosiation = new UnionAssosiation();
            unionAssosiation.addPlayerPosition(player);
            unionAssosiation.setUser(user);
            unionAssosiation.setUnion(team.getUnion());
            player.setUnionAssosiation(unionAssosiation);

            user.setUnionAssosiation(unionAssosiation);
            user.setUnion(team.getUnion());

            UserSeason userSeason = new UserSeason();
            userSeason.setUnionSeason(unionSeasonService.findByUnionAndSeasonYear(team.getUnion(), unionSeasonStringService.getSeasonStringByUnionSeason(unionAssosiation.getUnion())));
            userSeason.setUser(user);
            user.addUserSeason(userSeason);

            TeamSeason teamSeason = new TeamSeason();
            teamSeason.setPaymentCode(makeAlphaNumericString(20));
            teamSeason.setTeam(team);
            teamSeason.setPaymentSuccessful(false);
            teamSeason.setDateJoined(DateFactory.getCurrentDate());
            teamSeason.setUserSeason(userSeason);
            userSeason.addTeamSeason(teamSeason);

            team.addTeamSeason(teamSeason);

            MatchStats matchStats = new MatchStats();
            EloPoints eloPoints = new EloPoints(1000, 1000, 1000, 1000, 1000);
            eloPoints.setMatchStats(matchStats);
            matchStats.setEloPoints(eloPoints);
            matchStats.setUser(user);
            user.setMatchStats(matchStats);

            userService.createUser(user, "Spiller");

            user.setEventInvitations(handleEventInvitations(user, team));

            userService.saveOrUpdate(user);

            for (Training training : team.getTrainings()){
                DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
                DateTime today = formatter.parseDateTime(new DateTime().toString("dd-MM-yyyy"));
                DateTime trainingDate = formatter.parseDateTime(training.getDate());

                if (trainingDate.isAfter(today)){
                    PlayerParticipation playerParticipation = new PlayerParticipation();
                    playerParticipation.setPlayer(player);
                    playerParticipation.setTraining(training);
                    training.addPlayerParticipation(playerParticipation);

                    trainingService.saveOrUpdate(training);
                }
            }

            MailSender.playerWelcomeMail(user);

            // Payment stuff ---------------------------------------------------------------------------------------------------------------------

            PaymentGatewaySupport paymentGatewaySupport = new PaymentGatewaySupport(team, teamSeason.getPaymentCode());

            float priceWithoutAdditional = paymentGatewaySupport.calculatePrices(team, new ArrayList<>())[0];
            float priceWithAdditional = paymentGatewaySupport.calculatePrices(team, new ArrayList<>())[1];

            return new PaymentGatewaySupport(team, teamSeason.getPaymentCode()).getPaymentGateway(priceWithAdditional, priceWithoutAdditional);
        } else {
            return "email_in_use";
        }
    }

    private List<EventInvitation> handleEventInvitations(User user, Team team) {
        DateTime now = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm");

        List<EventInvitation> eventInvitations = new ArrayList<>();

        for (Event event : team.getEvents()){
            if(!eventInvitationService.existsByEventAndUser(event, user)){
                DateTime eventDeadlineTime = formatter.parseDateTime(event.getDeadlineDate() + " " + event.getDeadlineTime());

                if (now.isBefore(eventDeadlineTime)){
                    EventInvitation eventInvitation = new EventInvitation();
                    eventInvitation.setEvent(event);
                    eventInvitation.setUser(user);

                    event.addUserInvitation(eventInvitation);
                    eventService.saveOrUpdate(event);

                    eventInvitations.add(eventInvitation);
                }
            }
        }

        return eventInvitations;
    }

    private String makeAlphaNumericString(int capacity){
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        StringBuilder stringBuilder = new StringBuilder(capacity);
        SecureRandom secureRandom = new SecureRandom();

        for(int i=0; i<capacity; i++){
            stringBuilder.append(AB.charAt(secureRandom.nextInt(AB.length())));
        }

        if (teamSeasonService.existsByPaymentCode(stringBuilder.toString())){
            stringBuilder.replace(0, stringBuilder.length(), makeAlphaNumericString(20));
        }

        return stringBuilder.toString();
    }

    @GetMapping("/payforseason")
    @ResponseBody
    public RedirectView payNow(@RequestParam("season") String season, @RequestParam("team") int teamId, Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();
        User user = userService.findByUsername(principal.getName());
        UserSeason userSeason = userSeasonService.findByUnionSeasonAndUser(unionSeasonService.findByUnionAndSeasonYear(union, season), user);

        Team team = teamService.findById(teamId);

        List<Team> alreadyJoinedTeams = new ArrayList<>();
        for (Player player1 : user.getUnionAssosiation().getPlayerPositions()){
            if (teamSeasonService.findByUserSeasonAndTeam(userSeason, player1.getTeam()).isPaymentSuccessful()){
                alreadyJoinedTeams.add(player1.getTeam());
            }
        }

        alreadyJoinedTeams.remove(team);

        PaymentGatewaySupport paymentGatewaySupport = new PaymentGatewaySupport(team, teamSeasonService.findByUserSeasonAndTeam(userSeason, teamService.findById(teamId)).getPaymentCode());

        float priceWithoutAdditional = paymentGatewaySupport.calculatePrices(team, alreadyJoinedTeams)[0];
        float priceWithAdditional = paymentGatewaySupport.calculatePrices(team, alreadyJoinedTeams)[1];

        // Fetches a link where discount has been taken into account
        return new RedirectView(paymentGatewaySupport.getPaymentGateway(priceWithAdditional, priceWithoutAdditional));
    }
}
