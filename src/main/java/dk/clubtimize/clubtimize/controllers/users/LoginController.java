package dk.clubtimize.clubtimize.controllers.users;

import dk.clubtimize.clubtimize.services.UnionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

    private UnionService unionService;

    @Autowired
    public LoginController(UnionService unionService) {
        this.unionService = unionService;
    }

    @GetMapping("/login")
    public String getLoginPage(Model model){
        return "users/login";
    }
}
