package dk.clubtimize.clubtimize.controllers.logging;

import dk.clubtimize.clubtimize.controllers.support.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping("/logging")
public class LoggingController {

    private Logger logger = LoggerFactory.getLogger(LoggingController.class);

    @GetMapping("/log")
    @ResponseBody
    public String showLogsInConsole(){
        logger.trace("trace");
        logger.debug("debug");
        logger.info("info");
        logger.warn("warn");
        logger.error("error");

        return "see console";
    }

    @GetMapping("/readlog")
    @ResponseBody
    public String returnLog(@RequestHeader(value = "authentication", defaultValue = "noauth") String authToken, HttpServletRequest request){
        if (authToken.equals("85TcvYRevzYcSeC2")){
            String log = LogFactory.getLog();
            logger.info("Returned log to user with ip: " + getClientIp(request));
            return log;
        } else {
            return "Forbidden";
        }
    }

    private static String getClientIp(HttpServletRequest request) {

        String remoteAddr = "";

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }

        return remoteAddr;
    }
}
