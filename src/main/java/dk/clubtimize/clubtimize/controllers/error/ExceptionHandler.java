package dk.clubtimize.clubtimize.controllers.error;

import dk.clubtimize.clubtimize.services.UnionService;
import dk.clubtimize.clubtimize.services.UserService;
import org.apache.http.HttpStatus;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
public class ExceptionHandler implements ErrorController {

    private UserService userService;

    @Autowired
    public ExceptionHandler(UserService userService) {
        this.userService = userService;
    }

    /**
     * This class handles errors with custom error pages. The point of having this class instead of Thymleaf's error handling, is the ability to send attributes to the page.
     *
     * @param request       The HTML protocol information such as request headers
     * @param model         The communication between Thymeleaf and Java
     * @param principal     The current user loggged in
     * @return              The relevant error page
     */
    @GetMapping("/error")
    public String handleError(HttpServletRequest request, Model model, Principal principal){
        switch (request.getAttribute("javax.servlet.error.status_code").toString()) {
            case "404":
                if(principal == null){
                    return "errorOutsidePanel/404";
                } else {
                    model.addAttribute("currentUser", userService.findByUsername(principal.getName()));
                    model.addAttribute("currentUnion", userService.findByUsername(principal.getName()).getUnion());
                    model.addAttribute("year", new DateTime().getYear());
                    return "error/404";
                }
            case "500":
                if(principal == null){
                    return "errorOutsidePanel/500";
                } else {
                    model.addAttribute("currentUser", userService.findByUsername(principal.getName()));
                    model.addAttribute("currentUnion", userService.findByUsername(principal.getName()).getUnion());
                    model.addAttribute("year", new DateTime().getYear());
                    return "error/500";
                }
            default:
                model.addAttribute("currentUser", userService.findByUsername(principal.getName()));
                model.addAttribute("currentUnion", userService.findByUsername(principal.getName()).getUnion());
                return "/panel";
        }
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
