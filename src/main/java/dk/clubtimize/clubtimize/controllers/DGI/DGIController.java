package dk.clubtimize.clubtimize.controllers.DGI;

import dk.clubtimize.clubtimize.models.opsætning.ExerciseDGI;
import dk.clubtimize.clubtimize.services.ExerciseDGIService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/DGI")
public class DGIController {

    private ExerciseDGIService exerciseDGIService;

    @Autowired
    public DGIController(ExerciseDGIService exerciseDGIService) {
        this.exerciseDGIService = exerciseDGIService;
    }

    /**
     * Scrapes all exercises from the DGI training guide and puts it in the database.
     * The scraper uses a browser driver called chromedriver, which should be located together with the jar on runtime.
     *
     * @return      The 200 status code from HTTP protocol
     */
    @GetMapping("/scrapeøvelser")
    public ResponseEntity scrapeDGITrainingGuide() {

        List<ExerciseDGI> exerciseListDGI;
        exerciseListDGI = scrapeExercises();
        if (exerciseListDGI != null){
            exerciseDGIService.saveAll(exerciseListDGI);
        }

        return new ResponseEntity(HttpStatus.OK);
    }


    /**
     * Will scrape the exercises from their indivisual page based on their id.
     * Downloads the HTML and filters the correct elements into variables.
     * Finally instantiates and inizializes of type ExerciseDGI and lists them for saving
     *
     * @return      List of ExerciseDGI objects for saving
     */
    private List<ExerciseDGI> scrapeExercises() {

            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\chromedriver.exe");
            List<ExerciseDGI> exerciseDGIList = new ArrayList<>();
            List<Integer> integers = getExcerciseIdList();

            integers.forEach(id -> exerciseDGIList.add(makeExerciseDGIObject(id)));

            return exerciseDGIList;
    }

    private ExerciseDGI makeExerciseDGIObject(int id){
        try {
            String URL = "https://www.dgi.dk/badminton/oevelser/" + id;
            Document document = null;
            document = Jsoup.connect(URL).get();

            // ---------------------------------------------
            String nameOfExercise = document.getElementsByClass("oevelse").first().select("h1").first().text();
            String lead = document.getElementsByClass("oevelse").first().getElementsByClass("lead").text();


            // ---------------------------------------------
            Element quickinfo = document.getElementsByClass("quickinfo").first();
            String sport = quickinfo.select("dd").get(0).text();
            String category = quickinfo.select("dd").get(1).text();
            String emneord = quickinfo.select("dd").get(2).text();
            String time = quickinfo.select("dd").get(3).text();
            String level = quickinfo.select("dd").get(4).text();
            String agegroup = quickinfo.select("dd").get(5).text();
            String participants = quickinfo.select("dd").get(6).text();

            // ---------------------------------------------
            Element description = document.getElementsByClass("description").first();
            description.select("h2").first().remove();
            String descriptionAsHtml = description.html();

            // ---------------------------------------------
            Element mediaviewer = document.getElementsByClass("mediaviewer").first();
            String videoUrl = null;
            if (mediaviewer != null){
                if(mediaviewer.select("iframe").first() != null){
                    videoUrl = mediaviewer.select("iframe").first().attr("src");
                }
            }

            System.out.println("Scraped exercise with name: " + nameOfExercise);
            return new ExerciseDGI(nameOfExercise, lead, sport, category, emneord, time, level, agegroup, participants, descriptionAsHtml, videoUrl);

        } catch (Exception e){
            return null;
        }

    }

    /**
     * Gets the id of every exercise in the badminton category.
     * This is used when visiting every indivisual exercise site for scraping.
     *
     * @return      The list of exercise id's
     */
    private List<Integer> getExcerciseIdList() {
        final Document document = getDocument();
        List<Integer> integers = new ArrayList<>();
        Elements divs = document.getElementsByClass("content");
        divs.forEach(x -> integers.add(Integer.parseInt(x.select("a[href]").attr("href").split("/")[3])));

//        for (Element element : divs){
//            integers.add(Integer.parseInt(element.select("a[href]").attr("href").split("/")[3]));
//        }

        return integers;
    }

    private Document getDocument() {
        final String url = "https://www.dgi.dk/badminton/oevelser";
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        WebDriver driver = new ChromeDriver(options);
        driver.get(url);
        ExpectedCondition<Boolean> expectation = driver1 -> ((JavascriptExecutor) driver1).executeScript("return document.readyState").toString().equals("complete");
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, 120);
            wait.until(expectation);
        } catch (Throwable error) {
            System.out.println("An error happened: " + error.getMessage());
        }

        Document doc = Jsoup.parse(driver.getPageSource());
        driver.close();

        return doc;
    }

    /**
     * API for getting every exercise as JSON. Used in front end development
     * @return      Exercises as JSON string
     */
    @GetMapping("/getøvelser")
    @ResponseBody
    public String parseØvelser(){

        JSONArray jsonString = new JSONArray();

        Iterable<ExerciseDGI> øvelsesListe = exerciseDGIService.findAll();

        for (ExerciseDGI exercise : øvelsesListe){
            JSONObject object = new JSONObject();
            object.put("id", exercise.getId());
            object.put("name", exercise.getName());
            object.put("lead", exercise.getLead());
            object.put("sport", exercise.getSport());
            object.put("category", exercise.getCategory());
            object.put("emneord", exercise.getEmneord());
            object.put("time", exercise.getTime());
            object.put("level", exercise.getLead());
            object.put("agegroup", exercise.getAgegroup().substring(0, exercise.getAgegroup().length() - 3));
            object.put("participants", exercise.getParticipants().substring(0, exercise.getParticipants().length() - 10));
            object.put("description", exercise.getDescription());
            object.put("videoUrl", exercise.getVideoUrl());

            jsonString.put(object);
        }

        return jsonString.toString();
    }
}
