package dk.clubtimize.clubtimize.controllers.panel.træning;

import dk.clubtimize.clubtimize.controllers.support.OwnershipValidator;
import dk.clubtimize.clubtimize.controllers.support.PermissionEnum;
import dk.clubtimize.clubtimize.controllers.support.RoleSupport;
import dk.clubtimize.clubtimize.models.union.Coach;
import dk.clubtimize.clubtimize.models.union.matches.Match;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.models.adapters.ModuleAdapter;
import dk.clubtimize.clubtimize.models.adapters.ModuleCommentAdapter;
import dk.clubtimize.clubtimize.models.training.ModuleComment;
import dk.clubtimize.clubtimize.models.training.PlayerParticipation;
import dk.clubtimize.clubtimize.models.training.Training;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.training.TrainingModule;
import dk.clubtimize.clubtimize.services.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/panel")
public class TræningController {

    private TrainingService trainingService;
    private UserService userService;
    private TrainingModuleService trainingModuleService;
    private ExerciseDGIService exerciseDGIService;
    private PlayerParticipationService playerParticipationService;
    private UnionService unionService;

    @Autowired
    public TræningController(TrainingService trainingService, UserService userService, TrainingModuleService trainingModuleService, ExerciseDGIService exerciseDGIService, PlayerParticipationService playerParticipationService, UnionService unionService) {
        this.trainingService = trainingService;
        this.userService = userService;
        this.trainingModuleService = trainingModuleService;
        this.exerciseDGIService = exerciseDGIService;
        this.playerParticipationService = playerParticipationService;
        this.unionService = unionService;
    }

    @GetMapping("/træning")
    public String showTræning(@RequestParam("id") int id, Principal principal, Model model){
        Training training = trainingService.findById(id);
        User user = userService.findByUsername(principal.getName());
        Union union = user.getUnion();

        boolean isTeamCoach = isCoach(user, training);

        if ((OwnershipValidator.validateOwnership(training, userService.findByUsername(principal.getName())) && training != null) || isTeamCoach){
            model.addAttribute("training", training);
            return "panel/træning/træning";
        } else {
            return "error/404";
        }
    }

    private boolean isCoach(User user, Training training) {
        for (Coach coach : user.getUnionAssosiation().getCoachPositions()){
            if (training.getTeam().getCoaches().contains(coach)){
                return true;
            }
        }
        return false;
    }

    @GetMapping("/træning/redigertræningsplan")
    public String showRedigerTræningsplan(Model model, @RequestParam("trainingId") int id, Principal principal){
        Training training = trainingService.findById(id);
        User user = userService.findByUsername(principal.getName());
        Union union = userService.findByUsername(principal.getName()).getUnion();

        boolean isTeamCoach = false;

        for (Coach coach : user.getUnionAssosiation().getCoachPositions()){
            if (training.getTeam().getCoaches().contains(coach)){
                isTeamCoach = true;
            }
        }

        if ((OwnershipValidator.validateOwnership(training, userService.findByUsername(principal.getName())) && training != null) || isTeamCoach){
            if (RoleSupport.checkPermission(PermissionEnum.canUseTrainerFeatures, userService.findByUsername(principal.getName()))){
                model.addAttribute("training", training);
                model.addAttribute("exercises", exerciseDGIService.findAll());
                return "panel/træning/træningsplan/redigertræningsplan";
            } else {
                return "error/404";
            }
        } else {
            return "error/404";
        }
    }

    @PostMapping("/træning/redigertræningsplan")
    public ResponseEntity postRedigerTræningsplan(@RequestBody ModuleAdapter moduleAdapter, BindingResult bindingResult, Principal principal){
        Training training = trainingService.findById(moduleAdapter.getTrainingId());
        User user = userService.findByUsername(principal.getName());

        boolean isTeamCoach = false;

        for (Coach coach : user.getUnionAssosiation().getCoachPositions()){
            if (training.getTeam().getCoaches().contains(coach)){
                isTeamCoach = true;
            }
        }

        if ((OwnershipValidator.validateOwnership(training, userService.findByUsername(principal.getName())) && training != null) || isTeamCoach){
            if (moduleAdapter.getExerciseId() == 0){
                training.getTrainingsplan().addModule(new TrainingModule(moduleAdapter.getTitle(), moduleAdapter.getDescription(), moduleAdapter.getApproxTime(), training.getTrainingsplan()));
            } else {
                training.getTrainingsplan().addModule(new TrainingModule(moduleAdapter.getTitle(), moduleAdapter.getDescription(), moduleAdapter.getApproxTime(), training.getTrainingsplan(), exerciseDGIService.findById(moduleAdapter.getExerciseId())));
            }

            trainingService.saveOrUpdate(training);
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return  new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/træning/redigertræningsplan")
    public ResponseEntity deleteModule(@RequestBody int id, BindingResult bindingResult, Principal principal){
        TrainingModule trainingModule = trainingModuleService.findById(id);
        User user = userService.findByUsername(principal.getName());

        if (OwnershipValidator.validateOwnership(trainingModule, userService.findByUsername(principal.getName()))){
            trainingModuleService.deleteModule(trainingModule);
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping("/træning/modul/addcomment")
    public ResponseEntity addCommentToModule(@RequestBody ModuleCommentAdapter moduleCommentAdapter, Principal principal, BindingResult bindingResult){
        TrainingModule trainingModule = trainingModuleService.findById(moduleCommentAdapter.getModuleId());

        if(OwnershipValidator.validateOwnership(trainingModule, userService.findByUsername(principal.getName()))){
            ModuleComment moduleComment = new ModuleComment();
            moduleComment.setDate(new SimpleDateFormat("dd-MM-yyyy HH:mm").format(new Date()));
            User user = userService.findByUsername(principal.getName());
            moduleComment.setAuthor(user.getFirstname() + " " + user.getLastname());
            moduleComment.setComment(moduleCommentAdapter.getComment());
            moduleComment.setTrainingModule(trainingModule);

            trainingModule.addComment(moduleComment);

            trainingModuleService.saveOrUpdate(trainingModule);

            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/træning/afkrydsning/changeparticipationstate")
    public ResponseEntity changeParticipationState(@RequestBody int id, Principal  principal){
        PlayerParticipation playerParticipation = playerParticipationService.findById(id);

        if (OwnershipValidator.validateOwnership(playerParticipation, userService.findByUsername(principal.getName()))){
            if (playerParticipation.isParticipated()){
                playerParticipation.setParticipated(false);
            } else {
                playerParticipation.setParticipated(true);
            }
            playerParticipationService.saveOrUpdate(playerParticipation);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/træning/focus/editfocus")
    @ResponseBody
    public ResponseEntity editFocus(@RequestBody String json, Principal principal){
        JSONObject jsonObject = new JSONObject(json);

        Training training = trainingService.findById(jsonObject.getInt("id"));
        training.getFocus().setTitle(jsonObject.getString("title"));
        training.getFocus().setDescription(jsonObject.getString("description"));
        trainingService.saveOrUpdate(training);

        return new ResponseEntity(HttpStatus.OK);
    }
}
