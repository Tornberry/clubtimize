package dk.clubtimize.clubtimize.controllers.panel.administration.team;

import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/panel")
public class ManageTeamController {

    private UserService userService;

    @Autowired
    public ManageTeamController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/administrerhold")
    public String showPage(Model model, Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();

        model.addAttribute("departments", union.getDepartments());

        return "panel/management/manageTeams/manageTeams";
    }

}
