package dk.clubtimize.clubtimize.controllers.panel.setup;

import dk.clubtimize.clubtimize.controllers.support.OwnershipValidator;
import dk.clubtimize.clubtimize.controllers.support.PermissionEnum;
import dk.clubtimize.clubtimize.controllers.support.RoleSupport;
import dk.clubtimize.clubtimize.models.opsætning.booking.SingleBookingPeriod;
import dk.clubtimize.clubtimize.models.opsætning.Court;
import dk.clubtimize.clubtimize.models.opsætning.Hall;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.adapters.HallAdapter;
import dk.clubtimize.clubtimize.models.training.Training;
import dk.clubtimize.clubtimize.models.training.TrainingSchedule;
import dk.clubtimize.clubtimize.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@RequestMapping("/panel/opsætning")
public class HallSetupController {

    private UserService userService;
    private UnionService unionService;
    private HallService hallService;
    private TrainingService trainingService;
    private TrainingScheduleService trainingScheduleService;
    private SingleBookingPeriodService singleBookingPeriodService;
    private BookingService bookingService;

    @Autowired
    public HallSetupController(UserService userService, UnionService unionService, HallService hallService, TrainingService trainingService, TrainingScheduleService trainingScheduleService, SingleBookingPeriodService singleBookingPeriodService, BookingService bookingService) {
        this.userService = userService;
        this.unionService = unionService;
        this.hallService = hallService;
        this.trainingService = trainingService;
        this.trainingScheduleService = trainingScheduleService;
        this.singleBookingPeriodService = singleBookingPeriodService;
        this.bookingService = bookingService;
    }

    @GetMapping("/haller")
    public String showHallerOgbaner(Principal principal){
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            return "panel/setup/halls/halls";
        } else {
            return "error/404.html";
        }
    }

    @PostMapping("/haller")
    public ResponseEntity postNewCategory(@RequestBody HallAdapter hallAdapter, BindingResult bindingResult, Principal principal){

        Union union = userService.findByUsername(principal.getName()).getUnion();

        if (hallAdapter.getHallId() == 0){
            Hall hall = new Hall();
            hall.setCourtsAvalible(hallAdapter.getCourtsAvalible());
            hall.setLatitude(hallAdapter.getLatitude());
            hall.setLongitude(hallAdapter.getLongitude());
            hall.setTitle(hallAdapter.getTitle());
            hall.setUnion(union);

            union.addHall(hall);

            for (int i = 0; i < hall.getCourtsAvalible(); i++){
                Court court = new Court();
                court.setTitle("Bane " + (i + 1));
                court.setHall(hall);

                hall.addCourt(court);
            }

            unionService.saveOrUpdate(union);
        } else {
            Hall hall = hallService.findByHallId(hallAdapter.getHallId());
            hall.setTitle(hallAdapter.getTitle());
            hall.setCourtsAvalible(hallAdapter.getCourtsAvalible());
            hall.setLongitude(hallAdapter.getLongitude());
            hall.setLatitude(hallAdapter.getLatitude());

            hallService.saveOrUpdate(hall);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/haller/slethal")
    public ResponseEntity deleteHall(@RequestBody int id, Principal principal){
        Hall hall = hallService.findByHallId(id);
        if (OwnershipValidator.validateOwnership(hall, userService.findByUsername(principal.getName()))){

            for (Training training : hall.getTrainings()){
                training.setHall(null);
                trainingService.saveOrUpdate(training);
            }

            for (TrainingSchedule trainingSchedule : hall.getTrainingSchedules()){
                trainingSchedule.setHall(null);
                trainingScheduleService.saveOrUpdate(trainingSchedule);
            }

            for (SingleBookingPeriod singleBookingPeriod : hall.getSingleBookingPeriods()){
                bookingService.deleteAll(singleBookingPeriod.getBookings());
                singleBookingPeriodService.deleteBookingPeriod(singleBookingPeriod);
            }

            hallService.deleteHall(hall);
        }
        return new ResponseEntity(HttpStatus.OK);
    }
}
