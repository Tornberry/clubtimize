package dk.clubtimize.clubtimize.controllers.panel.setup;

import dk.clubtimize.clubtimize.controllers.support.PermissionEnum;
import dk.clubtimize.clubtimize.controllers.support.RoleSupport;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.adapters.RoleAdapter;
import dk.clubtimize.clubtimize.models.user.Role;
import dk.clubtimize.clubtimize.models.user.RolePermissions;
import dk.clubtimize.clubtimize.services.UnionService;
import dk.clubtimize.clubtimize.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/panel/opsætning")
public class RoleSetupController {

    private UserService userService;
    private UnionService unionService;

    @Autowired
    public RoleSetupController(UserService userService, UnionService unionService) {
        this.userService = userService;
        this.unionService = unionService;
    }

    @GetMapping("/roller")
    public String showRolleOpsætning(Principal principal){
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            return "panel/setup/rolls/rolls";
        } else {
            return "error/404.html";
        }
    }

    @PostMapping("/roller/newrole")
    public ResponseEntity postNyRolle(@RequestBody RoleAdapter roleAdapter, Principal principal){

        Role role = new Role();
        Union union = userService.findByUsername(principal.getName()).getUnion();
        role.setRoleName(roleAdapter.getRoleName());
        role.setUnion(union);

        RolePermissions rolePermissions = new RolePermissions();

        rolePermissions.setCanAdministrate(roleAdapter.isCanAdministrate());
        rolePermissions.setCanSeeAdministrationPages(roleAdapter.isCanSeeAdministrationPages());
        rolePermissions.setCanSeeAllTeams(roleAdapter.isCanSeeAllTeams());
        rolePermissions.setCanUseTrainerFeatures(roleAdapter.isCanUseTrainerFeatures());

        role.setRolePermissions(rolePermissions);

        union.addRole(role);

        unionService.saveOrUpdate(union);

        return new ResponseEntity(HttpStatus.OK);
    }

}
