package dk.clubtimize.clubtimize.controllers.panel.user;

import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/panel")
public class RegisterEventsController {

    private UserService userService;

    @Autowired
    public RegisterEventsController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/tilmeldevents")
    public String showPage(Model model, Principal principal){
        User user = userService.findByUsername(principal.getName());

        return "panel/user/registerevents/registerEvent";
    }
}
