package dk.clubtimize.clubtimize.controllers.panel.courtbooking;

import dk.clubtimize.clubtimize.controllers.support.CourtBookingFetcher;
import dk.clubtimize.clubtimize.models.opsætning.booking.Booking;
import dk.clubtimize.clubtimize.models.opsætning.Court;
import dk.clubtimize.clubtimize.models.opsætning.Hall;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.services.*;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.*;

@Controller
@RequestMapping("/panel")
public class CourtbookingController {

    private UnionService unionService;
    private UserService userService;
    private HallService hallService;
    private CourtService courtService;
    private SingleBookingPeriodService singleBookingPeriodService;
    private BookingService bookingService;

    @Autowired
    public CourtbookingController(UnionService unionService, UserService userService, HallService hallService, CourtService courtService, SingleBookingPeriodService singleBookingPeriodService, BookingService bookingService) {
        this.unionService = unionService;
        this.userService = userService;
        this.hallService = hallService;
        this.courtService = courtService;
        this.singleBookingPeriodService = singleBookingPeriodService;
        this.bookingService = bookingService;
    }

    @GetMapping("/banebooking")
    public String showPage(Principal principal){
        return "panel/courtbooking/courtbooking";
    }


    @GetMapping("/banebooking/fetch")
    @ResponseBody
    public String getBookingsForCalendar(Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();

        List<Booking> bookings = new ArrayList<>();

        for (Hall hall : union.getHalls()){
            for (Court court : hall.getCourts()){
                bookings.addAll(court.getBookings());
            }
        }

        return CourtBookingFetcher.makeJsonObject(bookings, union.getHalls());
    }


    @GetMapping("/banebooking/oversigt")
    public String showCourtBookingOverview(@RequestParam("hall") int id, Model model, Principal principal){
        Hall hall = hallService.findByHallId(id);

        model.addAttribute("hall", hall);
        model.addAttribute("courts", getCourtsMapByHall(hall));
        model.addAttribute("date", getCurrentTime().split(" ")[0]);
        model.addAttribute("hours", getCurrentTime().split(" ")[1]);

        return "panel/courtbooking/bookingoverview";
    }

    private List<Object[]> getCourtsMapByHall(Hall hall){
        List<Object[]> returnList = new ArrayList<>();

        for (Court court : hall.getCourts()){
            List<Object[]> objectList  = getCourtBookingMapByDateAndTime(addZero(new DateTime().getHourOfDay()) + ":00", addZero((new DateTime().getHourOfDay() + 1)) + ":00", new DateTime().toString("dd-MM-yyyy"), court);
            returnList.addAll(objectList);
        }

        return returnList;
    }

    private String addZero(int hourOfDay) {
        if (hourOfDay < 10){
            return "0" + hourOfDay;
        } else {
            return String.valueOf(hourOfDay);
        }
    }

    private List<Object[]> getCourtBookingMapByDateAndTime(String start, String end, String date, Court court) {
        List<Object[]> objectList = new ArrayList<>();

        for (Booking booking : court.getBookings()){
            if (booking.getStartHour().equals(start) && booking.getEndHour().equals(end) && booking.getDate().equals(date)){
                objectList.add(new Object[]{court, booking});
            }
        }

        return objectList;
    }

    private String getCurrentTime() {
        DateTime now = new DateTime();

        return now.toString("dd-MM-yyyy") + " " +  now.getHourOfDay() + ":00" + "-" + (now.getHourOfDay() + 1) + ":00";
    }


}
