package dk.clubtimize.clubtimize.controllers.panel.economics.bookkeepingSupport;

import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UnionDineroAPI {

    private String apiKey;
    private int firmId;

    public UnionDineroAPI(String apiKey, int firmId) {
        this.apiKey = apiKey;
        this.firmId = firmId;
    }

    // Get the access token for all calls
    private String getAccessToken(){
        HttpClient httpClient = HttpClientBuilder.create().build();

        String encoding = Base64.encodeBase64String("Clubtimize:nFrixeCZwyhs8UAfnzy71KWGkQZUxj8cRcbYMWOk".getBytes());

        try {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("username", apiKey));
            params.add(new BasicNameValuePair("password", apiKey));
            params.add(new BasicNameValuePair("grant_type", "password"));
            params.add(new BasicNameValuePair("scope", "read write"));

            HttpPost request = new HttpPost("https://authz.dinero.dk/dineroapi/oauth/token");
            request.addHeader("Authorization", "Basic " + encoding);
            request.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse response = httpClient.execute(request);

            JSONObject accessResultObject = new JSONObject(EntityUtils.toString(response.getEntity()));

            if (accessResultObject.has("access_token")){
                return accessResultObject.getString("access_token");
            } else {
                JSONObject errorObject = new JSONObject(accessResultObject.getString("message"));
                return errorObject.getString("error");
            }

        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    // Get a list of all accounts for the Clubtimize account
    public String getAccountList(){
        HttpClient httpClient = HttpClientBuilder.create().build();

        String accessToken = getAccessToken();

        try {
            HttpGet request = new HttpGet("https://api.dinero.dk/v1/" + firmId + "/accounts/entry");

            if (!accessToken.equals("invalid_grant")){
                request.addHeader("Authorization", "Bearer " + accessToken);
            } else {
                return "invalid_grant";
            }

            request.addHeader("content-type", "application/json");
            HttpResponse response = httpClient.execute(request);

            return EntityUtils.toString(response.getEntity());

        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "denied";
        }
    }

    public String addLedgerItem(List<Invoice> invoices, int accountNumber, int counter, boolean withFees){
        HttpClient httpClient = HttpClientBuilder.create().build();

        try {
            HttpPost request = new HttpPost("https://api.dinero.dk/v1.1/" + firmId + "/ledgeritems");
            request.addHeader("Authorization", "Bearer " + getAccessToken());
            request.addHeader("content-type", "application/json");
            StringEntity json = new StringEntity(getLedgerItemJSONFromArray(invoices, accountNumber, counter, withFees), "UTF-8");
            request.setEntity(json);
            HttpResponse response = httpClient.execute(request);

            return EntityUtils.toString(response.getEntity());

        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "Error";
        }
    }


    private String getLedgerItemJSONFromArray(List<Invoice> invoices, int accountNumber, int counter, boolean withFees) {
        JSONArray jsonArray = new JSONArray();

        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");

        int voucherCount = 1;
        for (Invoice invoice : invoices){
            if (withFees){
                if (invoice.getTotalCostWithAdditional() <= 0){
                    continue;
                }
            } else {
                if (invoice.getTotalCostWithoutAdditional() <= 0){
                    continue;
                }
            }

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("Id", JSONObject.NULL);
            jsonObject.put("VoucherNumber", voucherCount);
            jsonObject.put("AccountNumber", accountNumber);

            if (withFees){
                jsonObject.put("Amount", -invoice.getTotalCostWithAdditional());
            } else {
                jsonObject.put("Amount", -invoice.getTotalCostWithoutAdditional());
            }

            jsonObject.put("BalancingAccountNumber", counter);

            if (invoice.getUser() != null){
                jsonObject.put("Description", invoice.getItems().get(0).getDescription() + " - " + invoice.getUser().getFirstname() + " " + invoice.getUser().getLastname());
            } else {
                jsonObject.put("Description", invoice.getItems().get(0).getDescription() + " - SLETTET BRUGER: " + invoice.getItems().get(invoice.getItems().size() - 1).getDescription().split(":")[1]);
            }

            jsonObject.put("VoucherDate", formatter.parseDateTime(invoice.getDate()).toString("yyyy-MM-dd"));

            jsonArray.put(jsonObject);
            voucherCount++;
        }

        return  jsonArray.toString();
    }

    private String convertDanishCharacters(String data){
        data = data.replace("æ", "ae");
        data = data.replace("Æ", "AE");
        data = data.replace("ø", "oe");
        data = data.replace("Ø", "OE");
        data = data.replace("å", "aa");
        data = data.replace("Å", "AA");

        return data;
    }
}
