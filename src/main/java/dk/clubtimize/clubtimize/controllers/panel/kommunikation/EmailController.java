package dk.clubtimize.clubtimize.controllers.panel.kommunikation;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/panel")
public class EmailController {

    @GetMapping("/email")
    public String showEmail(){
        return "kommunikation/email";
    }
}
