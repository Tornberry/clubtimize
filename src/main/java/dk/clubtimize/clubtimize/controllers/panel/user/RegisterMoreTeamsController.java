package dk.clubtimize.clubtimize.controllers.panel.user;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.controllers.support.EconomicSupport;
import dk.clubtimize.clubtimize.controllers.support.PaymentGatewaySupport;
import dk.clubtimize.clubtimize.controllers.support.UnionSeasonStringService;
import dk.clubtimize.clubtimize.models.training.PlayerParticipation;
import dk.clubtimize.clubtimize.models.training.Training;
import dk.clubtimize.clubtimize.models.union.Player;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.UnionSeason;
import dk.clubtimize.clubtimize.models.union.discount.discountTypes.TwoTeamDiscount;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;
import dk.clubtimize.clubtimize.services.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/panel")
public class RegisterMoreTeamsController {

    private UserService userService;
    private TeamService teamService;
    private UnionSeasonService unionSeasonService;
    private PlayerService playerService;
    private TrainingService trainingService;
    private UserSeasonService userSeasonService;
    private UnionService unionService;
    private TeamSeasonService teamSeasonService;
    private UnionSeasonStringService unionSeasonStringService;

    @Autowired
    public RegisterMoreTeamsController(UserService userService, TeamService teamService, UnionSeasonService unionSeasonService, PlayerService playerService, TrainingService trainingService, UserSeasonService userSeasonService, UnionService unionService, TeamSeasonService teamSeasonService, UnionSeasonStringService unionSeasonStringService) {
        this.userService = userService;
        this.teamService = teamService;
        this.unionSeasonService = unionSeasonService;
        this.playerService = playerService;
        this.trainingService = trainingService;
        this.userSeasonService = userSeasonService;
        this.unionService = unionService;
        this.teamSeasonService = teamSeasonService;
        this.unionSeasonStringService = unionSeasonStringService;
    }

    @GetMapping("/tilmeldhold")
    public String showPage(Model model, Principal principal){
        User user = userService.findByUsername(principal.getName());

        model.addAttribute("teamsToJoin", getTeamsToJoin(user.getUnionAssosiation().getPlayerPositions(), user, user.getUnion()));
        return "panel/user/registermoreteams/registermoreteams";
    }

    private HashMap<Team, float[]> getTeamsToJoin(List<Player> playerPositions, User user, Union union) {
        List<Team> alreadyJoinedTeams = new ArrayList<>();
        Iterable<Team> avalibleTeams = teamService.findByUnion(union);
        List<Team> returnList = new ArrayList<>();
        UserSeason userSeason = userSeasonService.findByUnionSeasonAndUser(unionSeasonService.findByUnionAndSeasonYear(user.getUnion(), unionSeasonStringService.getSeasonStringByUnionSeason(union)), user);

        for (Player player : playerPositions){
            alreadyJoinedTeams.add(player.getTeam());
        }

        for (Team team : avalibleTeams){
            if (!alreadyJoinedTeams.contains(team)){
                returnList.add(team);
            }
        }

        return getTeamsWithDiscountMap(returnList, alreadyJoinedTeams, userSeason);
    }

    private HashMap<Team,float[]> getTeamsWithDiscountMap(List<Team> returnList, List<Team> alreadyJoinedTeams, UserSeason userSeason) {
        HashMap<Team, float[]> teamWithDiscountPrice = new HashMap<>();
        float feePercent = 3;

        for (Team team : returnList){
            feePercent = team.getUnion().getUnionEconomics().getFeePercent();
            TwoTeamDiscount largestAmount = null;

            boolean firstTime = true;
            for (TwoTeamDiscount twoTeamDiscount : team.getUnion().getDiscount().getTwoTeamDiscounts()){
                // If you are eligible for this discount
                if (twoTeamDiscount.getTeams().contains(team) && getTeamCount(alreadyJoinedTeams, twoTeamDiscount, userSeason) == 1){
                    //If not first time
                    if (!firstTime){
                        // If this discount amount is greater
                        if (largestAmount.getAmount() < twoTeamDiscount.getAmount()){
                            largestAmount = twoTeamDiscount;
                        }
                    } else {
                        largestAmount = twoTeamDiscount;
                        firstTime = false;
                    }
                }
            }

            if (largestAmount != null){
                if ((team.getPrice() - largestAmount.getAmount()) <= 0){
                    float price = 0;
                    float feesAndTax = EconomicSupport.calculateFees(0, feePercent)[0] + EconomicSupport.calculateFees(0, feePercent)[1];
                    teamWithDiscountPrice.put(team, new float[]{price, price + feesAndTax});
                } else {
                    float price = team.getPrice() - largestAmount.getAmount();
                    float feesAndTax = EconomicSupport.calculateFees(price, feePercent)[0] + EconomicSupport.calculateFees(price, feePercent)[1];
                    teamWithDiscountPrice.put(team, new float[]{price, price + feesAndTax});
                }
            } else {
                float price = team.getPrice();
                float feesAndTax = EconomicSupport.calculateFees(price, feePercent)[0] + EconomicSupport.calculateFees(price, feePercent)[1];
                teamWithDiscountPrice.put(team, new float[]{price, price + feesAndTax});
            }
        }

        return teamWithDiscountPrice;
    }

    private int getTeamCount(List<Team> teams, TwoTeamDiscount twoTeamDiscount, UserSeason userSeason){
        int teamCount = 0;

        for (Team team : teams){
            TeamSeason teamSeason = teamSeasonService.findByUserSeasonAndTeam(userSeason, team);

            if (teamSeason.isPaymentSuccessful()){
                if (twoTeamDiscount.getTeams().contains(team)){
                    teamCount++;
                }
            }
        }

        return teamCount;
    }

    /**
     * Triggers when registering for a new team from the panel. Returns the link of ScanPay with the discounted price. Contains the payment succesfull link with the discounted price without taxes and fees
     * @param id
     * @param principal
     * @return
     */
    @PostMapping("/tilmeldhold")
    @ResponseBody
    public String joinTeam(@RequestBody int id, Principal principal){
        Team team = teamService.findById(id);
        User user = userService.findByUsername(principal.getName());
        Union union = user.getUnion();

        Player player = new Player();

        player.setUnion(union);
        union.addPlayer(player);

        player.setUnionAssosiation(user.getUnionAssosiation());

        player.setTeam(team);
        team.addPlayer(player);

        UnionSeason unionSeason = unionSeasonService.findByUnionAndSeasonYear(union, unionSeasonStringService.getSeasonStringByUnionSeason(union));
        UserSeason userSeason = userSeasonService.findByUnionSeasonAndUser(unionSeason, user);

        TeamSeason teamSeason = new TeamSeason();
        teamSeason.setPaymentCode(makeAlphaNumericString(20));
        teamSeason.setTeam(team);
        teamSeason.setPaymentSuccessful(false);
        teamSeason.setDateJoined(DateFactory.getCurrentDate());
        teamSeason.setUserSeason(userSeason);
        userSeason.addTeamSeason(teamSeason);

        userSeasonService.saveOrUpdate(userSeason);

        playerService.saveOrUpdate(player);

        for (Training training : team.getTrainings()){
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
            DateTime today = formatter.parseDateTime(new DateTime().toString("dd-MM-yyyy"));
            DateTime trainingDate = formatter.parseDateTime(training.getDate());

            if (trainingDate.isAfter(today)){
                PlayerParticipation playerParticipation = new PlayerParticipation();
                playerParticipation.setPlayer(player);
                playerParticipation.setTraining(training);
                training.addPlayerParticipation(playerParticipation);

                trainingService.saveOrUpdate(training);
            }
        }

        user.getUnionAssosiation().addPlayerPosition(player);
        teamService.saveOrUpdate(team);
        userService.saveOrUpdate(user);
        unionService.saveOrUpdate(union);

        // Payment stuff ---------------------------------------------------------------------------------------------------------------------

        PaymentGatewaySupport paymentGatewaySupport = new PaymentGatewaySupport(team, teamSeason.getPaymentCode());

        List<Team> alreadyJoinedTeams = new ArrayList<>();
        for (Player player1 : user.getUnionAssosiation().getPlayerPositions()){
            if (teamSeasonService.findByUserSeasonAndTeam(userSeason, player1.getTeam()).isPaymentSuccessful()){
                alreadyJoinedTeams.add(player1.getTeam());
            }
        }

        float priceWithoutAdditional = paymentGatewaySupport.calculatePrices(team, alreadyJoinedTeams)[0];
        float priceWithAdditional = paymentGatewaySupport.calculatePrices(team, alreadyJoinedTeams)[1];

        return paymentGatewaySupport.getPaymentGateway(priceWithAdditional, priceWithoutAdditional);
    }

    private String makeAlphaNumericString(int capacity){
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        StringBuilder stringBuilder = new StringBuilder(capacity);
        SecureRandom secureRandom = new SecureRandom();

        for(int i=0; i<capacity; i++){
            stringBuilder.append(AB.charAt(secureRandom.nextInt(AB.length())));
        }

        if (teamSeasonService.existsByPaymentCode(stringBuilder.toString())){
            stringBuilder.replace(0, stringBuilder.length(), makeAlphaNumericString(20));
        }

        return stringBuilder.toString();
    }

}
