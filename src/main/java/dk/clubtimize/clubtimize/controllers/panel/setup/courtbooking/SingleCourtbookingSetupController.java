package dk.clubtimize.clubtimize.controllers.panel.setup.courtbooking;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.models.opsætning.booking.Booking;
import dk.clubtimize.clubtimize.models.opsætning.booking.SeasonBookingPeriod;
import dk.clubtimize.clubtimize.models.opsætning.booking.SingleBookingPeriod;
import dk.clubtimize.clubtimize.models.opsætning.Court;
import dk.clubtimize.clubtimize.models.opsætning.Hall;
import dk.clubtimize.clubtimize.models.opsætning.booking.TimePeriod;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.services.*;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Book;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/panel/opsætning")
public class SingleCourtbookingSetupController {

    private UnionService unionService;
    private UserService userService;
    private HallService hallService;
    private CourtService courtService;
    private SingleBookingPeriodService singleBookingPeriodService;
    private BookingService bookingService;
    private SeasonBookingPeriodService seasonBookingPeriodService;
    private TimePeriodService timePeriodService;

    @Autowired
    public SingleCourtbookingSetupController(UnionService unionService, UserService userService, HallService hallService, CourtService courtService, SingleBookingPeriodService singleBookingPeriodService, BookingService bookingService, SeasonBookingPeriodService seasonBookingPeriodService, TimePeriodService timePeriodService) {
        this.unionService = unionService;
        this.userService = userService;
        this.hallService = hallService;
        this.courtService = courtService;
        this.singleBookingPeriodService = singleBookingPeriodService;
        this.bookingService = bookingService;
        this.seasonBookingPeriodService = seasonBookingPeriodService;
        this.timePeriodService = timePeriodService;
    }

    @PostMapping("/banebooking/single")
    @ResponseBody
    public String makeSingleBookingPeriod(@RequestBody String json, Principal principal){
        JSONObject jsonObject = new JSONObject(json);
        Hall hall = hallService.findByHallId(jsonObject.getInt("hallId"));

        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");

        SingleBookingPeriod singleBookingPeriod = new SingleBookingPeriod();

        singleBookingPeriod.setPeriodStartDate(formatter.parseDateTime(jsonObject.getString("periodStartDate")).toString("dd-MM-yyyy"));
        singleBookingPeriod.setPeriodEndDate(formatter.parseDateTime(jsonObject.getString("periodEndDate")).toString("dd-MM-yyyy"));

        singleBookingPeriod.setBookingInterval(jsonObject.getString("interval"));
        singleBookingPeriod.setDayOfWeek(jsonObject.getString("dayOfWeek"));
        singleBookingPeriod.setStartTime(jsonObject.getString("startTime") + ":00");
        singleBookingPeriod.setEndTime(jsonObject.getString("endTime") + ":00");
        singleBookingPeriod.setPrice(jsonObject.getFloat("price"));
        singleBookingPeriod.setHall(hall);

        singleBookingPeriodService.saveOrUpdate(singleBookingPeriod);
        hallService.saveOrUpdate(hall);

        hall.addBookingPeriod(singleBookingPeriod);

        makeBookings(singleBookingPeriod, hall);

        return "OK";
    }

    @DeleteMapping("/banebooking/single")
    @ResponseBody
    public String deleteBookingPeriod(@RequestBody int id, Principal principal){
        SingleBookingPeriod singleBookingPeriod = singleBookingPeriodService.findById(id);

        for (Booking booking : singleBookingPeriod.getBookings()){
            if (booking.getTimePeriod() != null) {
                booking.setSingleBookingPeriod(null);
            } else {
                bookingService.deleteBooking(booking);
            }
        }

        singleBookingPeriodService.deleteBookingPeriod(singleBookingPeriod);

        return "OK";
    }

    private void makeBookings(SingleBookingPeriod singleBookingPeriod, Hall hall) {
        for (Court court : hall.getCourts()){
            List<Booking> bookings = assembleBookingList(court, singleBookingPeriod);

            bookings.forEach(court::addBookings);
            bookings.forEach(singleBookingPeriod::addBookings);

            courtService.saveOrUpdate(court);
            singleBookingPeriodService.saveOrUpdate(singleBookingPeriod);
        }

    }

    private List<Booking> assembleBookingList(Court court, SingleBookingPeriod singleBookingPeriod) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");

        DateTime today = new DateTime();

        DateTime startDate = formatter.parseDateTime(singleBookingPeriod.getPeriodStartDate());
        DateTime endDate = formatter.parseDateTime(singleBookingPeriod.getPeriodEndDate());

        List<DateTime> weekdayDates = new ArrayList<>();

        int day = 0;

        switch (singleBookingPeriod.getDayOfWeek()) {
            case "mandag":
                day = 1;
                break;
            case "tirsdag":
                day = 2;
                break;
            case "onsdag":
                day = 3;
                break;
            case "torsdag":
                day = 4;
                break;
            case "fredag":
                day = 5;
                break;
            case "lørdag":
                day = 6;
                break;
            case "søndag":
                day = 7;
                break;
        }

        List<Booking> bookings = new ArrayList<>();

        while(startDate.isBefore(endDate)){
            if (startDate.isAfter(today.minusDays(1))){
                if (startDate.getDayOfWeek() == day){
                    weekdayDates.add(startDate);
                }
            }

            startDate = startDate.plusDays(1);
        }

        for (DateTime dateTime : weekdayDates){
            bookings.addAll(makeBookingsForDate(dateTime, singleBookingPeriod, court));
        }

        return bookings;
    }

    private List<Booking> makeBookingsForDate(DateTime dateForBookings, SingleBookingPeriod singleBookingPeriod, Court court) {
        List<Booking> bookings = new ArrayList<>();

        DateTimeFormatter hourFormatter = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm");
        DateTime startTime = hourFormatter.parseDateTime(dateForBookings.toString("dd-MM-yyyy") + " " + singleBookingPeriod.getStartTime());
        DateTime endTime = hourFormatter.parseDateTime(dateForBookings.toString("dd-MM-yyyy") + " " + singleBookingPeriod.getEndTime());

        Period period = new Period(startTime, endTime);

        for (int i = 0; i < period.getHours(); i++){

            if (!bookingService.existsByDateAndStartHourAndEndHourAndCourt(startTime.toString("dd-MM-yyyy"), (Integer.parseInt(singleBookingPeriod.getStartTime().split(":")[0]) + i) + ":00", (Integer.parseInt(singleBookingPeriod.getStartTime().split(":")[0]) + (i+1)) + ":00", court)){
                Booking booking = new Booking();

                booking.setCourt(court);
                booking.setSingleBookingPeriod(singleBookingPeriod);

                booking.setDate(dateForBookings.toString("dd-MM-yyyy"));
                booking.setBooked(false);
                booking.setStartHour(startTime.plusHours(i).toString("HH:mm"));
                booking.setEndHour(startTime.plusHours(i+1).toString("HH:mm"));
                booking.setPrice(singleBookingPeriod.getPrice());

                bookings.add(booking);
            } else {
                Booking booking = bookingService.findByDateAndStartHourAndEndHourAndCourt(startTime.toString("dd-MM-yyyy"), (Integer.parseInt(singleBookingPeriod.getStartTime().split(":")[0]) + i) + ":00", (Integer.parseInt(singleBookingPeriod.getStartTime().split(":")[0]) + (i+1)) + ":00", court);
                booking.setSingleBookingPeriod(singleBookingPeriod);
                if (booking.getTimePeriod().getSeasonBookingPeriod().isReserveSingleBookings()){
                    booking.setPrice(singleBookingPeriod.getPrice());
                }

                bookingService.saveOrUpdate(booking);
            }


        }

        return bookings;
    }
}
