package dk.clubtimize.clubtimize.controllers.panel.coachcenter;

import dk.clubtimize.clubtimize.controllers.support.RoleSupport;
import dk.clubtimize.clubtimize.models.training.PlayerParticipation;
import dk.clubtimize.clubtimize.models.training.Training;
import dk.clubtimize.clubtimize.models.union.Player;
import dk.clubtimize.clubtimize.models.union.RecruitmentInvitation;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.services.TeamService;
import dk.clubtimize.clubtimize.services.UserService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/panel")
public class CoachCenterController {

    private UserService userService;
    private TeamService teamService;

    @Autowired
    public CoachCenterController(UserService userService, TeamService teamService) {
        this.userService = userService;
        this.teamService = teamService;
    }

    @GetMapping("/trænercenter")
    public Object showPage(@RequestParam(value = "team", defaultValue = "0") int team, Model model, Principal principal) throws UnsupportedEncodingException {
        if (RoleSupport.checkIfIsCoach(userService.findByUsername(principal.getName()))){
            if (team == 0){
                User user = userService.findByUsername(principal.getName());

                List<Team> teamList = user.getUnionAssosiation().getCoachPositions().get(0).getTeams();

                model.addAttribute("chosenTeam", teamList.get(0));
                model.addAttribute("invitations", teamList.get(0).getRecruitmentInvitations());
                return new RedirectView("/panel/tr" + URLEncoder.encode("æ", "UTF-8") + "nercenter?team=" + teamList.get(0).getTeamId(), true, true, false);
            } else {
                model.addAttribute("chosenTeam", teamService.findById(team));
                model.addAttribute("invitations", teamService.findById(team).getRecruitmentInvitations());
            }
            return "panel/trænertools/trænercenter";
        } else {
            return "error/404.html";
        }


    }

    @GetMapping("/træningscenter/latest52")
    @ResponseBody
    public String getMissingLastTenTrainings(@RequestParam("team") int teamId, Principal principal){
        Team team = teamService.findById(teamId);

        // Get the latest 52 trainings
        List<Training> latestYear = getLatestYearTrainings(team.getTrainings());

        JSONObject mainObject = new JSONObject();

        // labelarray
        JSONArray labelsArray = new JSONArray();

        // dataset
        JSONArray dataSets = new JSONArray();
        JSONObject dataSet = new JSONObject();
        JSONArray data = new JSONArray();

        // Add training data to object
        for (Training training : latestYear){
            float percent;
            int players = 0;
            int wasNotThere = 0;
            if (training.getPlayerParticipations().size() != 0){
                for (PlayerParticipation playerParticipation : training.getPlayerParticipations()){
                    if(!playerParticipation.isParticipated()){
                        wasNotThere++;
                    }
                    players++;
                }
            } else {
                continue;
            }
            if (wasNotThere == players){
                percent = 100;
            } else if(wasNotThere == 0){
                percent = 0;
            } else {
                percent = ((float)wasNotThere/(float)team.getPlayers().size())*100;
            }

            data.put(percent);
            labelsArray.put(training.getDate());

            dataSet.put("label", "Fraværd i %");
            dataSet.put("fill", false);
            dataSet.put("borderColor", "#44A2D2");
            dataSet.put("data", data);
        }

        dataSets.put(dataSet);

        mainObject.put("labels", labelsArray);
        mainObject.put("datasets", dataSets);

        return mainObject.toString();
    }

    private List<Training> getLatestYearTrainings(List<Training> trainings) {
        DateTime dateTime = DateTime.now();
        List<Training> trainingsBeforeToday = new ArrayList<>();

        for (Training training : trainings){
            DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern("dd-MM-yyyy");
            DateTime trainingDate = dateTimeFormat.parseDateTime(training.getDate());
            if (trainingDate.isBefore(dateTime)){
                trainingsBeforeToday.add(training);
            }
        }

        List<Training> latestYear;
        if (trainingsBeforeToday.size() < 52){
            latestYear = trainingsBeforeToday.subList(0, trainingsBeforeToday.size());
        } else {
            latestYear = trainingsBeforeToday.subList(0, 52);
        }

        return latestYear;
    }

    @GetMapping("/træningscenter/gender")
    @ResponseBody
    public String getGenderData(@RequestParam("team") int teamId, Principal principal){
        Team team = teamService.findById(teamId);

        // Get the latest 52 trainings
        List<Training> latestYear = getLatestYearTrainings(team.getTrainings());

        JSONArray menData = new JSONArray();
        JSONArray womenData = new JSONArray();
        JSONArray dates = new JSONArray();

        for (Training training : latestYear){
            dates.put(training.getDate());

            int menCount = 0;
            int womenCount = 0;

            for (PlayerParticipation playerParticipation : training.getPlayerParticipations()){
                if (playerParticipation.isParticipated()){
                    if (playerParticipation.getPlayer().getUnionAssosiation().getUser().getGender().equals("Mand")){
                        menCount++;
                    } else if (playerParticipation.getPlayer().getUnionAssosiation().getUser().getGender().equals("Kvinde")){
                        womenCount++;
                    }
                }
            }

            menData.put(menCount);
            womenData.put(womenCount);
        }

        JSONObject data = new JSONObject();
        data.put("men", menData);
        data.put("women", womenData);
        data.put("dates", dates);

        return data.toString();
    }

    @GetMapping("/træningscenter/points")
    @ResponseBody
    public String getPointsData(@RequestParam("team") int teamId, Principal principal){
        Team team = teamService.findById(teamId);

        return getPointsPrCategory(team).toString();
    }

    private JSONObject getPointsPrCategory(Team team) {
        if (team.getPlayers().size() != 0){
            JSONObject data = new JSONObject();

            Player highestPointsMensingle = null;
            Player highestPointsWomensingle = null;
            Player highestPointsMendouble = null;
            Player highestPointsWomendouble = null;
            Player highestPointsMixeddouble = null;

            Player lowestPointsMensingle = null;
            Player lowestPointsWomensingle = null;
            Player lowestPointsMendouble = null;
            Player lowestPointsWomendouble = null;
            Player lowestPointsMixeddouble = null;

            int menCount = 0;
            int womenCount = 0;

            int average_PointsMensingle_count = 0;
            int average_PointsWomensingle_count = 0;
            int average_PointsMendouble_count = 0;
            int average_PointsWomendouble_count = 0;
            int average_PointsMixeddouble_count = 0;

            boolean isFirst = true;
            for (Player player : team.getPlayers()){
                if (isFirst){
                    highestPointsMensingle = player;
                    highestPointsMendouble = player;
                    highestPointsWomensingle = player;
                    highestPointsWomendouble = player;
                    highestPointsMixeddouble = player;

                    lowestPointsMensingle = player;
                    lowestPointsWomensingle = player;
                    lowestPointsMendouble = player;
                    lowestPointsWomendouble = player;
                    lowestPointsMixeddouble = player;

                    isFirst = false;
                } else {
                    // Highest
                    if (highestPointsMensingle.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMensSingleElo() < player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMensSingleElo()){
                        highestPointsMensingle = player;
                    }
                    if (highestPointsMendouble.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMensDoubleElo() < player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMensDoubleElo()){
                        highestPointsMendouble = player;
                    }
                    if (highestPointsWomensingle.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getWomensSingleElo() < player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getWomensSingleElo()){
                        highestPointsWomensingle = player;
                    }
                    if (highestPointsWomendouble.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getWomensDoubleElo() < player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getWomensDoubleElo()){
                        highestPointsWomendouble = player;
                    }
                    if (highestPointsMixeddouble.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMixedDoubleElo() < player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMixedDoubleElo()){
                        highestPointsMixeddouble = player;
                    }

                    // Lowest
                    if (lowestPointsMensingle.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMensSingleElo() > player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMensSingleElo()){
                        lowestPointsMensingle = player;
                    }
                    if (lowestPointsMendouble.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMensDoubleElo() > player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMensDoubleElo()){
                        lowestPointsMendouble = player;
                    }
                    if (lowestPointsWomensingle.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getWomensSingleElo() > player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getWomensSingleElo()){
                        lowestPointsWomensingle = player;
                    }
                    if (lowestPointsWomendouble.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getWomensDoubleElo() > player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getWomensDoubleElo()){
                        lowestPointsWomendouble = player;
                    }
                    if (lowestPointsMixeddouble.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMixedDoubleElo() > player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMixedDoubleElo()){
                        lowestPointsMixeddouble = player;
                    }
                }

                if (player.getUnionAssosiation().getUser().getGender().equals("Mand")){
                    menCount++;

                    average_PointsMensingle_count += player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMensSingleElo();
                    average_PointsMendouble_count += player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMensDoubleElo();
                    average_PointsMixeddouble_count += player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMixedDoubleElo();
                } else if (player.getUnionAssosiation().getUser().getGender().equals("Kvinde")){
                    womenCount++;

                    average_PointsWomensingle_count += player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getWomensSingleElo();
                    average_PointsWomendouble_count += player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getWomensDoubleElo();
                    average_PointsMixeddouble_count += player.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMixedDoubleElo();
                }
            }
            
            
            JSONObject mensingleData = calculateData("mensingle", highestPointsMensingle, lowestPointsMensingle, menCount, womenCount, average_PointsMensingle_count);
            data.put("mensigle", mensingleData);

            JSONObject womensingleData = calculateData("womensingle", highestPointsWomensingle, lowestPointsWomensingle, menCount, womenCount, average_PointsWomensingle_count);
            data.put("womensingle", womensingleData);

            JSONObject mendoubleData = calculateData("mendouble", highestPointsMendouble, lowestPointsMendouble, menCount, womenCount, average_PointsMendouble_count);
            data.put("mendouble", mendoubleData);

            JSONObject womendoubleData = calculateData("womendouble", highestPointsWomendouble, lowestPointsWomendouble, menCount, womenCount, average_PointsWomendouble_count);
            data.put("womendouble", womendoubleData);

            JSONObject mixdoubleData = calculateData("mixdouble", highestPointsMixeddouble, lowestPointsMixeddouble, menCount, womenCount, average_PointsMixeddouble_count);
            data.put("mixdouble", mixdoubleData);

            return data;
        } else {
            return new JSONObject().put("Error", "Empty team");
        }
    }

    private JSONObject calculateData(String type, Player highestPoints, Player lowestPoints, int mencount, int womencount, float average) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Highest_name", highestPoints.getUnionAssosiation().getUser().getFirstname() + " " + highestPoints.getUnionAssosiation().getUser().getLastname());
        jsonObject.put("Highest_points", highestPoints.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMensSingleElo());
        jsonObject.put("Lowest_name", lowestPoints.getUnionAssosiation().getUser().getFirstname() + " " + lowestPoints.getUnionAssosiation().getUser().getLastname());
        jsonObject.put("Lowest_points", lowestPoints.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMensSingleElo());
        jsonObject.put("Difference", highestPoints.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMensSingleElo() - lowestPoints.getUnionAssosiation().getUser().getMatchStats().getEloPoints().getMensSingleElo());

        switch (type){
            case "mensingle":
                if (mencount != 0){
                    jsonObject.put("Average", average / (float)mencount);
                } else {
                    jsonObject.put("Average", 0);
                }
            case "womensingle":
                if (womencount != 0){
                    jsonObject.put("Average", average / (float)womencount);
                } else {
                    jsonObject.put("Average", 0);
                }
            case "mendouble":
                if (mencount != 0){
                    jsonObject.put("Average", average / (float)mencount);
                } else {
                    jsonObject.put("Average", 0);
                }
            case "womendouble":
                if (womencount != 0){
                    jsonObject.put("Average", average / (float)womencount);
                } else {
                    jsonObject.put("Average", 0);
                }
            case "mixdouble":
                if ((mencount + womencount) != 0){
                    jsonObject.put("Average", average / (float)mencount + (float)womencount);
                } else {
                    jsonObject.put("Average", 0);
                }
        }

        return jsonObject;
    }
}
