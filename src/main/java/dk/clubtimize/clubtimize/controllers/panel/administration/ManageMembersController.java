package dk.clubtimize.clubtimize.controllers.panel.administration;

import dk.clubtimize.clubtimize.controllers.support.*;
import dk.clubtimize.clubtimize.controllers.support.mail.MailSender;
import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.economics.invoice.InvoiceItem;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import dk.clubtimize.clubtimize.services.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/panel")
public class ManageMembersController {

    private UserService userService;
    private InvoiceService invoiceService;
    private TeamSeasonService teamSeasonService;
    private EconomicYearService economicYearService;
    private UnionEconomicsService unionEconomicsService;

    @Autowired
    public ManageMembersController(UserService userService, InvoiceService invoiceService, TeamSeasonService teamSeasonService, EconomicYearService economicYearService, UnionEconomicsService unionEconomicsService) {
        this.userService = userService;
        this.invoiceService = invoiceService;
        this.teamSeasonService = teamSeasonService;
        this.economicYearService = economicYearService;
        this.unionEconomicsService = unionEconomicsService;
    }

    @GetMapping("/administrermedlemmer")
    public String showAdministrerMedlemmer(Model model, Principal principal){
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            model.addAttribute("assosiates", getAllUnionUsers(userService.findByUsername(principal.getName()).getUnion()));
            return "panel/management/manageMembers/manageMembers";
        } else {
            return "error/404.html";
        }
    }

    private List<User> getAllUnionUsers(Union union) {
        List<User> users = new ArrayList<>();
        union.getUnionAssosiations().forEach(unionAssosiation -> users.add(unionAssosiation.getUser()));
        return users;
    }

    @GetMapping("/administrermedlemmer/individ")
    public String getSinleUser(@RequestParam("user") int id, Model model, Principal principal){
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            User user = userService.findById(id);
            if (OwnershipValidator.validateOwnership(user, user)){
                model.addAttribute("user", user);
                model.addAttribute("birthday", getBirthdayFormatted(user));
                return "panel/management/manageMembers/singleUser/singleUser";
            } else {
                return "error/404.html";
            }
        } else {
            return "error/404.html";
        }
    }

    @PostMapping("/administrermedlemmer/individ")
    @ResponseBody
    public String editUser(@RequestBody String json){
        JSONObject jsonObject = new JSONObject(json);
        User user = userService.findById(jsonObject.getInt("userId"));

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");

        user.setFirstname(jsonObject.getString("firstname"));
        user.setLastname(jsonObject.getString("lastname"));
        user.setEmail(jsonObject.getString("email"));
        user.setPhone(Integer.parseInt(jsonObject.getString("phone")));
        user.setAddress(jsonObject.getString("address"));
        user.setBirthday(formatter.parseDateTime(jsonObject.getString("birthday")).toString("dd-MM-yyyy"));
        user.setGender(jsonObject.getString("gender"));

        userService.saveOrUpdate(user);

        return "OK";
    }

    private String getBirthdayFormatted(User user) {
        String day = user.getBirthday().split("-")[0];
        String month = user.getBirthday().split("-")[1];
        String year = user.getBirthday().split("-")[2];

        return year + "-" + month + "-" + day;
    }

    @DeleteMapping("/administrermedlemmer/individ")
    @ResponseBody
    public String deleteUser(@RequestBody int id, Principal principal){
        if (userService.findByUsername(principal.getName()).isUnionAdministrator()){
            User user = userService.findById(id);

            handleInvoices(user);

            userService.deleteUser(user);

            MailSender.deleteUser(user);
        }

        return "OK";
    }

    private void handleInvoices(User user) {
        for (Invoice invoice : user.getInvoices()){
            invoice.setUser(null);

            InvoiceItem invoiceItem = new InvoiceItem();
            invoiceItem.setQuantity(0);
            invoiceItem.setItem("");
            invoiceItem.setDescription("Denne faktura omhandler tidligere medlem: " + user.getFirstname() + " " + user.getLastname() + ", " + user.getEmail());
            invoiceItem.setFee(0);
            invoiceItem.setTax(0);
            invoiceItem.setUnitCost(0);
            invoiceItem.setTotalCost(0);
            invoiceItem.setInvoice(invoice);

            invoice.addInvoiceItem(invoiceItem);

            invoiceService.saveOrUpdate(invoice);
        }
    }

    @PostMapping("/administrermedlemmer/individ/free")
    @ResponseBody
    public String awardFreeContingent(@RequestBody int id){
        TeamSeason teamSeason = teamSeasonService.findById(id);

        teamSeason.setPaymentSuccessful(true);

        EconomicYear economicYear = economicYearService.findByUnionEconomicsAndYear(teamSeason.getAssosiatedUnion().getUnionEconomics(), DateFactory.getCurrentSeasonYear());

        Invoice invoice = EconomicSupport.makeFreeContingentInvoice(teamSeason, economicYear, invoiceService.getNextInvoiceNumber(economicYear.getAssosiatedUnion().getName()) + "-" + DateFactory.getCurrentSeasonYear());

        teamSeason.setInvoice(invoice);

        economicYear.addInvoice(invoice);

        invoiceService.saveOrUpdate(invoice);
        economicYearService.saveOrUpdate(economicYear);
        teamSeasonService.saveOrUpdate(teamSeason);

        UnionEconomics unionEconomics = economicYear.getUnionEconomics();
        unionEconomics.updateBalance();

        unionEconomicsService.saveOrUpdate(unionEconomics);

        return "OK";
    }
}
