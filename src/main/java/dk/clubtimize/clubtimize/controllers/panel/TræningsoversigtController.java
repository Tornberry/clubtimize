package dk.clubtimize.clubtimize.controllers.panel;

import dk.clubtimize.clubtimize.controllers.support.PermissionEnum;
import dk.clubtimize.clubtimize.controllers.support.RoleSupport;
import dk.clubtimize.clubtimize.models.union.Coach;
import dk.clubtimize.clubtimize.models.union.Player;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.training.Training;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.services.UserService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/panel")
public class TræningsoversigtController {

    private UserService userService;

    @Autowired
    public TræningsoversigtController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/træningsoversigt")
    public String showHoldogbaner(Principal principal){
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            return "træningsoversigt";
        } else {
            return "error/404.html";
        }
    }

    @CrossOrigin
    @GetMapping("/api/getevents")
    @ResponseBody
    public String getEvents(Principal principal){

        User user = userService.findByUsername(principal.getName());

        if (RoleSupport.checkPermission(PermissionEnum.canSeeAllTeams, userService.findByUsername(principal.getName()))){
            JSONArray jsonString = new JSONArray();

            for (Team team : userService.findByUsername(principal.getName()).getUnion().getTeams()){
                for (JSONObject jsonObject : getTeamJson(team)){
                    jsonString.put(jsonObject);
                }
            }

            return jsonString.toString();
        } else {
            JSONArray jsonString = new JSONArray();

            for (Team team : userService.findByUsername(principal.getName()).getUnion().getTeams()){
                boolean isOnTeam = false;
                boolean isTeamCoach = false;
                for (Player player : user.getUnionAssosiation().getPlayerPositions()){
                    if (team.getPlayers().contains(player)){
                        isOnTeam = true;
                    }
                }
                for (Coach coach : user.getUnionAssosiation().getCoachPositions()){
                    if (team.getCoaches().contains(coach)){
                        isTeamCoach = true;
                    }
                }
                if (isOnTeam || isTeamCoach){
                    for (JSONObject jsonObject : getTeamJson(team)){
                        jsonString.put(jsonObject);
                    }
                }
            }

            return jsonString.toString();
        }
    }

    private List<JSONObject> getTeamJson(Team team){
        DateTimeFormatter pattern = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm").withZoneUTC();
        List<JSONObject> jsonObjects = new ArrayList<>();

        for (Training training : team.getTrainings()){
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("allDay", false);

            DateTime start = pattern.parseDateTime(training.getDate() + " " + training.getTimeFrom());

            jsonObject.put("start", start.getMillis());

            DateTime end = pattern.parseDateTime(training.getDate() + " " + training.getTimeTo());

            jsonObject.put("end", end.getMillis());

            jsonObject.put("color", team.getTeamDepartment().getColor());

            jsonObject.put("textColor", team.getTeamDepartment().getTextColor());

            jsonObject.put("title", team.getTitle() + " - træning");

            jsonObject.put("id", training.getId());

            jsonObjects.add(jsonObject);
        }
        return jsonObjects;
    }
}
