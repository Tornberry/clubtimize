package dk.clubtimize.clubtimize.controllers.panel.setup;

import dk.clubtimize.clubtimize.models.union.Player;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.event.Event;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.event.EventInvitation;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.services.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/panel/opsætning")
public class EventSetupController {

    private UserService userService;
    private UnionService unionService;
    private EventService eventService;
    private TeamService teamService;
    private EventInvitationService eventInvitationService;

    @Autowired
    public EventSetupController(UserService userService, UnionService unionService, EventService eventService, TeamService teamService, EventInvitationService eventInvitationService) {
        this.userService = userService;
        this.unionService = unionService;
        this.eventService = eventService;
        this.teamService = teamService;
        this.eventInvitationService = eventInvitationService;
    }

    @GetMapping("/events")
    public String shwoPage(Model model, Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();

        model.addAttribute("upcommingEvents", getUpcommingEvents(union));
        model.addAttribute("usersSorted", getMembersSortedAlphabetically(union));

        return "panel/setup/events/events";
    }

    private List<User> getMembersSortedAlphabetically(Union union) {
        Iterable<User> users =  userService.findByUnion(union);
        List<User> userList = new ArrayList<>();

        users.forEach(userList::add);

        userList.sort(Comparator.comparing(User::getFullNameUpperCase));

        return userList;
    }

    private List<Event> getUpcommingEvents(Union union) {
        DateTime now = new DateTime();

        List<Event> events = new ArrayList<>();

        for (Event event : union.getEvents()){
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm");
            DateTime dateTimeFrom = formatter.parseDateTime(event.getDateFrom() + " " + event.getTimeFrom());

            if (dateTimeFrom.isAfter(now)){
                events.add(event);
            }
        }

        return events;
    }

    @PostMapping("/events")
    @ResponseBody
    public String postEvent(@RequestBody String json, Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();
        JSONObject jsonObject = new JSONObject(json);

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");

        Event event = new Event();
        event.setDateFrom(formatter.parseDateTime(jsonObject.getString("dateFrom")).toString("dd-MM-yyyy"));
        event.setTimeFrom(jsonObject.getString("timeFrom"));
        event.setDateTo(formatter.parseDateTime(jsonObject.getString("dateTo")).toString("dd-MM-yyyy"));
        event.setTimeTo(jsonObject.getString("timeTo"));
        event.setDeadlineDate(formatter.parseDateTime(jsonObject.getString("deadlineDate")).toString("dd-MM-yyyy"));
        event.setDeadlineTime(jsonObject.getString("deadlineTime"));
        event.setTitle(jsonObject.getString("title"));
        event.setLocation(jsonObject.getString("location"));
        event.setPrice(jsonObject.getFloat("price"));
        event.setDescription(jsonObject.getString("description"));

        event.setUnion(union);

        union.addEvent(event);

        eventService.saveOrUpdate(event);

        unionService.saveOrUpdate(union);

        event = handleTeamInvitations(event, jsonObject.getJSONArray("teamsInvited"));
        event = handleUserInvitations(event, jsonObject.getJSONArray("usersInvited"));

        eventService.saveOrUpdate(event);

        return "OK";
    }

    private Event handleTeamInvitations(Event event, JSONArray teamsInvited) {

        for (Object id : teamsInvited){
            int teamId = Integer.parseInt((String)id);

            Team team = teamService.findById(teamId);

            team.addEvent(event);
            teamService.saveOrUpdate(team);

            event.addTeam(team);

            for (Player player : team.getPlayers()){
                User userToInvite = player.getUnionAssosiation().getUser();

                if (!eventInvitationService.existsByEventAndUser(event, userToInvite)){
                    EventInvitation eventInvitation = new EventInvitation();
                    eventInvitation.setEvent(event);
                    eventInvitation.setUser(userToInvite);

                    event.addUserInvitation(eventInvitation);
                    eventService.saveOrUpdate(event);

                    userToInvite.addEventInvitaion(eventInvitation);
                    userService.saveOrUpdate(userToInvite);
                }
            }
        }

        return event;
    }

    private Event handleUserInvitations(Event event, JSONArray usersInvited) {

        for (Object id : usersInvited){
            int userId = Integer.parseInt((String)id);

            User user = userService.findById(userId);

            if (!eventInvitationService.existsByEventAndUser(event, user)){
                EventInvitation eventInvitation = new EventInvitation();
                eventInvitation.setEvent(event);
                eventInvitation.setUser(user);

                event.addUserInvitation(eventInvitation);
                eventService.saveOrUpdate(event);

                user.addEventInvitaion(eventInvitation);
                userService.saveOrUpdate(user);
            }
        }

        return event;
    }
}
