package dk.clubtimize.clubtimize.controllers.panel.invitations;

import dk.clubtimize.clubtimize.controllers.support.PermissionEnum;
import dk.clubtimize.clubtimize.controllers.support.RoleSupport;
import dk.clubtimize.clubtimize.controllers.support.mail.MailSender;
import dk.clubtimize.clubtimize.models.union.RecruitmentInvitation;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.services.RecruitmentInvitationService;
import dk.clubtimize.clubtimize.services.TeamService;
import dk.clubtimize.clubtimize.services.UnionService;
import dk.clubtimize.clubtimize.services.UserService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Controller
@RequestMapping("/panel/invitation")
public class invitePlayerController {

    private UserService userService;
    private TeamService teamService;
    private UnionService unionService;
    private RecruitmentInvitationService recruitmentInvitationService;

    @Autowired
    public invitePlayerController(UserService userService, TeamService teamService, UnionService unionService, RecruitmentInvitationService recruitmentInvitationService) {
        this.userService = userService;
        this.teamService = teamService;
        this.unionService = unionService;
        this.recruitmentInvitationService = recruitmentInvitationService;
    }


    @GetMapping("/spiller")
    public String showInvitationsPage(Model model, Principal principal){
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            model.addAttribute("invitations", getPlayerInvitations(userService.findByUsername(principal.getName()).getUnion().getRecruitmentInvitations()));
            return "panel/invitation/invite/player/invitePlayer";
        } else {
            return "error/404.html";
        }
    }

    private List<RecruitmentInvitation> getPlayerInvitations(List<RecruitmentInvitation> recruitmentInvitations) {
        List<RecruitmentInvitation> coachInvitations = new ArrayList<>();

        for (RecruitmentInvitation recruitmentInvitation : recruitmentInvitations){
            if (recruitmentInvitation.getType().equals("player")){
                coachInvitations.add(recruitmentInvitation);
            }
        }

        return coachInvitations;
    }

    @PostMapping("/spiller")
    public ResponseEntity postNewPlayer(@RequestBody String json, Principal principal){

        Union union = userService.findByUsername(principal.getName()).getUnion();

        JSONObject jsonObject = new JSONObject(json);

        RecruitmentInvitation recruitmentInvitation = new RecruitmentInvitation(
                union,
                "player",
                getRandomInt(),
                jsonObject.getString("email"),
                teamService.findById(jsonObject.getInt("team")),
                "Afventer");

        union.addRecruitmentInvitation(recruitmentInvitation);

        MailSender.invitePlayer(recruitmentInvitation);

        unionService.saveOrUpdate(union);

        return new ResponseEntity(HttpStatus.OK);
    }

    private int getRandomInt() {
        return new Random().nextInt(10000000);
    }

    @DeleteMapping("/spiller")
    public ResponseEntity deleteNewPlayer(@RequestBody int invNumber, Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();

        RecruitmentInvitation recruitmentInvitation = recruitmentInvitationService.findByInvNumber(invNumber);

        union.removeRecruitmentInvitation(recruitmentInvitation);

        recruitmentInvitationService.deleteRecruitmentInvitation(recruitmentInvitation);

        return new ResponseEntity(HttpStatus.OK);
    }
}
