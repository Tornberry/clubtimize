package dk.clubtimize.clubtimize.controllers.panel.analytics;

import dk.clubtimize.clubtimize.controllers.support.PermissionEnum;
import dk.clubtimize.clubtimize.controllers.support.RoleSupport;
import dk.clubtimize.clubtimize.models.opsætning.TeamDepartment;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.services.UserService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@Controller
@RequestMapping("/panel")
public class AnalyticsController {

    private UserService userService;

    @Autowired
    public AnalyticsController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/analysecenter")
    public String showAnalyticsCenter(Principal principal) {
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            return "panel/analytics/analyticsCenter";
        } else {
            return "error/404.html";
        }
    }

    /**
     * @return Members pr. department
     */
    @GetMapping("/analysecenter/departmentsignups")
    @ResponseBody
    public String getDepartmentMembersData(Principal principal) {
        JSONObject mainObject = new JSONObject();

        // labelarray
        JSONArray labelsArray = new JSONArray();

        // dataset
        JSONArray dataSets = new JSONArray();
        JSONObject dataSet = new JSONObject();
        JSONArray backgroundColorArray = new JSONArray();
        JSONArray data = new JSONArray();

        for (TeamDepartment department : userService.findByUsername(principal.getName()).getUnion().getDepartments()) {
            int members = 0;
            for (Team team : department.getTeams()) {
                members += team.getPlayers().size();
            }

            data.put(members);
            labelsArray.put(department.getTitle());
            backgroundColorArray.put("#44A2D2");

            dataSet.put("label", "Tilmeldinger");
            dataSet.put("backgroundColor", backgroundColorArray);
            dataSet.put("data", data);
        }

        dataSets.put(dataSet);

        mainObject.put("labels", labelsArray);
        mainObject.put("datasets", dataSets);

        return mainObject.toString();
    }
}
