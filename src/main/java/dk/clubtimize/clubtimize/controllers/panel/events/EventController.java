package dk.clubtimize.clubtimize.controllers.panel.events;

import dk.clubtimize.clubtimize.controllers.support.EconomicSupport;
import dk.clubtimize.clubtimize.controllers.support.PaymentGatewaySupport;
import dk.clubtimize.clubtimize.models.union.event.Event;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.event.EventInvitation;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.services.EventInvitationService;
import dk.clubtimize.clubtimize.services.EventService;
import dk.clubtimize.clubtimize.services.UnionService;
import dk.clubtimize.clubtimize.services.UserService;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/panel")
public class EventController {

    private UserService userService;
    private UnionService unionService;
    private EventService eventService;
    private EventInvitationService eventInvitationService;

    @Autowired
    public EventController(UserService userService, UnionService unionService, EventService eventService, EventInvitationService eventInvitationService) {
        this.userService = userService;
        this.unionService = unionService;
        this.eventService = eventService;
        this.eventInvitationService = eventInvitationService;
    }

    @GetMapping("/events")
    public String showPage(Model model, Principal principal){
       return "panel/events/events";
    }

    @GetMapping("/event")
    public String getEvent(@RequestParam("event") int id, Principal principal, Model model){
        Event event = eventService.findById(id);

        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm");
        Seconds seconds = Seconds.secondsBetween(new DateTime(), formatter.parseDateTime(event.getDateFrom() + " " + event.getTimeFrom()));
        model.addAttribute("millis", seconds.getSeconds());

        model.addAttribute("notPastDeadline", isOverDeadline(event.getDeadlineDate(), event.getDeadlineTime()));
        model.addAttribute("event", event);
        model.addAttribute("paymentPrice", getPaymentPrice(event, event.getUnion()));
        model.addAttribute("isParticipating", getIsParticipating(userService.findByUsername(principal.getName()), event));


        return "panel/events/singleEvent/singleEvent";
    }

    private boolean isOverDeadline(String deadlineDate, String deadlineTime) {
        DateTime now = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm");
        DateTime deadline = formatter.parseDateTime(deadlineDate + " " + deadlineTime);

        return now.isBefore(deadline);
    }

    private boolean getIsParticipating(User user, Event event) {
        return event.getParticipants().contains(user);
    }

    private float getPaymentPrice(Event event, Union union) {
        if (union.getUnionEconomics().isPayingFees()){
            return event.getPrice();
        } else {
            return event.getPrice() + EconomicSupport.calculateFeesAddedUp(event.getPrice(), union.getUnionEconomics().getFeePercent());
        }
    }

    private List<Event> getOverEvents(Union union) {
        DateTime now = new DateTime();

        List<Event> events = new ArrayList<>();

        for (Event event : union.getEvents()){
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm");
            DateTime dateTimeFrom = formatter.parseDateTime(event.getDateFrom() + " " + event.getTimeFrom());

            if (dateTimeFrom.isBefore(now)){
                events.add(event);
            }
        }

        return events;
    }

    @PostMapping("/event/join")
    @ResponseBody
    public String joinEvent(@RequestBody int eventId, Principal principal){
        Event event = eventService.findById(eventId);
        User user = userService.findByUsername(principal.getName());

        EventInvitation eventInvitation = eventInvitationService.findByEventAndUser(event, user);

        if (event.getPrice() == 0){
            eventInvitation.setAnswered(true);
            eventInvitation.setComming(true);

            user.addEvent(event);
            event.addParticipant(user);

            userService.saveOrUpdate(user);
            eventService.saveOrUpdate(event);

            return "OK";
        } else {
            return new PaymentGatewaySupport(null, null).getPaymentGatewayForEvent(event.getPrice() + EconomicSupport.calculateFeesAddedUp(event.getPrice(), event.getUnion().getUnionEconomics().getFeePercent()), eventInvitation);
        }
    }

    @PostMapping("/event/leave")
    @ResponseBody
    public String leaveEvent(@RequestBody int eventId, Principal principal){
        Event event = eventService.findById(eventId);
        User user = userService.findByUsername(principal.getName());

        EventInvitation eventInvitation = eventInvitationService.findByEventAndUser(event, user);

        if (event.getParticipants().contains(user)){
            event.removeParticipant(user);
            if (event.getEventInvitations().contains(eventInvitation)){
                event.removeUserInvitation(eventInvitation);
            }
            eventService.saveOrUpdate(event);
        }

        if (user.getEvents().contains(event)){
            user.removeEvent(event);
            if (user.getEventInvitations().contains(eventInvitation)){
                user.removeEventInvitation(eventInvitation);
            }
            userService.saveOrUpdate(user);
        }

        eventInvitationService.deleteEventInvitation(eventInvitation);

        return "OK";
    }

    @PostMapping("/event/description")
    @ResponseBody
    public String editDescription(@RequestBody String json){
        JSONObject jsonObject = new JSONObject(json);
        Event event = eventService.findById(jsonObject.getInt("eventId"));

        event.setDescription(jsonObject.getString("description"));

        eventService.saveOrUpdate(event);

        return "OK";
    }

}
