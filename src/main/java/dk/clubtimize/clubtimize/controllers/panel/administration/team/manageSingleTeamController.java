package dk.clubtimize.clubtimize.controllers.panel.administration.team;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.controllers.support.PayedForCurrentSeasonSupport;
import dk.clubtimize.clubtimize.controllers.support.mail.MailSender;
import dk.clubtimize.clubtimize.models.opsætning.TeamDepartment;
import dk.clubtimize.clubtimize.models.union.Coach;
import dk.clubtimize.clubtimize.models.union.Player;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.UnionSeason;
import dk.clubtimize.clubtimize.models.user.UnionAssosiation;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;
import dk.clubtimize.clubtimize.services.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.SecureRandom;

@Controller
@RequestMapping("/panel/administrerhold/{teamId}")
public class manageSingleTeamController {

    private TeamService teamService;
    private DepartmentService departmentService;
    private UserService userService;
    private CoachService coachService;
    private UserSeasonService userSeasonService;
    private TeamSeasonService teamSeasonService;
    private UnionSeasonService unionSeasonService;
    private UnionAssosiationService unionAssosiationService;
    private PlayerService playerService;
    private PayedForCurrentSeasonSupport payedForCurrentSeasonSupport;

    @Autowired
    public manageSingleTeamController(TeamService teamService, DepartmentService departmentService, UserService userService, CoachService coachService, UserSeasonService userSeasonService, TeamSeasonService teamSeasonService, UnionSeasonService unionSeasonService, UnionAssosiationService unionAssosiationService, PlayerService playerService, PayedForCurrentSeasonSupport payedForCurrentSeasonSupport) {
        this.teamService = teamService;
        this.departmentService = departmentService;
        this.userService = userService;
        this.coachService = coachService;
        this.userSeasonService = userSeasonService;
        this.teamSeasonService = teamSeasonService;
        this.unionSeasonService = unionSeasonService;
        this.unionAssosiationService = unionAssosiationService;
        this.playerService = playerService;
        this.payedForCurrentSeasonSupport = payedForCurrentSeasonSupport;
    }

    @GetMapping("")
    public String showPage(@PathVariable("teamId") int teamId, Model model){
        Team team = teamService.findById(teamId);

        model.addAttribute("team", team);
        model.addAttribute("payedService", payedForCurrentSeasonSupport);

        return "panel/management/manageTeams/SingleTeam/singleTeam";
    }

    @PostMapping("/rediger")
    @ResponseBody
    public String editTeam(@PathVariable("teamId") int id, @RequestBody String json){
        JSONObject jsonObject = new JSONObject(json);
        Team team = teamService.findById(id);

        team.setTitle(jsonObject.getString("title"));

        team.getTeamDepartment().removeTeam(team);
        departmentService.saveOrUpdate(team.getTeamDepartment());

        TeamDepartment newDepartment = departmentService.findById(jsonObject.getInt("departmentId"));
        newDepartment.addTeam(team);
        departmentService.saveOrUpdate(newDepartment);

        team.setTeamDepartment(newDepartment);
        team.setAvalibleSpots(jsonObject.getInt("avalibleSpots"));
        team.setClosed(jsonObject.getBoolean("isClosed"));
        team.setAgeGroup(jsonObject.getString("ageGroup"));
        team.setNiveau(jsonObject.getString("niveau"));

        teamService.saveOrUpdate(team);

        return "OK";
    }

    @PostMapping("/addCoach")
    @ResponseBody
    public String addCoach(@PathVariable("teamId") int teamId, @RequestParam("userId") int userId){
        Team team = teamService.findById(teamId);
        User user = userService.findById(userId);

        Coach coach = new Coach();
        coach.setUnion(team.getUnion());
        coach.setHourlyWage(0);
        coach.addTeam(team);

        team.addCoach(coach);

        UnionAssosiation unionAssosiation = user.getUnionAssosiation();
        unionAssosiation.addCoachPosition(coach);
        unionAssosiation.setUser(user);
        unionAssosiation.setUnion(team.getUnion());
        coach.setUnionAssosiation(unionAssosiation);

        coachService.saveOrUpdate(coach);

        userService.saveOrUpdate(user);
        teamService.saveOrUpdate(team);

        return "OK";
    }

    @DeleteMapping("/addCoach")
    @ResponseBody
    public String removeCoach(@PathVariable("teamId") int teamId, @RequestParam("coachId") int coachId){
        Team team = teamService.findById(teamId);
        Coach coach = coachService.findById(coachId);
        User user = coach.getUnionAssosiation().getUser();
        UnionAssosiation unionAssosiation = user.getUnionAssosiation();

        team.removeCoach(coach);
        teamService.saveOrUpdate(team);

        unionAssosiation.removeCoachPosition(coach);
        userService.saveOrUpdate(user);

        coachService.deleteCoach(coach);

        return "OK";
    }

    @PostMapping("/addPlayer")
    @ResponseBody
    public String addPlayer(@PathVariable("teamId") int teamId, @RequestParam("userId") int userId){
        Team team = teamService.findById(teamId);
        User user = userService.findById(userId);
        UnionSeason unionSeason = unionSeasonService.findByUnionAndSeasonYear(team.getUnion(), DateFactory.getCurrentSeasonYear());
        UserSeason userSeason = userSeasonService.findByUnionSeasonAndUser(unionSeason, user);

        Player player = new Player();
        player.setUnion(team.getUnion());
        player.setTeam(team);
        team.addPlayer(player);

        playerService.saveOrUpdate(player);

        UnionAssosiation unionAssosiation = user.getUnionAssosiation();
        unionAssosiation.addPlayerPosition(player);
        unionAssosiation.setUser(user);
        unionAssosiation.setUnion(team.getUnion());
        player.setUnionAssosiation(unionAssosiation);

        unionAssosiationService.saveOrUpdate(unionAssosiation);

        TeamSeason teamSeason = new TeamSeason();
        teamSeason.setPaymentCode(makeAlphaNumericString(20));
        teamSeason.setTeam(team);
        teamSeason.setPaymentSuccessful(false);
        teamSeason.setDateJoined(DateFactory.getCurrentDate());
        teamSeason.setUserSeason(userSeason);
        userSeason.addTeamSeason(teamSeason);

        teamService.saveOrUpdate(team);

        teamSeasonService.saveOrUpdate(teamSeason);
        userSeasonService.saveOrUpdate(userSeason);

        MailSender.addMemberToTeam(user, team);

        return "OK";
    }

    private String makeAlphaNumericString(int capacity){
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        StringBuilder stringBuilder = new StringBuilder(capacity);
        SecureRandom secureRandom = new SecureRandom();

        for(int i=0; i<capacity; i++){
            stringBuilder.append(AB.charAt(secureRandom.nextInt(AB.length())));
        }

        if (teamSeasonService.existsByPaymentCode(stringBuilder.toString())){
            stringBuilder.replace(0, stringBuilder.length(), makeAlphaNumericString(20));
        }

        return stringBuilder.toString();
    }

    @DeleteMapping("/addPlayer")
    @ResponseBody
    public String removePlayer(@PathVariable("teamId") int teamId, @RequestParam("playerId") int playerId){
//        Team team = teamService.findById(teamId);
//        Player player = playerService.findById(playerId);
//        User user = player.getUnionAssosiation().getUser();
//        UnionAssosiation unionAssosiation = user.getUnionAssosiation();
//        UnionSeason unionSeason = unionSeasonService.findByUnionAndSeasonYear(team.getUnion(), DateFactory.getCurrentSeasonYear());
//        UserSeason userSeason = userSeasonService.findByUnionSeasonAndUser(unionSeason, user);
//        TeamSeason teamSeason = teamSeasonService.findByUserSeasonAndTeam(userSeason, team);
//
//        team.removePlayer(player);
//        teamService.saveOrUpdate(team);
//
//        unionAssosiation.removePlayerPosition(player);
//        unionAssosiationService.saveOrUpdate(unionAssosiation);
//
//        userSeason.removeTeamSeason(teamSeason);
//        userSeasonService.saveOrUpdate(userSeason);
//        teamSeasonService.deleteTeamSeason(teamSeason);
//
//        playerService.deletePlayer(player);

        Team team = teamService.findById(teamId);
        Player player = playerService.findById(playerId);
        User user = player.getUnionAssosiation().getUser();
        UnionAssosiation unionAssosiation = user.getUnionAssosiation();
        //UnionSeason unionSeason = unionSeasonService.findByUnionAndSeasonYear(team.getUnion(), DateFactory.getCurrentSeasonYear());

        Iterable<UnionSeason> unionSeasons = unionSeasonService.findByUnion(team.getUnion());

        team.removePlayer(player);
        teamService.saveOrUpdate(team);

        unionAssosiation.removePlayerPosition(player);
        unionAssosiationService.saveOrUpdate(unionAssosiation);


        for (UnionSeason unionSeason : unionSeasons) {
            UserSeason userSeason = userSeasonService.findByUnionSeasonAndUser(unionSeason, user);
            TeamSeason teamSeason = teamSeasonService.findByUserSeasonAndTeam(userSeason, team);

            userSeason.removeTeamSeason(teamSeason);
            userSeasonService.saveOrUpdate(userSeason);
            teamSeasonService.deleteTeamSeason(teamSeason);
        }

        playerService.deletePlayer(player);

        return "OK";
    }

}
