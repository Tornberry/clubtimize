package dk.clubtimize.clubtimize.controllers.panel.economics.bookkeepingSupport;

import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UnionEconomicAPI {
    private String apiKey;

    public UnionEconomicAPI(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getAccounts(){
        HttpClient httpClient = HttpClientBuilder.create().build();

        try {
            HttpGet request = new HttpGet("https://restapi.e-conomic.com/accounts?pagesize=1000");
            request.addHeader("X-AppSecretToken", "gBBIGpZtyDhkHWYUwSMS162YCOWXr54Lel7MtIziq5M1");
            request.addHeader("X-AgreementGrantToken", apiKey);
            request.addHeader("Content-Type", "application/json");
            HttpResponse response = httpClient.execute(request);

            return EntityUtils.toString(response.getEntity());

        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    public String getAccountingYears(){
        HttpClient httpClient = HttpClientBuilder.create().build();

        try {
            HttpGet request = new HttpGet("https://restapi.e-conomic.com/accounting-years?pagesize=1000");
            request.addHeader("X-AppSecretToken", "gBBIGpZtyDhkHWYUwSMS162YCOWXr54Lel7MtIziq5M1");
            request.addHeader("X-AgreementGrantToken", apiKey);
            request.addHeader("Content-Type", "application/json");
            HttpResponse response = httpClient.execute(request);

            return EntityUtils.toString(response.getEntity());

        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    public String addLedgerItem(List<Invoice> invoices, int accountNumber, int counter, boolean withFees, String year){
        HttpClient httpClient = HttpClientBuilder.create().build();

        try {
            List<NameValuePair> params = new ArrayList<>();

            HttpPost request = new HttpPost("https://restapi.e-conomic.com/journals-experimental/1/vouchers");
            request.addHeader("X-AppSecretToken", "gBBIGpZtyDhkHWYUwSMS162YCOWXr54Lel7MtIziq5M1");
            request.addHeader("X-AgreementGrantToken", apiKey);
            request.addHeader("Content-Type", "application/json");
            request.setEntity(new StringEntity(getLedgerItemJSONFromArray(invoices, accountNumber, counter, withFees, year), "UTF-8"));
            HttpResponse response = httpClient.execute(request);

            return EntityUtils.toString(response.getEntity());

        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    private String getLedgerItemJSONFromArray(List<Invoice> invoices, int accountNumber, int counter, boolean withFees, String year) {
        JSONObject mainObject = new JSONObject();

        JSONObject accountingYearObject = new JSONObject();
        accountingYearObject.put("year", year);
        mainObject.put("accountingYear", accountingYearObject);

        JSONObject entries = new JSONObject();
        JSONArray voucersArray = new JSONArray();

        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
        for (Invoice invoice : invoices){
            if (withFees){
                if (invoice.getTotalCostWithAdditional() <= 0){
                    continue;
                }
            } else {
                if (invoice.getTotalCostWithoutAdditional() <= 0){
                    continue;
                }
            }


            JSONObject mainVoucherObject = new JSONObject();

            if (withFees){
                mainVoucherObject.put("amount", -invoice.getTotalCostWithAdditional());
            } else {
                mainVoucherObject.put("amount", -invoice.getTotalCostWithoutAdditional());
            }

            mainVoucherObject.put("date", formatter.parseDateTime(invoice.getDate()).toString("yyyy-MM-dd"));

            JSONObject accountObject = new JSONObject();
            accountObject.put("accountNumber", accountNumber);
            mainVoucherObject.put("account", accountObject);

            JSONObject contraAccountObject = new JSONObject();
            contraAccountObject.put("accountNumber", counter);
            mainVoucherObject.put("contraAccount", contraAccountObject);

            if (invoice.getUser() != null){
                mainVoucherObject.put("text", invoice.getItems().get(0).getDescription() + " - " + invoice.getUser().getFirstname() + " " + invoice.getUser().getLastname());
            } else {
                mainVoucherObject.put("text", invoice.getItems().get(0).getDescription() + " - SLETTET BRUGER: " + invoice.getItems().get(invoice.getItems().size() - 1).getDescription().split(":")[1]);
            }

            voucersArray.put(mainVoucherObject);
        }

        entries.put("financeVouchers", voucersArray);
        mainObject.put("entries", entries);

        return mainObject.toString();
    }
}
