package dk.clubtimize.clubtimize.controllers.panel.economics;

import dk.clubtimize.clubtimize.controllers.support.PermissionEnum;
import dk.clubtimize.clubtimize.controllers.support.RoleSupport;
import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.invoice.DebtReport;
import dk.clubtimize.clubtimize.models.economics.invoice.FeeReport;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.services.InvoiceService;
import dk.clubtimize.clubtimize.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/panel")
public class InvoicesController {

    private UserService userService;
    private InvoiceService invoiceService;

    @Autowired
    public InvoicesController(UserService userService, InvoiceService invoiceService) {
        this.userService = userService;
        this.invoiceService = invoiceService;
    }

    @GetMapping("/transaktioner")
    public String getAllInvoices(Model model, Principal principal){
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            model.addAttribute("invoices", makeInvoiceList(principal.getName()));
            model.addAttribute("feeReports", getAllMonthlyFeeReportsByUnion(userService.findByUsername(principal.getName()).getUnion()));
            model.addAttribute("debtReports", getAllMonthlyDebtReportsByUnion(userService.findByUsername(principal.getName()).getUnion()));
            return "panel/economics/invoices/invoices";
        } else {
            return "error/404.html";
        }
    }

    private List<FeeReport> getAllMonthlyFeeReportsByUnion(Union union) {
        List<FeeReport> feeReports = new ArrayList<>();
        for (EconomicYear economicYear : union.getUnionEconomics().getEconomicYears()){
            feeReports.addAll(economicYear.getFeeReports());
        }

        return feeReports;
    }

    private List<DebtReport> getAllMonthlyDebtReportsByUnion(Union union) {
        List<DebtReport> debtReports = new ArrayList<>();
        for (EconomicYear economicYear : union.getUnionEconomics().getEconomicYears()){
            debtReports.addAll(economicYear.getDebtReports());
        }

        return debtReports;
    }

    @GetMapping("/fakturaer/faktura")
    public String getInvoice(Model model, @RequestParam("faknr") String invoiceNumber, Principal principal){
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            model.addAttribute("invoice", invoiceService.findByUnionNameAndInvoiceNumber(userService.findByUsername(principal.getName()).getUnion().getName(), invoiceNumber));
            return "panel/economics/invoices/invoice/invoice";
        } else {
            return "error/404.html";
        }
    }

    private List<Invoice> makeInvoiceList(String username) {
        List<EconomicYear> economicYears = userService.findByUsername(username).getUnion().getUnionEconomics().getEconomicYears();
        List<Invoice> invoices = new ArrayList<>();

        for (EconomicYear economicYear : economicYears){
            invoices.addAll(economicYear.getInvoices());
        }
        return invoices;
    }

}
