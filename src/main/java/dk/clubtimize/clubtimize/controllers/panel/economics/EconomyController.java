package dk.clubtimize.clubtimize.controllers.panel.economics;

import com.google.common.collect.Lists;
import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.controllers.support.PermissionEnum;
import dk.clubtimize.clubtimize.controllers.support.RoleSupport;
import dk.clubtimize.clubtimize.models.economics.BankinfoContainer;
import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.PaymentRequest;
import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.services.*;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/panel")
public class EconomyController {

    private UserService userService;
    private EconomicYearService economicYearService;
    private UnionEconomicsService unionEconomicsService;
    private BankinfoContainerService bankinfoContainerService;
    private InvoiceService invoiceService;

    @Autowired
    public EconomyController(UserService userService, EconomicYearService economicYearService, UnionEconomicsService unionEconomicsService, BankinfoContainerService bankinfoContainerService, InvoiceService invoiceService) {
        this.userService = userService;
        this.economicYearService = economicYearService;
        this.unionEconomicsService = unionEconomicsService;
        this.bankinfoContainerService = bankinfoContainerService;
        this.invoiceService = invoiceService;
    }

    @GetMapping("/økonomi")
    public String getEconomy(Model model,Principal principal){
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            model.addAttribute("activepaymentrequest", getActivePaymentRequest(principal.getName()));
            model.addAttribute("closedpaymentrequests", getClosedPaymentRequests(principal));
            return "panel/economics/economy/economy";
        } else {
            return "error/404.html";
        }
    }

    @GetMapping("/økonomi/balancehistory")
    @ResponseBody
    public String getBalanceHistory(@RequestParam("days") int days, Principal principal) {
        Union union = userService.findByUsername(principal.getName()).getUnion();

        // Main JSON array
        JSONObject main = new JSONObject();

        // Labels
        JSONArray labels = new JSONArray();

        // Datasets
        JSONArray datasets = new JSONArray();
        JSONObject datasetsObject = new JSONObject();
        datasetsObject.put("label", "Balance");

        // Data
        JSONArray data = new JSONArray();

        // Label color
        JSONArray colors = new JSONArray();

        boolean stillInDateRange = true;
        int daysBackCount = days;
        while (stillInDateRange){
            DateTime date = DateTime.now().minusDays(daysBackCount);

            float balance = 0;
            for (Invoice invoice : invoiceService.findByUnionNameAndDate(union.getName(), formatDateValue(date.getDayOfMonth()) + "-" + formatDateValue(date.getMonthOfYear()) + "-" + date.getYear())){
                balance += invoice.getTotalCostWithoutAdditional();
            }

            labels.put(formatDateValue(date.getDayOfMonth()) + "-" + formatDateValue(date.getMonthOfYear()) + "-" + date.getYear());
            colors.put("#44A2D2");
            data.put(String.valueOf(balance));

            daysBackCount -= 1;

            if (daysBackCount == -1){
                stillInDateRange = false;
            }
        }

        main.put("labels", labels);
        datasetsObject.put("data", data);
        datasetsObject.put("backgroundColor", colors);
        datasets.put(datasetsObject);
        main.put("datasets", datasets);

        return main.toString();
    }

    private String formatDateValue(int value) {
        if (value > 0 && value < 10){
            return "0" + String.valueOf(value);
        } else {
            return String.valueOf(value);
        }
    }

    private List<PaymentRequest> getClosedPaymentRequests(Principal principal) {
        Union union = userService.findByUsername(principal.getName()).getUnion();
        List<PaymentRequest> paymentRequests = new ArrayList<>();

        for (EconomicYear economicYear : union.getUnionEconomics().getEconomicYears()){
            for (PaymentRequest paymentRequest : economicYear.getPaymentRequests()){
                if (!paymentRequest.isOpen()){
                    paymentRequests.add(paymentRequest);
                }
            }
        }

        return paymentRequests;
    }

    private PaymentRequest getActivePaymentRequest(String name) {
        Union union = userService.findByUsername(name).getUnion();

        for (EconomicYear economicYear : union.getUnionEconomics().getEconomicYears()){
            for (PaymentRequest paymentRequest : economicYear.getPaymentRequests()){
                if (paymentRequest.isOpen()){
                    return paymentRequest;
                }
            }
        }

        return null;
    }

    @PostMapping("/paymentrequest")
    @ResponseBody
    public String makePaymentRequest(Principal principal){
        if (principal != null){
            Union union = userService.findByUsername(principal.getName()).getUnion();
            EconomicYear economicYear = economicYearService.findByUnionEconomicsAndYear(union.getUnionEconomics(), DateFactory.getCurrentSeasonYear());

            if (!hasOpenRequest(union)){
                PaymentRequest paymentRequest = new PaymentRequest(economicYear);

                economicYear.addPaymentRequest(paymentRequest);

                economicYearService.saveOrUpdate(economicYear);

                return "OK";
            } else {
                return "Already one open";
            }
        } else {
            return "Forbidden";
        }
    }

    private boolean hasOpenRequest(Union union) {
        boolean open = false;
        for (EconomicYear economicYear : union.getUnionEconomics().getEconomicYears()){
            for (PaymentRequest paymentRequest : economicYear.getPaymentRequests()){
                if (paymentRequest.isOpen()){
                    open = true;
                }
            }
        }
        return open;
    }

    @GetMapping("/hasbankinfo")
    @ResponseBody
    public boolean hasBankInfo(Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();
        return union.getUnionEconomics().getBankinfoContainer() != null;
    }

    @PostMapping("/setbankinformations")
    @ResponseBody
    public String setBankInformations(@RequestBody String json, Principal principal){
        JSONObject jsonObject = new JSONObject(json);
        UnionEconomics unionEconomics = userService.findByUsername(principal.getName()).getUnion().getUnionEconomics();

        if (unionEconomics.getBankinfoContainer() == null){
            BankinfoContainer bankinfoContainer = new BankinfoContainer(unionEconomics,DateFactory.getCurrentDate(),jsonObject.getString("accountNumber"),jsonObject.getString("registrationNumber"),jsonObject.getString("bankName"),jsonObject.getString("phoneNumber"));
            unionEconomics.setBankinfoContainer(bankinfoContainer);

            bankinfoContainerService.saveOrUpdate(bankinfoContainer);
            unionEconomicsService.saveOrUpdate(unionEconomics);
        }

        return "OK";
    }

}
