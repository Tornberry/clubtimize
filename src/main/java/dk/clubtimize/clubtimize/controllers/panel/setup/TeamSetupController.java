package dk.clubtimize.clubtimize.controllers.panel.setup;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.controllers.support.OwnershipValidator;
import dk.clubtimize.clubtimize.controllers.support.PermissionEnum;
import dk.clubtimize.clubtimize.controllers.support.RoleSupport;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.opsætning.Hall;
import dk.clubtimize.clubtimize.models.opsætning.TeamDepartment;
import dk.clubtimize.clubtimize.models.training.Focus;
import dk.clubtimize.clubtimize.models.training.Training;
import dk.clubtimize.clubtimize.models.training.TrainingSchedule;
import dk.clubtimize.clubtimize.models.training.Trainingsplan;
import dk.clubtimize.clubtimize.models.union.Coach;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import dk.clubtimize.clubtimize.services.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/panel/opsætning")
public class TeamSetupController {

    private UnionService unionService;
    private UserService userService;
    private TeamCategoryService teamCategoryService;
    private TeamService teamService;
    private UnionAssosiationService unionAssosiationService;
    private CoachService coachService;

    @Autowired
    public TeamSetupController(UnionService unionService, UserService userService, TeamCategoryService teamCategoryService, TeamService teamService, UnionAssosiationService unionAssosiationService, CoachService coachService) {
        this.unionService = unionService;
        this.userService = userService;
        this.teamCategoryService = teamCategoryService;
        this.teamService = teamService;
        this.unionAssosiationService = unionAssosiationService;
        this.coachService = coachService;
    }

    @GetMapping("/hold")
    public String showHoldadministration(Principal principal){
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            return "panel/setup/teams/teams";
        } else {
            return "error/404.html";
        }
    }

    @PostMapping("/hold")
    @ResponseBody
    public ResponseEntity postCreateTeam(@RequestBody String json, Principal principal){

        JSONObject jsonObject = new JSONObject(json);

        Team team = new Team(
                jsonObject.getString("title"),
                jsonObject.getBoolean("closed"),
                jsonObject.getInt("avalibleSpots"),
                jsonObject.getString("ageGroup"),
                jsonObject.getString("niveau"),
                jsonObject.getFloat("price"),
                jsonObject.getString("paymentSchedule"),
                jsonObject.getString("startDate")
                );


        List<Object> coachIDs = jsonObject.getJSONArray("coaches").toList();

        for (Object coachID : coachIDs){
            Coach coach = coachService.findById(Integer.parseInt((String)coachID));
            coach.addTeam(team);
            team.addCoach(coach);
        }

        TeamDepartment teamDepartment = userService.findByUsername(principal.getName()).getUnion().getDepartments().stream().filter(x -> x.getTitle().equals(jsonObject.getString("department"))).findAny().orElse(null);

        team.setTeamDepartment(teamDepartment);

        JSONArray trainingScheduleArray = jsonObject.getJSONArray("trainingScheduleAdapters");

        for (int i = 0; i < trainingScheduleArray.length(); i++){
            JSONObject trainingScheduleObject = trainingScheduleArray.getJSONObject(i);

            TrainingSchedule trainingSchedule = new TrainingSchedule();
            trainingSchedule.setWeekday(trainingScheduleObject.getString("weekday"));
            trainingSchedule.setTimeFrom(trainingScheduleObject.getString("timeFrom"));
            trainingSchedule.setTimeTo(trainingScheduleObject.getString("timeTo"));
            trainingSchedule.setTeam(team);

            Hall hall = userService.findByUsername(principal.getName()).getUnion().getHalls().stream().filter(x -> x.getTitle().equals(trainingScheduleObject.getString("location"))).findAny().orElse(null);

            trainingSchedule.setHall(hall);

            team.addTrainingSchedule(trainingSchedule);
        }

        team = createTrainings(team, userService.findByUsername(principal.getName()).getUnion());

        Union union = userService.findByUsername(principal.getName()).getUnion();

        team.setUnion(union);
        union.addTeam(team);

        teamService.saveOrUpdate(team);
        unionService.saveOrUpdate(union);

        return new ResponseEntity(HttpStatus.OK);

    }

    private Team createTrainings(Team team, Union union){
        for (TrainingSchedule trainingSchedule : team.getTrainingSchedules()){
            DateTimeFormatter pattern = DateTimeFormat.forPattern("dd-MM-yyyy");

            DateTime dt = new DateTime();

            String start = pattern.parseDateTime(team.getStartDate()).toString("dd-MM-yyyy");
            String end = getNextSeasonSwitchDate(union, team.getStartDate());
            DateTime startDate = pattern.parseDateTime(start);
            DateTime endDate = pattern.parseDateTime(end);

            List<DateTime> weekdayDates = new ArrayList<>();

            int day = 0;

            switch (trainingSchedule.getWeekday()) {
                case "Mandag":
                    day = 1;
                    break;
                case "Tirsdag":
                    day = 2;
                    break;
                case "Onsdag":
                    day = 3;
                    break;
                case "Torsdag":
                    day = 4;
                    break;
                case "Fredag":
                    day = 5;
                    break;
                case "Lørdag":
                    day = 6;
                    break;
                case "Søndag":
                    day = 7;
                    break;
            }


            while(startDate.isBefore(endDate)){
                if (startDate.getDayOfWeek() == day){
                    weekdayDates.add(startDate);
                }
                startDate = startDate.plusDays(1);
            }

            for (DateTime dateTime : weekdayDates){
                Training training = new Training();
                training.setDate(dateTime.toString("dd-MM-yyyy"));
                training.setTimeFrom(trainingSchedule.getTimeFrom());
                training.setTimeTo(trainingSchedule.getTimeTo());
                training.setTeam(team);
                training.setHall(trainingSchedule.getHall());

                Focus focus = new Focus();
                focus.setTraining(training);

                training.setFocus(focus);
                team.addTraining(training);

                Trainingsplan trainingsplan = new Trainingsplan();
                trainingsplan.setTraining(training);

                training.setTrainingsplan(trainingsplan);
            }
        }
        return team;
    }

    private String getNextSeasonSwitchDate(Union union, String startDateParse) {
        String seasonSwitchMonth = union.getSeasonSwitchMonth();

        DateTime startDate = DateTimeFormat.forPattern("dd-MM-yyyy").parseDateTime(startDateParse);
        DateTime seasonSwitchMonthThisYear = DateTimeFormat.forPattern("dd-MM-yyyy").parseDateTime("01-" + parseMonthNumber(DateFactory.monthToNumber(seasonSwitchMonth)) + "-" + new DateTime().getYear());

        if (startDate.isBefore(seasonSwitchMonthThisYear)){
            return "01-" + parseMonthNumber(DateFactory.monthToNumber(seasonSwitchMonth)) + "-" + new DateTime().getYear();
        } else if (startDate.isAfter(seasonSwitchMonthThisYear)){
            return "01-" + parseMonthNumber(DateFactory.monthToNumber(seasonSwitchMonth)) + "-" + (new DateTime().getYear() + 1);
        } else if (startDate.isEqual(seasonSwitchMonthThisYear)){
            return "01-" + parseMonthNumber(DateFactory.monthToNumber(seasonSwitchMonth)) + "-" + (new DateTime().getYear() + 1);
        } else {
            return "Error";
        }
    }

    private String parseMonthNumber(int monthOfYear) {
        if (monthOfYear < 10){
            return "0" + monthOfYear;
        } else {
            return String.valueOf(monthOfYear);
        }
    }

    @DeleteMapping("/hold/deleteteam")
    public ResponseEntity deleteTeam(@RequestBody int id, Principal principal){
        Team team = teamService.findById(id);

        if (OwnershipValidator.validateOwnership(team, userService.findByUsername(principal.getName()))){
            for (Coach coach : team.getCoaches()){
                coach.removeTeam(team);
                coachService.saveOrUpdate(coach);
            }

            for(TeamSeason teamSeason : team.getTeamSeasons()){
                teamSeason.setTeam(null);
            }

            for(Invoice invoice : team.getAssosiatedInvoices()){
                invoice.setAssosiatedTeam(null);
            }

            teamService.deleteTeam(team);
        }

        return new ResponseEntity(HttpStatus.OK);
    }
}
