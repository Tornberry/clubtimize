package dk.clubtimize.clubtimize.controllers.panel.unionsettings;

import com.google.common.collect.Lists;
import dk.clubtimize.clubtimize.controllers.support.*;
import dk.clubtimize.clubtimize.controllers.support.mail.MailSender;
import dk.clubtimize.clubtimize.models.economics.bookkeeping.accountingIntegrations.DineroIntegration;
import dk.clubtimize.clubtimize.models.economics.bookkeeping.accountingIntegrations.EconomicIntegration;
import dk.clubtimize.clubtimize.models.union.Player;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.UnionSeason;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;
import dk.clubtimize.clubtimize.services.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/panel")
public class UnionSettingsController {

    private UserService userService;
    private UnionService unionService;
    private TeamService teamService;
    private DineroIntegrationService dineroIntegrationService;
    private UnionSeasonService unionSeasonService;
    private TeamSeasonService teamSeasonService;
    private UserSeasonService userSeasonService;

    @Autowired
    public UnionSettingsController(UserService userService, UnionService unionService, TeamService teamService, DineroIntegrationService dineroIntegrationService, UnionSeasonService unionSeasonService, TeamSeasonService teamSeasonService, UserSeasonService userSeasonService) {
        this.userService = userService;
        this.unionService = unionService;
        this.teamService = teamService;
        this.dineroIntegrationService = dineroIntegrationService;
        this.unionSeasonService = unionSeasonService;
        this.teamSeasonService = teamSeasonService;
        this.userSeasonService = userSeasonService;
    }


    @GetMapping("/foreningsindstillinger")
    public String showPage(Principal principal){
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            return "panel/unionsettings/unionsettings";
        } else {
            return "error/404.html";
        }
    }

    @PostMapping("/foreningsindstillinger/logo")
    @ResponseBody
    public String changeUnionLogo(@RequestParam("file") MultipartFile file, Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();

        if (!file.isEmpty()){
            try {
                new File(ClubtimizeSettings.getUnionLogoResourcePath() + union.getUrl().substring(1) + "/").mkdirs();
                byte[] bytes = file.getBytes();
                Path path = Paths.get(ClubtimizeSettings.getUnionLogoResourcePath() + union.getUrl().substring(1) + "/" + "logo." + file.getOriginalFilename().split("\\.")[1]);
                Files.write(path, bytes);

                union.setLogoPath("/unionlogo/" + union.getUrl().substring(1) + "/" + "logo." + file.getOriginalFilename().split("\\.")[1]);
                unionService.saveOrUpdate(union);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "OK";
    }

    @PostMapping("/foreningsindstillinger/gebyrer")
    @ResponseBody
    public String changeWhoPaysFees(Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();

        if (!union.getUnionEconomics().isPayingFees()){
            union.getUnionEconomics().setPayingFees(true);
        } else {
            union.getUnionEconomics().setPayingFees(false);
        }

        unionService.saveOrUpdate(union);

        return "OK";
    }

    @PostMapping("/foreningsindstillinger/DineroSetup")
    @ResponseBody
    public String setupDinero(@RequestBody String json, Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();
        JSONObject jsonObject = new JSONObject(json);

        if (union.getUnionEconomics().getBookkeeping().getDineroIntegration() != null){
            union.getUnionEconomics().getBookkeeping().getDineroIntegration().setAPIKey(jsonObject.getString("APIKey"));
            union.getUnionEconomics().getBookkeeping().getDineroIntegration().setOrganizationID(jsonObject.getString("OrganizationID"));

            union.getUnionEconomics().getBookkeeping().setChosenIntegration("Dinero");

            unionService.saveOrUpdate(union);
        } else {
            DineroIntegration dineroIntegration = new DineroIntegration();
            dineroIntegration.setBookkeeping(union.getUnionEconomics().getBookkeeping());

            dineroIntegration.setAPIKey(jsonObject.getString("APIKey"));
            dineroIntegration.setOrganizationID(jsonObject.getString("OrganizationID"));


            union.getUnionEconomics().getBookkeeping().setDineroIntegration(dineroIntegration);
            union.getUnionEconomics().getBookkeeping().setChosenIntegration("Dinero");
            union.getUnionEconomics().getBookkeeping().setFirstTimeSetup(true);

            unionService.saveOrUpdate(union);
        }

        return "OK";
    }

    @PostMapping("/foreningsindstillinger/EconomicSetup")
    @ResponseBody
    public String setupEconomic(@RequestBody String json, Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();
        JSONObject jsonObject = new JSONObject(json);

        if (union.getUnionEconomics().getBookkeeping().getEconomicIntegration() != null){
            union.getUnionEconomics().getBookkeeping().getEconomicIntegration().setAPIKey(jsonObject.getString("APIKey"));

            union.getUnionEconomics().getBookkeeping().setChosenIntegration("Economic");

            unionService.saveOrUpdate(union);
        } else {
            EconomicIntegration economicIntegration = new EconomicIntegration();
            economicIntegration.setBookkeeping(union.getUnionEconomics().getBookkeeping());

            economicIntegration.setAPIKey(jsonObject.getString("APIKey"));

            union.getUnionEconomics().getBookkeeping().setEconomicIntegration(economicIntegration);
            union.getUnionEconomics().getBookkeeping().setChosenIntegration("Economic");
            union.getUnionEconomics().getBookkeeping().setFirstTimeSetup(true);

            unionService.saveOrUpdate(union);
        }

        return "OK";
    }

    @PostMapping("/foreningsindstillinger/EditDineroAPIKey")
    @ResponseBody
    public String editDineroAPIKey(@RequestBody String apiKey, Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();

        union.getUnionEconomics().getBookkeeping().getDineroIntegration().setAPIKey(apiKey);

        unionService.saveOrUpdate(union);

        return "OK";
    }

    @PostMapping("/foreningsindstillinger/EditDineroOrgID")
    @ResponseBody
    public String editDineroOrgID(@RequestBody String orgID, Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();

        union.getUnionEconomics().getBookkeeping().getDineroIntegration().setOrganizationID(orgID);

        unionService.saveOrUpdate(union);

        return "OK";
    }

    @PostMapping("/foreningsindstillinger/EditEconomicAPIKey")
    @ResponseBody
    public String editEconomicAPIKey(@RequestBody String apiKey, Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();

        union.getUnionEconomics().getBookkeeping().getEconomicIntegration().setAPIKey(apiKey);

        unionService.saveOrUpdate(union);

        return "OK";
    }

    @PostMapping("/foreningsindstillinger/nysæson")
    @ResponseBody
    public String createNewSeason(Principal principal, @RequestBody String json){
        JSONObject jsonObject = new JSONObject(json);
        Union union = userService.findByEmail(principal.getName()).getUnion();

        UnionSeason unionSeason = new UnionSeason();
        unionSeasonService.saveOrUpdate(unionSeason);

        if (jsonObject.getBoolean("halfWaySeason")){
            unionSeason.setSeasonYear(midwaySeasonString(unionSeasonService.findByUnion(union)));
        } else {
            unionSeason.setSeasonYear(nextUnionSeasonString(union.getUnionSeasons()));
        }


        UnionSeason currentSeason = unionSeasonService.findByUnionAndSeasonYear(union, getCurrentSeason(union.getUnionSeasons()));

        unionSeason = makeNewUserSeasons(unionSeason, currentSeason);

        unionSeason.setUnion(union);

        union.addUnionSeason(unionSeason);
        unionService.saveOrUpdate(union);

        unionSeasonService.saveOrUpdate(unionSeason);

        //---------------------------------------------------------

        if (jsonObject.getBoolean("sendMail") && ClubtimizeSettings.isProductionEnvironment()){
            sendMails(union);
        }

        return "OK";
    }

    private void sendMails(Union union) {

        for (Team team : teamService.findByUnion(union)){
            for (Player player : team.getPlayers()){
                MailSender.newSeasonForMembers(player.getUnionAssosiation().getUser());
            }
        }
    }

    private UnionSeason makeNewUserSeasons(UnionSeason unionSeason, UnionSeason currentSeason) {
        for (UserSeason userSeason : currentSeason.getUserSeasons()){
            UserSeason newUserSeason = new UserSeason();
            userSeasonService.saveOrUpdate(newUserSeason);

            newUserSeason.setUser(userSeason.getUser());
            newUserSeason.setUnionSeason(unionSeason);

            newUserSeason.setTeamSeasons(makeNewTeamSeasons(userSeason.getTeamSeasons(), userSeason, newUserSeason));

            userSeasonService.saveOrUpdate(newUserSeason);

            unionSeason.addUserSeason(newUserSeason);
        }

        return unionSeason;
    }

    private List<TeamSeason> makeNewTeamSeasons(List<TeamSeason> teamSeasons, UserSeason userSeason, UserSeason newUserSeason) {
        List<TeamSeason> newTeamSeasons = new ArrayList<>();

        for (TeamSeason teamSeason : teamSeasons){
            System.out.println(teamSeason.getDateJoined());
            TeamSeason newTeamSeason = new TeamSeason();
            teamSeasonService.saveOrUpdate(newTeamSeason);

            newTeamSeason.setDateJoined(DateFactory.getCurrentDate());
            newTeamSeason.setPaymentCode(SecretFactory.makeAlphaNumericString(20));
            newTeamSeason.setPaymentSuccessful(false);

            Team team = teamSeason.getTeam();

            newTeamSeason.setTeam(team);
            team.addTeamSeason(newTeamSeason);
            teamService.saveOrUpdate(team);

            newTeamSeason.setUserSeason(newUserSeason);

            newTeamSeasons.add(newTeamSeason);

            teamSeasonService.saveOrUpdate(newTeamSeason);
        }

        return newTeamSeasons;
    }


    private String getCurrentSeason(List<UnionSeason> unionSeasons) {
        String currentUnionSeasonString = null;

        for (UnionSeason unionSeason : unionSeasons){
            if (currentUnionSeasonString == null){
                currentUnionSeasonString = unionSeason.getSeasonYear();
            } else {
                int currentLateYear = Integer.parseInt(currentUnionSeasonString.split(" ")[0].split("/")[1]);
                int unionSeasonLateYear = Integer.parseInt(unionSeason.getSeasonYear().split(" ")[0].split("/")[1]);

                if (unionSeasonLateYear >= currentLateYear){
                    int currentCount = extractCount(currentUnionSeasonString);
                    int unionSeasonCount = extractCount(unionSeason.getSeasonYear());

                    if (currentLateYear == unionSeasonLateYear){
                        if (unionSeasonCount > currentCount){
                            currentUnionSeasonString = unionSeason.getSeasonYear();
                        }
                    } else {
                        currentUnionSeasonString = unionSeason.getSeasonYear();
                    }

                }
            }
        }

        return currentUnionSeasonString;
    }

    private int extractCount(String currentUnionSeasonString) {
        if (currentUnionSeasonString.split(" ").length > 1){
            return Integer.parseInt(currentUnionSeasonString.split(" ")[2]);
        } else {
            return 0;
        }
    }


    private String nextUnionSeasonString(List<UnionSeason> unionSeasons) {
        int currentCount = -1;

        for (UnionSeason unionSeason : unionSeasons){
            if (unionSeason.getSeasonYear().startsWith(DateFactory.getCurrentSeasonYear())){
                if (!unionSeason.getSeasonYear().equals(DateFactory.getCurrentSeasonYear())){
                    int seasonCount = Integer.parseInt(unionSeason.getSeasonYear().split(" ")[2]);

                    if (seasonCount > currentCount){
                        currentCount = seasonCount;
                    }
                } else {
                    if (0 > currentCount){
                        currentCount = 0;
                    }
                }
            }
        }

        if (currentCount == -1){
            return DateFactory.getCurrentSeasonYear();
        } else if (currentCount == 0){
            return DateFactory.getCurrentSeasonYear() + " - " + 1;
        } else {
            return DateFactory.getCurrentSeasonYear() + " - " + (currentCount + 1);
        }
    }

    private String midwaySeasonString(Iterable<UnionSeason> unionSeasons) {
        if(getCurrentSeason(Lists.newArrayList(unionSeasons)).startsWith(DateFactory.getCurrentSeasonYear())){
            int currentCount = 0;

            for (UnionSeason unionSeason : unionSeasons){
                if (unionSeason.getSeasonYear().startsWith(DateFactory.getCurrentSeasonYear())){
                    if (!unionSeason.getSeasonYear().equals(DateFactory.getCurrentSeasonYear())){
                        int seasonCount = Integer.parseInt(unionSeason.getSeasonYear().split(" ")[2]);

                        if (seasonCount > currentCount){
                            currentCount = seasonCount;
                        }
                    }
                }
            }

            if (currentCount == 0){
                return DateFactory.getCurrentSeasonYear() + " - " + 1;
            } else {
                return DateFactory.getCurrentSeasonYear() + " - " + (currentCount + 1);
            }
        } else if (getCurrentSeason(Lists.newArrayList(unionSeasons)).equals(DateFactory.getLastSeasonYear())){
            int currentCount = 0;

            for (UnionSeason unionSeason : unionSeasons){
                if (unionSeason.getSeasonYear().startsWith(DateFactory.getLastSeasonYear())){
                    if (!unionSeason.getSeasonYear().equals(DateFactory.getLastSeasonYear())){
                        int seasonCount = Integer.parseInt(unionSeason.getSeasonYear().split(" ")[2]);

                        if (seasonCount > currentCount){
                            currentCount = seasonCount;
                        }
                    }
                }
            }

            if (currentCount == 0){
                return DateFactory.getLastSeasonYear() + " - " + 1;
            } else {
                return DateFactory.getLastSeasonYear() + " - " + (currentCount + 1);
            }
        } else {
            return "";
        }
    }
}
