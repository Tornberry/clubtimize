package dk.clubtimize.clubtimize.controllers.panel.discounts;

import dk.clubtimize.clubtimize.controllers.support.PermissionEnum;
import dk.clubtimize.clubtimize.controllers.support.RoleSupport;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.discount.discountTypes.TwoTeamDiscount;
import dk.clubtimize.clubtimize.services.TeamService;
import dk.clubtimize.clubtimize.services.TwoTeamDiscountService;
import dk.clubtimize.clubtimize.services.UnionService;
import dk.clubtimize.clubtimize.services.UserService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/panel")
public class DiscountController {

    private UserService userService;
    private TeamService teamService;
    private UnionService unionService;
    private TwoTeamDiscountService twoTeamDiscountService;

    @Autowired
    public DiscountController(UserService userService, TeamService teamService, UnionService unionService, TwoTeamDiscountService twoTeamDiscountService) {
        this.userService = userService;
        this.teamService = teamService;
        this.unionService = unionService;
        this.twoTeamDiscountService = twoTeamDiscountService;
    }

    @GetMapping("/rabatter")
    public String showPage(Principal principal){
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            return "panel/discounts/discounts";
        } else {
            return "error/404.html";
        }
    }

    @PostMapping("/rabatter")
    @ResponseBody
    public String addTwoTeamDiscount(@RequestBody String json, Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();
        JSONObject jsonObject = new JSONObject(json);

        List<Team> teams = new ArrayList<>();
        for (Object team : jsonObject.getJSONArray("teams")){
            if (!team.toString().equals("null")){
                if (!teams.contains(teamService.findById((int)team))){
                    teams.add(teamService.findById((int)team));
                }
            }
        }

        TwoTeamDiscount twoTeamDiscount = new TwoTeamDiscount();
        twoTeamDiscount.setDiscount(union.getDiscount());
        twoTeamDiscount.setTeams(teams);
        twoTeamDiscount.setTitle(jsonObject.getString("title"));
        twoTeamDiscount.setAmount(jsonObject.getFloat("amount"));
        union.getDiscount().addTwoTeamDiscount(twoTeamDiscount);

        twoTeamDiscountService.saveOrUpdate(twoTeamDiscount);

        for (Team team : teams){
            team.addTwoTeamDiscount(twoTeamDiscount);
            teamService.saveOrUpdate(team);
        }

        unionService.saveOrUpdate(union);

        return "OK";
    }

    @DeleteMapping("/rabatter")
    @ResponseBody
    public String deleteTwoTeamDiscount(@RequestBody int id, Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();

        TwoTeamDiscount twoTeamDiscount = twoTeamDiscountService.findById(id);

        for (Team team : twoTeamDiscount.getTeams()){
            team.removeTwoTeamDiscount(twoTeamDiscount);
            teamService.saveOrUpdate(team);
        }

        twoTeamDiscountService.deleteTwoTeamDiscount(twoTeamDiscount);

        return "OK";

    }


}
