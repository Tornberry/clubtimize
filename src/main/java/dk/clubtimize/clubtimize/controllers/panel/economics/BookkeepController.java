package dk.clubtimize.clubtimize.controllers.panel.economics;

import dk.clubtimize.clubtimize.controllers.panel.economics.bookkeepingSupport.UnionDineroAPI;
import dk.clubtimize.clubtimize.controllers.panel.economics.bookkeepingSupport.UnionEconomicAPI;
import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.controllers.support.PermissionEnum;
import dk.clubtimize.clubtimize.controllers.support.RoleSupport;
import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import dk.clubtimize.clubtimize.models.economics.bookkeeping.BookkeepingHistory;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.services.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Controller
@RequestMapping("/panel")
public class BookkeepController {

    private UserService userService;
    private TeamService teamService;
    private UnionService unionService;
    private InvoiceService invoiceService;

    @Autowired
    public BookkeepController(UserService userService, TeamService teamService, UnionService unionService, InvoiceService invoiceService) {
        this.userService = userService;
        this.teamService = teamService;
        this.unionService = unionService;
        this.invoiceService = invoiceService;
    }

    @GetMapping("/bogføring")
    public String showPage(Model model, Principal principal){
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            Union union = userService.findByUsername(principal.getName()).getUnion();
            model.addAttribute("bookkeepinghistories", union.getUnionEconomics().getBookkeeping().getBookkeepingHistories());
            return "panel/economics/bookkeeping/bookkeeping";
        } else {
            return "error/404.html";
        }

    }

    @GetMapping("/bogføring/chosenIntegration")
    @ResponseBody
    public String getChosenIntegration(Principal principal){
        return userService.findByUsername(principal.getName()).getUnion().getUnionEconomics().getBookkeeping().getChosenIntegration();
    }

    @GetMapping("/bogføring/dinerokonto")
    @ResponseBody
    public String getAccountsDinero(Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();

        String apiKey = union.getUnionEconomics().getBookkeeping().getDineroIntegration().getAPIKey();
        String firmId = union.getUnionEconomics().getBookkeeping().getDineroIntegration().getOrganizationID();

        UnionDineroAPI api = new UnionDineroAPI(apiKey, Integer.parseInt(firmId));

        String accountList = api.getAccountList();

        switch (accountList) {
            case "invalid_grant":
                return "invalid_grant";
            case "{\"Message\":\"Authorization has been denied for this request.\"}":
                return "denied";
            default:
                return accountList;
        }
    }

    @GetMapping("/bogføring/economickonto")
    @ResponseBody
    public String getAccountsEconomic(Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();

        String apiKey = union.getUnionEconomics().getBookkeeping().getEconomicIntegration().getAPIKey();

        UnionEconomicAPI api = new UnionEconomicAPI(apiKey);

        return api.getAccounts();
    }

    @GetMapping("/bogføring/economicregnskabsår")
    @ResponseBody
    public String getAccountingYears(Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();

        String apiKey = union.getUnionEconomics().getBookkeeping().getEconomicIntegration().getAPIKey();

        UnionEconomicAPI api = new UnionEconomicAPI(apiKey);

        return api.getAccountingYears();

    }

    @PostMapping("/bogføring/bogfør")
    @ResponseBody
    public String book(@RequestBody String json, Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();
        JSONObject jsonObject = new JSONObject(json);

        switch (union.getUnionEconomics().getBookkeeping().getChosenIntegration()) {
            case "Dinero": {
                DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");

                List<Invoice> invoiceList = getInvoicesBetweenDates(formatter.parseDateTime(jsonObject.getString("StartDate")), formatter.parseDateTime(jsonObject.getString("EndDate")), union.getUnionEconomics());

                for (int i = 0; i < jsonObject.getJSONArray("Teams").length(); i++) {
                    JSONObject teamObject = jsonObject.getJSONArray("Teams").getJSONObject(i);

                    sendLedgerBundleDinero(invoiceList, teamObject, jsonObject.getBoolean("WithFees"), union);
                }

                BookkeepingHistory bookkeepingHistory = new BookkeepingHistory();
                bookkeepingHistory.setBookkeeping(union.getUnionEconomics().getBookkeeping());
                bookkeepingHistory.setDate(DateFactory.getCurrentDate());
                bookkeepingHistory.setDateStart(formatter.parseDateTime(jsonObject.getString("StartDate")).toString("dd/MM/yyyy"));
                bookkeepingHistory.setDateEnd(formatter.parseDateTime(jsonObject.getString("EndDate")).toString("dd/MM/yyyy"));
                bookkeepingHistory.setWithFees(jsonObject.getBoolean("WithFees"));

                if (union.getUnionEconomics().getBookkeeping().getChosenIntegration().equals("Dinero")){
                    bookkeepingHistory.setIntegration("Dinero");
                } else if (union.getUnionEconomics().getBookkeeping().getChosenIntegration().equals("Economic")) {
                    bookkeepingHistory.setIntegration("Economic");
                }

                union.getUnionEconomics().getBookkeeping().addBookkeepingHistory(bookkeepingHistory);

                unionService.saveOrUpdate(union);

                return "OK";
            }
            case "Economic": {
                DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");

                JSONObject bookkeepingSuccess = new JSONObject();

                List<Invoice> invoiceList = getInvoicesBetweenDates(formatter.parseDateTime(jsonObject.getString("StartDate")), formatter.parseDateTime(jsonObject.getString("EndDate")), union.getUnionEconomics());

                for (int i = 0; i < jsonObject.getJSONArray("Teams").length(); i++) {
                    JSONObject teamObject = jsonObject.getJSONArray("Teams").getJSONObject(i);

                    String response = sendLedgerBundleEconomic(invoiceList, teamObject, jsonObject.getBoolean("WithFees"), jsonObject.getString("Year"), union);

                    bookkeepingSuccess = handleResponseEconomic(bookkeepingSuccess, teamObject, response);
                }

                if (ifShouldMakeBookkeepingHistory(bookkeepingSuccess)){
                    BookkeepingHistory bookkeepingHistory = new BookkeepingHistory();
                    bookkeepingHistory.setBookkeeping(union.getUnionEconomics().getBookkeeping());
                    bookkeepingHistory.setDate(DateFactory.getCurrentDate());
                    bookkeepingHistory.setDateStart(formatter.parseDateTime(jsonObject.getString("StartDate")).toString("dd/MM/yyyy"));
                    bookkeepingHistory.setDateEnd(formatter.parseDateTime(jsonObject.getString("EndDate")).toString("dd/MM/yyyy"));
                    bookkeepingHistory.setWithFees(jsonObject.getBoolean("WithFees"));

                    if (union.getUnionEconomics().getBookkeeping().getChosenIntegration().equals("Dinero")){
                        bookkeepingHistory.setIntegration("Dinero");
                    } else if (union.getUnionEconomics().getBookkeeping().getChosenIntegration().equals("Economic")) {
                        bookkeepingHistory.setIntegration("Economic");
                    }

                    union.getUnionEconomics().getBookkeeping().addBookkeepingHistory(bookkeepingHistory);

                    unionService.saveOrUpdate(union);
                }

                return bookkeepingSuccess.toString();
            }
            default:
                return "Error";
        }
    }

    private boolean ifShouldMakeBookkeepingHistory(JSONObject bookkeepingSuccess) {
        boolean anyErrorsNotEmpty = false;
        int emptyCount = 0;
        int teamCount = 0;

        Iterator<String> mainKeys = bookkeepingSuccess.keys();
        while(mainKeys.hasNext()){
            String mainKey = mainKeys.next();
            teamCount++;

            JSONObject teamObject = bookkeepingSuccess.getJSONObject(mainKey);

            if (teamObject.getString("Status").equals("error(s)")){
                if (!teamObject.getJSONObject("Errors").has("Empty")){
                    anyErrorsNotEmpty = true;
                } else {
                    emptyCount++;
                }
            }
        }

        if (anyErrorsNotEmpty){
            return false;
        } else {
            return emptyCount != teamCount;
        }
    }

    private JSONObject handleResponseEconomic(JSONObject bookkeepingSuccess, JSONObject teamObject, String response){
        // The object with the status
        JSONObject responseObject = new JSONObject();

        //starts with '[' if success, else if '{' was error
        if (response.startsWith("[")){
            responseObject.put("Status", "Success");
        } else if (response.startsWith("{")){

            JSONObject errorMessageObject = new JSONObject(response);
            responseObject.put("Status", "error(s)");

            JSONObject firstErrorObject = new JSONObject();
            boolean empty = false;
            if (errorMessageObject.getJSONArray("errors").getJSONObject(0).getJSONObject("entries").has("items")){
                firstErrorObject = errorMessageObject.getJSONArray("errors").getJSONObject(0).getJSONObject("entries").getJSONArray("items").getJSONObject(0);
            } else if(errorMessageObject.getJSONArray("errors").getJSONObject(0).getJSONObject("entries").getJSONArray("errors").getJSONObject(0).getString("errorMessage").equals("Cannot create voucher without entries.")) {
                empty = true;
            }

            JSONObject errorsObject = new JSONObject();

            if (firstErrorObject.has("Account")){
                errorsObject.put("Account", firstErrorObject.getJSONObject("Account").getJSONArray("errors").getJSONObject(0).getString("errorMessage"));
            }

            if (firstErrorObject.has("ContraAccount")){
                errorsObject.put("ContraAccount", firstErrorObject.getJSONObject("ContraAccount").getJSONArray("errors").getJSONObject(0).getString("errorMessage"));
            }

            if (firstErrorObject.has("Date")){
                errorsObject.put("ContraAccount", firstErrorObject.getJSONObject("Date").getJSONArray("errors").getJSONObject(0).getString("errorMessage"));
            }

            if (empty){
                errorsObject.put("Empty", true);
            }


            responseObject.put("Errors", errorsObject);
        }

        bookkeepingSuccess.put(teamObject.getString("TeamTitle") + "-" + teamObject.getInt("TeamID"), responseObject);

        return bookkeepingSuccess;
    }

    private String sendLedgerBundleDinero(List<Invoice> invoiceList, JSONObject teamObject, boolean withFees, Union union) {
        if (teamObject.getString("TeamTitle").equals("invoicesWithoutTeam")){
            String apiKey = union.getUnionEconomics().getBookkeeping().getDineroIntegration().getAPIKey();
            int firmId = Integer.parseInt(union.getUnionEconomics().getBookkeeping().getDineroIntegration().getOrganizationID());

            List<Invoice> invoicesToBook = new ArrayList<>();

            for (Invoice invoice : invoiceList){
                if (invoice.getAssosiatedTeam() == null){
                    invoicesToBook.add(invoice);
                }
            }

            UnionDineroAPI unionDineroAPI = new UnionDineroAPI(apiKey, firmId);
            return unionDineroAPI.addLedgerItem(invoicesToBook, teamObject.getInt("Account"), teamObject.getInt("Counter"), withFees);
        } else {
            Team team = teamService.findById(teamObject.getInt("TeamID"));
            String apiKey = team.getUnion().getUnionEconomics().getBookkeeping().getDineroIntegration().getAPIKey();
            int firmId = Integer.parseInt(team.getUnion().getUnionEconomics().getBookkeeping().getDineroIntegration().getOrganizationID());

            List<Invoice> invoicesToBook = new ArrayList<>();

            for (Invoice invoice : invoiceList){
                if (team == invoice.getAssosiatedTeam()){
                    invoicesToBook.add(invoice);
                }
            }

            UnionDineroAPI unionDineroAPI = new UnionDineroAPI(apiKey, firmId);
            return unionDineroAPI.addLedgerItem(invoicesToBook, teamObject.getInt("Account"), teamObject.getInt("Counter"), withFees);
        }
    }

    private String sendLedgerBundleEconomic(List<Invoice> invoiceList, JSONObject teamObject, boolean withFees, String year, Union union) {
        if (teamObject.get("TeamTitle").equals("invoicesWithoutTeam")){
            String apiKey = union.getUnionEconomics().getBookkeeping().getEconomicIntegration().getAPIKey();

            List<Invoice> invoicesToBook = new ArrayList<>();

            for (Invoice invoice : invoiceList){
                if (invoice.getAssosiatedTeam() == null){
                    invoicesToBook.add(invoice);
                }
            }

            UnionEconomicAPI unionEconomicAPI = new UnionEconomicAPI(apiKey);
            return unionEconomicAPI.addLedgerItem(invoicesToBook, teamObject.getInt("Account"), teamObject.getInt("Counter"), withFees, year);
        } else {
            Team team = teamService.findById(teamObject.getInt("TeamID"));
            String apiKey = team.getUnion().getUnionEconomics().getBookkeeping().getEconomicIntegration().getAPIKey();

            List<Invoice> invoicesToBook = new ArrayList<>();

            for (Invoice invoice : invoiceList){
                if (team == invoice.getAssosiatedTeam()){
                    invoicesToBook.add(invoice);
                }
            }

            UnionEconomicAPI unionEconomicAPI = new UnionEconomicAPI(apiKey);
            return unionEconomicAPI.addLedgerItem(invoicesToBook, teamObject.getInt("Account"), teamObject.getInt("Counter"), withFees, year);
        }
    }

    private List<Invoice> getInvoicesBetweenDates(DateTime startDate, DateTime endDate, UnionEconomics unionEconomics) {
        List<Invoice> invoices = new ArrayList<>();

        for (EconomicYear economicYear : unionEconomics.getEconomicYears()){
            for (Invoice invoice : economicYear.getInvoices()){
                DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
                DateTime invoiceDate = formatter.parseDateTime(invoice.getDate());
                if (invoiceDate.isAfter(startDate.minusDays(1)) && invoiceDate.isBefore(endDate.plusDays(1))){
                    invoices.add(invoice);
                }
            }
        }

        return invoices;
    }

}
