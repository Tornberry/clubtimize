package dk.clubtimize.clubtimize.controllers.panel.setup.courtbooking;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.models.opsætning.Court;
import dk.clubtimize.clubtimize.models.opsætning.Hall;
import dk.clubtimize.clubtimize.models.opsætning.booking.Booking;
import dk.clubtimize.clubtimize.models.opsætning.booking.SingleBookingPeriod;
import dk.clubtimize.clubtimize.services.*;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/panel/opsætning")
public class CourtBookingController {

    private UnionService unionService;
    private UserService userService;
    private HallService hallService;
    private CourtService courtService;
    private SingleBookingPeriodService singleBookingPeriodService;
    private BookingService bookingService;
    private SeasonBookingPeriodService seasonBookingPeriodService;
    private TimePeriodService timePeriodService;

    @Autowired
    public CourtBookingController(UnionService unionService, UserService userService, HallService hallService, CourtService courtService, SingleBookingPeriodService singleBookingPeriodService, BookingService bookingService, SeasonBookingPeriodService seasonBookingPeriodService, TimePeriodService timePeriodService) {
        this.unionService = unionService;
        this.userService = userService;
        this.hallService = hallService;
        this.courtService = courtService;
        this.singleBookingPeriodService = singleBookingPeriodService;
        this.bookingService = bookingService;
        this.seasonBookingPeriodService = seasonBookingPeriodService;
        this.timePeriodService = timePeriodService;
    }

    @GetMapping("/banebooking")
    public String showPage(){
        return "panel/setup/courtbooking/courtbooking";
    }
}
