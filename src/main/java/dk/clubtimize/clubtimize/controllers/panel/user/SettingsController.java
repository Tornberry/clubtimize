package dk.clubtimize.clubtimize.controllers.panel.user;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.models.user.FormerEmail;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.services.UserService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@RequestMapping("/panel")
public class SettingsController {

    private UserService userService;

    @Autowired
    public SettingsController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/indstillinger")
    public String getSettings(Model model, Principal principal){
        User user = userService.findByUsername(principal.getName());

        String day = user.getBirthday().split("-")[0];
        String month = user.getBirthday().split("-")[1];
        String year = user.getBirthday().split("-")[2];

        model.addAttribute("birthday", year + "-" + formatDateNumber(month) + "-" + formatDateNumber(day));
        return "panel/user/settings/settings";
    }

    private String formatDateNumber(String value) {
        if (value.length() == 1){
            return "0" + value;
        } else {
            return value;
        }
    }

    @PostMapping("/indstillinger")
    @ResponseBody
    public String editUserInformation(@RequestBody String json, Principal principal){
        User user = userService.findByUsername(principal.getName());
        if (user != null){
            JSONObject jsonObject = new JSONObject(json);

            if (!user.getEmail().equals(jsonObject.getString("Email"))){
                if (userService.existsByEmail(jsonObject.getString("Email"))){
                    return "email_in_use";
                } else {
                    userService.saveOrUpdate(updateUserSettings(user, jsonObject, true));
                    return "OK_new";
                }
            } else {
                userService.saveOrUpdate(updateUserSettings(user, jsonObject, false));
                return "OK";
            }
        } else {
            return "no_such_user";
        }
    }

    private User updateUserSettings(User user, JSONObject jsonObject, boolean newEmail) {
        if (newEmail){
            user.addFormerEmail(new FormerEmail(user, DateFactory.getCurrentDate(), user.getEmail()));
        }

        user.setFirstname(jsonObject.getString("Firstname"));
        user.setLastname(jsonObject.getString("Lastname"));
        user.setEmail(jsonObject.getString("Email"));
        user.setPhone(jsonObject.getInt("Phone"));
        user.setAddress(jsonObject.getString("Address"));

        String day = jsonObject.getString("Birthday").split("-")[2];
        String month = jsonObject.getString("Birthday").split("-")[1];
        String year = jsonObject.getString("Birthday").split("-")[0];

        user.setBirthday(formatDateNumber(day) + "-" + formatDateNumber(month) + "-" + year);
        user.setGender(jsonObject.getString("Gender"));

        return user;
    }

}
