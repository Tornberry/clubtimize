package dk.clubtimize.clubtimize.controllers.panel.setup.courtbooking;

import dk.clubtimize.clubtimize.controllers.support.mail.MailSender;
import dk.clubtimize.clubtimize.models.opsætning.Court;
import dk.clubtimize.clubtimize.models.opsætning.Hall;
import dk.clubtimize.clubtimize.models.opsætning.booking.Booking;
import dk.clubtimize.clubtimize.models.opsætning.booking.SeasonBookingPeriod;
import dk.clubtimize.clubtimize.models.opsætning.booking.SingleBookingPeriod;
import dk.clubtimize.clubtimize.models.opsætning.booking.TimePeriod;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.services.*;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Book;
import java.security.Principal;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/panel/opsætning")
public class SeasonCourtbookingSetupController {

    private UnionService unionService;
    private UserService userService;
    private HallService hallService;
    private CourtService courtService;
    private SingleBookingPeriodService singleBookingPeriodService;
    private BookingService bookingService;
    private SeasonBookingPeriodService seasonBookingPeriodService;
    private TimePeriodService timePeriodService;

    @Autowired
    public SeasonCourtbookingSetupController(UnionService unionService, UserService userService, HallService hallService, CourtService courtService, SingleBookingPeriodService singleBookingPeriodService, BookingService bookingService, SeasonBookingPeriodService seasonBookingPeriodService, TimePeriodService timePeriodService) {
        this.unionService = unionService;
        this.userService = userService;
        this.hallService = hallService;
        this.courtService = courtService;
        this.singleBookingPeriodService = singleBookingPeriodService;
        this.bookingService = bookingService;
        this.seasonBookingPeriodService = seasonBookingPeriodService;
        this.timePeriodService = timePeriodService;
    }

    @PostMapping("/banebooking/season")
    @ResponseBody
    public String makeSeasonBookingPeriod(@RequestBody String json){
        JSONObject jsonObject = new JSONObject(json);
        Hall hall = hallService.findByHallId(jsonObject.getInt("hallId"));

        DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");

        SeasonBookingPeriod seasonBookingPeriod = new SeasonBookingPeriod();
        seasonBookingPeriod.setHall(hall);
        seasonBookingPeriod.setSeasonStartDate(formatter.parseDateTime(jsonObject.getString("seasonDateStart")).toString("dd-MM-yyyy"));
        seasonBookingPeriod.setSeasonEndDate(formatter.parseDateTime(jsonObject.getString("seasonDateEnd")).toString("dd-MM-yyyy"));
        seasonBookingPeriod.setDayOfWeek(jsonObject.getString("dayOfWeek"));
        seasonBookingPeriod.setStartTime(jsonObject.getString("timeStart") + ":00");
        seasonBookingPeriod.setEndTime(jsonObject.getString("timeEnd") + ":00");
        seasonBookingPeriod.setBookingInterval(jsonObject.getString("interval"));
        seasonBookingPeriod.setPrice(jsonObject.getFloat("price"));
        seasonBookingPeriod.setReserveSingleBookings(jsonObject.getBoolean("reserve"));
        if (seasonBookingPeriod.isReserveSingleBookings()){
            seasonBookingPeriod.setSinglePrice(0);
        } else {
            seasonBookingPeriod.setSinglePrice(jsonObject.getFloat("singlePrice"));
        }

        seasonBookingPeriod.setTimePeriods(makeTimePeriods(hall, seasonBookingPeriod));

        seasonBookingPeriodService.saveOrUpdate(seasonBookingPeriod);

        return "";
    }

    @DeleteMapping("/banebooking/season")
    @ResponseBody
    public String deleteSeasonBookingPeriod(@RequestBody int id){
        SeasonBookingPeriod seasonBookingPeriod = seasonBookingPeriodService.findById(id);

        for (TimePeriod timePeriod : seasonBookingPeriod.getTimePeriods()){
            for (Booking booking : timePeriod.getBookings()){
                if (booking.getSingleBookingPeriod() != null){
                    booking.setTimePeriod(null);
                } else {
                    bookingService.deleteBooking(booking);
                }
            }
        }

        seasonBookingPeriodService.delete(seasonBookingPeriod);

        return "OK";
    }

    private List<TimePeriod> makeTimePeriods(Hall hall, SeasonBookingPeriod seasonBookingPeriod) {
        List<TimePeriod> timePeriodReturnList = new ArrayList<>();

        for (Court court : hall.getCourts()){
            List<TimePeriod> timePeriods = assembleTimePeriods(court, seasonBookingPeriod);

            for (TimePeriod timePeriod : timePeriods){
                timePeriod.setCourt(court);

                timePeriods.forEach(court::addTimePeriod);

                List<Booking> timePeriodBookings = getTimePeriodBookings(hall.getUnion(), timePeriod);
                List<Booking> newBookings = createNewBookings(timePeriod, timePeriodBookings);

                List<Booking> allPeriodBookings = new ArrayList<>();

                allPeriodBookings.addAll(timePeriodBookings);
                allPeriodBookings.addAll(newBookings);

                // Set its bookings if any exists
                timePeriod.setBookings(allPeriodBookings);

                seasonBookingPeriodService.saveOrUpdate(timePeriod.getSeasonBookingPeriod());

                timePeriodService.saveOrUpdate(timePeriod);

                for (Booking booking : newBookings){
                    bookingService.saveOrUpdate(booking);

                    court.addBookings(booking);
                    courtService.saveOrUpdate(court);
                }
            }

            timePeriodReturnList.addAll(timePeriods);

            courtService.saveOrUpdate(court);
        }

        return timePeriodReturnList;
    }

    private List<Booking> createNewBookings(TimePeriod timePeriod, List<Booking> timePeriodBookings) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");

        DateTime today = new DateTime();
        DateTime startDate = formatter.parseDateTime(timePeriod.getSeasonBookingPeriod().getSeasonStartDate());
        DateTime endDate = formatter.parseDateTime(timePeriod.getSeasonBookingPeriod().getSeasonEndDate());

        List<Booking> returnList = new ArrayList<>();

        while(startDate.isBefore(endDate)){
            if (timePeriod.getSeasonBookingPeriod().getDayOfWeek().equals(getDayFromNumber(startDate))){
                if (!bookingService.existsByDateAndStartHourAndEndHourAndCourt(startDate.toString("dd-MM-yyyy"), timePeriod.getTimeFrom(), timePeriod.getTimeTo(), timePeriod.getCourt())){
                    if (startDate.isAfter(today.minusDays(1))){
                        Booking booking = new Booking();

                        booking.setCourt(timePeriod.getCourt());

                        booking.setDate(startDate.toString("dd-MM-yyyy"));
                        booking.setBooked(false);
                        booking.setStartHour(timePeriod.getTimeFrom());
                        booking.setEndHour(timePeriod.getTimeTo());
                        booking.setPrice(0);
                        booking.setTimePeriod(timePeriod);

                        returnList.add(booking);
                    }
                }
            }

            startDate = startDate.plusDays(1);
        }

        return returnList;
    }

    private List<Booking> getTimePeriodBookings(Union union, TimePeriod timePeriod) {
        SeasonBookingPeriod seasonBookingPeriod = timePeriod.getSeasonBookingPeriod();

        List<Booking> bookingsToReturn = new ArrayList<>();

        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");

        DateTime seasonStartDate = formatter.parseDateTime(seasonBookingPeriod.getSeasonStartDate());
        DateTime seasonEndDate = formatter.parseDateTime(seasonBookingPeriod.getSeasonEndDate());

        Interval interval = new Interval(seasonStartDate, seasonEndDate);

        List<Booking> bookings = new ArrayList<>();

        for (Hall hall : union.getHalls()){
            for (SingleBookingPeriod singleBookingPeriod : hall.getSingleBookingPeriods()){
                bookings.addAll(singleBookingPeriod.getBookings());
            }
        }

        for (Booking booking : bookings){
            DateTime bookingDate = formatter.parseDateTime(booking.getDate());

            String day = getDayFromNumber(bookingDate);

            if (interval.contains(bookingDate)){
                if (day.equals(seasonBookingPeriod.getDayOfWeek())){
                    if (booking.getStartHour().equals(timePeriod.getTimeFrom()) && booking.getEndHour().equals(timePeriod.getTimeTo())){
                        if (timePeriod.getCourt() == booking.getCourt()){
                            bookingsToReturn.add(booking);

                            seasonBookingPeriodService.saveOrUpdate(seasonBookingPeriod);
                            timePeriodService.saveOrUpdate(timePeriod);

                            booking.setTimePeriod(timePeriod);
                            bookingService.saveOrUpdate(booking);
                        }
                    }
                }
            }
        }

        return bookingsToReturn;
    }

    private String getDayFromNumber(DateTime bookingDate) {
        switch (bookingDate.getDayOfWeek()) {
            case 1:
                return  "mandag";
            case 2:
                return "tirsdag";
            case 3:
                return "onsdag";
            case 4:
                return "torsdag";
            case 5:
                return "fredag";
            case 6:
                return "lørdag";
            case 7:
                return "søndag";
            default:
                return "error";
        }
    }

    private List<TimePeriod> assembleTimePeriods(Court court, SeasonBookingPeriod seasonBookingPeriod) {
        List<Booking> bookings = new ArrayList<>();

        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");

        DateTimeFormatter hourFormatter = DateTimeFormat.forPattern("HH:mm");

        Period period = new Period(hourFormatter.parseDateTime(seasonBookingPeriod.getStartTime()), hourFormatter.parseDateTime(seasonBookingPeriod.getEndTime()));

        List<TimePeriod> timePeriods = new ArrayList<>();

        for (int i = 0; i < period.getHours(); i++){
            TimePeriod timePeriod = new TimePeriod();
            timePeriod.setSeasonBookingPeriod(seasonBookingPeriod);
            timePeriod.setTimeFrom((Integer.parseInt(seasonBookingPeriod.getStartTime().split(":")[0]) + i) + ":00");
            timePeriod.setTimeTo((Integer.parseInt(seasonBookingPeriod.getStartTime().split(":")[0]) + (i+1)) + ":00");

            timePeriods.add(timePeriod);
        }

        return timePeriods;
    }

    @PostMapping("/banebooking/season/inviter")
    @ResponseBody
    public String inviteToPeriod(@RequestBody String json, Principal principal){
        JSONObject jsonObject = new JSONObject(json);
        TimePeriod timePeriod = timePeriodService.findById(jsonObject.getInt("TimePeriodId"));

        timePeriod.setActiveInvitation(true);
        timePeriod.setReservedByInvited(jsonObject.getBoolean("Reserved"));
        timePeriod.setInviteEmail(jsonObject.getString("Email"));
        timePeriod.setInviteName(jsonObject.getString("Name"));

        timePeriodService.saveOrUpdate(timePeriod);

        MailSender.invitePeriodBooking(timePeriod);

        return "OK";
    }

    @DeleteMapping("/banebooking/season/inviter")
    @ResponseBody
    public String cancelInvite(@RequestBody int id){
        TimePeriod timePeriod = timePeriodService.findById(id);

        timePeriod.setActiveInvitation(false);
        timePeriod.setReservedByInvited(false);
        timePeriod.setInviteEmail(null);
        timePeriod.setInviteName(null);

        timePeriodService.saveOrUpdate(timePeriod);

        return "OK";
    }


}
