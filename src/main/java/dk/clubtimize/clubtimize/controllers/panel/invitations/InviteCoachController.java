package dk.clubtimize.clubtimize.controllers.panel.invitations;

import dk.clubtimize.clubtimize.controllers.support.PermissionEnum;
import dk.clubtimize.clubtimize.controllers.support.RoleSupport;
import dk.clubtimize.clubtimize.controllers.support.mail.MailSender;
import dk.clubtimize.clubtimize.models.union.RecruitmentInvitation;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.services.RecruitmentInvitationService;
import dk.clubtimize.clubtimize.services.TeamService;
import dk.clubtimize.clubtimize.services.UnionService;
import dk.clubtimize.clubtimize.services.UserService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


@Controller
@RequestMapping("/panel/invitation")
public class InviteCoachController {

    private UserService userService;
    private TeamService teamService;
    private UnionService unionService;
    private RecruitmentInvitationService recruitmentInvitationService;

    @Autowired
    public InviteCoachController(UserService userService, TeamService teamService, UnionService unionService, RecruitmentInvitationService recruitmentInvitationService) {
        this.userService = userService;
        this.teamService = teamService;
        this.unionService = unionService;
        this.recruitmentInvitationService = recruitmentInvitationService;
    }


    @GetMapping("/træner")
    public String showInvitationsPage(Model model, Principal principal){
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            model.addAttribute("invitations", getCoachInvitations(userService.findByUsername(principal.getName()).getUnion().getRecruitmentInvitations()));
            return "panel/invitation/invite/coach/inviteCoach";
        } else {
            return "error/404.html";
        }
    }

    private List<RecruitmentInvitation> getCoachInvitations(List<RecruitmentInvitation> recruitmentInvitations) {
        List<RecruitmentInvitation> coachInvitations = new ArrayList<>();

        for (RecruitmentInvitation recruitmentInvitation : recruitmentInvitations){
            if (recruitmentInvitation.getType().equals("coach")){
                coachInvitations.add(recruitmentInvitation);
            }
        }

        return coachInvitations;
    }

    @PostMapping("/træner")
    public ResponseEntity postNewCoach(@RequestBody String json, Principal principal){

        Union union = userService.findByUsername(principal.getName()).getUnion();

        JSONObject jsonObject = new JSONObject(json);

        RecruitmentInvitation recruitmentInvitation = new RecruitmentInvitation(
                union,
                "coach",
                getRandomInt(),
                jsonObject.getString("email"),
                teamService.findById(jsonObject.getInt("team")),
                jsonObject.getFloat("wage"),
                "Afventer");

        union.addRecruitmentInvitation(recruitmentInvitation);

        MailSender.inviteCoach(recruitmentInvitation);

        unionService.saveOrUpdate(union);

        return new ResponseEntity(HttpStatus.OK);
    }

    private int getRandomInt() {
        return new Random().nextInt(10000000);
    }

    @DeleteMapping("/træner")
    public ResponseEntity deleteNewCoach(@RequestBody int invNumber, Principal principal){
        Union union = userService.findByUsername(principal.getName()).getUnion();

        RecruitmentInvitation recruitmentInvitation = recruitmentInvitationService.findByInvNumber(invNumber);

        union.removeRecruitmentInvitation(recruitmentInvitation);

        recruitmentInvitationService.deleteRecruitmentInvitation(recruitmentInvitation);

        return new ResponseEntity(HttpStatus.OK);
    }

}
