package dk.clubtimize.clubtimize.controllers.panel.setup;

import dk.clubtimize.clubtimize.controllers.support.OwnershipValidator;
import dk.clubtimize.clubtimize.controllers.support.PermissionEnum;
import dk.clubtimize.clubtimize.controllers.support.RoleSupport;
import dk.clubtimize.clubtimize.models.opsætning.TeamDepartment;
import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.adapters.CategoryAdapter;
import dk.clubtimize.clubtimize.services.TeamCategoryService;
import dk.clubtimize.clubtimize.services.TeamService;
import dk.clubtimize.clubtimize.services.UnionService;
import dk.clubtimize.clubtimize.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.security.Principal;

@Controller
@RequestMapping("/panel/opsætning")
public class DepartmentSetupController {

    private TeamCategoryService teamCategoryService;
    private UserService userService;
    private UnionService unionService;
    private TeamService teamService;

    @Autowired
    public DepartmentSetupController(TeamCategoryService teamCategoryService, UserService userService, UnionService unionService, TeamService teamService) {
        this.teamCategoryService = teamCategoryService;
        this.userService = userService;
        this.unionService = unionService;
        this.teamService = teamService;
    }

    @GetMapping("/afdelinger")
    public String showHold(Model model, Principal principal){
        if (RoleSupport.checkPermission(PermissionEnum.canSeeAdministrationPages, userService.findByUsername(principal.getName())) || RoleSupport.checkPermission(PermissionEnum.canAdministrate, userService.findByUsername(principal.getName()))){
            return "panel/setup/departments/departments";
        } else {
            return "error/404.html";
        }
    }

    @PostMapping("/afdelinger")
    public ResponseEntity postNewCategory(@RequestBody CategoryAdapter categoryAdapter, BindingResult bindingResult, Principal principal){
        if (categoryAdapter.getId() == 0){
            int[] rgbSplit = splitRGB(categoryAdapter.getColor());
            Color normal = new Color(rgbSplit[0], rgbSplit[1], rgbSplit[2]);
            Color opacity = bleach(normal);

            TeamDepartment teamDepartment = new TeamDepartment();
            teamDepartment.setTitle(categoryAdapter.getTitle());
            teamDepartment.setColor("rgb(" + opacity.getRed() + "," + opacity.getGreen() + "," + opacity.getBlue() + ")");
            teamDepartment.setTextColor("rgb(" + normal.getRed() + "," + normal.getGreen() + "," + normal.getBlue() + ")");
            Union union = userService.findByUsername(principal.getName()).getUnion();
            union.addTeamDepartment(teamDepartment);
            teamDepartment.setUnion(union);

            unionService.saveOrUpdate(union);
        } else {
            int[] rgbSplit = splitRGB(categoryAdapter.getColor());
            Color normal = new Color(rgbSplit[0], rgbSplit[1], rgbSplit[2]);
            Color opacity = bleach(normal);

            TeamDepartment teamDepartment = teamCategoryService.findById(categoryAdapter.getId());
            teamDepartment.setTitle(categoryAdapter.getTitle());
            teamDepartment.setTextColor("rgb(" + normal.getRed() + "," + normal.getGreen() + "," + normal.getBlue() + ")");
            teamDepartment.setColor("rgb(" + opacity.getRed() + "," + opacity.getGreen() + "," + opacity.getBlue() + ")");

            teamCategoryService.saveOrUpdate(teamDepartment);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    private int[] splitRGB(String color) {

        String red = color.split("\\(")[1].split("\\)")[0].split(",")[0];
        String green = color.split("\\(")[1].split("\\)")[0].split(",")[1];
        String blue = color.split("\\(")[1].split("\\)")[0].split(",")[2];

        return new int[]{Integer.parseInt(red), Integer.parseInt(green), Integer.parseInt(blue)};
    }

    private Color bleach(Color color) {
        int red = (int) ((color.getRed() * (1 - (float)0.7) / 255 + (float)0.7) * 255);
        int green = (int) ((color.getGreen() * (1 - (float)0.7) / 255 + (float)0.7) * 255);
        int blue = (int) ((color.getBlue() * (1 - (float)0.7) / 255 + (float)0.7) * 255);
        return new Color(red, green, blue);
    }

    private String toHex(Color color){
        return String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue());
    }

    @DeleteMapping("/afdelinger/sletafdeling")
    public ResponseEntity deleteCategory(@RequestBody int id, Principal principal){
        TeamDepartment teamDepartment = teamCategoryService.findById(id);
        if (OwnershipValidator.validateOwnership(teamDepartment, userService.findByUsername(principal.getName()))){
            for (Team team : teamDepartment.getTeams()){
                teamService.deleteTeam(team);
            }
            teamCategoryService.deleteTeamCategory(teamDepartment);
        }

        return new ResponseEntity(HttpStatus.OK);
    }
}
