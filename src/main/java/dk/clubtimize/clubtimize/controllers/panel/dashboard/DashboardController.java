package dk.clubtimize.clubtimize.controllers.panel.dashboard;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.models.opsætning.TeamDepartment;
import dk.clubtimize.clubtimize.models.training.Training;
import dk.clubtimize.clubtimize.models.union.*;
import dk.clubtimize.clubtimize.models.user.UnionAssosiation;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;
import dk.clubtimize.clubtimize.services.UnionSeasonService;
import dk.clubtimize.clubtimize.services.UnionService;
import dk.clubtimize.clubtimize.services.UserSeasonService;
import dk.clubtimize.clubtimize.services.UserService;
import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

@Controller
@RequestMapping("/panel")
public class DashboardController {

    private UnionService unionService;
    private UserService userService;
    private UnionSeasonService unionSeasonService;
    private UserSeasonService userSeasonService;

    @Autowired
    public DashboardController(UnionService unionService, UserService userService, UnionSeasonService unionSeasonService, UserSeasonService userSeasonService) {
        this.unionService = unionService;
        this.userService = userService;
        this.unionSeasonService = unionSeasonService;
        this.userSeasonService = userSeasonService;
    }

    @GetMapping("")
    public String showDashBoard(@RequestParam(value = "perspektiv", defaultValue = "None", required = false) String perspective,  Principal principal, Model model){

        DateTime dateTime = new DateTime();
        User user = userService.findByUsername(principal.getName());

        String chosenPerspective = getChosenPerspective(perspective, user);

        model.addAttribute("chosenPerspective", chosenPerspective);
        model.addAttribute("perspectives", getPerspectives(user));

        switch (chosenPerspective) {
            case "Spiller":
                if (isPlayer(user)) {
                    model.addAttribute("curentUserSeason", getCurrentUserSeason(principal.getName()));
                    model.addAttribute("seasonstring", dateTime.getYear() + "/" + (dateTime.getYear() + 1));
                    model.addAttribute("nextTraining", getNextTraining(user));
                }
                break;
            case "Administrator":
                if (isAdministrator(user)) {
                    model.addAttribute("missingPayments", getMissingPayments(user.getUnion()));
                    model.addAttribute("confirmedPayments", getConfirmedPayments(user.getUnion()));
                    model.addAttribute("seasonmeter", getSeasonMeterData(user.getUnion().getSeasonSwitchMonth())[0]);
                    model.addAttribute("seasonmeterlabel", (int)getSeasonMeterData(user.getUnion().getSeasonSwitchMonth())[1]);
                    model.addAttribute("signedupprdepartment", getSignedUpPrDepartment(user.getUnion()));
                }
                break;
            case "Træner":
                if (isCoach(user)) {
                    model.addAttribute("myTeams", getMyTeams(user));
                    model.addAttribute("nextTraining", getNextTrainingCoach(user));
                }
                break;
        }

        return "panel/dashboard/dashboard";
    }

    private List<Object[]> getSignedUpPrDepartment(Union union) {
        List<Object[]> departmentsData = new ArrayList<>();

        for (TeamDepartment department : union.getDepartments()){
            int signedUp = 0;
            for (Team team : department.getTeams()){
                signedUp += team.getPlayers().size();
            }
            departmentsData.add(new Object[]{department.getTitle(), signedUp});
        }

        return departmentsData;
    }

    private float[] getSeasonMeterData(String seasonSwitchMonth) {

        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd-MM-yyyy");
        DateTime dateTime = DateTime.parse("01-" + getMonth(seasonSwitchMonth) + "-" + (DateFactory.getCurrentYear()), dateTimeFormatter);

        int months = Months.monthsBetween(dateTime, DateTime.now()).getMonths();

        if (months > 0){
            return new float[]{((float)months/(float)12)*100, (float)12-months};
        } else {
            return new float[]{((float)(months+12)/(float)12)*100, (float)12-(months+12)};
        }
    }

    private String getMonth(String seasonSwitchMonth) {
        switch (seasonSwitchMonth){
            case "Januar":
                return "01";
            case "Febuar":
                return "02";
            case "Marts":
                return "03";
            case "April":
                return "04";
            case "Maj":
                return "05";
            case "Juni":
                return "06";
            case "Juli":
                return "07";
            case "August":
                return "08";
            case "September":
                return "09";
            case "Oktober":
                return "10";
            case "November":
                return "11";
            case "December":
                return "12";
            default:
                return "invalid";
        }
    }


    private Training getNextTrainingCoach(User user) {
        DateTime today = DateTime.now();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");

        boolean first = true;
        Training nextTraining = null;
        for (Coach coach : user.getUnionAssosiation().getCoachPositions()){
            for (Team team : coach.getTeams()){
                for (Training training : team.getTrainings()){
                    if (formatter.parseDateTime(training.getDate()).isAfter(today.minusDays(1))){
                        if (first){
                            nextTraining = training;
                            first = false;
                        } else if (formatter.parseDateTime(nextTraining.getDate()).isAfter(formatter.parseDateTime(training.getDate()))){
                            nextTraining = training;
                        }
                    }
                }
            }
        }
        return nextTraining;
    }

    private List<Team> getMyTeams(User user) {
        List<Team> teams = new ArrayList<>();

        for (Coach coach : user.getUnionAssosiation().getCoachPositions()){
            teams.addAll(coach.getTeams());
        }

        return teams;
    }

    private List<TeamSeason> getConfirmedPayments(Union union) {
        List<TeamSeason> confirmedPayments = new ArrayList<>();

        for (UnionAssosiation unionAssosiation : union.getUnionAssosiations()){
            for (UserSeason userSeason : unionAssosiation.getUser().getUserSeasons()){
                for (TeamSeason teamSeason : userSeason.getTeamSeasons()){
                    if (teamSeason.isPaymentSuccessful()){
                        confirmedPayments.add(teamSeason);
                    }
                }
            }
        }

        return confirmedPayments;
    }

    private List<TeamSeason> getMissingPayments(Union union) {
        List<TeamSeason> missingPayments = new ArrayList<>();

        for (UnionAssosiation unionAssosiation : union.getUnionAssosiations()){
            for (UserSeason userSeason : unionAssosiation.getUser().getUserSeasons()){
                for (TeamSeason teamSeason : userSeason.getTeamSeasons()){
                    if (!teamSeason.isPaymentSuccessful()){
                       missingPayments.add(teamSeason);
                    }
                }
            }
        }

        return missingPayments;
    }

    private Training getNextTraining(User user) {
        boolean first = true;
        long nextDayCompare = 0;
        Training nextTraining = null;

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

        for (Player player : user.getUnionAssosiation().getPlayerPositions()){
            for (Training training : player.getTeam().getTrainings()){
                if (first){
                    try{
                        Date date = df.parse(training.getDate());
                        nextDayCompare = date.getTime();
                        nextTraining = training;
                        first = false;
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                } else {
                    try{
                        Date date = df.parse(training.getDate());
                        if (date.getTime() < nextDayCompare){
                            nextTraining = training;
                        }
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                }
            }
        }
        return nextTraining;
    }

    private boolean isPlayer(User user) {
        return !user.getUnionAssosiation().getPlayerPositions().isEmpty();
    }
    private boolean isCoach(User user) {
        return !user.getUnionAssosiation().getCoachPositions().isEmpty();
    }

    private boolean isAdministrator(User user) {
        return user.isUnionAdministrator();
    }

    private String getChosenPerspective(String perspective, User user) {
        switch (perspective) {
            case "Spiller":
                if (isPlayer(user)){
                    return "Spiller";
                } else {
                    return "None";
                }
            case "Træner":
                if (isCoach(user)){
                    return "Træner";
                } else {
                    return "None";
                }
            case "Administrator":
                if (isAdministrator(user)){
                    return "Administrator";
                } else {
                    return "None";
                }
            default:
                List<String> perspectives = getPerspectives(user);
                if (perspectives.size() > 0) {
                    return perspectives.get(0);
                } else {
                    return "None";
                }
        }
    }

    private List<String> getPerspectives(User user) {
        List<String> perspectives = new ArrayList<>();

        if (!user.getUnionAssosiation().getPlayerPositions().isEmpty()){
            perspectives.add("Spiller");
        }

        if (!user.getUnionAssosiation().getCoachPositions().isEmpty()){
            perspectives.add("Træner");
        }

        if (user.isUnionAdministrator()){
            perspectives.add("Administrator");
        }

        return perspectives;
    }

    private UserSeason getCurrentUserSeason(String principal) {
        DateTime dateTime = new DateTime();
        UnionSeason unionSeason = unionSeasonService.findByUnionAndSeasonYear(userService.findByUsername(principal).getUnion(), dateTime.getYear() + "/" + (dateTime.getYear() + 1));

        return userSeasonService.findByUnionSeasonAndUser(unionSeason, userService.findByUsername(principal));
    }

    private String getCurrentSeason(){
        DateTime dateTime = new DateTime();
        return dateTime.getYear() + "/" + (dateTime.getYear() + 1);
    }

}
