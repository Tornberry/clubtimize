package dk.clubtimize.clubtimize.controllers;

import dk.clubtimize.clubtimize.Global;
import dk.clubtimize.clubtimize.controllers.support.*;
import dk.clubtimize.clubtimize.controllers.support.mail.MailSender;
import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.opsætning.booking.Booking;
import dk.clubtimize.clubtimize.models.opsætning.booking.SeasonBookingPeriod;
import dk.clubtimize.clubtimize.models.opsætning.Court;
import dk.clubtimize.clubtimize.models.opsætning.Hall;
import dk.clubtimize.clubtimize.models.opsætning.booking.TimePeriod;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.event.Event;
import dk.clubtimize.clubtimize.models.union.event.EventInvitation;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import dk.clubtimize.clubtimize.services.*;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ClubController {

    private UnionService unionService;
    private UserSeasonService userSeasonService;
    private TeamSeasonService teamSeasonService;
    private EconomicYearService economicYearService;
    private InvoiceService invoiceService;
    private UnionEconomicsService unionEconomicsService;
    private BookingService bookingService;
    private HallService hallService;
    private SingleBookingPeriodService singleBookingPeriodService;
    private TimePeriodService timePeriodService;
    private EventInvitationService eventInvitationService;
    private EventService eventService;
    private UserService userService;
    private UnionSeasonStringService unionSeasonStringService;

    @Autowired
    public ClubController(UnionService unionService, UserSeasonService userSeasonService, TeamSeasonService teamSeasonService, EconomicYearService economicYearService, InvoiceService invoiceService, UnionEconomicsService unionEconomicsService, BookingService bookingService, HallService hallService, SingleBookingPeriodService singleBookingPeriodService, TimePeriodService timePeriodService, EventInvitationService eventInvitationService, EventService eventService, UserService userService, UnionSeasonStringService unionSeasonStringService) {
        this.unionService = unionService;
        this.userSeasonService = userSeasonService;
        this.teamSeasonService = teamSeasonService;
        this.economicYearService = economicYearService;
        this.invoiceService = invoiceService;
        this.unionEconomicsService = unionEconomicsService;
        this.bookingService = bookingService;
        this.hallService = hallService;
        this.singleBookingPeriodService = singleBookingPeriodService;
        this.timePeriodService = timePeriodService;
        this.eventInvitationService = eventInvitationService;
        this.eventService = eventService;
        this.userService = userService;
        this.unionSeasonStringService = unionSeasonStringService;
    }

    @GetMapping("/{unionUrl}")
    public Object showHome(@PathVariable String unionUrl, Model model){
        Union union = unionService.findByUrl("/" + unionUrl);

        // If you try to enter url for a non-exsisting union, redirect to homepage
        if (union != null){
            model.addAttribute("version", new Global(null, null).getVersion());
            model.addAttribute("union", union);
            return "clubpage/clubpage";
        } else {
            return new RedirectView("/");
        }
    }

    @GetMapping("/{unionUrl}/banebooking")
    public String showCourtbookingPage(@PathVariable String unionUrl, Model model){
        Union union = unionService.findByUrl("/" + unionUrl);

        model.addAttribute("union", union);

        return "clubpage/courtbooking/single/courtbooking";
    }

    @GetMapping("/{unionUrl}/banebooking/periode")
    public String showPeriodBooking(@PathVariable String unionUrl, @RequestParam("hall") int hallId, Model model){
        Union union = unionService.findByUrl("/" + unionUrl);
        Hall hall = hallService.findByHallId(hallId);

        model.addAttribute("union", union);
        model.addAttribute("hall", hall);
        model.addAttribute("timePeriods", getTimePeriodsForHall(hall));

        return "clubpage/courtbooking/season/courtbooking_period";
    }

    @PostMapping("/{unionUrl}/banebooking/periode")
    @ResponseBody
    public String bookPeriod(@RequestBody String json, @PathVariable String unionUrl){
        Union union = unionService.findByUrl("/" + unionUrl);

        JSONObject jsonObject = new JSONObject(json);
        TimePeriod timePeriod = timePeriodService.findById(jsonObject.getInt("timePeriodId"));

        timePeriod.setRecipientName(jsonObject.getString("recipientName"));
        timePeriod.setRecipientEmail(jsonObject.getString("recipientEmail"));
        timePeriod.setRecipientPhone(jsonObject.getString("recipientPhone"));
        timePeriod.setRecipientAddress(jsonObject.getString("recipientAddress"));

        return new PaymentGatewaySupport(null, null).getPaymentGatewayForSeasonCourtBooking(timePeriod.getSeasonBookingPeriod().getPrice() + EconomicSupport.calculateFeesAddedUp(timePeriod.getSeasonBookingPeriod().getPrice(), timePeriod.getCourt().getHall().getUnion().getUnionEconomics().getFeePercent()), timePeriod);
    }

    private List<Object[]> getTimePeriodsForHall(Hall hall) {
        List<TimePeriod> timePeriods = new ArrayList<>();

        for(SeasonBookingPeriod seasonBookingPeriod : hall.getSeasonBookingPeriods()){
            timePeriods.addAll(seasonBookingPeriod.getTimePeriods());
        }

        List<Object[]> returnList = new ArrayList<>();

        boolean isPayingFees = hall.getAssosiatedUnion().getUnionEconomics().isPayingFees();
        for (TimePeriod timePeriod : timePeriods){
            if (isPayingFees){
                returnList.add(new Object[]{timePeriod, timePeriod.getSeasonBookingPeriod().getPrice()});
            } else {
                float priceWithFees = timePeriod.getSeasonBookingPeriod().getPrice() + EconomicSupport.calculateFeesAddedUp(timePeriod.getSeasonBookingPeriod().getPrice(), hall.getUnion().getUnionEconomics().getFeePercent());

                returnList.add(new Object[]{timePeriod, priceWithFees});
            }
        }

        return returnList;
    }

    @GetMapping("/{unionUrl}/banebooking/fetch")
    @ResponseBody
    public String showCourtBooking(@PathVariable String unionUrl){
        Union union = unionService.findByUrl("/" + unionUrl);

        List<Booking> bookings = new ArrayList<>();

        for (Hall hall : union.getHalls()){
            for (Court court : hall.getCourts()){
                bookings.addAll(court.getBookings());
            }
        }

        return CourtBookingFetcher.makeJsonObject(bookings, union.getHalls());
    }

    @PostMapping("/{unionUrl}/banebooking")
    @ResponseBody
    public String book(@RequestBody String json, @PathVariable String unionUrl){
        Union union = unionService.findByUrl("/" + unionUrl);

        JSONObject jsonObject = new JSONObject(json);
        Booking booking = bookingService.findById(jsonObject.getInt("bookingId"));

        booking.setRecipientName(jsonObject.getString("recipientName"));
        booking.setRecipientEmail(jsonObject.getString("recipientEmail"));
        booking.setRecipientPhone(jsonObject.getString("recipientPhone"));
        booking.setRecipientAddress(jsonObject.getString("recipientAddress"));

        return new PaymentGatewaySupport(null, null).getPaymentGatewayForSingleCourtBooking(booking.getPrice() + EconomicSupport.calculateFeesAddedUp(booking.getPrice(), booking.getCourt().getHall().getUnion().getUnionEconomics().getFeePercent()), booking);
    }

    /**
     *
     * @param paymentCode
     * @param price If this is "-1", means it was not a discounted team that was payed for. Otherwise it was a discounted team
     * @param model
     * @return
     */
    @GetMapping("/paymentsuccessfull/contingent")
    public String showPaymentSuccessContingent(@RequestParam(value = "c") String paymentCode, @RequestParam(value = "price", defaultValue = "-1") float price, Model model){
        TeamSeason teamSeason = teamSeasonService.findByPaymentCode(paymentCode);

        if (teamSeasonService.existsByPaymentCode(paymentCode) && !teamSeason.isPaymentSuccessful()) {
            // If not a discounted team
            if (price == -1) {
                // Set teamSeason payed ------------------------------------------------------------------------------------
                teamSeason.setPaymentSuccessful(true);
                teamSeasonService.saveOrUpdate(teamSeason);

                // Do everything regarding economicYear --------------------------------------------------------------------
                DateTime dateTime = new DateTime();
                EconomicYear economicYear = economicYearService.findByUnionEconomicsAndYear(teamSeason.getAssosiatedUnion().getUnionEconomics(), DateFactory.getCurrentSeasonYear());

                Invoice invoice = EconomicSupport.makeContingentInvoice(teamSeason, economicYear, invoiceService.getNextInvoiceNumber(economicYear.getAssosiatedUnion().getName()) + "-" + DateFactory.getCurrentSeasonYear());

                teamSeason.setInvoice(invoice);

                model.addAttribute("invoice", invoice);

                economicYear.addInvoice(invoice);

                invoiceService.saveOrUpdate(invoice);
                economicYearService.saveOrUpdate(economicYear);
                teamSeasonService.saveOrUpdate(teamSeason);

                // Do everything regarding unionEconomics --------------------------------------------------------
                UnionEconomics unionEconomics = economicYear.getUnionEconomics();
                unionEconomics.updateBalance();

                unionEconomicsService.saveOrUpdate(unionEconomics);

                MailSender.sendInvoice(invoice, invoice.getUser().getEmail(), invoice.getUser().getFirstname() + " " + invoice.getUser().getLastname(), invoice.getUser().getAddress());

                model.addAttribute("union", unionService.findByUrl(teamSeason.getAssosiatedUnion().getUrl()));
                return "clubpage/paymentsuccessfull";

                // If it was a discounted team
            } else {
                // Set teamSeason payed ------------------------------------------------------------------------------------
                teamSeason.setPaymentSuccessful(true);
                teamSeasonService.saveOrUpdate(teamSeason);

                // Do everything regarding economicYear --------------------------------------------------------------------
                EconomicYear economicYear = economicYearService.findByUnionEconomicsAndYear(teamSeason.getAssosiatedUnion().getUnionEconomics(), DateFactory.getCurrentSeasonYear());

                Invoice invoice = EconomicSupport.makeContingentInvoiceWithDiscount(teamSeason, economicYear, invoiceService.getNextInvoiceNumber(economicYear.getAssosiatedUnion().getName()) + "-" + DateFactory.getCurrentSeasonYear(), price);

                teamSeason.setInvoice(invoice);

                model.addAttribute("invoice", invoice);

                economicYear.addInvoice(invoice);

                invoiceService.saveOrUpdate(invoice);
                economicYearService.saveOrUpdate(economicYear);
                teamSeasonService.saveOrUpdate(teamSeason);

                // Do everything regarding unionEconomics --------------------------------------------------------
                UnionEconomics unionEconomics = economicYear.getUnionEconomics();
                unionEconomics.updateBalance();

                unionEconomicsService.saveOrUpdate(unionEconomics);

                MailSender.sendInvoice(invoice, invoice.getUser().getEmail(), invoice.getUser().getFirstname() + " " + invoice.getUser().getLastname(), invoice.getUser().getAddress());

                model.addAttribute("union", unionService.findByUrl(teamSeason.getAssosiatedUnion().getUrl()));
                return "clubpage/paymentsuccessfull";
            }
        } else {
            return "errorOutsidePanel/404";
        }
    }

    @GetMapping("/paymentsuccessfull/courtbooking_single")
    public String showPaymentSuccessSingleCourtBooking(@RequestParam("recipientName") String recipientName, @RequestParam("recipientEmail") String recipientEmail, @RequestParam("recipientAddress") String recipientAddress, @RequestParam("recipientPhone") String recipientPhone, @RequestParam(value = "bookingId") int bookingId, Model model){
        Booking booking = bookingService.findById(bookingId);
        if (booking != null && !booking.isBooked()){
            booking.setRecipientName(recipientName);
            booking.setRecipientEmail(recipientEmail);
            booking.setRecipientAddress(recipientAddress);
            booking.setRecipientPhone(recipientPhone);
            booking.setBooked(true);
            booking.setBookingTime(DateFactory.getCurrentDateAndTime());

            bookingService.saveOrUpdate(booking);

            EconomicYear economicYear = economicYearService.findByUnionEconomicsAndYear(booking.getCourt().getHall().getUnion().getUnionEconomics(), unionSeasonStringService.getSeasonStringByUnionSeason(booking.getCourt().getHall().getUnion()));

            Invoice invoice = EconomicSupport.makeSingleCourtBookingInvoice(booking, economicYear, invoiceService.getNextInvoiceNumber(economicYear.getAssosiatedUnion().getName()) + "-" + DateFactory.getCurrentSeasonYear());

            booking.setInvoice(invoice);

            model.addAttribute("invoice", invoice);

            economicYear.addInvoice(invoice);

            invoiceService.saveOrUpdate(invoice);
            bookingService.saveOrUpdate(booking);
            economicYearService.saveOrUpdate(economicYear);

            // Do everything regarding unionEconomics --------------------------------------------------------
            UnionEconomics unionEconomics = economicYear.getUnionEconomics();
            unionEconomics.updateBalance();

            unionEconomicsService.saveOrUpdate(unionEconomics);

            MailSender.sendInvoice(invoice, booking.getRecipientEmail(), booking.getRecipientName(), booking.getRecipientAddress());

            model.addAttribute("union", booking.getCourt().getHall().getUnion().getUrl());
            return "clubpage/paymentsuccessfull";
        } else {
            return "errorOutsidePanel/404";
        }
    }

    @GetMapping("/paymentsuccessfull/courtbooking_period")
    public String showPaymentSuccessSeasonCourtBooking(@RequestParam("recipientName") String recipientName, @RequestParam("recipientEmail") String recipientEmail, @RequestParam("recipientAddress") String recipientAddress, @RequestParam("recipientPhone") String recipientPhone, @RequestParam(value = "timePeriodId") int timePeriodId, Model model){
        TimePeriod timePeriod = timePeriodService.findById(timePeriodId);
        if (timePeriod != null && !timePeriod.isBooked()){
            timePeriod.setRecipientName(recipientName);
            timePeriod.setRecipientEmail(recipientEmail);
            timePeriod.setRecipientAddress(recipientAddress);
            timePeriod.setRecipientPhone(recipientPhone);
            timePeriod.setBooked(true);
            timePeriod.setBookingTime(DateFactory.getCurrentDateAndTime());

            timePeriodService.saveOrUpdate(timePeriod);

            BookTimePeriodBookings(timePeriod);

            EconomicYear economicYear = economicYearService.findByUnionEconomicsAndYear(timePeriod.getCourt().getHall().getUnion().getUnionEconomics(), unionSeasonStringService.getSeasonStringByUnionSeason(timePeriod.getCourt().getHall().getUnion()));

            Invoice invoice = EconomicSupport.makeSeasonCourtBookingInvoice(timePeriod, economicYear, invoiceService.getNextInvoiceNumber(economicYear.getAssosiatedUnion().getName()) + "-" + DateFactory.getCurrentSeasonYear());

            timePeriod.setInvoice(invoice);

            model.addAttribute("invoice", invoice);

            economicYear.addInvoice(invoice);

            invoiceService.saveOrUpdate(invoice);
            timePeriodService.saveOrUpdate(timePeriod);
            economicYearService.saveOrUpdate(economicYear);

            // Do everything regarding unionEconomics --------------------------------------------------------
            UnionEconomics unionEconomics = economicYear.getUnionEconomics();
            unionEconomics.updateBalance();

            unionEconomicsService.saveOrUpdate(unionEconomics);

            MailSender.sendInvoice(invoice, timePeriod.getRecipientEmail(), timePeriod.getRecipientName(), timePeriod.getRecipientAddress());

            model.addAttribute("union", timePeriod.getCourt().getHall().getUnion().getUrl());
            return "clubpage/paymentsuccessfull";
        } else {
            return "errorOutsidePanel/404";
        }
    }

    private void BookTimePeriodBookings(TimePeriod timePeriod) {
        for (Booking booking : timePeriod.getBookings()){
            booking.setBooked(true);

            booking.setBookingTime(DateFactory.getCurrentDateAndTime());
            booking.setRecipientAddress(timePeriod.getRecipientAddress());
            booking.setRecipientEmail(timePeriod.getRecipientEmail());
            booking.setRecipientName(timePeriod.getRecipientName());
            booking.setRecipientPhone(timePeriod.getRecipientPhone());

            bookingService.saveOrUpdate(booking);
        }
    }

    @GetMapping("/paymentsuccessfull/event")
    public String showPaymentSuccessSeasonCourtBooking(@RequestParam(value = "eventInv") int eventInv, Model model){
        EventInvitation eventInvitation = eventInvitationService.findById(eventInv);
        if (eventInvitation != null){
            Event event = eventInvitation.getEvent();
            User user = eventInvitation.getUser();

            eventInvitation.setAnswered(true);
            eventInvitation.setComming(true);

            user.addEvent(event);
            event.addParticipant(user);

            userService.saveOrUpdate(user);
            eventService.saveOrUpdate(event);

            EconomicYear economicYear = economicYearService.findByUnionEconomicsAndYear(eventInvitation.getEvent().getUnion().getUnionEconomics(), unionSeasonStringService.getSeasonStringByUnionSeason(eventInvitation.getEvent().getUnion()));

            Invoice invoice = EconomicSupport.makeEventInvoice(eventInvitation, economicYear, invoiceService.getNextInvoiceNumber(economicYear.getAssosiatedUnion().getName()) + "-" + DateFactory.getCurrentSeasonYear());

            eventInvitation.setInvoice(invoice);

            model.addAttribute("invoice", invoice);

            economicYear.addInvoice(invoice);

            invoiceService.saveOrUpdate(invoice);
            eventInvitationService.saveOrUpdate(eventInvitation);
            economicYearService.saveOrUpdate(economicYear);

            // Do everything regarding unionEconomics --------------------------------------------------------
            UnionEconomics unionEconomics = economicYear.getUnionEconomics();
            unionEconomics.updateBalance();

            unionEconomicsService.saveOrUpdate(unionEconomics);

            MailSender.sendInvoice(invoice, eventInvitation.getUser().getEmail(), eventInvitation.getUser().getFirstname() + " " + eventInvitation.getUser().getLastname(), eventInvitation.getUser().getAddress());

            model.addAttribute("union", eventInvitation.getEvent().getUnion().getUrl());
            return "clubpage/paymentsuccessfull";
        } else {
            return "errorOutsidePanel/404";
        }
    }
}
