package dk.clubtimize.clubtimize.controllers.Invitations;

import dk.clubtimize.clubtimize.controllers.support.EconomicSupport;
import dk.clubtimize.clubtimize.controllers.support.PaymentGatewaySupport;
import dk.clubtimize.clubtimize.models.opsætning.booking.TimePeriod;
import dk.clubtimize.clubtimize.models.opsætning.booking.TimePeriodPlayer;
import dk.clubtimize.clubtimize.services.TimePeriodService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Time;

@Controller
@RequestMapping("/invitation")
public class invitedSeasonBookingPeriodController {

    private TimePeriodService timePeriodService;

    @Autowired
    public invitedSeasonBookingPeriodController(TimePeriodService timePeriodService) {
        this.timePeriodService = timePeriodService;
    }

    @GetMapping("/periodebooking")
    public String showPage(@RequestParam("id") int id, Model model){
        TimePeriod timePeriod = timePeriodService.findById(id);

        model.addAttribute("timePeriod", timePeriod);
        model.addAttribute("priceToPay", getPriceToPay(timePeriod));

        return "panel/invitation/invited/seasonPeriodBooking/invitedSeasonPeriodBooking";
    }

    private float getPriceToPay(TimePeriod timePeriod) {
        float price = timePeriod.getSeasonBookingPeriod().getPrice();

        if (timePeriod.getSeasonBookingPeriod().getHall().getUnion().getUnionEconomics().isPayingFees()){
            return price;
        } else {
            return price + EconomicSupport.calculateFeesAddedUp(price, timePeriod.getSeasonBookingPeriod().getHall().getUnion().getUnionEconomics().getFeePercent());
        }
    }

    @PostMapping("/periodebooking")
    @ResponseBody
    public String getPaymentLink(@RequestBody String json){
        JSONObject jsonObject = new JSONObject(json);
        TimePeriod timePeriod = timePeriodService.findById(jsonObject.getInt("TimePeriodId"));

        timePeriod.setRecipientName(jsonObject.getString("Name"));
        timePeriod.setRecipientEmail(jsonObject.getString("Email"));
        timePeriod.setRecipientAddress(jsonObject.getString("Address"));
        timePeriod.setRecipientPhone(jsonObject.getString("Phone"));

        jsonObject.getJSONArray("Players").forEach(player -> {
            JSONObject playerObject = (JSONObject) player;

            TimePeriodPlayer timePeriodPlayer = new TimePeriodPlayer();
            timePeriodPlayer.setName(playerObject.getString("Name"));
            timePeriodPlayer.setEmail(playerObject.getString("Email"));
            timePeriodPlayer.setAddress(playerObject.getString("Address"));
            timePeriodPlayer.setTimePeriod(timePeriod);

            timePeriod.addPlayer(timePeriodPlayer);
        });

        timePeriodService.saveOrUpdate(timePeriod);

        float priceWithAdditional = timePeriod.getSeasonBookingPeriod().getPrice() + EconomicSupport.calculateFeesAddedUp(timePeriod.getSeasonBookingPeriod().getPrice(), timePeriod.getCourt().getHall().getUnion().getUnionEconomics().getFeePercent());

        return new PaymentGatewaySupport(null, null).getPaymentGatewayForSeasonCourtBooking(priceWithAdditional, timePeriod);
    }
}
