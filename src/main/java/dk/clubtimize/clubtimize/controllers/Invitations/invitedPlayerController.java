package dk.clubtimize.clubtimize.controllers.Invitations;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.controllers.support.PaymentGatewaySupport;
import dk.clubtimize.clubtimize.controllers.support.mail.MailSender;
import dk.clubtimize.clubtimize.models.training.PlayerParticipation;
import dk.clubtimize.clubtimize.models.training.Training;
import dk.clubtimize.clubtimize.models.union.*;
import dk.clubtimize.clubtimize.models.user.EloPoints;
import dk.clubtimize.clubtimize.models.user.MatchStats;
import dk.clubtimize.clubtimize.models.user.UnionAssosiation;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;
import dk.clubtimize.clubtimize.services.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/invitation")
public class invitedPlayerController {

    private RecruitmentInvitationService recruitmentInvitationService;
    private UserService userService;
    private TeamSeasonService teamSeasonService;
    private UnionSeasonService unionSeasonService;
    private TrainingService trainingService;

    @Autowired
    public invitedPlayerController(RecruitmentInvitationService recruitmentInvitationService, UserService userService, TeamSeasonService teamSeasonService, UnionSeasonService unionSeasonService, TrainingService trainingService) {
        this.recruitmentInvitationService = recruitmentInvitationService;
        this.userService = userService;
        this.teamSeasonService = teamSeasonService;
        this.unionSeasonService = unionSeasonService;
        this.trainingService = trainingService;
    }

    @GetMapping("/spiller")
    public Object showPlayerInvitation(@RequestParam("inv") int invNumber, Model model) {
        RecruitmentInvitation recruitmentInvitation = recruitmentInvitationService.findByInvNumber(invNumber);

        if (recruitmentInvitation != null) {
            model.addAttribute("invitation", recruitmentInvitation);
            return "panel/invitation/invited/player/invitedPlayer";
        } else {
            return new RedirectView("/");
        }
    }

    @PostMapping("/spiller")
    @ResponseBody
    public String postCoachInvitation(@RequestBody String json) {

        JSONObject jsonObject = new JSONObject(json);

        if (!userService.existsByEmail(jsonObject.getString("email"))){
            RecruitmentInvitation recruitmentInvitation = recruitmentInvitationService.findByInvNumber(jsonObject.getInt("invId"));

            Team team = recruitmentInvitation.getTeam();

            User user = new User();
            user.setEmail(jsonObject.getString("email"));
            user.setPassword(jsonObject.getString("password"));
            user.setFirstname(jsonObject.getString("firstname"));
            user.setLastname(jsonObject.getString("lastname"));

            String day = jsonObject.getString("birthday").split("-")[0];
            String month = jsonObject.getString("birthday").split("-")[1];
            String year = jsonObject.getString("birthday").split("-")[2];

            user.setBirthday(formatDateNumber(day) + "-" + formatDateNumber(month) + "-" + year);
            user.setGender(jsonObject.getString("gender"));
            user.setAddress(jsonObject.getString("address"));
            user.setPhone(jsonObject.getInt("phone"));
            user.setUnion(team.getUnion());

            Player player = new Player();
            player.setUnion(team.getUnion());
            player.setTeam(team);
            team.addPlayer(player);

            UnionAssosiation unionAssosiation = new UnionAssosiation();
            unionAssosiation.addPlayerPosition(player);
            unionAssosiation.setUser(user);
            unionAssosiation.setUnion(team.getUnion());
            player.setUnionAssosiation(unionAssosiation);

            user.setUnionAssosiation(unionAssosiation);
            user.setUnion(team.getUnion());

            UserSeason userSeason = new UserSeason();
            DateTime dateTime = new DateTime();
            userSeason.setUnionSeason(unionSeasonService.findByUnionAndSeasonYear(team.getUnion(), dateTime.getYear() + "/" + (dateTime.getYear() + 1)));
            userSeason.setUser(user);
            user.addUserSeason(userSeason);

            TeamSeason teamSeason = new TeamSeason();
            teamSeason.setPaymentCode(makeAlphaNumericString(20));
            teamSeason.setTeam(team);
            teamSeason.setPaymentSuccessful(false);
            teamSeason.setDateJoined(DateFactory.getCurrentDate());
            teamSeason.setUserSeason(userSeason);
            userSeason.addTeamSeason(teamSeason);

            team.addTeamSeason(teamSeason);

            MatchStats matchStats = new MatchStats();
            EloPoints eloPoints = new EloPoints(1000, 1000, 1000, 1000, 1000);
            eloPoints.setMatchStats(matchStats);
            matchStats.setEloPoints(eloPoints);
            matchStats.setUser(user);
            user.setMatchStats(matchStats);

            userService.createUser(user, "Spiller");

            for (Training training : team.getTrainings()){
                DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
                DateTime today = formatter.parseDateTime(new DateTime().toString("dd-MM-yyyy"));
                DateTime trainingDate = formatter.parseDateTime(training.getDate());

                if (trainingDate.isAfter(today)){
                    PlayerParticipation playerParticipation = new PlayerParticipation();
                    playerParticipation.setPlayer(player);
                    playerParticipation.setTraining(training);
                    training.addPlayerParticipation(playerParticipation);

                    trainingService.saveOrUpdate(training);
                }
            }

            MailSender.playerWelcomeMail(user);

            recruitmentInvitationService.deleteRecruitmentInvitation(recruitmentInvitation);

            // Payment stuff ---------------------------------------------------------------------------------------------------------------------

            PaymentGatewaySupport paymentGatewaySupport = new PaymentGatewaySupport(team, teamSeason.getPaymentCode());

            List<Team> alreadyJoinedTeams = new ArrayList<>();
            for (Player player1 : user.getUnionAssosiation().getPlayerPositions()){
                if (teamSeasonService.findByUserSeasonAndTeam(userSeason, player1.getTeam()).isPaymentSuccessful()){
                    alreadyJoinedTeams.add(player1.getTeam());
                }
            }

            float priceWithoutAdditional = paymentGatewaySupport.calculatePrices(team, alreadyJoinedTeams)[0];
            float priceWithAdditional = paymentGatewaySupport.calculatePrices(team, alreadyJoinedTeams)[1];

            return paymentGatewaySupport.getPaymentGateway(priceWithAdditional, priceWithoutAdditional);
        } else {
            return "email_in_use";
        }
    }

    private String formatDateNumber(String value) {
        if (value.length() == 1) {
            return "0" + value;
        } else {
            return value;
        }
    }

    private String makeAlphaNumericString(int capacity){
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        StringBuilder stringBuilder = new StringBuilder(capacity);
        SecureRandom secureRandom = new SecureRandom();

        for(int i=0; i<capacity; i++){
            stringBuilder.append(AB.charAt(secureRandom.nextInt(AB.length())));
        }

        if (teamSeasonService.existsByPaymentCode(stringBuilder.toString())){
            stringBuilder.replace(0, stringBuilder.length(), makeAlphaNumericString(20));
        }

        return stringBuilder.toString();
    }
}
