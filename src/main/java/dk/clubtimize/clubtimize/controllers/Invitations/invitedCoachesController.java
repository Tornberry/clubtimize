package dk.clubtimize.clubtimize.controllers.Invitations;

import dk.clubtimize.clubtimize.controllers.support.DateFactory;
import dk.clubtimize.clubtimize.models.union.*;
import dk.clubtimize.clubtimize.models.user.UnionAssosiation;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;
import dk.clubtimize.clubtimize.services.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/invitation")
public class invitedCoachesController {

    private RecruitmentInvitationService recruitmentInvitationService;
    private TeamService teamService;
    private UserService userService;
    private UnionService unionService;
    private CoachService coachService;
    private UnionAssosiationService unionAssosiationService;
    private UnionSeasonService unionSeasonService;


    @Autowired
    public invitedCoachesController(RecruitmentInvitationService recruitmentInvitationService, TeamService teamService, UserService userService, UnionService unionService, CoachService coachService, UnionAssosiationService unionAssosiationService, UnionSeasonService unionSeasonService) {
        this.recruitmentInvitationService = recruitmentInvitationService;
        this.teamService = teamService;
        this.userService = userService;
        this.unionService = unionService;
        this.coachService = coachService;
        this.unionAssosiationService = unionAssosiationService;
        this.unionSeasonService = unionSeasonService;
    }

    @GetMapping("/træner")
    public Object showCoachInvitation(@RequestParam("inv") int invNumber, Model model) {
        RecruitmentInvitation recruitmentInvitation = recruitmentInvitationService.findByInvNumber(invNumber);

        if (recruitmentInvitation != null) {
            model.addAttribute("invitation", recruitmentInvitation);
            return "panel/invitation/invited/coach/invitedCoach";
        } else {
            return new RedirectView("/");
        }
    }

    @PostMapping("/træner")
    @ResponseBody
    public String postCoachInvitation(@RequestBody String json) {

        JSONObject jsonObject = new JSONObject(json);

        if (!userService.existsByEmail(jsonObject.getString("email"))){
            RecruitmentInvitation recruitmentInvitation = recruitmentInvitationService.findByInvNumber(jsonObject.getInt("invId"));

            User user = new User();
            user.setEmail(jsonObject.getString("email"));
            user.setPassword(jsonObject.getString("password"));
            user.setFirstname(jsonObject.getString("firstname"));
            user.setLastname(jsonObject.getString("lastname"));

            String day = jsonObject.getString("birthday").split("-")[0];
            String month = jsonObject.getString("birthday").split("-")[1];
            String year = jsonObject.getString("birthday").split("-")[2];

            user.setBirthday(formatDateNumber(day) + "-" + formatDateNumber(month) + "-" + year);
            user.setGender(jsonObject.getString("gender"));
            user.setAddress(jsonObject.getString("address"));
            user.setPhone(jsonObject.getInt("phone"));
            user.setUnion(recruitmentInvitation.getUnion());

            Coach coach = new Coach();
            coach.setUnion(recruitmentInvitation.getUnion());
            coach.setHourlyWage(recruitmentInvitation.getHourWageOffered());
            coach.addTeam(recruitmentInvitation.getTeam());

            Team team = recruitmentInvitation.getTeam();
            team.addCoach(coach);

            UnionAssosiation unionAssosiation = new UnionAssosiation();
            unionAssosiation.addCoachPosition(coach);
            unionAssosiation.setUser(user);
            unionAssosiation.setUnion(recruitmentInvitation.getUnion());
            coach.setUnionAssosiation(unionAssosiation);

            user.setUnionAssosiation(unionAssosiation);
            user.setUnion(recruitmentInvitation.getUnion());

            UserSeason userSeason = new UserSeason();
            userSeason.setUser(user);
            userSeason.setUnionSeason(unionSeasonService.findByUnionAndSeasonYear(recruitmentInvitation.getUnion(), DateFactory.getCurrentSeasonYear()));

            user.addUserSeason(userSeason);

            userService.createUser(user, "Træner");

            Union union = recruitmentInvitation.getUnion();
            union.addCoach(coach);
            union.removeRecruitmentInvitation(recruitmentInvitation);
            unionService.saveOrUpdate(union);

            coachService.saveOrUpdate(coach);
            teamService.saveOrUpdate(team);

            recruitmentInvitationService.deleteRecruitmentInvitation(recruitmentInvitation);


            return "OK";
        } else {
            return "email_in_use";
        }
    }

    private String formatDateNumber(String value) {
        if (value.length() == 1) {
            return "0" + value;
        } else {
            return value;
        }
    }
}
