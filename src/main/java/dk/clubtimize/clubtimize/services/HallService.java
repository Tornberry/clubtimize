package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.opsætning.Hall;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.repository.HallRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HallService {

    private HallRepository hallRepository;

    @Autowired
    public HallService(HallRepository hallRepository) {
        this.hallRepository = hallRepository;
    }

    public Hall findByHallId(int hallId){
        return hallRepository.findByHallId(hallId);
    }

    public void deleteHall(Hall hall){
        hallRepository.delete(hall);
    }

    public void saveOrUpdate(Hall hall){
        hallRepository.save(hall);
    }

    public Iterable<Hall> findByUnion(Union union){
        return hallRepository.findByUnion(union);
    }
}

