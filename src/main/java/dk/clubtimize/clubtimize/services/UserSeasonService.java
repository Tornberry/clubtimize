package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.union.UnionSeason;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.repository.UserSeasonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserSeasonService {

    private UserSeasonRepository userSeasonRepository;

    @Autowired
    public UserSeasonService(UserSeasonRepository userSeasonRepository) {
        this.userSeasonRepository = userSeasonRepository;
    }

    public UserSeason findByUnionSeasonAndUser(UnionSeason unionSeason, User user){
        return userSeasonRepository.findByUnionSeasonAndUser(unionSeason, user);
    }

    public void saveOrUpdate(UserSeason userSeason){
        userSeasonRepository.save(userSeason);
    }
}
