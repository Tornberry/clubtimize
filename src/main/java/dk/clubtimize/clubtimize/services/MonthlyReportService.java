package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.invoice.FeeReport;
import dk.clubtimize.clubtimize.repository.MonthlyReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MonthlyReportService {

    private MonthlyReportRepository monthlyReportRepository;

    @Autowired
    public MonthlyReportService(MonthlyReportRepository monthlyReportRepository) {
        this.monthlyReportRepository = monthlyReportRepository;
    }

    public Iterable<FeeReport> findByEconomicYear(EconomicYear economicYear){
        return monthlyReportRepository.findByEconomicYear(economicYear);
    }
}
