package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.economics.bookkeeping.accountingIntegrations.DineroIntegration;
import dk.clubtimize.clubtimize.repository.DineroIntegrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DineroIntegrationService {

    private DineroIntegrationRepository dineroIntegrationRepository;

    @Autowired
    public DineroIntegrationService(DineroIntegrationRepository dineroIntegrationRepository) {
        this.dineroIntegrationRepository = dineroIntegrationRepository;
    }

    public void deleteDineroIntegration(DineroIntegration dineroIntegration){
        dineroIntegrationRepository.delete(dineroIntegration);
    }
}
