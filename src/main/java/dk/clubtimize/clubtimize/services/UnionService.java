package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.repository.UnionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UnionService {

    private UnionRepository unionRepository;

    @Autowired
    public UnionService(UnionRepository unionRepository) {
        this.unionRepository = unionRepository;
    }

    public Union findByName(String name){
        return unionRepository.findByName(name);
    }

    public void saveOrUpdate(Union union){
        unionRepository.save(union);
    }

    public Union findByUrl(String url){
        return unionRepository.findByUrl(url);
    }

    public Iterable<Union> findAll(){
        return unionRepository.findAll();
    }

    public boolean existsByName(String name){
        return unionRepository.existsByName(name);
    }

    public boolean existsByUrl(String url){
        return unionRepository.existsByUrl(url);
    }

    public void deleteUnion(Union union){
        unionRepository.delete(union);
    }

    public Union findById(int id){
        return unionRepository.findById(id);
    }
}
