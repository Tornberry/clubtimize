package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.union.discount.discountTypes.TwoTeamDiscount;
import dk.clubtimize.clubtimize.repository.TwoTeamDiscountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TwoTeamDiscountService {

    private TwoTeamDiscountRepository twoTeamDiscountRepository;

    @Autowired
    public TwoTeamDiscountService(TwoTeamDiscountRepository twoTeamDiscountRepository) {
        this.twoTeamDiscountRepository = twoTeamDiscountRepository;
    }

    public TwoTeamDiscount findById(int id){
        return twoTeamDiscountRepository.findById(id);
    }

    public void deleteTwoTeamDiscount(TwoTeamDiscount twoTeamDiscount){
        this.twoTeamDiscountRepository.delete(twoTeamDiscount);
    }

    public void saveOrUpdate(TwoTeamDiscount twoTeamDiscount){
        twoTeamDiscountRepository.save(twoTeamDiscount);
    }
}
