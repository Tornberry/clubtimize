package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.repository.RolePermissionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RolePermissionsService {

    private RolePermissionsRepository rolePermissionsRepository;

    @Autowired
    public RolePermissionsService(RolePermissionsRepository rolePermissionsRepository) {
        this.rolePermissionsRepository = rolePermissionsRepository;
    }
}
