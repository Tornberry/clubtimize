package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.opsætning.Court;
import dk.clubtimize.clubtimize.models.opsætning.booking.Booking;
import dk.clubtimize.clubtimize.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookingService {

    private BookingRepository bookingRepository;

    @Autowired
    public BookingService(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    public void deleteAll(List<Booking> bookings){
        bookingRepository.deleteAll(bookings);
    }

    public Booking findById(int id){
        return bookingRepository.findById(id);
    }

    public void saveOrUpdate(Booking booking){
        bookingRepository.save(booking);
    }

    public boolean existsByDateAndStartHourAndEndHourAndCourt(String date, String startHour, String endHour, Court court){
        return bookingRepository.existsByDateAndStartHourAndEndHourAndCourt(date, startHour, endHour, court);
    }

    public Booking findByDateAndStartHourAndEndHourAndCourt(String date, String startHour, String endHour, Court court){
        return bookingRepository.findByDateAndStartHourAndEndHourAndCourt(date, startHour, endHour, court);
    }

    public void deleteBooking(Booking booking){
        bookingRepository.delete(booking);
    }
}
