package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.user.Role;
import dk.clubtimize.clubtimize.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    private RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public void saveOrUpdate(Role role){
        roleRepository.save(role);
    }

    public Role findByRoleName(String roleName){
        return roleRepository.findByRoleName(roleName);
    }

    public Role findByUnionAndRole(Union union, String role){
        return roleRepository.findByUnionAndRoleName(union, role);
    }
}

