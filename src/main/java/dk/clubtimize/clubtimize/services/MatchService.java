package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.union.matches.Match;
import dk.clubtimize.clubtimize.repository.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MatchService {

    private MatchRepository matchRepository;

    @Autowired
    public MatchService(MatchRepository matchRepository) {
        this.matchRepository = matchRepository;
    }

    public void saveOrUpdate(Match match){
        matchRepository.save(match);
    }

    public Iterable<Match> findAll(){
        return matchRepository.findAll();
    }
}
