package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.opsætning.ExerciseDGI;
import dk.clubtimize.clubtimize.repository.ExerciseDGIRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

@Service
public class ExerciseDGIService {

    private ExerciseDGIRepository exerciseDGIRepository;

    @Autowired
    public ExerciseDGIService(ExerciseDGIRepository exerciseDGIRepository) {
        this.exerciseDGIRepository = exerciseDGIRepository;
    }

    public void saveAll(List<ExerciseDGI> list){
        exerciseDGIRepository.saveAll(list);
    }

    public Iterable<ExerciseDGI> findAll(){
        return exerciseDGIRepository.findAll();
    }

    public ExerciseDGI findById(int id){
        return exerciseDGIRepository.findById(id);
    }
}
