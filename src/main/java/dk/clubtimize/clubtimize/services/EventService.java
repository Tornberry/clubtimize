package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.union.event.Event;
import dk.clubtimize.clubtimize.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventService {

    private EventRepository eventRepository;

    @Autowired
    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public void saveOrUpdate(Event event){
        eventRepository.save(event);
    }

    public Event findById(int id){
        return eventRepository.findById(id);
    }
}
