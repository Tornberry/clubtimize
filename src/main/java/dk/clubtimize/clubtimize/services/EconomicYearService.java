package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import dk.clubtimize.clubtimize.repository.EconomicYearRepository;
import org.springframework.stereotype.Service;

@Service
public class EconomicYearService {

    private EconomicYearRepository economicYearRepository;

    public EconomicYearService(EconomicYearRepository economicYearRepository) {
        this.economicYearRepository = economicYearRepository;
    }

    public EconomicYear findByUnionEconomicsAndYear(UnionEconomics unionEconomics, String year){
        return economicYearRepository.findByUnionEconomicsAndYear(unionEconomics, year);
    }

    public void saveOrUpdate(EconomicYear economicYear){
        economicYearRepository.save(economicYear);
    }
}
