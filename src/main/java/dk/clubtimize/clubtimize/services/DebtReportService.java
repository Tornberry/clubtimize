package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.economics.invoice.DebtReport;
import dk.clubtimize.clubtimize.repository.DebtReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DebtReportService {

    private DebtReportRepository debtReportRepository;

    @Autowired
    public DebtReportService(DebtReportRepository debtReportRepository) {
        this.debtReportRepository = debtReportRepository;
    }

    public DebtReport findById(int id){
        return debtReportRepository.findById(id);
    }

    public void deleteDebtReport(DebtReport debtReport){
        debtReportRepository.delete(debtReport);
    }
}
