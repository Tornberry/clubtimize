package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamService {

    private TeamRepository teamRepository;

    @Autowired
    public TeamService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    public Team findById(int id){
        return teamRepository.findById(id);
    }

    public void deleteTeam(Team team){
        teamRepository.delete(team);
    }

    public void saveOrUpdate(Team team){
        teamRepository.save(team);
    }

    public Iterable<Team> findByUnion(Union union){
        return teamRepository.findByUnion(union);
    }
}
