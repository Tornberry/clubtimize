package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.user.PasswordReset;
import dk.clubtimize.clubtimize.repository.PasswordResetRepository;
import org.springframework.stereotype.Service;

@Service
public class PasswordResetService {

    private PasswordResetRepository passwordResetRepository;

    public PasswordResetService(PasswordResetRepository passwordResetRepository) {
        this.passwordResetRepository = passwordResetRepository;
    }

    public PasswordReset findByResetId(String id){
        return passwordResetRepository.findByResetId(id);
    }

    public PasswordReset findByEmail(String email){
        return passwordResetRepository.findByEmail(email);
    }

    public boolean existsByEmail(String email){
        return passwordResetRepository.existsByEmail(email);
    }

    public boolean existsByResetId(String id){
        return passwordResetRepository.existsByResetId(id);
    }

    public void deleteByResetId(String id){
        passwordResetRepository.delete(passwordResetRepository.findByResetId(id));
    }

    public void deleteByEmail(String email){
        passwordResetRepository.delete(passwordResetRepository.findByEmail(email));
    }

    public void saveOrUpdate(PasswordReset passwordReset){
        passwordResetRepository.save(passwordReset);
    }
}
