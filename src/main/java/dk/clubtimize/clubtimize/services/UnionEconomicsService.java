package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.economics.UnionEconomics;
import dk.clubtimize.clubtimize.repository.UnionEconomicsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UnionEconomicsService {

    private UnionEconomicsRepository unionEconomicsRepository;

    @Autowired
    public UnionEconomicsService(UnionEconomicsRepository unionEconomicsRepository) {
        this.unionEconomicsRepository = unionEconomicsRepository;
    }

    public void saveOrUpdate(UnionEconomics unionEconomics){
        unionEconomicsRepository.save(unionEconomics);
    }
}
