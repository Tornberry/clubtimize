package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.training.TrainingModule;
import dk.clubtimize.clubtimize.repository.TrainingModuleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainingModuleService {

    private TrainingModuleRepository trainingModuleRepository;

    @Autowired
    public TrainingModuleService(TrainingModuleRepository trainingModuleRepository) {
        this.trainingModuleRepository = trainingModuleRepository;
    }

    public TrainingModule findById(int id){
        return trainingModuleRepository.findById(id);
    }

    public void deleteModule(TrainingModule trainingModule){
        trainingModuleRepository.delete(trainingModule);
    }

    public void saveOrUpdate(TrainingModule trainingModule){
        trainingModuleRepository.save(trainingModule);
    }
}
