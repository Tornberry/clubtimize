package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.training.Training;
import dk.clubtimize.clubtimize.repository.TrainingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainingService {
    private TrainingRepository trainingRepository;

    @Autowired
    public TrainingService(TrainingRepository trainingRepository) {
        this.trainingRepository = trainingRepository;
    }

    public Training findById(int id){
        return trainingRepository.findById(id);
    }

    public void saveOrUpdate(Training training){
       trainingRepository.save(training);
    }
}
