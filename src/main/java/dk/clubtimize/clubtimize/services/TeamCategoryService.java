package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.opsætning.TeamDepartment;
import dk.clubtimize.clubtimize.repository.TeamCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamCategoryService {
    private TeamCategoryRepository teamCategoryRepository;

    @Autowired
    public TeamCategoryService(TeamCategoryRepository teamCategoryRepository) {
        this.teamCategoryRepository = teamCategoryRepository;
    }

    public Iterable<TeamDepartment> findAll(){
        return teamCategoryRepository.findAll();
    }

    public TeamDepartment findById(int id){
        return teamCategoryRepository.findById(id);
    }

    public void deleteTeamCategory(TeamDepartment teamDepartment){
        teamCategoryRepository.delete(teamDepartment);
    }

    public void saveOrUpdate(TeamDepartment teamDepartment){
        teamCategoryRepository.save(teamDepartment);
    }
}

