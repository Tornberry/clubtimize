package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.user.Role;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class UserService {

    private UserRepository userRepository;
    private RoleService roleService;

    @Autowired
    public UserService(UserRepository userRepository, RoleService roleService) {
        this.userRepository = userRepository;
        this.roleService = roleService;
    }


    public void createUser(User user, String roleName){

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        user.setPassword(encoder.encode(user.getPassword()));

        Role role = roleService.findByUnionAndRole(user.getUnion(), roleName);
        role.addUser(user);

        user.setRole(role);
        user.setDateJoined(new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime()));

        userRepository.save(user);
        roleService.saveOrUpdate(role);
    }

    public User findByUsername(String email){
        return userRepository.findByEmail(email);
    }

    public User findById(int id){
        return userRepository.findByUserId(id);
    }

    public User findByEmail(String email){
        return userRepository.findByEmail(email);
    }

    public boolean existsByEmail(String email){
        return userRepository.existsByEmail(email);
    }

    public void saveOrUpdate(User user){
        userRepository.save(user);
    }

    public void deleteUser(User user){
        userRepository.delete(user);
    }

    public Iterable<User> findAll(){
         return userRepository.findAll();
    }

    public Iterable<User> findByUnion(Union union){
        return userRepository.findByUnion(union);
    }
}
