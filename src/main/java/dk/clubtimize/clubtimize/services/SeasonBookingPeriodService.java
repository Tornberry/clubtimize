package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.opsætning.booking.SeasonBookingPeriod;
import dk.clubtimize.clubtimize.repository.SeasonBookingPeriodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SeasonBookingPeriodService {

    private SeasonBookingPeriodRepository seasonBookingPeriodRepository;

    @Autowired
    public SeasonBookingPeriodService(SeasonBookingPeriodRepository seasonBookingPeriodRepository) {
        this.seasonBookingPeriodRepository = seasonBookingPeriodRepository;
    }

    public void saveOrUpdate(SeasonBookingPeriod seasonBookingPeriod){
        seasonBookingPeriodRepository.save(seasonBookingPeriod);
    }

    public SeasonBookingPeriod findById(int id){
        return seasonBookingPeriodRepository.findById(id);
    }

    public void delete(SeasonBookingPeriod seasonBookingPeriod){
        seasonBookingPeriodRepository.delete(seasonBookingPeriod);
    }
}
