package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.controllers.logging.LoggingController;
import dk.clubtimize.clubtimize.models.user.CustomUserDetails;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;
    private Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);

    @Autowired
    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null){
            logger.info("Tried to login with non existing user with email: " + email);
            throw new UsernameNotFoundException("User not found");
        } else{
            return new CustomUserDetails(user);
        }
    }
}
