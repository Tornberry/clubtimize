package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.union.Player;
import dk.clubtimize.clubtimize.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlayerService {

    private PlayerRepository playerRepository;

    @Autowired
    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public void saveOrUpdate(Player player){
        playerRepository.save(player);
    }

    public Player findById(int id){
        return playerRepository.findByPlayerId(id);
    }

    public void deletePlayer(Player player){
        playerRepository.delete(player);
    }
}
