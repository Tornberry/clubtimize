package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.economics.invoice.FeeReport;
import dk.clubtimize.clubtimize.repository.FeeReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FeeReportService {

    private FeeReportRepository feeReportRepository;

    @Autowired
    public FeeReportService(FeeReportRepository feeReportRepository) {
        this.feeReportRepository = feeReportRepository;
    }

    public FeeReport findById(int id){
        return feeReportRepository.findById(id);
    }

    public void deleteFeeReport(FeeReport feeReport){
        feeReportRepository.delete(feeReport);
    }
}
