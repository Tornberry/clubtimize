package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.union.RecruitmentInvitation;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.repository.RecruitmentInvitationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecruitmentInvitationService {

    private RecruitmentInvitationRepository recruitmentInvitationRepository;

    @Autowired
    public RecruitmentInvitationService(RecruitmentInvitationRepository recruitmentInvitationRepository) {
        this.recruitmentInvitationRepository = recruitmentInvitationRepository;
    }

   public List<RecruitmentInvitation> findByUnion(Union union){
        return recruitmentInvitationRepository.findByUnion(union);
   }

   public void saveOrUpdate(RecruitmentInvitation recruitmentInvitation){
       recruitmentInvitationRepository.save(recruitmentInvitation);
   }

   public RecruitmentInvitation findByInvNumber(int number){
        return recruitmentInvitationRepository.findByInvNumber(number);
   }

   public void deleteRecruitmentInvitation(RecruitmentInvitation recruitmentInvitation){
        recruitmentInvitationRepository.delete(recruitmentInvitation);
   }
}
