package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.opsætning.Court;
import dk.clubtimize.clubtimize.models.union.Coach;
import dk.clubtimize.clubtimize.repository.CourtRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourtService {

    private CourtRepository courtRepository;

    @Autowired
    public CourtService(CourtRepository courtRepository) {
        this.courtRepository = courtRepository;
    }

    public void saveOrUpdate(Court court){
        courtRepository.save(court);
    }

    public Court findById(int id){
        return courtRepository.findById(id);
    }
}
