package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.training.TrainingSchedule;
import dk.clubtimize.clubtimize.repository.TrainingScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainingScheduleService {

    private TrainingScheduleRepository trainingScheduleRepository;

    @Autowired
    public TrainingScheduleService(TrainingScheduleRepository trainingScheduleRepository) {
        this.trainingScheduleRepository = trainingScheduleRepository;
    }

    public void saveOrUpdate(TrainingSchedule trainingSchedule){
        trainingScheduleRepository.save(trainingSchedule);
    }
}
