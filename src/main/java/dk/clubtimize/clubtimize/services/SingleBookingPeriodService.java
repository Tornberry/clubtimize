package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.opsætning.booking.SingleBookingPeriod;
import dk.clubtimize.clubtimize.repository.SingleBookingPeriodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SingleBookingPeriodService {

    private SingleBookingPeriodRepository singleBookingPeriodRepository;

    @Autowired
    public SingleBookingPeriodService(SingleBookingPeriodRepository singleBookingPeriodRepository) {
        this.singleBookingPeriodRepository = singleBookingPeriodRepository;
    }

    public void saveOrUpdate(SingleBookingPeriod singleBookingPeriod){
        singleBookingPeriodRepository.save(singleBookingPeriod);
    }

    public SingleBookingPeriod findById(int id){
        return singleBookingPeriodRepository.findById(id);
    }

    public void deleteBookingPeriod(SingleBookingPeriod singleBookingPeriod){
        singleBookingPeriodRepository.delete(singleBookingPeriod);
    }
}
