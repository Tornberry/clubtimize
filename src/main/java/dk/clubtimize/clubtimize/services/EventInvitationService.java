package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.union.event.Event;
import dk.clubtimize.clubtimize.models.union.event.EventInvitation;
import dk.clubtimize.clubtimize.models.user.User;
import dk.clubtimize.clubtimize.repository.EventInvitationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventInvitationService {

    private EventInvitationRepository eventInvitationRepository;

    @Autowired
    public EventInvitationService(EventInvitationRepository eventInvitationRepository) {
        this.eventInvitationRepository = eventInvitationRepository;
    }

    public void saveOrUpdate(EventInvitation eventInvitation){
        eventInvitationRepository.save(eventInvitation);
    }

    public EventInvitation findByEventAndUser(Event event, User user){
        return eventInvitationRepository.findByEventAndUser(event, user);
    }

    public EventInvitation findById(int id){
        return eventInvitationRepository.findById(id);
    }

    public void deleteEventInvitation(EventInvitation eventInvitation){
        eventInvitationRepository.delete(eventInvitation);
    }

    public boolean existsByEventAndUser(Event event, User user){
        return eventInvitationRepository.existsByEventAndUser(event, user);
    }
}
