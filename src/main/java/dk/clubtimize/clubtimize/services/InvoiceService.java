package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.economics.EconomicYear;
import dk.clubtimize.clubtimize.models.economics.invoice.Invoice;
import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.repository.InvoiceRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InvoiceService {

    private InvoiceRepository invoiceRepository;

    public InvoiceService(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    public int getNextInvoiceNumber(String unionName){
        Iterable<Invoice> invoices = invoiceRepository.findByUnionAssosiatedName(unionName);
        List<Invoice> invoiceList = new ArrayList<>();
        invoices.forEach(invoiceList::add);
        return invoiceList.size() + 1;
    }

    public Invoice findByInvoiceNumber(String invoiceNumber){
        return invoiceRepository.findByInvoiceNumber(invoiceNumber);
    }

    public Invoice findByUnionNameAndInvoiceNumber(String unionName, String invoiceNumber){
        return invoiceRepository.findByUnionAssosiatedNameAndInvoiceNumber(unionName, invoiceNumber);
    }

    public Iterable<Invoice> findAll(){
        return invoiceRepository.findAll();
    }

    public void saveOrUpdate(Invoice invoice){
        invoiceRepository.save(invoice);
    }

    public Iterable<Invoice> findByUnionNameAndDate(String unionName, String date){
        return invoiceRepository.findByUnionAssosiatedNameAndDate(unionName, date);
    }

    public void deleteInvoice(Invoice invoice){
        invoiceRepository.delete(invoice);
    }

    public Invoice findById(int id){
        return invoiceRepository.findById(id);
    }
}
