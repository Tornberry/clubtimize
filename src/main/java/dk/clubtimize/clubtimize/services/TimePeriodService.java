package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.opsætning.booking.TimePeriod;
import dk.clubtimize.clubtimize.repository.TimePeriodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TimePeriodService {

    private TimePeriodRepository timePeriodRepository;

    @Autowired
    public TimePeriodService(TimePeriodRepository timePeriodRepository) {
        this.timePeriodRepository = timePeriodRepository;
    }

    public void saveOrUpdate(TimePeriod timePeriod){
        timePeriodRepository.save(timePeriod);
    }

    public TimePeriod findById(int id){
        return timePeriodRepository.findById(id);
    }
}
