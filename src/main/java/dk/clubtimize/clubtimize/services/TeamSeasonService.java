package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.union.Team;
import dk.clubtimize.clubtimize.models.user.season.TeamSeason;
import dk.clubtimize.clubtimize.models.user.season.UserSeason;
import dk.clubtimize.clubtimize.repository.TeamSeasonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamSeasonService {
    private TeamSeasonRepository teamSeasonRepository;

    @Autowired
    public TeamSeasonService(TeamSeasonRepository teamSeasonRepository) {
        this.teamSeasonRepository = teamSeasonRepository;
    }

    public boolean existsByPaymentCode(String paymentCode){
        return teamSeasonRepository.existsByPaymentCode(paymentCode);
    }

    public TeamSeason findByPaymentCode(String paymentCode){
        return teamSeasonRepository.findByPaymentCode(paymentCode);
    }

    public void saveOrUpdate(TeamSeason teamSeason){
        teamSeasonRepository.save(teamSeason);
    }

    public TeamSeason findByUserSeasonAndTeam(UserSeason userSeason, Team team){
        return teamSeasonRepository.findByUserSeasonAndTeam(userSeason, team);
    }

    public Iterable<TeamSeason> findAll(){
        return teamSeasonRepository.findAll();
    }

    public void deleteTeamSeason(TeamSeason teamSeason){
        teamSeasonRepository.delete(teamSeason);
    }

    public TeamSeason findById(int id){
        return teamSeasonRepository.findById(id);
    }
}
