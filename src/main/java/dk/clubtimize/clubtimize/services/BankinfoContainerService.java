package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.economics.BankinfoContainer;
import dk.clubtimize.clubtimize.repository.BankinfoContainerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BankinfoContainerService {

    private BankinfoContainerRepository bankinfoContainerRepository;

    @Autowired
    public BankinfoContainerService(BankinfoContainerRepository bankinfoContainerRepository) {
        this.bankinfoContainerRepository = bankinfoContainerRepository;
    }

    public void saveOrUpdate(BankinfoContainer bankinfoContainer){
        bankinfoContainerRepository.save(bankinfoContainer);
    }
}
