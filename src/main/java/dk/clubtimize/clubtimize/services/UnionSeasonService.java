package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.union.Union;
import dk.clubtimize.clubtimize.models.union.UnionSeason;
import dk.clubtimize.clubtimize.repository.UnionSeasonRepository;
import org.springframework.stereotype.Service;

@Service
public class UnionSeasonService {

    private UnionSeasonRepository unionSeasonRepository;

    public UnionSeasonService(UnionSeasonRepository unionSeasonRepository) {
        this.unionSeasonRepository = unionSeasonRepository;
    }

    public UnionSeason findByUnionAndSeasonYear(Union union, String seasonYear){
        return unionSeasonRepository.findByUnionAndSeasonYear(union, seasonYear);
    }

    public void saveOrUpdate(UnionSeason unionSeason){
        unionSeasonRepository.save(unionSeason);
    }

    public Iterable<UnionSeason> findByUnion(Union union){
        return unionSeasonRepository.findByUnion(union);
    }
}
