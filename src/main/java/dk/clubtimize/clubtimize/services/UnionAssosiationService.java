package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.user.UnionAssosiation;
import dk.clubtimize.clubtimize.repository.UnionAssosiationReposetory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UnionAssosiationService {

    private UnionAssosiationReposetory unionAssosiationReposetory;

    @Autowired
    public UnionAssosiationService(UnionAssosiationReposetory unionAssosiationReposetory) {
        this.unionAssosiationReposetory = unionAssosiationReposetory;
    }

    public UnionAssosiation findById(int id){
        return unionAssosiationReposetory.findById(id);
    }

    public void saveOrUpdate(UnionAssosiation unionAssosiation){
        unionAssosiationReposetory.save(unionAssosiation);
    }
}
