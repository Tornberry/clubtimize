package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.economics.PaymentRequest;
import dk.clubtimize.clubtimize.repository.PaymentRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentRequestService {

    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    public PaymentRequestService(PaymentRequestRepository paymentRequestRepository) {
        this.paymentRequestRepository = paymentRequestRepository;
    }

    public Iterable<PaymentRequest> findByOpen(boolean isOpen){
        return paymentRequestRepository.findByOpen(isOpen);
    }

    public PaymentRequest findById(int id){
        return paymentRequestRepository.findById(id);
    }

    public void saveOrUpdate(PaymentRequest paymentRequest){
        paymentRequestRepository.save(paymentRequest);
    }

    public void deletePaymentRequest(PaymentRequest paymentRequest){
        paymentRequestRepository.delete(paymentRequest);
    }
}
