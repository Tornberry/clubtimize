package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.opsætning.TeamDepartment;
import dk.clubtimize.clubtimize.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepartmentService {

    private DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentService(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    public TeamDepartment findById(int id){
        return departmentRepository.findById(id);
    }

    public void saveOrUpdate(TeamDepartment teamDepartment){
        departmentRepository.save(teamDepartment);
    }
}
