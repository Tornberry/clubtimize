package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.union.Coach;
import dk.clubtimize.clubtimize.repository.CoachRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoachService {

    private CoachRepository coachRepository;

    @Autowired
    public CoachService(CoachRepository coachRepository) {
        this.coachRepository = coachRepository;
    }

    public Coach findById(int id){
        return coachRepository.findByCoachId(id);
    }

    public void saveOrUpdate(Coach coach){
        coachRepository.save(coach);
    }

    public void deleteCoach(Coach coach){
        coachRepository.delete(coach);
    }

}
