package dk.clubtimize.clubtimize.services;

import dk.clubtimize.clubtimize.models.training.PlayerParticipation;
import dk.clubtimize.clubtimize.repository.PlayerParticipationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlayerParticipationService {

    private PlayerParticipationRepository playerParticipationRepository;

    @Autowired
    public PlayerParticipationService(PlayerParticipationRepository playerParticipationRepository) {
        this.playerParticipationRepository = playerParticipationRepository;
    }

    public PlayerParticipation findById(int id){
        return playerParticipationRepository.findById(id);
    }

    public void saveOrUpdate(PlayerParticipation playerParticipation){
        playerParticipationRepository.save(playerParticipation);
    }
}
